define([ "vjo", "./Exception" ], function(vjo, Exception) {

	return vjo.ctype("osgi.framework.BundleException")

	.inherits(Exception)

	.props({

		UNSPECIFIED : 0,

		UNSUPPORTED_OPERATION : 1,

		INVALID_OPERATION : 2,

		MANIFEST_ERROR : 3,

		RESOLVE_ERROR : 4,

		ACTIVATOR_ERROR : 5,

		SECURITY_ERROR : 6,

		STATECHANGE_ERROR : 7,

		NATIVECODE_ERROR : 8,

		DUPLICATE_BUNDLE_ERROR : 9,

		START_TRANSIENT_ERROR : 10,

		READ_ERROR : 11,

		REJECTED_BY_HOOK : 12,

		TYPE : null

	})

	.protos({

		type : 0,

		constructs : function() {
			if (arguments.length === 2) {
				if ((arguments[0] instanceof String || typeof arguments[0] == "string") && (arguments[1] instanceof Exception || arguments[1] instanceof Error)) {
					this.constructs_2_0_BundleException_ovld(arguments[0], arguments[1]);
				}
				else if ((arguments[0] instanceof String || typeof arguments[0] == "string") && typeof arguments[1] == "number") {
					this.constructs_2_1_BundleException_ovld(arguments[0], arguments[1]);
				}
			}
			else if (arguments.length === 1) {
				this.constructs_1_0_BundleException_ovld(arguments[0]);
			}
			else if (arguments.length === 3) {
				this.constructs_3_0_BundleException_ovld(arguments[0], arguments[1], arguments[2]);
			}
			else if (arguments.length === 0) {
				this.constructs_1_0_BundleException_ovld();
			}
		},

		constructs_2_0_BundleException_ovld : function(msg, cause) {
			this.constructs_3_0_BundleException_ovld(msg, this.vj$.BundleException.UNSPECIFIED, cause);
		},

		constructs_1_0_BundleException_ovld : function(msg) {
			this.constructs_2_1_BundleException_ovld(msg, this.vj$.BundleException.UNSPECIFIED);
		},

		constructs_3_0_BundleException_ovld : function(msg, type, cause) {
			this.base(msg, cause);
			//this.name = this.getClass().getName();
			this.type = type;
		},

		constructs_2_1_BundleException_ovld : function(msg, type) {
			this.base(msg);
			//this.name = this.getClass().getName();
			this.type = type;
		},

		toString : function() {
			var builder =  [];
			builder.push(this.name);
			builder.push(": \n");
			builder.push(this.vj$.BundleException.TYPE[this.type]);
			builder.push(": \n");
			builder.push(this.message);
			var rootCause = this;
			while (rootCause != null && rootCause.cause) {
				rootCause = rootCause.cause;
				if (rootCause !== null) {
					builder.push(": \n");
					builder.push(rootCause.message);
				}
			}
			return builder.join("");
		}

	})
	
	.inits(function() {
		this.TYPE = [ "UNSPECIFIED", "UNSUPPORTED_OPERATION", "INVALID_OPERATION", "MANIFEST_ERROR", "RESOLVE_ERROR", "ACTIVATOR_ERROR", "SECURITY_ERROR", "STATECHANGE_ERROR", "NATIVECODE_ERROR", "DUPLICATE_BUNDLE_ERROR", "START_TRANSIENT_ERROR", "READ_ERROR", "REJECTED_BY_HOOK" ];
	})

	.endType();

})