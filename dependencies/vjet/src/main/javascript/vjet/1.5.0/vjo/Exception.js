define([ "vjo" ], function(vjo) {

	return vjo.ctype("osgi.framework.Exception")

	.protos({

		message : null,

		stack : null,

		stackTrace : null,

		cause : null,

		name : null,

		constructs : function(msg, cause) {
			var inst = Error.call(this, msg);

			this.name = this.getClass().getName();

			if (inst.stack) {
				this.stack = inst.stack;
			}
			if (inst.stackTrace) {
				this.stackTrace = inst.stackTrace;
			}

			this.message = msg || "Exception";

			this.cause = cause || null;
		},

		getName : function() {
			return this.name;
		},

		getMessage : function() {
			return this.message;
		},

		getCause : function() {
			return this.cause;
		},

		getStackTrace : function() {
			return this.stackTrace;
		},

		getStack : function() {
			return this.stack;
		},

		toString : function() {
			var builder = [];
			builder.push(this.name);
			builder.push(": \n");
			builder.push(this.message);
			var rootCause = this;
			while (rootCause != null && rootCause.cause) {
				rootCause = rootCause.cause;
				if (rootCause !== null) {
					builder.push(": \n");
					builder.push(rootCause.message);
				}
			}
			return builder.join("");
		},

		getRootCause : function() {
			var rootCause = this.cause;
			while (rootCause != null) {
				rootCause = rootCause.cause;
			}
			return rootCause;
		}

	})

	.endType();

});