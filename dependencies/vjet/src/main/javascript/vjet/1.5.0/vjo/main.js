require.config({
    paths: {
        'vjo': 'vjo',
    }
});
require([ './Exception', './BundleException' ], function (Exception, BundleException) {
	
	var e = new BundleException("foo", new Error("bar"));
	console.debug(e);
});