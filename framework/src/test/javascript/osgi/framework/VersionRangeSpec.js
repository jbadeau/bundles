define([ "osgi/framework/VersionRange", "osgi/framework/Version" ], function(VersionRange, Version) {

	describe("constructor", function() {
		it("should create a version range from a specified string", function() {
			var versionRange = new VersionRange("(1.0.0,2.0.0)");
			expect(versionRange.toString()).toEqual("(1.0.0,2.0.0)");

			versionRange = new VersionRange("(1.0.0,2.0.0]");
			expect(versionRange.toString()).toEqual("(1.0.0,2.0.0]");

			versionRange = new VersionRange("[1.0.0,2.0.0]");
			expect(versionRange.toString()).toEqual("[1.0.0,2.0.0]");

			versionRange = new VersionRange("[1.0.0,2.0.0)");
			expect(versionRange.toString()).toEqual("[1.0.0,2.0.0)");
		});

		it("should create a version range from specified versions", function() {
			var leftEndpoint = new Version("1.0.0");
			var rightEndpoint = new Version("2.0.0");
			var versionRange = new VersionRange(VersionRange.LEFT_OPEN, leftEndpoint, rightEndpoint, VersionRange.RIGHT_OPEN);
			expect(versionRange.toString()).toEqual("(1.0.0,2.0.0)");

			leftEndpoint = new Version("1.0.0");
			rightEndpoint = new Version("2.0.0");
			versionRange = new VersionRange(VersionRange.LEFT_OPEN, leftEndpoint, rightEndpoint, VersionRange.RIGHT_CLOSED);
			expect(versionRange.toString()).toEqual("(1.0.0,2.0.0]");

			leftEndpoint = new Version("1.0.0");
			rightEndpoint = new Version("2.0.0");
			versionRange = new VersionRange(VersionRange.LEFT_CLOSED, leftEndpoint, rightEndpoint, VersionRange.RIGHT_CLOSED);
			expect(versionRange.toString()).toEqual("[1.0.0,2.0.0]");

			leftEndpoint = new Version("1.0.0");
			rightEndpoint = new Version("2.0.0");
			versionRange = new VersionRange(VersionRange.LEFT_CLOSED, leftEndpoint, rightEndpoint, VersionRange.RIGHT_OPEN);
			expect(versionRange.toString()).toEqual("[1.0.0,2.0.0)");
		});

	});

	describe("getLeft", function() {
		it("should return the left endpoint", function() {
			var leftEndpoint = new Version("1.0.0");
			var rightEndpoint = new Version("2.0.0");
			var versionRange = new VersionRange(VersionRange.LEFT_OPEN, leftEndpoint, rightEndpoint, VersionRange.RIGHT_OPEN);
			expect(versionRange.getLeft().equals(leftEndpoint)).toBeTruthy();
		});
	});

	describe("getRight", function() {
		it("should return the right endpoint", function() {
			var leftEndpoint = new Version("1.0.0");
			var rightEndpoint = new Version("2.0.0");
			var versionRange = new VersionRange(VersionRange.LEFT_OPEN, leftEndpoint, rightEndpoint, VersionRange.RIGHT_OPEN);
			expect(versionRange.getRight().equals(rightEndpoint)).toBeTruthy();
		});
	});

	describe("getLeftType", function() {
		it("should return the type of the left endpoint", function() {
			var leftEndpoint = new Version("1.0.0");
			var rightEndpoint = new Version("2.0.0");
			var versionRange = new VersionRange(VersionRange.LEFT_OPEN, leftEndpoint, rightEndpoint, VersionRange.RIGHT_OPEN);
			expect(versionRange.getLeftType()).toEqual(VersionRange.LEFT_OPEN);
		});
	});

	describe("getRightType", function() {
		it("should return the type of the right endpoint", function() {
			var leftEndpoint = new Version("1.0.0");
			var rightEndpoint = new Version("2.0.0");
			var versionRange = new VersionRange(VersionRange.LEFT_OPEN, leftEndpoint, rightEndpoint, VersionRange.RIGHT_OPEN);
			expect(versionRange.getRightType()).toEqual(VersionRange.RIGHT_OPEN);
		});
	});

	describe("includes", function() {
		it("should return whether a version range includes a specified version", function() {
			var beforeleftEndpoint = new Version("0.1.0");
			var leftEndpoint = new Version("1.0.0");
			var middleEndpoint = new Version("1.5.0");
			var rightEndpoint = new Version("2.0.0");
			var afterRightEndpoint = new Version("3.0.0");

			var openVersionRange = new VersionRange(VersionRange.LEFT_OPEN, leftEndpoint, rightEndpoint, VersionRange.RIGHT_OPEN);
			var closedVersionRange = new VersionRange(VersionRange.LEFT_CLOSED, leftEndpoint, rightEndpoint, VersionRange.RIGHT_CLOSED);
			// open
			expect(openVersionRange.includes(leftEndpoint)).toBeFalsy();
			expect(openVersionRange.includes(middleEndpoint)).toBeTruthy();
			expect(openVersionRange.includes(rightEndpoint)).toBeFalsy();
			expect(openVersionRange.includes(beforeleftEndpoint)).toBeFalsy();
			expect(openVersionRange.includes(afterRightEndpoint)).toBeFalsy();

			// closed
			expect(closedVersionRange.includes(leftEndpoint)).toBeTruthy();
			expect(closedVersionRange.includes(middleEndpoint)).toBeTruthy();
			expect(closedVersionRange.includes(rightEndpoint)).toBeTruthy();
			expect(closedVersionRange.includes(beforeleftEndpoint)).toBeFalsy();
			expect(closedVersionRange.includes(afterRightEndpoint)).toBeFalsy();
		});
	});

	describe("intersection", function() {
		it("should return the intersection of the version range with the specified * version ranges", function() {
			var beforeleftEndpoint = new Version("0.1.0");
			var leftEndpoint = new Version("1.0.0");
			var middleEndpoint = new Version("1.5.0");
			var rightEndpoint = new Version("2.0.0");
			var afterRightEndpoint = new Version("3.0.0");

			var beforeVersionRange = new VersionRange(VersionRange.LEFT_OPEN, beforeleftEndpoint, leftEndpoint, VersionRange.RIGHT_OPEN);
			var versionRange = new VersionRange(VersionRange.LEFT_OPEN, leftEndpoint, rightEndpoint, VersionRange.RIGHT_OPEN);
			var afterVersionRange = new VersionRange(VersionRange.LEFT_OPEN, rightEndpoint, afterRightEndpoint, VersionRange.RIGHT_OPEN);

			expect(versionRange.intersection().equals(versionRange)).toBeTruthy();
			expect(versionRange.intersection(beforeVersionRange, afterVersionRange)).toEqual(null);
			expect(versionRange.intersection(versionRange, afterVersionRange).equals(new VersionRange(VersionRange.LEFT_OPEN, rightEndpoint, rightEndpoint, VersionRange.RIGHT_OPEN))).toBeTruthy();
			expect(versionRange.intersection(versionRange, beforeVersionRange).equals(new VersionRange(VersionRange.LEFT_OPEN, leftEndpoint, leftEndpoint, VersionRange.RIGHT_OPEN))).toBeTruthy();
		});
	});

	describe("isEmpty", function() {
		it("should return whether this version range is empty", function() {
			var versionRange = new VersionRange("[1.0.0,]");
			expect(versionRange.isEmpty()).toBeTruthy();
			versionRange = new VersionRange("[1.0.0,1.0.0]");
			expect(versionRange.isEmpty()).toBeFalsy();
		});
	});

	describe("isExact", function() {
		it("should return whether this version range contains only a single version", function() {
			var versionRange = new VersionRange("[1.0.0,1.0.0]");
			expect(versionRange.isExact()).toBeTruthy();
			versionRange = new VersionRange("[1.0.0,2.0.0]");
			expect(versionRange.isExact()).toBeFalsy();
		});
	});

	describe("toString", function() {
		it("should return the string representation of this version range", function() {
			expect(new VersionRange("(1.0.0,2.0.0)").toString()).toEqual("(1.0.0,2.0.0)");
			expect(new VersionRange("(1.0.0,2.0.0]").toString()).toEqual("(1.0.0,2.0.0]");
			expect(new VersionRange("[1.0.0,2.0.0]").toString()).toEqual("[1.0.0,2.0.0]");
			expect(new VersionRange("[1.0.0,2.0.0)").toString()).toEqual("[1.0.0,2.0.0)");
			expect(new VersionRange("(1.0.0,2.0.0]").toString()).toEqual("(1.0.0,2.0.0]");

			expect(new VersionRange("(1.0.0,]").toString()).toEqual("(1.0.0,0.0.0]");
		});
	});

	describe("equals", function() {

		it("should support === comparisons", function() {
			expect(new VersionRange("(1.0.0,2.0.0)").equals(new VersionRange("(1.0.0,2.0.0)"))).toBeTruthy();
		});

		it("should support !== comparison", function() {
			expect(new VersionRange("(1.0.0,2.0.0)").equals(new VersionRange("(1.0.0,2.1.0)"))).toBeFalsy();
		});
	});

})