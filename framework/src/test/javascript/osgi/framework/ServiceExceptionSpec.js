define([ "osgi/framework/ServiceException" ], function(ServiceException) {

	describe("osgi.framework.ServiceException", function() {

		describe("constructor()", function() {
			it("must support no arguments", function() {
				var exception = new ServiceException();
				expect(exception.toString()).toEqual("osgi.framework.ServiceException: \nUNSPECIFIED: \nException");
			});
			it("must support a message argument", function() {
				exception = new ServiceException("foo exception");
				expect(exception.toString()).toEqual("osgi.framework.ServiceException: \nUNSPECIFIED: \nfoo exception");
			});
			it("must support message and Exception arguments", function() {
				exception = new ServiceException("foo exception", new ServiceException("nested foo exception"));
				expect(exception.toString()).toEqual("osgi.framework.ServiceException: \nUNSPECIFIED: \nfoo exception: \nnested foo exception");
			});
			it("must support message and Error arguments", function() {
				exception = new ServiceException("foo exception", new Error("nested foo error"));
				expect(exception.toString()).toEqual("osgi.framework.ServiceException: \nUNSPECIFIED: \nfoo exception: \nnested foo error");
			});
		});

		describe("getMessage()", function() {
			it("must return the message", function() {
				exception = new ServiceException("foo exception");
				expect(exception.message).toEqual("foo exception");
			});
		});

		describe("getName", function() {
			it("must return the name", function() {
				exception = new ServiceException("foo exception");
				expect(exception.name).toEqual("osgi.framework.ServiceException");
			});
		});

		describe("toString()", function() {
			it("must support n levels of nested exceptions", function() {
				exception = new ServiceException("foo exception", new ServiceException("nested foo exception", new ServiceException("2nd nested foo exception")));
				expect(exception.toString()).toEqual("osgi.framework.ServiceException: \nUNSPECIFIED: \nfoo exception: \nnested foo exception: \n2nd nested foo exception");
			});
		});

	});

})