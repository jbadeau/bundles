define([ "osgi/framework/Version" ], function(Version) {
	
	describe("osgi.framework.Version", function() {

		describe("constructor()", function() {

			it('must support an argument string like : ("1")', function() {
				var version = new Version("1");
				expect(version.toString()).toEqual("1.0.0");
			});

			it('must support an argument string like : ("1.2")', function() {
				version = new Version("1.2");
				expect(version.toString()).toEqual("1.2.0");
			});
			
			it('must support an argument string like : ("1.2.3")', function() {
				version = new Version("1.2.3");
				expect(version.toString()).toEqual("1.2.3");
			});
			
			it('must support an argument string like : ("1.2.3.QUALIFIER")', function() {
				version = new Version("1.2.3.QUALIFIER");
				expect(version.toString()).toEqual("1.2.3.QUALIFIER");
			});
			
			it('must support an argument string like : ("0.0.0")', function() {
				version = new Version("0.0.0");
				expect(version.toString()).toEqual(Version.emptyVersion.toString());
			});

			it('must support argument strings like : "1", "2", "3"', function() {
				var version = new Version("1", "2", "3");
				expect(version.toString()).toEqual("1.2.3");
			});

			it('must support argument strings like : ("1", "2", "3", "QUALIFIER")', function() {
				var version = new Version("1", "2", "3", "QUALIFIER");
				expect(version.toString()).toEqual("1.2.3.QUALIFIER");
			});

			it("must throw an error from an invalid specified string", function() {
				var func = function() {
					return new Version(".2.3.SNAPSHOT");
				};

				expect(func).toThrow();

				func = function() {
					return new Version("1..3.SNAPSHOT");
				};

				expect(func).toThrow();

				func = function() {
					return new Version("1.2..SNAPSHOT");
				};

				expect(func).toThrow();

				func = function() {
					return new Version("1.2.3.");
				};

				func = function() {
					return new Version("1.2.3.*");
				};

				expect(func).toThrow();
			});

			it("must throw error from invalid specified numerical components", function() {
				var func = function() {
					return new Version("", "2", "3", "SNAPSHOT");
				};

				expect(func).toThrow();

				func = function() {
					return new Version("1", "", "3", "SNAPSHOT");
				};

				expect(func).toThrow();

				func = function() {
					return new Version("1", "2", "", "SNAPSHOT");
				};

				expect(func).toThrow();

				func = function() {
					return new Version("1", "2", "3", "%");
				};

				expect(func).toThrow();
			});

		});

		describe("validate", function() {

			it("must not throw error if valid components", function() {
				var version = new Version("1.2.3.SNAPSHOT");
				expect(version.validate.bind(version)).not.toThrow();
			});

			it("must throw error if invalid components", function() {
				var version = new Version("1.2.3.SNAPSHOT");
				version.major = -1;
				expect(version.validate.bind(version)).toThrow();

				version = new Version("1.2.3.SNAPSHOT");
				version.minor = -1;
				expect(version.validate.bind(version)).toThrow();

				version = new Version("1.2.3.SNAPSHOT");
				version.micro = -1;
				expect(version.validate.bind(version)).toThrow();

				version = new Version("1.2.3.SNAPSHOT");
				version.qualifier = "&";
				expect(version.validate.bind(version)).toThrow();
			});

		});

		describe("getMajor", function() {

			it("must return the major version", function() {
				var version = new Version("1.2.3");
				expect(version.getMajor()).toEqual(1);
			});

		});

		describe("getMinor", function() {

			it("must return the minor version", function() {
				var version = new Version("1.2.3");
				expect(version.getMinor()).toEqual(2);
			});

		});

		describe("getMicro", function() {

			it("must return the micro version", function() {
				var version = new Version("1.2.3");
				expect(version.getMicro()).toEqual(3);
			});

		});

		describe("getQualifier", function() {

			it("must return the qualifier", function() {
				var version = new Version("1.2.3.SNAPSHOT");
				expect(version.getQualifier()).toEqual("SNAPSHOT");
			});

		});

		describe("toString", function() {

			it("must serialize to 'major.minor.micro.qualifier'", function() {
				var version = new Version("1.2.3.SNAPSHOT");
				expect(version.toString()).toEqual("1.2.3.SNAPSHOT");
			});

		});

		describe("equals", function() {

			it("must support === comparisons", function() {
				var thisVersion = new Version("1.2.3.SNAPSHOT");
				var thatVersion = new Version("1.2.3.SNAPSHOT");
				expect(thisVersion.equals(thatVersion)).toBeTruthy();
			});

			it("must support !== comparison", function() {
				var thisVersion = new Version("1.2.3.SNAPSHOT");
				var thatVersion = new Version("1.2.3.FINAL");
				expect(thisVersion.equals(thatVersion)).toBeFalsy();
			});

		});

		describe("compareTo", function() {

			it("must support = comparison", function() {
				var thisVersion = new Version("1.2.3.SNAPSHOT");
				var thatVersion = new Version("1.2.3.SNAPSHOT");
				expect(thisVersion.compareTo(thatVersion)).toEqual(0);
			});

			it("must support < comparison", function() {
				var thisVersion = new Version("1.2.2.SNAPSHOT");
				var thatVersion = new Version("1.2.3.FINAL");
				expect(thisVersion.compareTo(thatVersion)).toEqual(-1);
			});

			it("must support > comparison", function() {
				var thisVersion = new Version("1.2.4.SNAPSHOT");
				var thatVersion = new Version("1.2.3.FINAL");
				expect(thisVersion.compareTo(thatVersion)).toEqual(1);
			});

		});

	});

})