define([ "osgi/framework/BundleException" ], function(BundleException) {

	describe("osgi.framework.BundleException", function() {

		describe("constructor()", function() {
			it("must support no arguments", function() {
				var exception = new BundleException();
				expect(exception.toString()).toEqual("osgi.framework.BundleException: \nUNSPECIFIED: \nException");
			});
			it("must support a message argument", function() {
				exception = new BundleException("foo exception");
				expect(exception.toString()).toEqual("osgi.framework.BundleException: \nUNSPECIFIED: \nfoo exception");
			});
			it("must support message and Exception arguments", function() {
				exception = new BundleException("foo exception", new BundleException("nested foo exception"));
				expect(exception.toString()).toEqual("osgi.framework.BundleException: \nUNSPECIFIED: \nfoo exception: \nnested foo exception");
			});
			it("must support message and Error arguments", function() {
				exception = new BundleException("foo exception", new Error("nested foo error"));
				expect(exception.toString()).toEqual("osgi.framework.BundleException: \nUNSPECIFIED: \nfoo exception: \nnested foo error");
			});
		});

		describe("getMessage()", function() {
			it("must return the message", function() {
				exception = new BundleException("foo exception");
				expect(exception.message).toEqual("foo exception");
			});
		});

		describe("getName", function() {
			it("must return the name", function() {
				exception = new BundleException("foo exception");
				expect(exception.name).toEqual("osgi.framework.BundleException");
			});
		});

		describe("toString()", function() {
			it("must support n levels of nested exceptions", function() {
				exception = new BundleException("foo exception", new BundleException("nested foo exception", new BundleException("2nd nested foo exception")));
				expect(exception.toString()).toEqual("osgi.framework.BundleException: \nUNSPECIFIED: \nfoo exception: \nnested foo exception: \n2nd nested foo exception");
			});
		});

	});

})