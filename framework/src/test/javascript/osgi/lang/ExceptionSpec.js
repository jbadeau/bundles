define([ "osgi/lang/Exception" ], function(Exception) {

	describe("osgi.lang.Exception", function() {

		describe("constructor()", function() {
			it("must support no arguments", function() {
				var exception = new Exception();
				expect(exception.toString()).toEqual("osgi.lang.Exception: \nException");
			});
			it("must support a message argument", function() {
				exception = new Exception("foo exception");
				expect(exception.toString()).toEqual("osgi.lang.Exception: \nfoo exception");
			});
			it("must support message and Exception arguments", function() {
				exception = new Exception("foo exception", new Exception("nested foo exception"));
				expect(exception.toString()).toEqual("osgi.lang.Exception: \nfoo exception: \nnested foo exception");
			});
			it("must support message and Error arguments", function() {
				exception = new Exception("foo exception", new Error("nested foo error"));
				expect(exception.toString()).toEqual("osgi.lang.Exception: \nfoo exception: \nnested foo error");
			});
		});

		describe("getMessage()", function() {
			it("must return the message", function() {
				exception = new Exception("foo exception");
				expect(exception.message).toEqual("foo exception");
			});
		});

		describe("getName", function() {
			it("must return the name", function() {
				exception = new Exception("foo exception");
				expect(exception.name).toEqual("osgi.lang.Exception");
			});
		});

		describe("toString()", function() {
			it("must support n levels of nested exceptions", function() {
				exception = new Exception("foo exception", new Exception("nested foo exception", new Exception("2nd nested foo exception")));
				expect(exception.toString()).toEqual("osgi.lang.Exception: \nfoo exception: \nnested foo exception: \n2nd nested foo exception");
			});
		});

	});

})