define([ "bundles/framework/FrameworkFactoryImpl",  "osgi/framework/Constants" ], function(FrameworkFactoryImpl, Constants) {

	describe("bundles.framework.FrameworkFactoryImpl", function() {

		describe("constructor", function() {

			it("must be public and have no arguments", function() {
				var factory = new FrameworkFactoryImpl();
				expect(factory).not.toBeNull();
			});
		});

		describe(".newFramework", function() {

			it("must return a Framework instance", function() {
				var properties = new Map();
				properties.set(Constants.FRAMEWORK_BOOTDELEGATION, "*");
				properties.set(Constants.BSNVERSION, Constants.FRAMEWORK_BSNVERSION_SINGLE);
				var factory = new FrameworkFactoryImpl();
				var framework = factory.newFramework(properties);
				framework.init();
				framework.start();
				expect(framework).not.toBeNull();
			});
		});

	});

})