define([ "vjo" ], function(vjo) {

	return vjo.ctype("osgi.util.StringBuilder")

	.protos({

		buffer : null,

		constructs : function() {
			if (arguments.length === 0) {
				this.constructs_0_0_StringBuilder_ovld();
			}
			else if (arguments.length === 1) {
				this.constructs_1_0_StringBuilder_ovld(arguments[0]);
			}
		},

		constructs_0_0_StringBuilder_ovld : function() {
			this.buffer = new Array();
		},

		constructs_1_0_StringBuilder_ovld : function(str) {
			this.buffer = new Array();
			this.buffer.push(str)
		},

		append : function(str) {
			this.buffer.push(str)
			return this;
		},

		toString : function() {
			return this.buffer.join("");
		}

	})

	.endType();

})