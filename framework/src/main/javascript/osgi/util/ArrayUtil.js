define([ "vjo" ], function(vjo) {

	return vjo.ctype("osgi.util.ArrayUtil")

	.props({

		contains : function(array, value) {
			if (!array || array === null || array.length === 0) {
				return false;
			}
			array.forEach(function(val, index, array) {
				if (val === value) {
					return true;
				}
			});
			return false;
		}

	})

	.endType();

})
