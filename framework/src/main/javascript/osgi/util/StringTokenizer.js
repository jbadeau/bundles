define([ '../../vjo', '../lang/Character', '../lang/NullPointerException', './NoSuchElementException', './Enumeration' ], function(vjo, Character, NullPointerException, NoSuchElementException, Enumeration) {
	var StringTokenizer = vjo.ctype('java.util.StringTokenizer') // < public
	.satisfies(Enumeration)
	.protos({
		currentPosition : 0, // < private int
		newPosition : 0, // < private int
		maxPosition : 0, // < private int
		str : null, // < private String
		delimiters : null, // < private String
		retDelims : false, // < private boolean
		delimsChanged : false, // < private boolean
		maxDelimCodePoint : 0, // < private int
		hasSurrogates : false, // < private boolean
		delimiterCodePoints : null, // < private int[]
		// > public constructs()
		// > public constructs(String str,String delim,boolean returnDelims)
		// > public constructs(String str,String delim)
		// > public constructs(String str)
		constructs : function() {
			if (arguments.length === 3) {
				this.constructs_3_0_StringTokenizer_ovld(arguments[0], arguments[1], arguments[2]);
			}
			else if (arguments.length === 2) {
				this.constructs_2_0_StringTokenizer_ovld(arguments[0], arguments[1]);
			}
			else if (arguments.length === 1) {
				this.constructs_1_0_StringTokenizer_ovld(arguments[0]);
			}
		},
		// > private void setMaxDelimCodePoint()
		setMaxDelimCodePoint : function() {
			if (this.delimiters === null) {
				this.maxDelimCodePoint = 0;
				return;
			}
			var m = 0; // <int
			var c; // <int
			var count = 0; // <int
			for ( var i = 0; i < this.delimiters.length; i += Character.charCount(c)) {
				c = this.delimiters.charAt(i);
				if (c >= Character.MIN_HIGH_SURROGATE && c <= Character.MAX_LOW_SURROGATE) {
					c = this.delimiters.charCodeAt(i);
					this.hasSurrogates = true;
				}
				if (m < c) {
					m = c;
				}
				count++;
			}
			this.maxDelimCodePoint = m;
			if (this.hasSurrogates) {
				this.delimiterCodePoints = vjo.createArray(0, count);
				for ( var i = 0, j = 0; i < count; i++, j += Character.charCount(c)) {
					c = this.delimiters.charCodeAt(j);
					this.delimiterCodePoints[i] = c;
				}
			}
		},
		// > protected constructs_3_0_StringTokenizer_ovld(String str,String
		// delim,boolean returnDelims)
		constructs_3_0_StringTokenizer_ovld : function(str, delim, returnDelims) {
			this.currentPosition = 0;
			this.newPosition = -1;
			this.delimsChanged = false;
			this.str = str;
			this.maxPosition = str.length;
			this.delimiters = delim;
			this.retDelims = returnDelims;
			this.setMaxDelimCodePoint();
		},
		// > protected constructs_2_0_StringTokenizer_ovld(String str,String
		// delim)
		constructs_2_0_StringTokenizer_ovld : function(str, delim) {
			this.constructs_3_0_StringTokenizer_ovld(str, delim, false);
		},
		// > protected constructs_1_0_StringTokenizer_ovld(String str)
		constructs_1_0_StringTokenizer_ovld : function(str) {
			this.constructs_3_0_StringTokenizer_ovld(str, " \t\n\r\f", false);
		},
		// > private int skipDelimiters(int startPos)
		skipDelimiters : function(startPos) {
			if (this.delimiters === null) {
				throw new NullPointerException();
			}
			var position = startPos; // <int
			while (!this.retDelims && position < this.maxPosition) {
				if (!this.hasSurrogates) {
					var c = this.str.charAt(position); // <char
					if ((c > this.maxDelimCodePoint) || (this.delimiters.indexOf(c) < 0)) {
						break;
					}
					position++;
				}
				else {
					var c = this.str.charCodeAt(position); // <int
					if ((c > this.maxDelimCodePoint) || !this.isDelimiter(c)) {
						break;
					}
					position += Character.charCount(c);
				}
			}
			return position;
		},
		// > private int scanToken(int startPos)
		scanToken : function(startPos) {
			var position = startPos; // <int
			while (position < this.maxPosition) {
				if (!this.hasSurrogates) {
					var c = this.str.charAt(position); // <char
					if ((c <= this.maxDelimCodePoint) && (this.delimiters.indexOf(c) >= 0)) {
						break;
					}
					position++;
				}
				else {
					var c = this.str.charCodeAt(position); // <int
					if ((c <= this.maxDelimCodePoint) && this.isDelimiter(c)) {
						break;
					}
					position += Character.charCount(c);
				}
			}
			if (this.retDelims && (startPos === position)) {
				if (!this.hasSurrogates) {
					var c = this.str.charAt(position); // <char
					if ((c <= this.maxDelimCodePoint) && (this.delimiters.indexOf(c) >= 0)) {
						position++;
					}
				}
				else {
					var c = this.str.charCodeAt(position); // <int
					if ((c <= this.maxDelimCodePoint) && this.isDelimiter(c)) {
						position += Character.charCount(c);
					}
				}
			}
			return position;
		},
		// > private boolean isDelimiter(int codePoint)
		isDelimiter : function(codePoint) {
			for ( var i = 0; i < this.delimiterCodePoints.length; i++) {
				if (this.delimiterCodePoints[i] === codePoint) {
					return true;
				}
			}
			return false;
		},
		// > public boolean hasMoreTokens()
		hasMoreTokens : function() {
			this.newPosition = this.skipDelimiters(this.currentPosition);
			return (this.newPosition < this.maxPosition);
		},
		// > public String nextToken()
		// > public String nextToken(String delim)
		nextToken : function() {
			if (arguments.length === 0) {
				return this.nextToken_0_0_StringTokenizer_ovld();
			}
			else if (arguments.length === 1) {
				return this.nextToken_1_0_StringTokenizer_ovld(arguments[0]);
			}
		},
		// > protected String nextToken_0_0_StringTokenizer_ovld()
		nextToken_0_0_StringTokenizer_ovld : function() {
			this.currentPosition = (this.newPosition >= 0 && !this.delimsChanged) ? this.newPosition : this.skipDelimiters(this.currentPosition);
			this.delimsChanged = false;
			this.newPosition = -1;
			if (this.currentPosition >= this.maxPosition) {
				throw new org.eclipse.vjet.vjo.java.util.NoSuchElementException();
			}
			var start = this.currentPosition; // <int
			this.currentPosition = this.scanToken(this.currentPosition);
			return this.str.substring(start, this.currentPosition);
		},
		// > protected String nextToken_1_0_StringTokenizer_ovld(String delim)
		nextToken_1_0_StringTokenizer_ovld : function(delim) {
			this.delimiters = delim;
			this.delimsChanged = true;
			this.setMaxDelimCodePoint();
			return this.nextToken();
		},
		// > public boolean hasMoreElements()
		hasMoreElements : function() {
			return this.hasMoreTokens();
		},
		// > public Object nextElement()
		nextElement : function() {
			return this.nextToken();
		},
		// > public int countTokens()
		countTokens : function() {
			var count = 0; // <int
			var currpos = this.currentPosition; // <int
			while (currpos < this.maxPosition) {
				currpos = this.skipDelimiters(currpos);
				if (currpos >= this.maxPosition) {
					break;
				}
				currpos = this.scanToken(currpos);
				count++;
			}
			return count;
		}
	}).endType();

	return StringTokenizer

});
