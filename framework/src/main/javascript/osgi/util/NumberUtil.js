define([ "vjo" ], function(vjo) {

	return vjo.ctype("osgi.util.NumberUtil")

	.props({

		MIN_INT : -2147483648,

		MAX_INT : 0x7fffffff

	})

	.endType();

})