define([ "vjo" ], function(vjo) {

	return vjo.ctype("osgi.util.BooleanUtil")
	
	.props({

		TRUE : null,

		FALSE : null,

		booleanValue : function(b) {
			return b.valueOf();
		},

		getBoolean : function(name) {
			return ((name == "true") ? true : false);
		},

		parseBoolean : function(s) {
			return this.toBoolean(s);
		},

		toBoolean : function(name) {
			return (name != null && name.toLowerCase() == "true");
		},

		toString : function(val) {
			var b;
			if (typeof val == "boolean") {
				b = val;
			}
			else {
				b = val.valueOf();
			}
			return b ? "true" : "false";
		},

		valueOf_ : function(val) {
			var b;
			if (typeof val == "string" && (val == "true" || val == "false")) {
				b = eval(val);
			}
			else if (typeof val == "boolean") {
				b = val;
			}
			return (b ? this.TRUE : this.FALSE);
		}

	})
	
	.inits(function() {
		this.FALSE = new Boolean(false);
		this.TRUE = new Boolean(true);
	})
	
	.endType();

})