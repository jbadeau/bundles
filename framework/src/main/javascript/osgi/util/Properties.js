define([ "vjo" ], function(vjo) {

	return vjo.ctype("osgi.util.Properties")

	.protos({

		map : null,

		constructs : function() {
			if (arguments.length === 0) {
				this.constructs_0_0_Properties_ovld();
			}
			else if (arguments.length === 1) {
				this.constructs_1_0_Properties_ovld(arguments[0]);
			}
		},

		constructs_0_0_Properties_ovld : function() {
			this.map = new Map();
		},

		constructs_1_0_Properties_ovld : function(properties) {
			this.map = new Map();
			if(properties.forEach) {
				properties.forEach(function (value, key, map) {
					this.map.set(key, value);
				}.bind(this));
			}
			else {
				for(var key in properties) {
					this.map.set(key, properties[key]);
				}
			}
		},

		getProperty : function(key) {
			if (arguments.length === 1) {
				if (arguments[0] instanceof String || typeof arguments[0] == "string") {
					return this.getProperty_1_0_Properties_ovld(arguments[0]);
				}
				else if (this.base && this.base.get) {
					return this.base.get.apply(this, arguments);
				}
			}
			else if (arguments.length === 2) {
				return this.getProperty_2_0_Properties_ovld(arguments[0], arguments[1]);
			}
		},

		getProperty_1_0_Properties_ovld : function(key) {
			var result = this.map.get(key);
			return (result && result !== null) ? result : null;
		},

		getProperty_2_0_Properties_ovld : function(key, defaultValue) {
			var result = this.getProperty(key);
			return (result !== null) ? result : defaultValue;
		},

		size : function() {
			return this.map.size;
		},
		

		clear : function() {
			this.map.clear();
		},
		

		delete : function(key) {
			return this.map.delete(key);
		},
		

		entries : function() {
			return this.map.entries();
		},
		

		forEach : function(callback, thisArg) {
			this.map.forEach(callback, thisArg);
		},

		has : function(key) {
			return this.map.has(key);
		},
		

		propertyNames : function() {
			return this.map.keys();
		},
		

		setProperty : function(key, value) {
			return this.map.set(key, value);
		},

		setAll : function(properties) {
			if(properties.forEach) {
				properties.forEach(function (value, key, map) {
					this.map.set(key, value);
				}.bind(this));
			}
			else {
				for(var key in properties) {
					this.map.set(key, properties[key]);
				}
			}
		},

		values : function() {
			return this.map.values();
		}

	})

	.endType();

})