define([ "vjo" ], function(vjo) {

	return vjo.ctype("osgi.util.Environment")

	.props({

		properties : null,

		// browser
		BROWSER_NAME : "browser.name",
		BROWSER_VERSION : "browser.version",
		BROWSER_DOCUMENT_MODE : "browser.document.mode",
		BROWSER_QUIRKS_MODE : "browser.quirks.mode",

		// runtime
		RUNTIME_NAME : "runtime.name",

		// device
		DEVICE_NAME : "device.name",
		DEVICE_TYPE : "device.type",
		DEVICE_PIXEL_RATIO : "device.pixelRatio",
		DEVICE_TOUCH : "device.touch",

		// locale
		LOCALE : "locale",
		LOCALE_VARIANT : "locale.variant",

		// engine
		ENGINE : "engine.name",
		ENGINE_VERSION : "engine.version",

		// os
		OS_NAME : "os.name",
		OS_VERSION : "os.version",

		getProperty : function(key) {
			var value = this.vj$.Environment.properties.get(key);
			return this.vj$.Environment.check(value);
		},

		getProperties : function() {
			var checkedProperties = new Map();
			this.vj$.Environment.properties.forEach(function(value, key, map) {
				var checkedValue = this.vj$.Environment.check(value);
				checkedProperties.set(key, checkedValue);
			}.bind(this));
			return checkedProperties;
		},

		addProperty : function(key, check) {
			this.vj$.Environment.properties.set(key, check);
		},

		check : function(value) {
			return typeof value == "function" ? value.call(this) : value;
		},

		/**
		 * Checks for the name of the operating system.
		 * 
		 * @return {String} The name of the operating system.
		 */
		getOperatingSystemName : function() {
			if (!navigator) {
				return "";
			}
			var input = navigator.platform || "";
			var agent = navigator.userAgent || "";

			if (input.indexOf("Windows") != -1 || input.indexOf("Win32") != -1 || input.indexOf("Win64") != -1) {
				return "win";

			}
			else if (input.indexOf("Macintosh") != -1 || input.indexOf("MacPPC") != -1 || input.indexOf("MacIntel") != -1 || input.indexOf("Mac OS X") != -1) {
				return "osx";

			}
			else if (agent.indexOf("RIM Tablet OS") != -1) {
				return "rim_tabletos";

			}
			else if (agent.indexOf("webOS") != -1) {
				return "webos";

			}
			else if (input.indexOf("iPod") != -1 || input.indexOf("iPhone") != -1 || input.indexOf("iPad") != -1) {
				return "ios";

			}
			else if (agent.indexOf("Android") != -1) {
				return "android";

			}
			else if (input.indexOf("Linux") != -1) {
				return "linux";

			}
			else if (input.indexOf("X11") != -1 || input.indexOf("BSD") != -1 || input.indexOf("Darwin") != -1) {
				return "unix";

			}
			else if (input.indexOf("SymbianOS") != -1) {
				return "symbian";
			}

			else if (input.indexOf("BlackBerry") != -1) {
				return "blackberry";
			}

			// don't know
			return "";
		},

		/**
		 * Maps user agent names to system IDs
		 */
		__OperatingSystemIds : {
			// Windows
			"Windows NT 6.3" : "8.1",
			"Windows NT 6.2" : "8",
			"Windows NT 6.1" : "7",
			"Windows NT 6.0" : "vista",
			"Windows NT 5.2" : "2003",
			"Windows NT 5.1" : "xp",
			"Windows NT 5.0" : "2000",
			"Windows 2000" : "2000",
			"Windows NT 4.0" : "nt4",

			"Win 9x 4.90" : "me",
			"Windows CE" : "ce",
			"Windows 98" : "98",
			"Win98" : "98",
			"Windows 95" : "95",
			"Win95" : "95",

			// OS X
			"Mac OS X 10_9" : "10.9",
			"Mac OS X 10.9" : "10.9",
			"Mac OS X 10_8" : "10.8",
			"Mac OS X 10.8" : "10.8",
			"Mac OS X 10_7" : "10.7",
			"Mac OS X 10.7" : "10.7",
			"Mac OS X 10_6" : "10.6",
			"Mac OS X 10.6" : "10.6",
			"Mac OS X 10_5" : "10.5",
			"Mac OS X 10.5" : "10.5",
			"Mac OS X 10_4" : "10.4",
			"Mac OS X 10.4" : "10.4",
			"Mac OS X 10_3" : "10.3",
			"Mac OS X 10.3" : "10.3",
			"Mac OS X 10_2" : "10.2",
			"Mac OS X 10.2" : "10.2",
			"Mac OS X 10_1" : "10.1",
			"Mac OS X 10.1" : "10.1",
			"Mac OS X 10_0" : "10.0",
			"Mac OS X 10.0" : "10.0"
		},

		/**
		 * Checks for the version of the operating system using the internal
		 * map.
		 * 
		 * @return {String} The version as strin or an empty string if the
		 *         version could not be detected.
		 */
		getOperatingSystemVersion : function() {
			var version = this.vj$.Environment.__getVersionForDesktopOs(navigator.userAgent);

			if (version == null) {
				version = this.vj$.Environment.__getVersionForMobileOs(navigator.userAgent);
			}

			if (version != null) {
				return version;
			}
			else {
				return "";
			}
		},

		/**
		 * Detect OS version for desktop devices
		 * 
		 * @param userAgent
		 *            {String} userAgent parameter, needed for detection.
		 * @return {String} version number as string or null.
		 */
		__getVersionForDesktopOs : function(userAgent) {
			var str = [];
			for ( var key in this.vj$.Environment.__OperatingSystemIds) {
				str.push(key);
			}

			var reg = new RegExp("(" + str.join("|").replace(/\./g, "\.") + ")", "g");
			var match = reg.exec(userAgent);

			if (match && match[1]) {
				return this.vj$.Environment.__OperatingSystemIds[match[1]];
			}

			return null;
		},

		/**
		 * Detect OS version for mobile devices
		 * 
		 * @param userAgent
		 *            {String} userAgent parameter, needed for detection.
		 * @return {String} version number as string or null.
		 */
		__getVersionForMobileOs : function(userAgent) {
			var android = userAgent.indexOf("Android") != -1;
			var iOs = userAgent.match(/(iPad|iPhone|iPod)/i) ? true : false;

			if (android) {
				var androidVersionRegExp = new RegExp(/ Android (\d+(?:\.\d+)+)/i);
				var androidMatch = androidVersionRegExp.exec(userAgent);

				if (androidMatch && androidMatch[1]) {
					return androidMatch[1];
				}
			}
			else if (iOs) {
				var iOsVersionRegExp = new RegExp(/(CPU|iPhone|iPod) OS (\d+)_(\d+)(?:_(\d+))*\s+/);
				var iOsMatch = iOsVersionRegExp.exec(userAgent);

				if (iOsMatch && iOsMatch[2] && iOsMatch[3]) {
					if (iOsMatch[4]) {
						return iOsMatch[2] + "." + iOsMatch[3] + "." + iOsMatch[4];
					}
					else {
						return iOsMatch[2] + "." + iOsMatch[3];
					}
				}
			}

			return null;
		},

		/**
		 * Returns the version of the engine.
		 * 
		 * @return {String} The version number of the current engine.
		 * @internal
		 */
		getEngineVersion : function() {
			var agent = window.navigator.userAgent;

			var version = "";
			if (this.vj$.Environment.__isOpera()) {
				// Opera has a special versioning scheme, where the second part
				// is
				// combined
				// e.g. 8.54 which should be handled like 8.5.4 to be compatible
				// to
				// the
				// common versioning system used by other browsers
				if (/Opera[\s\/]([0-9]+)\.([0-9])([0-9]*)/.test(agent)) {
					// opera >= 10 has as a first verison 9.80 and adds the
					// proper
					// version
					// in a separate "Version/" postfix
					// http://my.opera.com/chooseopera/blog/2009/05/29/changes-in-operas-user-agent-string-format
					if (agent.indexOf("Version/") != -1) {
						var match = agent.match(/Version\/(\d+)\.(\d+)/);
						// ignore the first match, its the whole version string
						version = match[1] + "." + match[2].charAt(0) + "." + match[2].substring(1, match[2].length);
					}
					else {
						version = RegExp.$1 + "." + RegExp.$2;
						if (RegExp.$3 != "") {
							version += "." + RegExp.$3;
						}
					}
				}
			}
			else if (this.vj$.Environment.__isWebkit()) {
				if (/AppleWebKit\/([^ ]+)/.test(agent)) {
					version = RegExp.$1;

					// We need to filter these invalid characters
					var invalidCharacter = RegExp("[^\\.0-9]").exec(version);

					if (invalidCharacter) {
						version = version.slice(0, invalidCharacter.index);
					}
				}
			}
			else if (this.vj$.Environment.__isGecko() || this.vj$.Environment.__isMaple()) {
				// Parse "rv" section in user agent string
				if (/rv\:([^\);]+)(\)|;)/.test(agent)) {
					version = RegExp.$1;
				}
			}
			else if (this.vj$.Environment.__isMshtml()) {
				var isTrident = /Trident\/([^\);]+)(\)|;)/.test(agent);
				if (/MSIE\s+([^\);]+)(\)|;)/.test(agent)) {
					version = RegExp.$1;

					// If the IE8 or IE9 is running in the compatibility mode,
					// the
					// MSIE value
					// is set to an older version, but we need the correct
					// version.
					// The only
					// way is to compare the trident version.
					if (version < 8 && isTrident) {
						if (RegExp.$1 == "4.0") {
							version = "8.0";
						}
						else if (RegExp.$1 == "5.0") {
							version = "9.0";
						}
					}
				}
				else if (isTrident) {
					// IE 11 dropped the "MSIE" string
					var match = /\brv\:(\d+?\.\d+?)\b/.exec(agent);
					if (match) {
						version = match[1];
					}
				}
			}
			else {
				var failFunction = window.qxFail;
				if (failFunction && typeof failFunction === "function") {
					version = failFunction().FULLVERSION;
				}
				else {
					version = "1.9.0.0";
					console.warn("Unsupported client: " + agent + "! Assumed gecko version 1.9.0.0 (Firefox 3.0).");
				}
			}

			return version;
		},

		/**
		 * Returns the name of the engine.
		 * 
		 * @return {String} The name of the current engine.
		 * @internal
		 */
		getEngineName : function() {
			var name;
			if (this.vj$.Environment.__isOpera()) {
				name = "opera";
			}
			else if (this.vj$.Environment.__isWebkit()) {
				name = "webkit";
			}
			else if (this.vj$.Environment.__isGecko() || this.vj$.Environment.__isMaple()) {
				name = "gecko";
			}
			else if (this.vj$.Environment.__isMshtml()) {
				name = "mshtml";
			}
			else {
				// check for the fallback
				var failFunction = window.qxFail;
				if (failFunction && typeof failFunction === "function") {
					name = failFunction().NAME;
				}
				else {
					name = "gecko";
					console.warn("Unsupported client: " + window.navigator.userAgent + "! Assumed gecko version 1.9.0.0 (Firefox 3.0).");
				}
			}
			return name;
		},

		/**
		 * Internal helper for checking for opera (presto powered).
		 * 
		 * Note that with opera >= 15 their engine switched to blink, so things
		 * like "window.opera" don't work anymore or changed (e.g. user agent).
		 * 
		 * @return {Boolean} true, if its opera (presto powered).
		 */
		__isOpera : function() {
			return window.opera && Object.prototype.toString.call(window.opera) == "[object Opera]";
		},

		/**
		 * Internal helper for checking for webkit.
		 * 
		 * @return {Boolean} true, if its webkit.
		 */
		__isWebkit : function() {
			return window.navigator.userAgent.indexOf("AppleWebKit/") != -1;
		},

		/**
		 * Internal helper for checking for Maple . Maple is used in Samsung
		 * SMART TV 2010-2011 models. It's based on Gecko engine 1.8.1.11.
		 * 
		 * @return {Boolean} true, if its maple.
		 */
		__isMaple : function() {
			return window.navigator.userAgent.indexOf("Maple") != -1;
		},

		/**
		 * Internal helper for checking for gecko.
		 * 
		 * @return {Boolean} true, if its gecko.
		 */
		__isGecko : function() {
			return window.controllers && window.navigator.product === "Gecko" && window.navigator.userAgent.indexOf("Maple") == -1 && window.navigator.userAgent.indexOf("Trident") == -1;
		},

		/**
		 * Internal helper to check for MSHTML.
		 * 
		 * @return {Boolean} true, if its MSHTML.
		 */
		__isMshtml : function() {
			return window.navigator.cpuClass && (/MSIE\s+([^\);]+)(\)|;)/.test(window.navigator.userAgent) || /Trident\/\d+?\.\d+?/.test(window.navigator.userAgent));
		},

		/**
		 * Checks for the name of the browser and returns it.
		 * 
		 * @return {String} The name of the current browser.
		 * @internal
		 */
		getBrowserName : function() {
			var agent = navigator.userAgent;
			var reg = new RegExp("(" + this.vj$.Environment.__getAgents(this.vj$.Environment.getEngineName()) + ")(/| )([0-9]+\.[0-9])");
			var match = agent.match(reg);
			if (!match) {
				return "";
			}

			var name = match[1].toLowerCase();

			var engine = this.vj$.Environment.getEngineName();
			if (engine === "webkit") {
				if (name === "android") {
					// Fix Chrome name (for instance wrongly defined in user
					// agent
					// on Android 1.6)
					name = "mobile chrome";
				}
				else if (agent.indexOf("Mobile Safari") !== -1 || agent.indexOf("Mobile/") !== -1) {
					// Fix Safari name
					name = "mobile safari";
				}
				else if (agent.indexOf(" OPR/") != -1) {
					name = "opera";
				}
			}
			else if (engine === "mshtml") {
				// IE 11's ua string no longer contains "MSIE" or even "IE"
				if (name === "msie" || name === "trident") {
					name = "ie";

					// Fix IE mobile before Microsoft added IEMobile string
					if (this.vj$.Environment.getOperatingSystemVersion() === "ce") {
						name = "iemobile";
					}
				}
			}
			else if (engine === "opera") {
				if (name === "opera mobi") {
					name = "operamobile";
				}
				else if (name === "opera mini") {
					name = "operamini";
				}
			}
			else if (engine === "gecko") {
				if (agent.indexOf("Maple") !== -1) {
					name = "maple"
				}
			}

			return name;
		},

		/**
		 * Determines the version of the current browser.
		 * 
		 * @return {String} The name of the current browser.
		 * @internal
		 */
		getBrowserVersion : function() {
			var agent = navigator.userAgent;
			var reg = new RegExp("(" + this.vj$.Environment.__getAgents(this.vj$.Environment.getEngineName()) + ")(/| )([0-9]+\.[0-9])");
			var match = agent.match(reg);
			if (!match) {
				return "";
			}

			var name = match[1].toLowerCase();
			var version = match[3];

			// Support new style version string used by Opera and Safari
			if (agent.match(/Version(\/| )([0-9]+\.[0-9])/)) {
				version = RegExp.$2;
			}

			if (this.vj$.Environment.getEngineName() == "mshtml") {
				// Use the Engine version, because IE8 and higher change the
				// user
				// agent
				// string to an older version in compatibility mode
				version = this.vj$.Environment.getEngineVersion();

				if (name === "msie" && this.vj$.Environment.getOperatingSystemVersion() == "ce") {
					// Fix IE mobile before Microsoft added IEMobile string
					version = "5.0";
				}
			}

			if (this.vj$.Environment.getBrowserName() == "maple") {
				// Fix version detection for Samsung Smart TVs Maple browser
				// from
				// 2010 and 2011 models
				reg = new RegExp("(Maple )([0-9]+\.[0-9]+\.[0-9]*)");
				match = agent.match(reg);
				if (!match) {
					return "";
				}

				version = match[2];
			}

			if (this.vj$.Environment.getEngineName() == "webkit" || this.vj$.Environment.getBrowserName() == "opera") {
				if (agent.match(/OPR(\/| )([0-9]+\.[0-9])/)) {
					version = RegExp.$2;
				}
			}

			return version;
		},

		/**
		 * Returns in which document mode the current document is (only for IE).
		 * 
		 * @internal
		 * @return {Number} The mode in which the browser is.
		 */
		getBrowserDocumentMode : function() {
			if (document.documentMode) {
				return document.documentMode;
			}
			return 0;
		},

		/**
		 * Check if in quirks mode.
		 * 
		 * @internal
		 * @return {Boolean} <code>true</code>, if the environment is in
		 *         quirks mode
		 */
		getBrowserQuirksMode : function() {
			if (this.vj$.Environment.getEngineName() == "mshtml" && parseFloat(this.vj$.Environment.getEngineVersion()) >= 8) {
				return this.vj$.Environment.DOCUMENT_MODE === 5;
			}
			else {
				return document.compatMode !== "CSS1Compat";
			}
		},

		/**
		 * Internal helper map for picking the right browser names to check.
		 */
		__getAgents : function(engineName) {
			var agents = new Map();
			// Safari should be the last one to check, because some other
			// Webkit-based browsers
			// use this identifier together with their own one.
			// "Version" is used in Safari 4 to define the Safari version. After
			// "Safari" they place the
			// Webkit version instead. Silly.
			// Palm Pre uses both Safari (contains Webkit version) and "Version"
			// contains the "Pre" version. But
			// as "Version" is not Safari here, we better detect this as the
			// Pre-Browser version. So place
			// "Pre" in front of both "Version" and "Safari".
			agents.set("webkit", "AdobeAIR|Titanium|Fluid|Chrome|Android|Epiphany|Konqueror|iCab|OmniWeb|Maxthon|Pre|PhantomJS|Mobile Safari|Safari");

			// Better security by keeping Firefox the last one to match
			agents.set("gecko", "prism|Fennec|Camino|Kmeleon|Galeon|Netscape|SeaMonkey|Namoroka|Firefox");

			// No idea what other browsers based on IE's engine
			agents.set("mshtml", "IEMobile|Maxthon|MSIE|Trident");

			// Keep "Opera" the last one to correctly prefer/match the mobile
			// clients
			agents.set("opera", "Opera Mini|Opera Mobi|Opera");

			return agents.get(engineName);
		},

		/**
		 * The name of the system locale e.g. "de" when the full locale is
		 * "de_AT"
		 * 
		 * @return {String} The current locale
		 * @internal
		 */
		getLocale : function() {
			var locale = this.vj$.Environment.__getNavigatorLocale();

			var index = locale.indexOf("-");
			if (index != -1) {
				locale = locale.substr(0, index);
			}

			return locale;
		},

		/**
		 * The name of the variant for the system locale e.g. "at" when the full
		 * locale is "de_AT"
		 * 
		 * @return {String} The locales variant.
		 * @internal
		 */
		getLocaleVariant : function() {
			var locale = this.vj$.Environment.__getNavigatorLocale();
			var variant = "";

			var index = locale.indexOf("-");

			if (index != -1) {
				variant = locale.substr(index + 1);
			}

			return variant;
		},

		/**
		 * Internal helper for accessing the navigators language.
		 * 
		 * @return {String} The language set by the navigator.
		 */
		__getNavigatorLocale : function() {
			var locale = (navigator.userLanguage || navigator.language || "");

			// Android Bug: Android does not return the system language from the
			// navigator language. Try to parse the language from the userAgent.
			// See http://code.google.com/p/android/issues/detail?id=4641
			if (this.vj$.Environment.getOperatingSystemName() == "android") {
				var match = /(\w{2})-(\w{2})/i.exec(navigator.userAgent);
				if (match) {
					locale = match[0];
				}
			}

			return locale.toLowerCase();
		},

		/** Maps user agent names to device IDs */
		__deviceIds : {
			"iPod" : "ipod",
			"iPad" : "ipad",
			"iPhone" : "iPhone",
			"PSP" : "psp",
			"PLAYSTATION 3" : "ps3",
			"Nintendo Wii" : "wii",
			"Nintendo DS" : "ds",
			"XBOX" : "xbox",
			"Xbox" : "xbox"
		},

		/**
		 * Returns the name of the current device if detectable. It falls back
		 * to <code>pc</code> if the detection for other devices fails.
		 * 
		 * @internal
		 * @return {String} The string of the device found.
		 */
		getDeviceName : function() {
			var str = [];
			for ( var key in this.__deviceIds) {
				str.push(key);
			}
			var reg = new RegExp("(" + str.join("|").replace(/\./g, "\.") + ")", "g");
			var match = reg.exec(navigator.userAgent);

			if (match && match[1]) {
				return this.vj$.Environment.__deviceIds[match[1]];
			}

			return "pc";
		},

		/**
		 * Determines on what type of device the application is running. Valid
		 * values are: "mobile", "tablet" or "desktop".
		 * 
		 * @return {String} The device type name of determined device.
		 */
		getDeviceType : function() {
			return this.vj$.Environment.detectDeviceType(navigator.userAgent);
		},

		/**
		 * Detects the device type, based on given userAgentString.
		 * 
		 * @param userAgentString
		 *            {String} userAgent parameter, needed for decision.
		 * @return {String} The device type name of determined device:
		 *         "mobile","desktop","tablet"
		 */
		detectDeviceType : function(userAgentString) {
			if (this.vj$.Environment.detectTabletDevice(userAgentString)) {
				return "tablet";
			}
			else if (this.vj$.Environment.detectMobileDevice(userAgentString)) {
				return "mobile";
			}

			return "desktop";
		},

		/**
		 * Detects if a device is a mobile phone. (Tablets excluded.)
		 * 
		 * @param userAgentString
		 *            {String} userAgent parameter, needed for decision.
		 * @return {Boolean} Flag which indicates whether it is a mobile device.
		 */
		detectMobileDevice : function(userAgentString) {
			return /android.+mobile|ip(hone|od)|bada\/|blackberry|BB10|maemo|opera m(ob|in)i|fennec|NetFront|phone|psp|symbian|IEMobile|windows (ce|phone)|xda/i.test(userAgentString);
		},

		/**
		 * Detects if a device is a tablet device.
		 * 
		 * @param userAgentString
		 *            {String} userAgent parameter, needed for decision.
		 * @return {Boolean} Flag which indicates whether it is a tablet device.
		 */
		detectTabletDevice : function(userAgentString) {
			var isIE10Tablet = (/MSIE 10/i.test(userAgentString)) && (/ARM/i.test(userAgentString)) && !(/windows phone/i.test(userAgentString));
			var isCommonTablet = (!(/Fennec|HTC.Magic|Nexus|android.+mobile|Tablet PC/i.test(userAgentString)) && (/Android|ipad|tablet|playbook|silk|kindle|psp/i.test(userAgentString)));

			return isIE10Tablet || isCommonTablet;
		},

		/**
		 * Detects the device's pixel ratio. Returns 1 if detection is not
		 * possible.
		 * 
		 * @return {Number} The device's pixel ratio
		 */
		getDevicePixelRatio : function() {
			if (typeof window.devicePixelRatio !== "undefined") {
				return window.devicePixelRatio;
			}

			return 1;
		},

		/**
		 * Detects if either touch events or pointer events are supported.
		 * Additionally it checks if touch is enabled for pointer events.
		 * 
		 * @return {Boolean} <code>true</code>, if the device supports touch
		 */
		getDeviceTouch : function() {
			return (("ontouchstart" in window) || window.navigator.maxTouchPoints > 0 || window.navigator.msMaxTouchPoints > 0);
		},

		/**
		 * Checks for the name of the runtime and returns it. In general, it
		 * checks for rhino and node.js and if that could not be detected, it
		 * falls back to the browser name defined by
		 * {@link qx.bom.client.Browser#getName}.
		 * 
		 * @return {String} The name of the current runtime.
		 * @ignore(environment, process, Titanium.*)
		 */
		getRuntimeName : function() {
			var name = "";

			// check for the Rhino runtime
			if (typeof environment !== "undefined") {
				name = "rhino";
				// check for the Node.js runtime
			}
			else if (typeof process !== "undefined") {
				name = "node.js";
			}
			else if (typeof Titanium !== "undefined" && typeof Titanium.userAgent !== "undefined") {
				name = "titanium";
			}
			else {
				// otherwise, we think its a browser
				name = this.vj$.Environment.getBrowserName();
			}

			return name;
		}

	})

	.inits(function() {
		this.vj$.Environment.properties = new Map();

		// os
		this.vj$.Environment.addProperty(this.vj$.Environment.OS_NAME, this.vj$.Environment.getOperatingSystemName);
		this.vj$.Environment.addProperty(this.vj$.Environment.OS_VERSION, this.vj$.Environment.getOperatingSystemVersion);

		// engine
		this.vj$.Environment.addProperty(this.vj$.Environment.ENGINE, this.vj$.Environment.getEngineVersion);
		this.vj$.Environment.addProperty(this.vj$.Environment.ENGINE_VERSION, this.vj$.Environment.getEngineName);

		// browser
		this.vj$.Environment.addProperty(this.vj$.Environment.BROWSER_NAME, this.vj$.Environment.getBrowserName);
		this.vj$.Environment.addProperty(this.vj$.Environment.BROWSER_VERSION, this.vj$.Environment.getBrowserVersion);
		this.vj$.Environment.addProperty(this.vj$.Environment.BROWSER_DOCUMENT_MODE, this.vj$.Environment.getBrowserDocumentMode);
		this.vj$.Environment.addProperty(this.vj$.Environment.BROWSER_QUIRKS_MODE, this.vj$.Environment.getBrowserQuirksMode);

		// locale
		this.vj$.Environment.addProperty(this.vj$.Environment.LOCALE, this.vj$.Environment.getLocale);
		this.vj$.Environment.addProperty(this.vj$.Environment.LOCALE_VARIANT, this.vj$.Environment.getLocaleVariant);

		// device
		this.vj$.Environment.addProperty(this.vj$.Environment.DEVICE_NAME, this.vj$.Environment.getDeviceName);
		this.vj$.Environment.addProperty(this.vj$.Environment.DEVICE_TYPE, this.vj$.Environment.getDeviceType);
		this.vj$.Environment.addProperty(this.vj$.Environment.DEVICE_PIXEL_RATIO, this.vj$.Environment.getDevicePixelRatio);
		this.vj$.Environment.addProperty(this.vj$.Environment.DEVICE_TOUCH, this.vj$.Environment.getDeviceTouch);

		// runtime
		this.vj$.Environment.addProperty(this.vj$.Environment.RUNTIME_NAME, this.vj$.Environment.getRuntimeName);
	})

	.endType();

})