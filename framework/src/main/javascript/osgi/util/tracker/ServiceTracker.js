/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('osgi.util.tracker.ServiceTracker<S,T>') //< public
.needs(['org.eclipse.vjet.vjo.java.lang.IllegalArgumentException','org.eclipse.vjet.vjo.java.lang.NullPointerException',
    'org.eclipse.vjet.vjo.java.lang.RuntimeException','org.eclipse.vjet.vjo.java.lang.IllegalStateException',
    'org.eclipse.vjet.vjo.java.util.SortedMap','org.eclipse.vjet.vjo.java.util.TreeMap',
    'org.eclipse.vjet.vjo.java.util.Collections','vjo.java.lang.reflect.Array',
    'osgi.framework.BundleContext','osgi.framework.Filter',
    'osgi.framework.ServiceReference','osgi.framework.Constants',
    'osgi.framework.InvalidSyntaxException','org.eclipse.vjet.vjo.java.lang.System',
    'org.eclipse.vjet.vjo.java.lang.Integer','org.eclipse.vjet.vjo.java.lang.Long',
    'osgi.util.tracker.AbstractTracked','osgi.framework.ServiceListener',
    'osgi.framework.ServiceEvent','osgi.framework.AllServiceListener'])
.satisfies('osgi.util.tracker.ServiceTrackerCustomizer<S,T>')
.props({
    DEBUG:false //< final boolean
})
.protos({
    context:null, //< protected final BundleContext
    filter:null, //< protected final Filter
    customizer:null, //< final ServiceTrackerCustomizer<S,T> customizer
    listenerFilter:null, //< final String
    trackClass:null, //< private final String
    trackReference:null, //< private final ServiceReference<S> trackReference
    tracked:null, //< private ServiceTracker.Tracked
    cachedReference:null, //< private ServiceReference<S> cachedReference
    cachedService:null, //< private T
    Tracked:vjo.ctype() //< private
    .inherits('osgi.util.tracker.AbstractTracked<ServiceReference,T,ServiceEvent>')
    .satisfies('osgi.framework.ServiceListener')
    .protos({
        //> constructs()
        constructs:function(){
            this.base();
        },
        //> final public void serviceChanged(final ServiceEvent event)
        serviceChanged:function(event){
            if(this.closed){
                return;
            }
            var reference=event.getServiceReference(); //<<ServiceReference
            if(this.vj$.AbstractTracked.DEBUG){
                this.vj$.System.out.println("ServiceTracker.Tracked.serviceChanged["+event.getType()+"]: "+reference);
            }
            switch(event.getType()){
                case this.vj$.ServiceEvent.REGISTERED:
                case this.vj$.ServiceEvent.MODIFIED:
                    this.track(reference,event);
                    break;
                case this.vj$.ServiceEvent.MODIFIED_ENDMATCH:
                case this.vj$.ServiceEvent.UNREGISTERING:
                    this.untrack(reference,event);
                    break;
            }
        },
        //> final void modified()
        modified:function(){
            this.base.modified();
            this.vj$.outer.modified();
        },
        //> final T customizerAdding(final ServiceReference<S> item,final ServiceEvent related)
        customizerAdding:function(item,related){
            return this.vj$.outer.customizer.addingService(item);
        },
        //> final void customizerModified(final ServiceReference<S> item,final ServiceEvent related,final T object)
        customizerModified:function(item,related,object){
            this.vj$.outer.customizer.modifiedService(item,object);
        },
        //> final void customizerRemoved(final ServiceReference<S> item,final ServiceEvent related,final T object)
        customizerRemoved:function(item,related,object){
            this.vj$.outer.customizer.removedService(item,object);
        }
    })
    .endType(),
    AllTracked:vjo.ctype() //< private
    .inherits('osgi.util.tracker.ServiceTracker.Tracked')
    .satisfies('osgi.framework.AllServiceListener')
    .protos({
        //> constructs()
        constructs:function(){
            this.base();
        }
    })
    .endType(),
    //> public constructs()
    //> public constructs(final BundleContext context,final ServiceReference<S> reference,final ServiceTrackerCustomizer<S,T> customizer)
    //> public constructs(final BundleContext context,final String clazz,final ServiceTrackerCustomizer<S,T> customizer)
    //> public constructs(final BundleContext context,final Filter filter,final ServiceTrackerCustomizer<S,T> customizer)
    //> public constructs(final BundleContext context,final vjo.Class<S> clazz,final ServiceTrackerCustomizer<S,T> customizer)
    constructs:function(){
        if(arguments.length===3){
            if(osgi.framework.BundleContext.clazz.isInstance(arguments[0]) && osgi.framework.ServiceReference.clazz.isInstance(arguments[1]) && osgi.util.tracker.ServiceTrackerCustomizer.clazz.isInstance(arguments[2])){
                this.constructs_3_0_ServiceTracker_ovld(arguments[0],arguments[1],arguments[2]);
            }else if(osgi.framework.BundleContext.clazz.isInstance(arguments[0]) && (arguments[1] instanceof String || typeof arguments[1]=="string") && osgi.util.tracker.ServiceTrackerCustomizer.clazz.isInstance(arguments[2])){
                this.constructs_3_1_ServiceTracker_ovld(arguments[0],arguments[1],arguments[2]);
            }else if(osgi.framework.BundleContext.clazz.isInstance(arguments[0]) && osgi.framework.Filter.clazz.isInstance(arguments[1]) && osgi.util.tracker.ServiceTrackerCustomizer.clazz.isInstance(arguments[2])){
                this.constructs_3_2_ServiceTracker_ovld(arguments[0],arguments[1],arguments[2]);
            }else if(osgi.framework.BundleContext.clazz.isInstance(arguments[0]) && arguments[1] instanceof vjo.Class && osgi.util.tracker.ServiceTrackerCustomizer.clazz.isInstance(arguments[2])){
                this.constructs_3_3_ServiceTracker_ovld(arguments[0],arguments[1],arguments[2]);
            }
        }
    },
    //> private ServiceTracker.Tracked tracked()
    tracked:function(){
        return this.tracked;
    },
    //> protected constructs_3_0_ServiceTracker_ovld(final BundleContext context,final ServiceReference<S> reference,final ServiceTrackerCustomizer<S,T> customizer)
    constructs_3_0_ServiceTracker_ovld:function(context,reference,customizer){
        this.context=context;
        this.trackReference=reference;
        this.trackClass=null;
        this.customizer=(customizer===null)?this:customizer;
        this.listenerFilter="("+this.vj$.Constants.SERVICE_ID+"="+reference.getProperty(this.vj$.Constants.SERVICE_ID).toString()+")";
        try {
            this.filter=context.createFilter(this.listenerFilter);
        }
        catch(e){
            var iae=new this.vj$.IllegalArgumentException("unexpected InvalidSyntaxException: "+e.getMessage()); //<IllegalArgumentException
            iae.initCause(e);
            throw iae;
        }
    },
    //> protected constructs_3_1_ServiceTracker_ovld(final BundleContext context,final String clazz,final ServiceTrackerCustomizer<S,T> customizer)
    constructs_3_1_ServiceTracker_ovld:function(context,clazz,customizer){
        this.context=context;
        this.trackReference=null;
        this.trackClass=clazz;
        this.customizer=(customizer===null)?this:customizer;
        this.listenerFilter="("+this.vj$.Constants.OBJECTCLASS+"="+clazz+")";
        try {
            this.filter=context.createFilter(this.listenerFilter);
        }
        catch(e){
            var iae=new this.vj$.IllegalArgumentException("unexpected InvalidSyntaxException: "+e.getMessage()); //<IllegalArgumentException
            iae.initCause(e);
            throw iae;
        }
    },
    //> protected constructs_3_2_ServiceTracker_ovld(final BundleContext context,final Filter filter,final ServiceTrackerCustomizer<S,T> customizer)
    constructs_3_2_ServiceTracker_ovld:function(context,filter,customizer){
        this.context=context;
        this.trackReference=null;
        this.trackClass=null;
        this.listenerFilter=filter.toString();
        this.filter=filter;
        this.customizer=(customizer===null)?this:customizer;
        if((context===null)||(filter===null)){
            throw new this.vj$.NullPointerException();
        }
    },
    //> protected constructs_3_3_ServiceTracker_ovld(final BundleContext context,final vjo.Class<S> clazz,final ServiceTrackerCustomizer<S,T> customizer)
    constructs_3_3_ServiceTracker_ovld:function(context,clazz,customizer){
        this.constructs_3_1_ServiceTracker_ovld(context,clazz.getName(),customizer);
    },
    //> public void open()
    //> public void open(boolean trackAllServices)
    open:function(){
        if(arguments.length===0){
            this.open_0_0_ServiceTracker_ovld();
        }else if(arguments.length===1){
            this.open_1_0_ServiceTracker_ovld(arguments[0]);
        }
    },
    //> protected void open_0_0_ServiceTracker_ovld()
    open_0_0_ServiceTracker_ovld:function(){
        this.open(false);
    },
    //> protected void open_1_0_ServiceTracker_ovld(boolean trackAllServices)
    open_1_0_ServiceTracker_ovld:function(trackAllServices){
        var t; //<ServiceTracker.Tracked
        {
            if(this.tracked!==null){
                return;
            }
            if(this.vj$.ServiceTracker.DEBUG){
                this.vj$.System.out.println("ServiceTracker.open: "+this.filter);
            }
            t=trackAllServices?new this.AllTracked():new this.Tracked();
            {
                try {
                    this.context.addServiceListener(t,this.listenerFilter);
                    var references=null; //<ServiceReference[]
                    if(this.trackClass!==null){
                        references=this.getInitialReferences(trackAllServices,this.trackClass,null);
                    }else {
                        if(this.trackReference!==null){
                            if(this.trackReference.getBundle()!==null){
                                var single=[this.trackReference]; //<ServiceReference[]
                                references=single;
                            }
                        }else {
                            references=this.getInitialReferences(trackAllServices,null,this.listenerFilter);
                        }
                    }
                    t.setInitial(references);
                }
                catch(e){
                    throw new this.vj$.RuntimeException("unexpected InvalidSyntaxException: "+e.getMessage(),e);
                }
            }
            this.tracked=t;
        }
        t.trackInitial();
    },
    //> private ServiceReference[] getInitialReferences(boolean trackAllServices,String className,String filterString)
    getInitialReferences:function(trackAllServices,className,filterString){
        var result=((trackAllServices)?this.context.getAllServiceReferences(className,filterString):this.context.getServiceReferences(className,filterString)); //<<ServiceReference[]
        return result;
    },
    //> public void close()
    close:function(){
        var outgoing; //<ServiceTracker.Tracked
        var references; //<ServiceReference[]
        {
            outgoing=this.tracked;
            if(outgoing===null){
                return;
            }
            if(this.vj$.ServiceTracker.DEBUG){
                this.vj$.System.out.println("ServiceTracker.close: "+this.filter);
            }
            outgoing.close();
            references=this.getServiceReferences();
            this.tracked=null;
            try {
                this.context.removeServiceListener(outgoing);
            }
            catch(e){
            }
        }
        this.modified();
        {
            outgoing.notifyAll();
        }
        if(references!==null){
            for (var i=0;i<references.length;i++){
                outgoing.untrack(references[i],null);
            }
        }
        if(this.vj$.ServiceTracker.DEBUG){
            if((this.cachedReference===null)&&(this.cachedService===null)){
                this.vj$.System.out.println("ServiceTracker.close[cached cleared]: "+this.filter);
            }
        }
    },
    //> public T addingService(ServiceReference<S> reference)
    addingService:function(reference){
        var result=this.context.getService(reference); //<<T
        return result;
    },
    //> public void modifiedService(ServiceReference<S> reference,T service)
    modifiedService:function(reference,service){
    },
    //> public void removedService(ServiceReference<S> reference,T service)
    removedService:function(reference,service){
        this.context.ungetService(reference);
    },
    //> public T waitForService(long timeout)
    waitForService:function(timeout){
        if(timeout<0){
            throw new this.vj$.IllegalArgumentException("timeout value is negative");
        }
        var object=this.getService(); //<T
        if(object!==null){
            return object;
        }
        var endTime=(timeout===0)?0:(this.vj$.System.currentTimeMillis()+timeout); //<long
        do{
            var t=this.tracked(); //<ServiceTracker.Tracked
            if(t===null){
                return null;
            }
            {
                if(t.size()===0){
                    t.wait(timeout);
                }
            }
            object=this.getService();
            if(endTime>0){
                timeout=endTime-this.vj$.System.currentTimeMillis();
                if(timeout<=0){
                    break;
                }
            }
        }while(object===null);
        return object;
    },
    //> public ServiceReference[] getServiceReferences()
    getServiceReferences:function(){
        var t=this.tracked(); //<ServiceTracker.Tracked
        if(t===null){
            return null;
        }
        {
            var length=t.size(); //<int
            if(length===0){
                return null;
            }
            var result=vjo.createArray(null, length); //<ServiceReference[]
            return t.copyKeys(result);
        }
    },
    //> public ServiceReference<S> getServiceReference()
    getServiceReference:function(){
        var reference=this.cachedReference; //<ServiceReference
        if(reference!==null){
            if(this.vj$.ServiceTracker.DEBUG){
                this.vj$.System.out.println("ServiceTracker.getServiceReference[cached]: "+this.filter);
            }
            return reference;
        }
        if(this.vj$.ServiceTracker.DEBUG){
            this.vj$.System.out.println("ServiceTracker.getServiceReference: "+this.filter);
        }
        var references=this.getServiceReferences(); //<ServiceReference[]
        var length=(references===null)?0:references.length; //<int
        if(length===0){
            return null;
        }
        var index=0; //<int
        if(length>1){
            var rankings=vjo.createArray(0, length); //<int[]
            var count=0; //<int
            var maxRanking=this.vj$.Integer.MIN_VALUE; //<int
            for (var i=0;i<length;i++){
                var property=references[i].getProperty(this.vj$.Constants.SERVICE_RANKING);
                var ranking=(property instanceof this.vj$.Integer)?property.intValue():0; //<int
                rankings[i]=ranking;
                if(ranking>maxRanking){
                    index=i;
                    maxRanking=ranking;
                    count=1;
                }else {
                    if(ranking===maxRanking){
                        count++;
                    }
                }
            }
            if(count>1){
                var minId=this.vj$.Long.MAX_VALUE; //<long
                for (var i=0;i<length;i++){
                    if(rankings[i]===maxRanking){
                        var id=((references[i].getProperty(this.vj$.Constants.SERVICE_ID))).longValue(); //<long
                        if(id<minId){
                            index=i;
                            minId=id;
                        }
                    }
                }
            }
        }
        return this.cachedReference=references[index];
    },
    //> public T getService(ServiceReference<S> reference)
    //> public T getService()
    getService:function(reference){
        if(arguments.length===1){
            return this.getService_1_0_ServiceTracker_ovld(arguments[0]);
        }else if(arguments.length===0){
            return this.getService_0_0_ServiceTracker_ovld();
        }
    },
    //> protected T getService_1_0_ServiceTracker_ovld(ServiceReference<S> reference)
    getService_1_0_ServiceTracker_ovld:function(reference){
        var t=this.tracked(); //<ServiceTracker.Tracked
        if(t===null){
            return null;
        }
        {
            return t.getCustomizedObject(reference);
        }
    },
    //> protected T getService_0_0_ServiceTracker_ovld()
    getService_0_0_ServiceTracker_ovld:function(){
        var service=this.cachedService; //<T
        if(service!==null){
            if(this.vj$.ServiceTracker.DEBUG){
                this.vj$.System.out.println("ServiceTracker.getService[cached]: "+this.filter);
            }
            return service;
        }
        if(this.vj$.ServiceTracker.DEBUG){
            this.vj$.System.out.println("ServiceTracker.getService: "+this.filter);
        }
        var reference=this.getServiceReference(); //<ServiceReference
        if(reference===null){
            return null;
        }
        return this.cachedService=this.getService(reference);
    },
    //> public void remove(ServiceReference<S> reference)
    remove:function(reference){
        var t=this.tracked(); //<ServiceTracker.Tracked
        if(t===null){
            return;
        }
        t.untrack(reference,null);
    },
    //> public int size()
    size:function(){
        var t=this.tracked(); //<ServiceTracker.Tracked
        if(t===null){
            return 0;
        }
        {
            return t.size();
        }
    },
    //> public int getTrackingCount()
    getTrackingCount:function(){
        var t=this.tracked(); //<ServiceTracker.Tracked
        if(t===null){
            return -1;
        }
        {
            return t.getTrackingCount();
        }
    },
    //> void modified()
    modified:function(){
        this.cachedReference=null;
        this.cachedService=null;
        if(this.vj$.ServiceTracker.DEBUG){
            this.vj$.System.out.println("ServiceTracker.modified: "+this.filter);
        }
    },
    //> public SortedMap<ServiceReference<S>,T> getTracked()
    getTracked:function(){
        var map=new this.vj$.TreeMap(this.vj$.Collections.reverseOrder()); //<SortedMap
        var t=this.tracked(); //<ServiceTracker.Tracked
        if(t===null){
            return map;
        }
        {
            return t.copyEntries(map);
        }
    },
    //> public boolean isEmpty()
    isEmpty:function(){
        var t=this.tracked(); //<ServiceTracker.Tracked
        if(t===null){
            return true;
        }
        {
            return t.isEmpty();
        }
    },
    //> public Object[] getServices()
    //> public T[] getServices(T[] array)
    getServices:function(){
        if(arguments.length===0){
            return this.getServices_0_0_ServiceTracker_ovld();
        }else if(arguments.length===1){
            return this.getServices_1_0_ServiceTracker_ovld(arguments[0]);
        }
    },
    //> protected Object[] getServices_0_0_ServiceTracker_ovld()
    getServices_0_0_ServiceTracker_ovld:function(){
        var t=this.tracked(); //<ServiceTracker.Tracked
        if(t===null){
            return null;
        }
        {
            var references=this.getServiceReferences(); //<ServiceReference[]
            var length=(references===null)?0:references.length; //<int
            if(length===0){
                return null;
            }
            var objects=vjo.createArray(null, length); //<Object[]
            for (var i=0;i<length;i++){
                objects[i]=this.getService(references[i]);
            }
            return objects;
        }
    },
    //> protected T[] getServices_1_0_ServiceTracker_ovld(T[] array)
    getServices_1_0_ServiceTracker_ovld:function(array){
        var t=this.tracked(); //<ServiceTracker.Tracked
        if(t===null){
            if(array.length>0){
                array[0]=null;
            }
            return array;
        }
        {
            var references=this.getServiceReferences(); //<ServiceReference[]
            var length=(references===null)?0:references.length; //<int
            if(length===0){
                if(array.length>0){
                    array[0]=null;
                }
                return array;
            }
            if(length>array.length){
                array=this.vj$.Array.newInstance(array.getClass().getComponentType(),length); //<<T[]
            }
            for (var i=0;i<length;i++){
                array[i]=this.getService(references[i]);
            }
            if(array.length>length){
                array[length]=null;
            }
            return array;
        }
    }
})
.endType();