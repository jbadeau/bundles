/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('osgi.util.tracker.BundleTracker<T>') //< public
.needs(['org.eclipse.vjet.vjo.java.util.HashMap','org.eclipse.vjet.vjo.java.lang.System',
    'osgi.util.tracker.AbstractTracked','osgi.framework.SynchronousBundleListener'])
//> needs org.eclipse.vjet.vjo.java.lang.IllegalStateException,org.eclipse.vjet.vjo.java.util.Map,osgi.framework.BundleContext,osgi.framework.Bundle,osgi.framework.BundleEvent
.satisfies('osgi.util.tracker.BundleTrackerCustomizer<T>')
.props({
    DEBUG:false //< final boolean
})
.protos({
    context:null, //< protected final BundleContext
    customizer:null, //< final BundleTrackerCustomizer<T> customizer
    tracked:null, //< private BundleTracker.Tracked
    mask:0, //< final int
    Tracked:vjo.ctype() //< private final
    .inherits('osgi.util.tracker.AbstractTracked<Bundle,T,BundleEvent>')
    .satisfies('osgi.framework.SynchronousBundleListener')
    .protos({
        //> constructs()
        constructs:function(){
            this.base();
        },
        //> public void bundleChanged(final BundleEvent event)
        bundleChanged:function(event){
            if(this.closed){
                return;
            }
            var bundle=event.getBundle(); //<Bundle
            var state=bundle.getState(); //<int
            if(this.vj$.AbstractTracked.DEBUG){
                this.vj$.System.out.println("BundleTracker.Tracked.bundleChanged["+state+"]: "+bundle);
            }
            if((state&this.vj$.outer.mask)!==0){
                this.track(bundle,event);
            }else {
                this.untrack(bundle,event);
            }
        },
        //> T customizerAdding(final Bundle item,final BundleEvent related)
        customizerAdding:function(item,related){
            return this.vj$.outer.customizer.addingBundle(item,related);
        },
        //> void customizerModified(final Bundle item,final BundleEvent related,final T object)
        customizerModified:function(item,related,object){
            this.vj$.outer.customizer.modifiedBundle(item,related,object);
        },
        //> void customizerRemoved(final Bundle item,final BundleEvent related,final T object)
        customizerRemoved:function(item,related,object){
            this.vj$.outer.customizer.removedBundle(item,related,object);
        }
    })
    .endType(),
    //> public constructs(BundleContext context,int stateMask,BundleTrackerCustomizer<T> customizer)
    constructs:function(context,stateMask,customizer){
        this.context=context;
        this.mask=stateMask;
        this.customizer=(customizer===null)?this:customizer;
    },
    //> private BundleTracker.Tracked tracked()
    tracked:function(){
        return this.tracked;
    },
    //> public void open()
    open:function(){
        var t; //<BundleTracker.Tracked
        {
            if(this.tracked!==null){
                return;
            }
            if(this.vj$.BundleTracker.DEBUG){
                this.vj$.System.out.println("BundleTracker.open");
            }
            t=new this.Tracked();
            {
                this.context.addBundleListener(t);
                var bundles=this.context.getBundles(); //<Bundle[]
                if(bundles!==null){
                    var length=bundles.length; //<int
                    for (var i=0;i<length;i++){
                        var state=bundles[i].getState(); //<int
                        if((state&this.mask)===0){
                            bundles[i]=null;
                        }
                    }
                    t.setInitial(bundles);
                }
            }
            this.tracked=t;
        }
        t.trackInitial();
    },
    //> public void close()
    close:function(){
        var bundles; //<Bundle[]
        var outgoing; //<BundleTracker.Tracked
        {
            outgoing=this.tracked;
            if(outgoing===null){
                return;
            }
            if(this.vj$.BundleTracker.DEBUG){
                this.vj$.System.out.println("BundleTracker.close");
            }
            outgoing.close();
            bundles=this.getBundles();
            this.tracked=null;
            try {
                this.context.removeBundleListener(outgoing);
            }
            catch(e){
            }
        }
        if(bundles!==null){
            for (var i=0;i<bundles.length;i++){
                outgoing.untrack(bundles[i],null);
            }
        }
    },
    //> public T addingBundle(Bundle bundle,BundleEvent event)
    addingBundle:function(bundle,event){
        var result=bundle; //<<T
        return result;
    },
    //> public void modifiedBundle(Bundle bundle,BundleEvent event,T object)
    modifiedBundle:function(bundle,event,object){
    },
    //> public void removedBundle(Bundle bundle,BundleEvent event,T object)
    removedBundle:function(bundle,event,object){
    },
    //> public Bundle[] getBundles()
    getBundles:function(){
        var t=this.tracked(); //<BundleTracker.Tracked
        if(t===null){
            return null;
        }
        {
            var length=t.size(); //<int
            if(length===0){
                return null;
            }
            return t.copyKeys(vjo.createArray(null, length));
        }
    },
    //> public T getObject(Bundle bundle)
    getObject:function(bundle){
        var t=this.tracked(); //<BundleTracker.Tracked
        if(t===null){
            return null;
        }
        {
            return t.getCustomizedObject(bundle);
        }
    },
    //> public void remove(Bundle bundle)
    remove:function(bundle){
        var t=this.tracked(); //<BundleTracker.Tracked
        if(t===null){
            return;
        }
        t.untrack(bundle,null);
    },
    //> public int size()
    size:function(){
        var t=this.tracked(); //<BundleTracker.Tracked
        if(t===null){
            return 0;
        }
        {
            return t.size();
        }
    },
    //> public int getTrackingCount()
    getTrackingCount:function(){
        var t=this.tracked(); //<BundleTracker.Tracked
        if(t===null){
            return -1;
        }
        {
            return t.getTrackingCount();
        }
    },
    //> public Map<Bundle,T> getTracked()
    getTracked:function(){
        var map=new this.vj$.HashMap(); //<Map
        var t=this.tracked(); //<BundleTracker.Tracked
        if(t===null){
            return map;
        }
        {
            return t.copyEntries(map);
        }
    },
    //> public boolean isEmpty()
    isEmpty:function(){
        var t=this.tracked(); //<BundleTracker.Tracked
        if(t===null){
            return true;
        }
        {
            return t.isEmpty();
        }
    }
})
.endType();