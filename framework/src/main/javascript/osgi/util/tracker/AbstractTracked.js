/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('osgi.util.tracker.AbstractTracked<S,T,R>') //< abstract
.needs(['org.eclipse.vjet.vjo.java.util.LinkedList','org.eclipse.vjet.vjo.java.util.HashMap',
    'org.eclipse.vjet.vjo.java.util.ArrayList','org.eclipse.vjet.vjo.java.lang.System'])
//> needs org.eclipse.vjet.vjo.java.util.Map,org.eclipse.vjet.vjo.java.util.List
.props({
    DEBUG:false //< final boolean
})
.protos({
    tracked:null, //< private final Map<S,T> tracked
    trackingCount:0, //< private int
    adding:null, //< private final List<S> adding
    closed:false, //< boolean
    initial:null, //< private final LinkedList<S> initial
    //> constructs()
    constructs:function(){
        this.tracked=new this.vj$.HashMap();
        this.trackingCount=0;
        this.adding=new this.vj$.ArrayList(6);
        this.initial=new this.vj$.LinkedList();
        this.closed=false;
    },
    //> void setInitial(S[] list)
    setInitial:function(list){
        if(list===null){
            return;
        }
        for (var item,_$i0=0;_$i0<list.length;_$i0++){
            item=list[_$i0];
            if(item===null){
                continue;
            }
            if(this.vj$.AbstractTracked.DEBUG){
                this.vj$.System.out.println("AbstractTracked.setInitial: "+item);
            }
            this.initial.add(item);
        }
    },
    //> void trackInitial()
    trackInitial:function(){
        while(true){
            var item; //<S
            {
                if(this.closed||(this.initial.size()===0)){
                    return;
                }
                item=this.initial.removeFirst();
                if(this.tracked.get(item)!==null){
                    if(this.vj$.AbstractTracked.DEBUG){
                        this.vj$.System.out.println("AbstractTracked.trackInitial[already tracked]: "+item);
                    }
                    continue;
                }
                if(this.adding.contains(item)){
                    if(this.vj$.AbstractTracked.DEBUG){
                        this.vj$.System.out.println("AbstractTracked.trackInitial[already adding]: "+item);
                    }
                    continue;
                }
                this.adding.add(item);
            }
            if(this.vj$.AbstractTracked.DEBUG){
                this.vj$.System.out.println("AbstractTracked.trackInitial: "+item);
            }
            this.trackAdding(item,null);
        }
    },
    //> void close()
    close:function(){
        this.closed=true;
    },
    //> void track(final S item,final R related)
    track:function(item,related){
        var object; //<T
        {
            if(this.closed){
                return;
            }
            object=this.tracked.get(item);
            if(object===null){
                if(this.adding.contains(item)){
                    if(this.vj$.AbstractTracked.DEBUG){
                        this.vj$.System.out.println("AbstractTracked.track[already adding]: "+item);
                    }
                    return;
                }
                this.adding.add(item);
            }else {
                if(this.vj$.AbstractTracked.DEBUG){
                    this.vj$.System.out.println("AbstractTracked.track[modified]: "+item);
                }
                this.modified();
            }
        }
        if(object===null){
            this.trackAdding(item,related);
        }else {
            this.customizerModified(item,related,object);
        }
    },
    //> private void trackAdding(final S item,final R related)
    trackAdding:function(item,related){
        if(this.vj$.AbstractTracked.DEBUG){
            this.vj$.System.out.println("AbstractTracked.trackAdding: "+item);
        }
        var object=null; //<T
        var becameUntracked=false; //<boolean
        try {
            object=this.customizerAdding(item,related);
        }
        finally {
            {
                if(this.adding.remove(item)&& !this.closed){
                    if(object!==null){
                        this.tracked.put(item,object);
                        this.modified();
                        notifyAll();
                    }
                }else {
                    becameUntracked=true;
                }
            }
        }
        if(becameUntracked&&(object!==null)){
            if(this.vj$.AbstractTracked.DEBUG){
                this.vj$.System.out.println("AbstractTracked.trackAdding[removed]: "+item);
            }
            this.customizerRemoved(item,related,object);
        }
    },
    //> void untrack(final S item,final R related)
    untrack:function(item,related){
        var object; //<T
        {
            if(this.initial.remove(item)){
                if(this.vj$.AbstractTracked.DEBUG){
                    this.vj$.System.out.println("AbstractTracked.untrack[removed from initial]: "+item);
                }
                return;
            }
            if(this.adding.remove(item)){
                if(this.vj$.AbstractTracked.DEBUG){
                    this.vj$.System.out.println("AbstractTracked.untrack[being added]: "+item);
                }
                return;
            }
            object=this.tracked.remove(item);
            if(object===null){
                return;
            }
            this.modified();
        }
        if(this.vj$.AbstractTracked.DEBUG){
            this.vj$.System.out.println("AbstractTracked.untrack[removed]: "+item);
        }
        this.customizerRemoved(item,related,object);
    },
    //> int size()
    size:function(){
        return this.tracked.size();
    },
    //> boolean isEmpty()
    isEmpty:function(){
        return this.tracked.isEmpty();
    },
    //> T getCustomizedObject(final S item)
    getCustomizedObject:function(item){
        return this.tracked.get(item);
    },
    //> S[] copyKeys(final S[] list)
    copyKeys:function(list){
        return this.tracked.keySet().toArray(list);
    },
    //> void modified()
    modified:function(){
        this.trackingCount++;
    },
    //> int getTrackingCount()
    getTrackingCount:function(){
        return this.trackingCount;
    },
    //> <M extends Map<? super S,? super T>> M copyEntries(final M map)
    copyEntries:function(map){
        map.putAll(this.tracked);
        return map;
    },
    //> abstract T customizerAdding(final S item,final R related)
    customizerAdding:function(item,related){
    },
    //> abstract void customizerModified(final S item,final R related,final T object)
    customizerModified:function(item,related,object){
    },
    //> abstract void customizerRemoved(final S item,final R related,final T object)
    customizerRemoved:function(item,related,object){
    }
})
.endType();