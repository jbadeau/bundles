/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.itype('osgi.util.tracker.BundleTrackerCustomizer<T>') //< public
//> needs osgi.framework.Bundle,osgi.framework.BundleEvent
.protos({
    //> public T addingBundle(Bundle bundle,BundleEvent event)
    addingBundle:function(bundle,event){
    },
    //> public void modifiedBundle(Bundle bundle,BundleEvent event,T object)
    modifiedBundle:function(bundle,event,object){
    },
    //> public void removedBundle(Bundle bundle,BundleEvent event,T object)
    removedBundle:function(bundle,event,object){
    }
})
.endType();