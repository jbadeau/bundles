/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.itype('osgi.util.tracker.ServiceTrackerCustomizer<S,T>') //< public
//> needs osgi.framework.ServiceReference
.protos({
    //> public T addingService(ServiceReference<S> reference)
    addingService:function(reference){
    },
    //> public void modifiedService(ServiceReference<S> reference,T service)
    modifiedService:function(reference,service){
    },
    //> public void removedService(ServiceReference<S> reference,T service)
    removedService:function(reference,service){
    }
})
.endType();