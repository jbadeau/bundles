define([ "vjo", "./IllegalArgumentException" ], function(vjo, IllegalArgumentException) {

	return vjo.ctype("osgi.lang.EventObject")

	.protos({

		source : null,

		constructs : function(source) {
			if (source === null) {
				throw new IllegalArgumentException("null source");
			}
			this.source = source;
		},

		getSource : function() {
			return this.source;
		},

		toString : function() {
			return this.getClass().getName() + "[source=" + this.source + "]";
		}

	})

	.endType();

});