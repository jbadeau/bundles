define([ "vjo", "./Exception" ], function(vjo, Exception) {

	return vjo.ctype("osgi.lang.IllegalArgumentException")

	.inherits(Exception)

	.protos({

		constructs : function() {
			if (arguments.length === 0) {
				this.constructs_0_0_IllegalArgumentException_ovld();
			}
			else if (arguments.length === 1) {
				if (arguments[0] instanceof String || typeof arguments[0] == "string") {
					this.constructs_1_0_IllegalArgumentException_ovld(arguments[0]);
				}
				else if (Throwable.clazz.isInstance(arguments[0])) {
					this.constructs_1_1_IllegalArgumentException_ovld(arguments[0]);
				}
			}
			else if (arguments.length === 2) {
				this.constructs_2_0_IllegalArgumentException_ovld(arguments[0], arguments[1]);
			}
		},

		constructs_0_0_IllegalArgumentException_ovld : function() {
			this.base();
		},

		constructs_1_0_IllegalArgumentException_ovld : function(s) {
			this.base(s);
		},

		constructs_2_0_IllegalArgumentException_ovld : function(message, cause) {
			this.base(message, cause);
		},

		constructs_1_1_IllegalArgumentException_ovld : function(cause) {
			this.base(cause);
		}

	})

	.endType();

});
