define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.lang.Enumeration")

	.protos({

		hasMoreElements : function() {
		},

		nextElement : function() {
		}

	})

	.endType();

});
