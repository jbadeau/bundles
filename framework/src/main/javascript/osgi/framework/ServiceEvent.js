define([ "vjo", "./EventObject" ], function(vjo, EventObject) {

	return vjo.ctype("osgi.framework.ServiceEvent")

	.inherits(EventObject)

	.props({

		REGISTERED : 0x00000001,

		MODIFIED : 0x00000002,

		UNREGISTERING : 0x00000004,

		MODIFIED_ENDMATCH : 0x00000008

	})

	.protos({

		reference : null,

		type : 0,

		constructs : function(type, reference) {
			this.base(reference);
			this.reference = reference;
			this.type = type;
		},

		getServiceReference : function() {
			return this.reference;
		},

		getType : function() {
			return this.type;
		}

	})

	.endType();

})