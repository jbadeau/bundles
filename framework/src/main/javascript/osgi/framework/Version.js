define([ "vjo" ], function(vjo) {

	/**
	 * Version identifier for capabilities such as bundles and packages.
	 * 
	 * <p>
	 * Version identifiers have four components.
	 * <ol>
	 * <li>Major version. A non-negative integer.</li>
	 * <li>Minor version. A non-negative integer.</li>
	 * <li>Micro version. A non-negative integer.</li>
	 * <li>Qualifier. A text string. See
	 * {@linkcode module:osgi/framework/Version} for the format of the qualifier
	 * string.</li>
	 * </ol>
	 * 
	 * <p>
	 * {@linkcode module:osgi/framework/Version} objects are immutable.
	 * 
	 * @author Jose Badeau
	 * @exports osgi/framework/Version
	 * @Immutable
	 */
	return vjo.ctype("osgi.framework.Version")

	.props({

		SEPARATOR : ".",

		/**
		 * The empty version "0.0.0".
		 * 
		 * @memberof module:osgi/framework/Version
		 */
		emptyVersion : null,

		/**
		 * Parse numeric component into an int.
		 * 
		 * @memberof module:osgi/framework/Version
		 * @param {string}
		 *            value Numeric component.
		 * @param {string}
		 *            version Complete version string for exception message, if
		 *            any.
		 * @return {number} int value of numeric component.
		 */
		parseInt : function(value, version) {
			var int = parseInt(value);
			if (isNaN(int)) {
				throw new Error("invalid version '" + version + "': non-numeric '" + value + "'");
			}
			return int;
		},

		/**
		 * 
		 * Parses a version identifier from the specified string.
		 * 
		 * <p>
		 * See {@linkcode module:osgi/framework/Version(string)} for the format
		 * of the version string.
		 * 
		 * @memberof module:osgi/framework/Version
		 * @param {string}
		 *            version String representation of the version identifier.
		 *            Leading and trailing whitespace will be ignored.
		 * @return {module:osgi/framework/Version} A
		 *         {@linkcode module:osgi/framework/Version} object representing
		 *         the version identifier. If
		 *         {@linkcode module:osgi/framework/Version} is {@linkcode null}
		 *         or the empty string then
		 *         {@linkcode module:osgi/framework/Version.emptyVersion} will
		 *         be returned.
		 * @throws IllegalArgumentError
		 *             If {@linkcode version} is improperly formatted.
		 */
		parseVersion : function(version) {
			if (version == null) {
				return this.vj$.Version.emptyVersion;
			}
			version = version.trim();
			if (version.length == 0) {
				return this.vj$.Version.emptyVersion;
			}
			return new this.vj$.Version(version);
		}

	})

	.protos({

		major : 0,

		minor : 0,

		micro : 0,

		qualifier : null,

		versionString : null,

		hash : 0,

		/**
		 * Creates a version identifier from the specified string or from the
		 * specified numerical components or from the specified components.
		 * 
		 * <p>
		 * Version string grammar:
		 * 
		 * <pre>
		 * version ::= major('.'minor('.'micro('.'qualifier)?)?)?
		 * major ::= digit+
		 * minor ::= digit+
		 * micro ::= digit+
		 * qualifier ::= (alpha|digit|'_'|'-')+
		 * digit ::= [0..9]
		 * alpha ::= [a..zA..Z]
		 * </pre>
		 * 
		 * <p>
		 * Version construction examples:
		 * 
		 * <pre>
		 * new Version('1.2.1.SNAPSHOT')
		 * new Version('1', '2', '1')
		 * new Version('1', '2', '1', 'SNAPSHOT')
		 * </pre>
		 * 
		 * @constructs module:osgi/framework/Version
		 * @param {string}
		 *            versionORmajor String representation of the version
		 *            identifier or major component of the version identifier.
		 *            There must be no whitespace in the argument.
		 * @param {string}
		 *            minor Minor component of the version identifier.
		 * @param {string}
		 *            micro Micro component of the version identifier.
		 * @param {string}
		 *            qualifier Qualifier component of the version identifier.
		 *            If {@linkcode null} is specified, then the qualifier will
		 *            be set to the empty string.
		 * 
		 * @throws IllegalArgumentError
		 *             If {@linkcode version$major} is improperly formatted or
		 *             if either {@linkcode version$major}, {@linkcode minor}
		 *             or {@linkcode micro} are negative or if
		 *             {@linkcode qualifier} is invalid.
		 */
		constructs : function(versionORmajor, minor, micro, qualifier) {
			if (arguments.length === 1) {
				return this.constructs_1_0(versionORmajor);
			}
			else if (arguments.length === 3) {
				return this.constructs_3_0(versionORmajor, minor, micro);
			}
			else if (arguments.length === 4) {
				return this.constructs_4_0(versionORmajor, minor, micro, qualifier);
			}
		},

		constructs_1_0 : function(version) {
			var major, minor, micro, qualifier;

			try {
				if (version === null)
					this.vj$.Version.emptyVersion;

				if (typeof version !== "string") {
					throw new Error("invalid version '" + version + "': invalid format");
				}

				version = version.replace(/^\s+|\s+$/g, '');
				if (version.length === 0 || version === "0.0.0") {
					return this.vj$.Version.emptyVersion;
				}

				var tokens = version.split(".");
				if (tokens.length > 4) {
					throw new Error("invalid version '" + version + "': invalid format");
				}

				if (!/[\d]+/.test(tokens[0])) {
					throw new Error("invalid version '" + version + "': invalid format");
				}

				major = this.vj$.Version.parseInt(tokens[0]);
				minor = 0;
				micro = 0;
				qualifier = "";

				if (tokens.length > 1) {
					if (!/[\d]+/.test(tokens[1])) {
						throw new Error("invalid version '" + version + "': invalid format");
					}
					minor = this.vj$.Version.parseInt(tokens[1]);

					if (tokens.length > 2) {
						if (!/[\d]+/.test(tokens[2])) {
							throw new Error("invalid version '" + version + "': invalid format");
						}
						micro = this.vj$.Version.parseInt(tokens[2]);

						if (tokens.length > 3) {
							qualifier = tokens[3];
						}
					}
				}
			}
			catch (error) {
				throw new Error("invalid version '" + version + "': invalid format: " + error.message);
			}

			this.constructs_4_0(major, minor, micro, qualifier);
		},

		constructs_3_0 : function(major, minor, micro) {
			this.constructs_4_0(major, minor, micro, null);
		},

		constructs_4_0 : function(major, minor, micro, qualifier) {
			var version = major + this.vj$.Version.SEPARATOR + minor + this.vj$.Version.SEPARATOR + micro;
			if (qualifier && qualifier != null) {
				version = version + this.vj$.Version.SEPARATOR + qualifier;
			}
			this.major = this.vj$.Version.parseInt(major, version);
			this.minor = this.vj$.Version.parseInt(minor, version);
			this.micro = this.vj$.Version.parseInt(micro, version);
			this.qualifier = (qualifier && qualifier != null) ? qualifier : "";
			this.validate();
		},

		/**
		 * Called by the Version constructors to validate the version
		 * components.
		 * 
		 * @memberof module:osgi/framework/Version.prototype
		 * @throws IllegalArgumentError
		 *             If the numerical components are negative or the qualifier
		 *             string is invalid.
		 */
		validate : function() {
			if (this.major < 0) {
				throw new Error("invalid version '" + this.toString0() + "': negative number '" + this.major + "'");
			}
			if (this.minor < 0) {
				throw new Error("invalid version '" + this.toString0() + "': negative number '" + this.minor + "'");
			}
			if (this.micro < 0) {
				throw new Error("invalid version '" + this.toString0() + "': negative number '" + this.micro + "'");
			}
			this.qualifier.split("").forEach(function(ch) {
				if (("A" <= ch) && (ch <= "Z")) {
					return;
				}
				if (("a" <= ch) && (ch <= "z")) {
					return;
				}
				if (("0" <= ch) && (ch <= "9")) {
					return;
				}
				if ((ch == "_") || (ch == "-")) {
					return;
				}
				throw new Error("invalid version '" + toString0() + "': invalid qualifier '" + qualifier + "'");
			});
		},

		/**
		 * Returns the major component of this version identifier.
		 * 
		 * @memberof module:osgi/framework/Version.prototype
		 * @return {number} The major component.
		 */
		getMajor : function() {
			return this.major;
		},

		/**
		 * Returns the minor component of this version identifier.
		 * 
		 * @memberof module:osgi/framework/Version.prototype
		 * @return {number} The minor component.
		 */
		getMinor : function() {
			return this.minor;
		},

		/**
		 * Returns the micro component of this version identifier.
		 * 
		 * @memberof module:osgi/framework/Version.prototype
		 * @return {number} The micro component.
		 */
		getMicro : function() {
			return this.micro;
		},

		/**
		 * Returns the qualifier component of this version identifier.
		 * 
		 * @memberof module:osgi/framework/Version.prototype
		 * @return {string} The qualifier component.
		 */
		getQualifier : function() {
			return this.qualifier;
		},

		/**
		 * Returns the string representation of this version identifier.
		 * 
		 * <p>
		 * The format of the version string will be {@code major.minor.micro} if
		 * qualifier is the empty string or {@code major.minor.micro.qualifier}
		 * otherwise.
		 * 
		 * @memberof module:osgi/framework/Version.prototype
		 * @return {string} The string representation of this version
		 *         identifier.
		 */
		toString : function() {
			return this.toString0();
		},

		/**
		 * Internal toString behavior
		 * 
		 * @memberof module:osgi/framework/Version.prototype
		 * @return {string} The string representation of this version
		 *         identifier.
		 */
		toString0 : function() {
			if (this.versionString != null) {
				return this.versionString;
			}
			var q = this.qualifier.length;
			var result = [];
			result.push(this.major);
			result.push(this.vj$.Version.SEPARATOR);
			result.push(this.minor);
			result.push(this.vj$.Version.SEPARATOR);
			result.push(this.micro);
			if (q > 0) {
				result.push(this.vj$.Version.SEPARATOR);
				result.push(this.qualifier);
			}
			return this.versionString = result.join("");
		},

		/**
		 * Returns a hash code value for the object.
		 * 
		 * @memberof module:osgi/framework/Version.prototype
		 * @return {number} An integer which is a hash code value for this
		 *         object.
		 */
		hashCode : function() {
			throw new Error("the requested operation is not supported");
		},

		/**
		 * Compares this {@code module:osgi/framework/Version} object to another
		 * object.
		 * 
		 * <p>
		 * A version is considered to be <b>equal to </b> another version if the
		 * major, minor and micro components are equal and the qualifier
		 * component is equal (using {@code string.equals}).
		 * 
		 * @memberof module:osgi/framework/Version.prototype
		 * @param {*}
		 *            object The {@code module:osgi/framework/Version} object to
		 *            be compared.
		 * @return {boolean} {@code true} if {@code object} is a {@code Version}
		 *         and is equal to this object; {@code false} otherwise.
		 */
		equals : function(object) {
			return object instanceof this.vj$.Version && this.major === object.major && this.minor === object.minor && this.micro === object.micro && this.qualifier === object.qualifier;
		},

		/**
		 * Compares this {@code Version} object to another {@code Version}.
		 * 
		 * <p>
		 * A version is considered to be <b>less than</b> another version if
		 * its major component is less than the other version's major component,
		 * or the major components are equal and its minor component is less
		 * than the other version's minor component, or the major and minor
		 * components are equal and its micro component is less than the other
		 * version's micro component, or the major, minor and micro components
		 * are equal and it's qualifier component is less than the other
		 * version's qualifier component (using {@code string.compareTo}).
		 * 
		 * <p>
		 * A version is considered to be <b>equal to</b> another version if the
		 * major, minor and micro components are equal and the qualifier
		 * component is equal (using {@linkcode string.compareTo}).
		 * 
		 * @memberof module:osgi/framework/Version.prototype
		 * @param {module:osgi/framework/Version}
		 *            other The {@code Version} object to be compared.
		 * @return {number} A negative integer, zero, or a positive integer if
		 *         this version is less than, equal to, or greater than the
		 *         specified {@code Version} object.
		 * @throws ClassCastError
		 *             If the specified object is not a {@code Version} object.
		 */
		compareTo : function(other) {
			if (this === other) {
				return 0;
			}
			var result = this.major - other.major;
			if (result !== 0) {
				return result;
			}
			result = this.minor - other.minor;
			if (result !== 0) {
				return result;
			}

			result = this.micro - other.micro;
			if (result !== 0) {
				return result;
			}

			if (this.qualifier === other.qualifier) {
				return 0;
			}

			return this.qualifier > other.qualifier ? 1 : -1;
		}

	})

	.inits(function() {
		this.vj$.Version.emptyVersion = new this(0, 0, 0);
	})

	.endType();

})