define([ "vjo", "../BundleReference" ], function(vjo, BundleReference) {

	return vjo.itype("osgi.framework.startlevel.FrameworkStartLevel")

	.inherits(BundleReference)
	
	.protos({

		getStartLevel : function() {
		},

		setStartLevel : function(startlevel) {
			var listeners;
			if (arguments.length == 2 && arguments[1] instanceof Array) {
				listeners = arguments[1];
			}
			else {
				listeners = [];
				for (var i = 1; i < arguments.length; i++) {
					listeners[i - 1] = arguments[i];
				}
			}
		},

		getInitialBundleStartLevel : function() {
		},

		setInitialBundleStartLevel : function(startlevel) {
		}

	})

	.endType();

})