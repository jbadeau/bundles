define([ "vjo", "../BundleReference" ], function(vjo, BundleReference) {

	return vjo.itype("osgi.framework.startlevel.BundleStartLevel")

	.inherits(BundleReference).protos({

		getStartLevel : function() {
		},

		setStartLevel : function(startlevel) {
		},

		isPersistentlyStarted : function() {
		},

		isActivationPolicyUsed : function() {
		}

	})

	.endType();

})