define([ "vjo", "./EventObject" ], function(vjo, EventObject) {

	return vjo.ctype("osgi.framework.FrameworkEvent")

	.inherits(EventObject)

	.props({

		STARTED : 0x00000001,

		ERROR : 0x00000002,

		PACKAGES_REFRESHED : 0x00000004,

		STARTLEVEL_CHANGED : 0x00000008,

		WARNING : 0x00000010,

		INFO : 0x00000020,

		STOPPED : 0x00000040,

		STOPPED_UPDATE : 0x00000080,

		STOPPED_BOOTCLASSPATH_MODIFIED : 0x00000100,

		WAIT_TIMEDOUT : 0x00000200

	})

	.protos({

		bundle : null,

		throwable : null,

		type : 0,

		constructs : function() {
			if (arguments.length === 2) {
				this.constructs_2_0_FrameworkEvent_ovld(arguments[0], arguments[1]);
			}
			else if (arguments.length === 3) {
				this.constructs_3_0_FrameworkEvent_ovld(arguments[0], arguments[1], arguments[2]);
			}
		},

		constructs_2_0_FrameworkEvent_ovld : function(type, source) {
			this.base(source);
			this.type = type;
			this.bundle = null;
			this.throwable = null;
		},

		constructs_3_0_FrameworkEvent_ovld : function(type, bundle, throwable) {
			this.base(bundle);
			this.type = type;
			this.bundle = bundle;
			this.throwable = throwable;
		},

		getThrowable : function() {
			return this.throwable;
		},

		getBundle : function() {
			return this.bundle;
		},

		getType : function() {
			return this.type;
		}

	})

	.endType();

})