define([ "vjo", "../util/ObjectUtil", "./Version" ], function(vjo, ObjectUtil, Version) {

	/**
	 * Version range. A version range is an interval describing a set of
	 * {@link module:osgi/framework/Version versions}.
	 * 
	 * <p>
	 * A range has a left (lower) endpoint and a right (upper) endpoint. Each
	 * endpoint can be open (excluded from the set) or closed (included in the
	 * set).
	 * 
	 * <p>
	 * {@linkcode module:osgi/framework/VersionRange} objects are immutable.
	 * 
	 * @author Jose Badeau
	 * @exports osgi/framework/VersionRange
	 * @Immutable
	 */
	return vjo.ctype("osgi.framework.VersionRange")

	.props({

		/**
		 * The left endpoint is open and is excluded from the range.
		 * <p>
		 * The value of {@linkcode module:osgi/framework/VersionRange.LEFT_OPEN}
		 * is {@linkcode char '('}.
		 * 
		 * @memberof module:osgi/framework/VersionRange
		 */
		LEFT_OPEN : '(',

		/**
		 * The left endpoint is closed and is included in the range.
		 * <p>
		 * The value of
		 * {@linkcode module:osgi/framework/VersionRange.LEFT_CLOSED} is
		 * {@linkcode char '['}.
		 * 
		 * @memberof module:osgi/framework/VersionRange
		 */
		LEFT_CLOSED : '[',

		/**
		 * The right endpoint is open and is excluded from the range.
		 * <p>
		 * The value of
		 * {@linkcode module:osgi/framework/VersionRange.RIGHT_OPEN} is
		 * {@linkcode char ')'}.
		 * 
		 * @memberof module:osgi/framework/VersionRange
		 */
		RIGHT_OPEN : ')',

		/**
		 * The right endpoint is closed and is included in the range.
		 * <p>
		 * The value of
		 * {@linkcode module:osgi/framework/VersionRange.RIGHT_CLOSED} is
		 * {@linkcode char ']'}.
		 * 
		 * @memberof module:osgi/framework/VersionRange
		 */
		RIGHT_CLOSED : ']',

		LEFT_OPEN_DELIMITER : "(",

		LEFT_CLOSED_DELIMITER : "[",

		LEFT_DELIMITERS : null,

		RIGHT_OPEN_DELIMITER : ")",

		RIGHT_CLOSED_DELIMITER : "]",

		RIGHT_DELIMITERS : null,

		ENDPOINT_DELIMITER : ",",

		/**
		 * Parse version component into a Version.
		 * 
		 * @memberof module:osgi/framework/VersionRange
		 * @param {string}
		 *            version Version component string.
		 * @param {string}
		 *            range Complete range string for exception message, if any.
		 * @return {module:osgi/framework/Version}
		 */
		parseVersion : function(version, range) {
			try {
				return Version.parseVersion(version);
			}
			catch (error) {
				throw new Error("invalid range '" + range + "': " + error.message);
			}
		}

	})

	.protos({

		leftClosed : false,

		left : null,

		right : null,

		rightClosed : false,

		empty : false,

		versionRangeString : null,

		hash : 0,

		/**
		 * Creates a version range from the specified string or from the
		 * specified versions.
		 * 
		 * <p>
		 * Version range string grammar:
		 * 
		 * <pre>
		 * range ::= interval | atleast
		 * interval ::= ( '[' | '(' ) left ',' right ( ']' | ')' )
		 * left ::= version
		 * right ::= version
		 * atleast ::= version
		 * </pre>
		 * 
		 * <p>
		 * VersionRange construction examples:
		 * 
		 * <pre>
		 * new VersionRange('(1.0.0.SNAPSHOT, 2.0.0.SNAPSHOT)')
		 * new VersionRange(VersionRange.LEFT_OPEN, new Version('1.0.0.SNAPSHOT'), new Version('2.0.0.SNAPSHOT'), VersionRange.RIGHT_OPEN)
		 * </pre>
		 * 
		 * @constructs module:osgi/framework/VersionRange
		 * @param {string}
		 *            rangeORleftType String representation of the version
		 *            range. The versions in the range must contain no
		 *            whitespace. Other whitespace in the range string is
		 *            ignored.
		 * @param {string}
		 *            leftType Must be either
		 *            {@link module:osgi/framework/VersionRange.LEFT_CLOSED} or
		 *            {@link module:osgi/framework/VersionRange.LEFT_OPEN} .
		 * @param {module:osgi/framework/Version}
		 *            leftEndpoint Left endpoint of range. Must not be
		 *            {@linkcode null}.
		 * @param {module:osgi/framework/Version}
		 *            rightEndpoint Right endpoint of range. May be
		 *            {@linkcode null} to indicate the right endpoint is
		 *            <i>Infinity</i>.
		 * @param {string}
		 *            rightType Must be either
		 *            {@link module:osgi/framework/VersionRange.RIGHT_CLOSED} or
		 *            {@link module:osgi/framework/VersionRange.RIGHT_OPEN}.
		 * @throws IllegalArgumentError
		 *             If {@linkcode range} is improperly formatted or if the
		 *             arguments are invalid.
		 */
		constructs : function(rangeORleftType, leftEndpoint, rightEndpoint, rightType) {
			if (arguments.length === 1) {
				this.constructs_1_0(rangeORleftType);
			}
			else if (arguments.length === 4) {
				this.constructs_4_0(rangeORleftType, leftEndpoint, rightEndpoint, rightType);
			}
		},

		constructs_1_0 : function(range) {
			var closedLeft, endpointLeft, closedRight, endpointRight;

			try {
				if (range === null) {
					return VersionRange.EMPTY_RANGE;
				}

				if (typeof range !== "string") {
					throw new Error("invalid range \"" + range + "\": invalid format");
				}

				range = range.replace(/^\s+|\s+$/g, '');
				if (range.length === 0) {
					return VersionRange.EMPTY_RANGE;
				}

				var minVersion = null;
				var includeMin = false;
				var maxVersion = null;
				var includeMax = false;

				if (range.charAt(0) === '[' || range.charAt(0) == '(') {
					var comma = range.indexOf(',');
					if (comma === -1) {
						throw new Error("invalid range \"" + range + "\": invalid format");
					}
					var last = range.charAt(range.length - 1);
					if (last !== ']' && last !== ')') {
						throw new Error("invalid range \"" + range + "\": invalid format");
					}

					minVersion = Version.parseVersion(range.substring(1, comma).replace(/^\s+|\s+$/g, ''));
					includeMin = range.charAt(0) == '[';
					maxVersion = Version.parseVersion(range.substring(comma + 1, range.length - 1).replace(/^\s+|\s+$/g, ''));
					includeMax = last == ']';
				}
				else {
					minVersion = Version.parseVersion(range);
					includeMin = true;
					maxVersion = VersionRange._MAX_VERSION;
					includeMax = true;
				}
			}
			catch (error) {
				throw new Error("invalid range \"" + range + "\": invalid format\:" + error.message);
			}

			this.leftClosed = includeMin;
			this.rightClosed = includeMax;
			this.left = minVersion;
			this.right = maxVersion;
			this.empty = this.isEmpty0();
		},

		constructs_4_0 : function(leftType, leftEndpoint, rightEndpoint, rightType) {
			if ((leftType !== this.vj$.VersionRange.LEFT_CLOSED) && (leftType !== this.vj$.VersionRange.LEFT_OPEN)) {
				throw new Error("invalid leftType \"" + leftType + "\"");
			}
			if ((rightType !== this.vj$.VersionRange.RIGHT_OPEN) && (rightType !== this.vj$.VersionRange.RIGHT_CLOSED)) {
				throw Error("invalid rightType \"" + rightType + "\"");
			}
			if (leftEndpoint === null) {
				throw Error("null leftEndpoint argument");
			}
			this.leftClosed = leftType === this.vj$.VersionRange.LEFT_CLOSED;
			this.rightClosed = rightType === this.vj$.VersionRange.RIGHT_CLOSED;
			this.left = leftEndpoint;
			this.right = rightEndpoint;
			this.empty = this.isEmpty0();
		},

		/**
		 * Returns the left endpoint of this version range.
		 * 
		 * @memberof module:osgi/framework/VersionRange.prototype
		 * @return {module:osgi/framework/Version} The left endpoint.
		 */
		getLeft : function() {
			return this.left;
		},

		/**
		 * Returns the right endpoint of this version range.
		 * 
		 * @memberof module:osgi/framework/VersionRange.prototype
		 * @return {module:osgi/framework/Version} The right endpoint. May be
		 *         {@linkcode null} which indicates the right endpoint is
		 *         <i>Infinity</i>.
		 */
		getRight : function() {
			return this.right;
		},

		/**
		 * Returns the type of the left endpoint of this version range.
		 * 
		 * @memberof module:osgi/framework/VersionRange.prototype
		 * @return {string}
		 *         {@link module:osgi/framework/VersionRange.LEFT_CLOSED} if the
		 *         left endpoint is closed or
		 *         {@link module:osgi/framework/VersionRange.LEFT_OPEN} if the
		 *         left endpoint is open.
		 */
		getLeftType : function() {
			return this.leftClosed ? this.vj$.VersionRange.LEFT_CLOSED : this.vj$.VersionRange.LEFT_OPEN;
		},

		/**
		 * Returns the type of the right endpoint of this version range.
		 * 
		 * @memberof module:osgi/framework/VersionRange.prototype
		 * @return {string}
		 *         {@link module:osgi/framework/VersionRange.RIGHT_CLOSED} if
		 *         the right endpoint is closed or
		 *         {@link module:osgi/framework/VersionRange.RIGHT_OPEN} if the
		 *         right endpoint is open.
		 */
		getRightType : function() {
			return this.rightClosed ? this.vj$.VersionRange.RIGHT_CLOSED : this.vj$.VersionRange.RIGHT_OPEN;
		},

		/**
		 * Returns whether this version range includes the specified version.
		 * 
		 * @memberof module:osgi/framework/VersionRange.prototype
		 * @param {module:osgi/framework/Version}
		 *            version The version to test for inclusion in this version
		 *            range.
		 * @return {boolean} {@linkcode true} if the specified version is
		 *         included in this version range; {@linkcode false} otherwise.
		 */
		includes : function(version) {
			if (this.empty) {
				return false;
			}
			if (this.left.compareTo(version) >= (this.leftClosed ? 1 : 0)) {
				return false;
			}
			if (this.right === null) {
				return true;
			}
			return this.right.compareTo(version) >= (this.rightClosed ? 0 : 1);
		},

		/**
		 * Returns the intersection of this version range with the specified
		 * version ranges.
		 * 
		 * @memberof module:osgi/framework/VersionRange.prototype
		 * @param {...module:osgi/framework/Version}
		 *            ranges The version ranges to intersect with this version
		 *            range.
		 * @return {module:osgi/framework/Version | null} A version range
		 *         representing the intersection of this version range and the
		 *         specified version ranges otherwise; null if no possible
		 *         intersection. If no version ranges are specified, then this
		 *         version range is returned.
		 */
		intersection : function() {
			var ranges;
			if (arguments.length == 1 && arguments[0] instanceof Array) {
				ranges = arguments[0];
			}
			else {
				ranges = [];
				for (var i = 0; i < arguments.length; i++) {
					ranges[i - 0] = arguments[i];
				}
			}
			if ((ranges === null) || (ranges.length === 0)) {
				return this;
			}
			var closedLeft = this.leftClosed;
			var closedRight = this.rightClosed;
			var endpointLeft = this.left;
			var endpointRight = this.right;
			for (var range, _$i0 = 0; _$i0 < ranges.length; _$i0++) {
				range = ranges[_$i0];
				var comparison = endpointLeft.compareTo(range.left);
				if (comparison === 0) {
					closedLeft = closedLeft && range.leftClosed;
				}
				else {
					if (comparison < 0) {
						endpointLeft = range.left;
						closedLeft = range.leftClosed;
					}
				}
				if (range.right !== null) {
					if (endpointRight === null) {
						endpointRight = range.right;
						closedRight = range.rightClosed;
					}
					else {
						comparison = endpointRight.compareTo(range.right);
						if (comparison === 0) {
							closedRight = closedRight && range.rightClosed;
						}
						else {
							if (comparison > 0) {
								endpointRight = range.right;
								closedRight = range.rightClosed;
							}
						}
					}
				}
			}
			var intersection = new this.vj$.VersionRange(closedLeft ? this.vj$.VersionRange.LEFT_CLOSED : this.vj$.VersionRange.LEFT_OPEN, endpointLeft, endpointRight, closedRight ? this.vj$.VersionRange.RIGHT_CLOSED : this.vj$.VersionRange.RIGHT_OPEN);
			return (intersection.left > intersection.right) ? null : intersection;
		},

		/**
		 * Returns whether this version range is empty. A version range is empty
		 * if the set of versions defined by the interval is empty.
		 * 
		 * @memberof module:osgi/framework/VersionRange.prototype
		 * @return {boolean} {@linkcode true} if this version range is empty;
		 *         {@linkcode false} otherwise.
		 */
		isEmpty : function() {
			return this.empty;
		},

		/**
		 * Internal isEmpty behavior.
		 * 
		 * @memberof module:osgi/framework/VersionRange.prototype
		 * @return {boolean} {@linkcode true} if this version range is empty;
		 *         {@linkcode false} otherwise.
		 */
		isEmpty0 : function() {
			if (this.right === null) {
				return false;
			}
			var comparison = this.left.compareTo(this.right);
			if (comparison === 0) {
				return !this.leftClosed || !this.rightClosed;
			}
			return comparison > 0;
		},

		/**
		 * Returns whether this version range contains only a single version.
		 * 
		 * @memberof module:osgi/framework/VersionRange.prototype
		 * @return {boolean} {@linkcode true} if this version range contains
		 *         only a single version; {@linkcode false} otherwise.
		 */
		isExact : function() {
			if (this.empty || (this.right === null)) {
				return false;
			}
			if (this.leftClosed) {
				if (this.rightClosed) {
					return ObjectUtil.equals(this.left, this.right);
				}
				else {
					var adjacent1 = new Version(this.left.getMajor(), this.left.getMinor(), this.left.getMicro(), this.left.getQualifier() + "-");
					return adjacent1.compareTo(this.right) >= 0;
				}
			}
			else {
				if (this.rightClosed) {
					var adjacent1 = new Version(this.left.getMajor(), this.left.getMinor(), this.left.getMicro(), this.left.getQualifier() + "-");
					return ObjectUtil.equals(adjacent1, this.right);
				}
				else {
					var adjacent2 = new Version(this.left.getMajor(), this.left.getMinor(), this.left.getMicro(), this.left.getQualifier() + "--");
					return adjacent2.compareTo(this.right) >= 0;
				}
			}
		},

		/**
		 * Returns the string representation of this version range.
		 * 
		 * <p>
		 * The format of the version range string will be a version string if
		 * the right end point is <i>Infinity</i> ({@linkcode null}) or an
		 * interval string.
		 * 
		 * @memberof module:osgi/framework/VersionRange.prototype
		 * @return {string} The string representation of this version range.
		 */
		toString : function() {
			if (this.versionRangeString !== null) {
				return this.versionRangeString;
			}
			var leftVersion = this.left.toString();
			if (this.right === null) {
				var result = [];
				result.push(this.left.toString0());
				return this.versionRangeString = result.join("");
			}
			var rightVerion = this.right.toString();
			var result = [];
			result.push(this.leftClosed ? this.vj$.VersionRange.LEFT_CLOSED : this.vj$.VersionRange.LEFT_OPEN);
			result.push(this.left.toString0());
			result.push(this.vj$.VersionRange.ENDPOINT_DELIMITER);
			result.push(this.right.toString0());
			result.push(this.rightClosed ? this.vj$.VersionRange.RIGHT_CLOSED : this.vj$.VersionRange.RIGHT_OPEN);
			return this.versionRangeString = result.join("");
		},

		/**
		 * Returns a hash code value for the object.
		 * 
		 * @memberof module:osgi/framework/VersionRange.prototype
		 * @return {number} An integer which is a hash code value for this
		 *         object.
		 */
		hashCode : function() {
			throw new Error("the requested operation is not supported");
		},

		/**
		 * Compares this {@linkcode module:osgi/framework/VersionRange} object
		 * to another object.
		 * 
		 * <p>
		 * A version range is considered to be <b>equal to </b> another version
		 * range if both the endpoints and their types are equal or if both
		 * version ranges are
		 * {@link module:osgi/framework/VersionRange.isEmpty() empty}.
		 * 
		 * @memberof module:osgi/framework/VersionRange.prototype
		 * @param {*}
		 *            object The {@linkcode module:osgi/framework/VersionRange}
		 *            object to be compared.
		 * @return {boolean} {@linkcode true} if {@linkcode object} is a
		 *         {@linkcode module:osgi/framework/VersionRange} and is equal
		 *         to this object; {@linkcode false} otherwise.
		 */
		equals : function(object) {
			if (object === this) {
				return true;
			}
			if (!(osgi.framework.VersionRange.isInstance(object))) {
				return false;
			}
			var other = object;
			if (this.empty && other.empty) {
				return true;
			}
			if (this.right === null) {
				return (this.leftClosed === other.leftClosed) && (other.right === null) && ObjectUtil.equals(this.left, other.left);
			}
			return (this.leftClosed === other.leftClosed) && (this.rightClosed === other.rightClosed) && ObjectUtil.equals(this.left, other.left) && ObjectUtil.equals(this.right, other.right);
		},

		toFilterString : function(attributeName) {
			if (attributeName.length === 0) {
				throw new Error("invalid attributeName \"" + attributeName + "\"");
			}
		}
	})

	.inits(function() {
		this.vj$.VersionRange.LEFT_DELIMITERS = this.LEFT_CLOSED_DELIMITER + this.LEFT_OPEN_DELIMITER;
		this.vj$.VersionRange.RIGHT_DELIMITERS = this.RIGHT_OPEN_DELIMITER + this.RIGHT_CLOSED_DELIMITER;
	})

	.endType();

})