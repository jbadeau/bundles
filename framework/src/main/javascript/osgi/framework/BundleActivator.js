define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.framework.BundleActivator").protos({

		start : function(context) {
		},

		stop : function(context) {
		}

	})

	.endType();

})