define([ "vjo", "../lang/Exception", "../util/StringBuilder" ], function(vjo, Exception, StringBuilder) {

	return vjo.ctype("osgi.framework.ServiceException")

	.inherits(Exception)

	.props({

		UNSPECIFIED : 0,

		UNREGISTERED : 1,

		FACTORY_ERROR : 2,

		FACTORY_EXCEPTION : 3,

		SUBCLASSED : 4,

		REMOTE : 5,

		FACTORY_RECURSION : 6,

		TYPE : null

	})

	.protos({

		type : 0,

		constructs : function() {
			if (arguments.length === 2) {
				if ((arguments[0] instanceof String || typeof arguments[0] == "string") && (arguments[1] instanceof Exception || arguments[1] instanceof Error)) {
					this.constructs_2_0_ServiceException_ovld(arguments[0], arguments[1]);
				}
				else if ((arguments[0] instanceof String || typeof arguments[0] == "string") && typeof arguments[1] == "number") {
					this.constructs_2_1_ServiceException_ovld(arguments[0], arguments[1]);
				}
			}
			else if (arguments.length === 1) {
				this.constructs_1_0_ServiceException_ovld(arguments[0]);
			}
			else if (arguments.length === 3) {
				this.constructs_3_0_ServiceException_ovld(arguments[0], arguments[1], arguments[2]);
			}
			else if (arguments.length === 0) {
				this.constructs_1_0_ServiceException_ovld();
			}
		},

		constructs_2_0_ServiceException_ovld : function(msg, cause) {
			this.constructs_3_0_ServiceException_ovld(msg, this.vj$.ServiceException.UNSPECIFIED, cause);
		},

		constructs_1_0_ServiceException_ovld : function(msg) {
			this.constructs_2_1_ServiceException_ovld(msg, this.vj$.ServiceException.UNSPECIFIED);
		},

		constructs_3_0_ServiceException_ovld : function(msg, type, cause) {
			this.base(msg, cause);
			this.name = this.getClass().getName();
			this.type = type;
		},

		constructs_2_1_ServiceException_ovld : function(msg, type) {
			this.base(msg);
			this.name = this.getClass().getName();
			this.type = type;
		},

		toString : function() {
			var builder = new StringBuilder();
			builder.append(this.name);
			builder.append(": \n");
			builder.append(this.vj$.ServiceException.TYPE[this.type]);
			builder.append(": \n");
			builder.append(this.message);
			var rootCause = this;
			while (rootCause != null && rootCause.cause) {
				rootCause = rootCause.cause;
				if (rootCause !== null) {
					builder.append(": \n");
					builder.append(rootCause.message);
				}
			}
			return builder.toString();
		}

	})

	.inits(function() {
		this.TYPE = [ "UNSPECIFIED", "UNREGISTERED", "FACTORY_ERROR", "FACTORY_EXCEPTION", "SUBCLASSED", "REMOTE", "FACTORY_RECURSION" ];
	})

	.endType();

})