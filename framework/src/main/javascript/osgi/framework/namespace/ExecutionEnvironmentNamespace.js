define([ "vjo", "../../resource/Namespace" ], function(vjo, Namespace) {

	return vjo.ctype("osgi.framework.namespace.ExecutionEnvironmentNamespace")

	.inherits(Namespace)

	.props({

		EXECUTION_ENVIRONMENT_NAMESPACE : "osgi.ee",

		CAPABILITY_VERSION_ATTRIBUTE : "version"

	})

	.protos({

		constructs : function() {
			this.base();
		}
	})

	.endType();

})