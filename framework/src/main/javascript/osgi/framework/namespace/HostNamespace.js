define([ "vjo", "./AbstractWiringNamespace" ], function(vjo, AbstractWiringNamespace) {

	return vjo.ctype("osgi.framework.namespace.HostNamespace")

	.inherits(AbstractWiringNamespace)

	.props({

		HOST_NAMESPACE : "osgi.wiring.host",

		CAPABILITY_SINGLETON_DIRECTIVE : "singleton",

		CAPABILITY_FRAGMENT_ATTACHMENT_DIRECTIVE : "fragment-attachment",

		FRAGMENT_ATTACHMENT_ALWAYS : "always",

		FRAGMENT_ATTACHMENT_RESOLVETIME : "resolve-time",

		FRAGMENT_ATTACHMENT_NEVER : "never",

		REQUIREMENT_EXTENSION_DIRECTIVE : "extension",

		EXTENSION_FRAMEWORK : "framework",

		EXTENSION_BOOTCLASSPATH : "bootclasspath",

		REQUIREMENT_VISIBILITY_DIRECTIVE : "visibility"

	})

	.protos({

		constructs : function() {
			this.base();
		}

	})

	.endType();
})