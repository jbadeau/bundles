define([ "vjo", "../../resource/Namespace" ], function(vjo, Namespace) {

	return vjo.ctype("osgi.framework.namespace.IdentityNamespace")

	.inherits(Namespace)

	.props({

		IDENTITY_NAMESPACE : "osgi.identity",

		CAPABILITY_SINGLETON_DIRECTIVE : "singleton",

		CAPABILITY_VERSION_ATTRIBUTE : "version",

		CAPABILITY_TYPE_ATTRIBUTE : "type",

		TYPE_BUNDLE : "osgi.bundle",

		TYPE_FRAGMENT : "osgi.fragment",

		TYPE_UNKNOWN : "unknown",

		CAPABILITY_COPYRIGHT_ATTRIBUTE : "copyright",

		CAPABILITY_DESCRIPTION_ATTRIBUTE : "description",

		CAPABILITY_DOCUMENTATION_ATTRIBUTE : "documentation",

		CAPABILITY_LICENSE_ATTRIBUTE : "license",

		REQUIREMENT_CLASSIFIER_DIRECTIVE : "classifier",

		CLASSIFIER_SOURCES : "sources",

		CLASSIFIER_JAVADOC : "javadoc"

	})

	.protos({

		constructs : function() {
			this.base();
		}

	})

	.endType();

})