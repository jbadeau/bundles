define([ "vjo", "../../resource/Namespace" ], function(vjo, Namespace) {

	return vjo.ctype("osgi.framework.namespace.AbstractWiringNamespace")

	.inherits(Namespace)

	.props({

		CAPABILITY_MANDATORY_DIRECTIVE : "mandatory", 

		CAPABILITY_BUNDLE_VERSION_ATTRIBUTE : "bundle-version" 

	})

	.protos({

		constructs : function() {
			this.base();
		}

	})

	.endType();

})