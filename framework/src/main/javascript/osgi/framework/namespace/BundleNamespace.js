define([ "vjo", "./AbstractWiringNamespace" ], function(vjo, AbstractWiringNamespace) {

	return vjo.ctype("osgi.framework.namespace.BundleNamespace")

	.inherits(AbstractWiringNamespace)

	.props({

		BUNDLE_NAMESPACE : "osgi.wiring.bundle",

		CAPABILITY_SINGLETON_DIRECTIVE : "singleton",

		CAPABILITY_FRAGMENT_ATTACHMENT_DIRECTIVE : "fragment-attachment",

		REQUIREMENT_EXTENSION_DIRECTIVE : "extension",

		REQUIREMENT_VISIBILITY_DIRECTIVE : "visibility",

		VISIBILITY_PRIVATE : "private",

		VISIBILITY_REEXPORT : "reexport"

	})

	.protos({

		constructs : function() {
			this.base();
		}

	})

	.endType();

})