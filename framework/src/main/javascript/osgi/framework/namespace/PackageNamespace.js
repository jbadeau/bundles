define([ "vjo", "./AbstractWiringNamespace" ], function(vjo, AbstractWiringNamespace) {

	return vjo.ctype("osgi.framework.namespace.PackageNamespace")

	.inherits(AbstractWiringNamespace)

	.props({

		PACKAGE_NAMESPACE : "osgi.wiring.package",

		CAPABILITY_INCLUDE_DIRECTIVE : "include",

		CAPABILITY_EXCLUDE_DIRECTIVE : "exclude",

		CAPABILITY_VERSION_ATTRIBUTE : "version",

		CAPABILITY_BUNDLE_SYMBOLICNAME_ATTRIBUTE : "bundle-symbolic-name",

		RESOLUTION_DYNAMIC : "dynamic"

	})

	.protos({

		constructs : function() {
			this.base();
		}

	})

	.endType();

})