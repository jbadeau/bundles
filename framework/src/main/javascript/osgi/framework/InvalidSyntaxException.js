define([ "vjo", "../lang/Exception" ], function(vjo, Exception) {

	return vjo.ctype("osgi.framework.InvalidSyntaxException")

	.inherits(Exception).props({

		message : function(msg, filter) {
			if ((msg === null) || (filter === null) || msg.indexOf(filter) >= 0) {
				return msg;
			}
			return msg + ": " + filter;
		}

	})

	.protos({

		filter : null,

		constructs : function() {
			if (arguments.length === 2) {
				this.constructs_2_0_InvalidSyntaxException_ovld(arguments[0], arguments[1]);
			}
			else if (arguments.length === 3) {
				this.constructs_3_0_InvalidSyntaxException_ovld(arguments[0], arguments[1], arguments[2]);
			}
			else if (arguments.length === 0) {
				this.constructs_2_0_InvalidSyntaxException_ovld(arguments[0], arguments[1], arguments[2]);
			}
		},

		constructs_2_0_InvalidSyntaxException_ovld : function(msg, filter) {
			this.base(this.vj$.InvalidSyntaxException.message(msg, filter));
			this.filter = filter;
		},

		constructs_3_0_InvalidSyntaxException_ovld : function(msg, filter, cause) {
			this.base(this.vj$.InvalidSyntaxException.message(msg, filter), cause);
			this.filter = filter;
		},

		getFilter : function() {
			return this.filter;
		}

	})

	.endType();

})