define([ "vjo", "../lang/Comparable" ], function(vjo, Comparable) {

	return vjo.itype("osgi.framework.Bundle")

	//.inherits([ Comparable ])

	.props({

		UNINSTALLED : 0x00000001,

		INSTALLED : 0x00000002,

		RESOLVED : 0x00000004,

		STARTING : 0x00000008,

		STOPPING : 0x00000010,

		ACTIVE : 0x00000020,

		START_TRANSIENT : 0x00000001,

		START_ACTIVATION_POLICY : 0x00000002,

		STOP_TRANSIENT : 0x00000001,

		SIGNERS_ALL : 1,

		SIGNERS_TRUSTED : 2

	})

	.protos({

		getState : function() {
		},

		start : function(options) {
			if (arguments.length === 1) {
				if (typeof arguments[0] == "number") {
					this.start_1_0_Bundle_ovld(arguments[0]);
				}
				else if (this.base && this.base.start) {
					this.base.start.apply(this, arguments);
				}
			}
			else if (arguments.length === 0) {
				if (arguments.length == 0) {
					this.start_0_0_Bundle_ovld();
				}
				else if (this.base && this.base.start) {
					this.base.start.apply(this, arguments);
				}
			}
			else if (this.base && this.base.start) {
				this.base.start.apply(this, arguments);
			}
		},

		start_1_0_Bundle_ovld : function(options) {
		},

		start_0_0_Bundle_ovld : function() {
		},

		stop : function(options) {
			if (arguments.length === 1) {
				if (typeof arguments[0] == "number") {
					this.stop_1_0_Bundle_ovld(arguments[0]);
				}
				else if (this.base && this.base.stop) {
					this.base.stop.apply(this, arguments);
				}
			}
			else if (arguments.length === 0) {
				if (arguments.length == 0) {
					this.stop_0_0_Bundle_ovld();
				}
				else if (this.base && this.base.stop) {
					this.base.stop.apply(this, arguments);
				}
			}
			else if (this.base && this.base.stop) {
				this.base.stop.apply(this, arguments);
			}
		},

		stop_1_0_Bundle_ovld : function(options) {
		},

		stop_0_0_Bundle_ovld : function() {
		},

		update : function(input) {
			if (arguments.length === 1) {
				if (arguments[0] instanceof org.eclipse.vjet.vjo.java.io.InputStream) {
					this.update_1_0_Bundle_ovld(arguments[0]);
				}
				else if (this.base && this.base.update) {
					this.base.update.apply(this, arguments);
				}
			}
			else if (arguments.length === 0) {
				if (arguments.length == 0) {
					this.update_0_0_Bundle_ovld();
				}
				else if (this.base && this.base.update) {
					this.base.update.apply(this, arguments);
				}
			}
			else if (this.base && this.base.update) {
				this.base.update.apply(this, arguments);
			}
		},

		update_1_0_Bundle_ovld : function(input) {
		},

		update_0_0_Bundle_ovld : function() {
		},

		uninstall : function() {
		},

		getBundleId : function() {
		},

		getLocation : function() {
		},

		getRegisteredServices : function() {
		},

		getServicesInUse : function() {
		},

		hasPermission : function(permission) {
		},

		getResource : function(name) {
		},

		getHeaders : function() {
			if (arguments.length === 0) {
				if (arguments.length == 0) {
					return this.getHeaders_0_0_Bundle_ovld();
				}
				else if (this.base && this.base.getHeaders) {
					return this.base.getHeaders.apply(this, arguments);
				}
			}
			else if (arguments.length === 1) {
				if (arguments[0] instanceof String || typeof arguments[0] == "string") {
					return this.getHeaders_1_0_Bundle_ovld(arguments[0]);
				}
				else if (this.base && this.base.getHeaders) {
					return this.base.getHeaders.apply(this, arguments);
				}
			}
			else if (this.base && this.base.getHeaders) {
				return this.base.getHeaders.apply(this, arguments);
			}
		},

		getHeaders_0_0_Bundle_ovld : function() {
		},

		getHeaders_1_0_Bundle_ovld : function(locale) {
		},

		getSymbolicName : function() {
		},

		loadClass : function(name) {
		},

		getResources : function(name) {
		},

		getEntryPaths : function(path) {
		},

		getEntry : function(path) {
		},

		getLastModified : function() {
		},

		findEntries : function(path, filePattern, recurse) {
		},

		getBundleContext : function() {
		},

		getSignerCertificates : function(signersType) {
		},

		getVersion : function() {
		},

		adapt : function(type) {
		},

		getDataFile : function(filename) {
		}

	})

	.endType();

})