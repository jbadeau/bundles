define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.framework.ServiceRegistration")

	.protos({

		getReference : function() {
		},

		setProperties : function(properties) {
		},

		unregister : function() {
		}

	})

	.endType();

})