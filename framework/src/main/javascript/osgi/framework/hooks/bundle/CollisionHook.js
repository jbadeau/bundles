define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.framework.hooks.bundle.CollisionHook")

	.props({

		INSTALLING : 1,

		UPDATING : 2

	})

	.protos({

		filterCollisions : function(operationType, target, collisionCandidates) {
		}

	})

	.endType();

})