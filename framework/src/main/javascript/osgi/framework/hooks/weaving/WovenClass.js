define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.framework.hooks.weaving.WovenClass")

	.protos({

		getBytes : function() {
		},

		setBytes : function(newBytes) {
		},

		getDynamicImports : function() {
		},

		isWeavingComplete : function() {
		},

		getClassName : function() {
		},

		getProtectionDomain : function() {
		},

		getDefinedClass : function() {
		},

		getBundleWiring : function() {
		}

	})

	.endType();

})