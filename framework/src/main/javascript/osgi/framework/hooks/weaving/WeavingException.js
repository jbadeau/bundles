define([ "vjo", "../../Exception" ], function(vjo, Exception) {

	return vjo.ctype("osgi.framework.hooks.weaving.WeavingException")

	.inherits(Exception)

	.protos({

		constructs : function() {
			if (arguments.length === 2) {
				this.constructs_2_0_WeavingException_ovld(arguments[0], arguments[1]);
			}
			else if (arguments.length === 1) {
				this.constructs_1_0_WeavingException_ovld(arguments[0]);
			}
		},

		constructs_2_0_WeavingException_ovld : function(msg, cause) {
			this.base(msg, cause);
		},

		constructs_1_0_WeavingException_ovld : function(msg) {
			this.base(msg);
		}

	})

	.endType();

})