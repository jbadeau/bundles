define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.framework.hooks.resolver.ResolverHook")

	.protos({

		filterResolvable : function(candidates) {
		},

		filterSingletonCollisions : function(singleton, collisionCandidates) {
		},

		filterMatches : function(requirement, candidates) {
		},

		end : function() {
		}

	})

	.endType();

})