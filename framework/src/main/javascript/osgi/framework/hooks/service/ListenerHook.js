define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.framework.hooks.service.ListenerHook")

	.props({

		ListenerInfo : vjo.itype()

		.protos({

			getBundleContext : function() {
			},

			getFilter : function() {
			},

			isRemoved : function() {
			},

			equals : function(obj) {
			},

			hashCode : function() {
			}

		}).endType()

	})

	.protos({

		added : function(listeners) {
		},

		removed : function(listeners) {
		}

	})

	.endType();

})