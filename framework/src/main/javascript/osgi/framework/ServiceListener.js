define([ "vjo", "./EventListener" ], function(vjo, EventListener) {

	return vjo.itype("osgi.framework.ServiceListener")

	.inherits(EventListener).protos({

		serviceChanged : function(event) {
		}

	})

	.endType();

})