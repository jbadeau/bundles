define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.framework.Filter")

	.protos({

		match : function(reference) {
			if (arguments.length === 1) {
				if (osgi.framework.ServiceReference.clazz.isInstance(arguments[0])) {
					return this.match_1_0_Filter_ovld(arguments[0]);
				}
				else if (arguments[0] instanceof java.util.Dictionary) {
					return this.match_1_1_Filter_ovld(arguments[0]);
				}
			}
		},

		match_1_0_Filter_ovld : function(reference) {
		},

		match_1_1_Filter_ovld : function(dictionary) {
		},

		toString : function() {
		},

		equals : function(obj) {
		},

		hashCode : function() {
		},

		matchCase : function(dictionary) {
		},

		matches : function(map) {
		}

	})

	.endType();

})