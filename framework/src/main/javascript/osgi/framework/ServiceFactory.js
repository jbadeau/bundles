define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.framework.ServiceFactory")

	.protos({

		getService : function(bundle, registration) {
		},

		ungetService : function(bundle, registration, service) {
		}

	})

	.endType();

})