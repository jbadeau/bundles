define([ "vjo", "./BundleListener" ], function(vjo, BundleListener) {

	return vjo.itype("osgi.framework.SynchronousBundleListener")

	.inherits(BundleListener)

	.endType();

})