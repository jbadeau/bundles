define([ "vjo", "./Filter" ], function(vjo, Filter) {
	
	return vjo.ctype("osgi.framework.FrameworkUtil")

	.props({
	
		FilterImpl : vjo.ctype().satisfies(Filter)
		
		.props({
		
			EQUAL : 1,
			
			APPROX : 2,
			
			GREATER : 3,
			
			LESS : 4,
			
			PRESENT : 5,
			
			SUBSTRING : 6,
			
			AND : 7,
			
			OR : 8,
			
			NOT : 9,
			
			Parser : vjo.ctype().protos({
				filterstring : null,
				filterChars : null,
				pos : 0,

				constructs : function(filterstring) {
					this.filterstring = filterstring;
					this.filterChars = org.eclipse.vjet.vjo.java.lang.StringUtil.toCharArray(filterstring);
					this.pos = 0;
				},

				parse : function() {
					var filter;
					try {
						filter = this.parse_filter();
					}
					catch (e) {
						throw new this.vj$.InvalidSyntaxException("Filter ended abruptly", this.filterstring, e);
					}
					if (this.pos !== this.filterChars.length) {
						throw new this.vj$.InvalidSyntaxException("Extraneous trailing characters: " + this.filterstring.substring(this.pos), this.filterstring);
					}
					return filter;
				},

				parse_filter : function() {
					var filter;
					this.skipWhiteSpace();
					if (this.filterChars[this.pos] !== '(') {
						throw new this.vj$.InvalidSyntaxException("Missing '(': " + this.filterstring.substring(this.pos), this.filterstring);
					}
					this.pos++;
					filter = this.parse_filtercomp();
					this.skipWhiteSpace();
					if (this.filterChars[this.pos] !== ')') {
						throw new this.vj$.InvalidSyntaxException("Missing ')': " + this.filterstring.substring(this.pos), this.filterstring);
					}
					this.pos++;
					this.skipWhiteSpace();
					return filter;
				},

				parse_filtercomp : function() {
					this.skipWhiteSpace();
					var c = this.filterChars[this.pos];
					switch (c) {
						case '&': {
							this.pos++;
							return this.parse_and();
						}
						case '|': {
							this.pos++;
							return this.parse_or();
						}
						case '!': {
							this.pos++;
							return this.parse_not();
						}
					}
					return this.parse_item();
				},

				parse_and : function() {
					var lookahead = this.pos;
					this.skipWhiteSpace();
					if (this.filterChars[this.pos] !== '(') {
						this.pos = lookahead - 1;
						return this.parse_item();
					}
					var operands = new ArrayList(10);
					while (this.filterChars[this.pos] === '(') {
						var child = this.parse_filter();
						operands.add(child);
					}
					return new this.vj$.FilterImpl(this.vj$.FilterImpl.AND, null, operands.toArray(vjo.createArray(null, operands.size())));
				},

				parse_or : function() {
					var lookahead = this.pos;
					this.skipWhiteSpace();
					if (this.filterChars[this.pos] !== '(') {
						this.pos = lookahead - 1;
						return this.parse_item();
					}
					var operands = new ArrayList(10);
					while (this.filterChars[this.pos] === '(') {
						var child = this.parse_filter();
						operands.add(child);
					}
					return new this.vj$.FilterImpl(this.vj$.FilterImpl.OR, null, operands.toArray(vjo.createArray(null, operands.size())));
				},

				parse_not : function() {
					var lookahead = this.pos;
					this.skipWhiteSpace();
					if (this.filterChars[this.pos] !== '(') {
						this.pos = lookahead - 1;
						return this.parse_item();
					}
					var child = this.parse_filter();
					return new this.vj$.FilterImpl(this.vj$.FilterImpl.NOT, null, child);
				},

				parse_item : function() {
					var attr = this.parse_attr();
					this.skipWhiteSpace();
					switch (this.filterChars[this.pos]) {
						case '~': {
							if (this.filterChars[this.pos + 1] === '=') {
								this.pos += 2;
								return new this.vj$.FilterImpl(this.vj$.FilterImpl.APPROX, attr, this.parse_value());
							}
							break;
						}
						case '>': {
							if (this.filterChars[this.pos + 1] === '=') {
								this.pos += 2;
								return new this.vj$.FilterImpl(this.vj$.FilterImpl.GREATER, attr, this.parse_value());
							}
							break;
						}
						case '<': {
							if (this.filterChars[this.pos + 1] === '=') {
								this.pos += 2;
								return new this.vj$.FilterImpl(this.vj$.FilterImpl.LESS, attr, this.parse_value());
							}
							break;
						}
						case '=': {
							if (this.filterChars[this.pos + 1] === '*') {
								var oldpos = this.pos;
								this.pos += 2;
								this.skipWhiteSpace();
								if (this.filterChars[this.pos] === ')') {
									return new this.vj$.FilterImpl(this.vj$.FilterImpl.PRESENT, attr, null);
								}
								this.pos = oldpos;
							}
							this.pos++;
							var string = this.parse_substring();
							if (string instanceof String) {
								return new this.vj$.FilterImpl(this.vj$.FilterImpl.EQUAL, attr, string);
							}
							return new this.vj$.FilterImpl(this.vj$.FilterImpl.SUBSTRING, attr, string);
						}
					}
					throw new this.vj$.InvalidSyntaxException("Invalid operator: " + this.filterstring.substring(this.pos), this.filterstring);
				},

				parse_attr : function() {
					this.skipWhiteSpace();
					var begin = this.pos;
					var end = this.pos;
					var c = this.filterChars[this.pos];
					while (c !== '~' && c !== '<' && c !== '>' && c !== '=' && c !== '(' && c !== ')') {
						this.pos++;
						if (!this.vj$.Character.isWhitespace(c)) {
							end = this.pos;
						}
						c = this.filterChars[this.pos];
					}
					var length = end - begin;
					if (length === 0) {
						throw new this.vj$.InvalidSyntaxException("Missing attr: " + this.filterstring.substring(this.pos), this.filterstring);
					}
					return org.eclipse.vjet.vjo.java.lang.StringFactory.build(this.filterChars, begin, length);
				},

				parse_value : function() {
					var sb = new StringBuffer(this.filterChars.length - this.pos);
					parseloop: while (true) {
						var c = this.filterChars[this.pos];
						switch (c) {
							case ')': {
								break parseloop;
							}
							case '(': {
								throw new this.vj$.InvalidSyntaxException("Invalid value: " + this.filterstring.substring(this.pos), this.filterstring);
							}
							case '\\': {
								this.pos++;
								c = this.filterChars[this.pos];
							}
							default: {
								sb.append(c);
								this.pos++;
								break;
							}
						}
					}
					if (sb.length() === 0) {
						throw new this.vj$.InvalidSyntaxException("Missing value: " + this.filterstring.substring(this.pos), this.filterstring);
					}
					return sb.toString();
				},

				parse_substring : function() {
					var sb = new StringBuffer(this.filterChars.length - this.pos);
					var operands = new ArrayList(10);
					parseloop: while (true) {
						var c = this.filterChars[this.pos];
						switch (c) {
							case ')': {
								if (sb.length() > 0) {
									operands.add(sb.toString());
								}
								break parseloop;
							}
							case '(': {
								throw new this.vj$.InvalidSyntaxException("Invalid value: " + this.filterstring.substring(this.pos), this.filterstring);
							}
							case '*': {
								if (sb.length() > 0) {
									operands.add(sb.toString());
								}
								sb.setLength(0);
								operands.add(null);
								this.pos++;
								break;
							}
							case '\\': {
								this.pos++;
								c = this.filterChars[this.pos];
							}
							default: {
								sb.append(c);
								this.pos++;
								break;
							}
						}
					}
					var size = operands.size();
					if (size === 0) {
						return "";
					}
					if (size === 1) {
						var single = operands.get(0);
						if (single !== null) {
							return single;
						}
					}
					return operands.toArray(vjo.createArray(null, size));
				},

				skipWhiteSpace : function() {
					for (var length = this.filterChars.length; (this.pos < length) && this.vj$.Character.isWhitespace(this.filterChars[this.pos]);) {
						this.pos++;
					}
				}
				
			})
			
			.endType(),

			newInstance : function(filterString) {
				return (new this.Parser(filterString)).parse();
			},

			encodeValue : function(value) {
				var encoded = false;
				var inlen = value.length;
				var outlen = inlen << 1;
				var output = vjo.createArray("", outlen);
				org.eclipse.vjet.vjo.java.lang.StringUtil.getChars(value, 0, inlen, output, inlen);
				var cursor = 0;
				for (var i = inlen; i < outlen; i++) {
					var c = output[i];
					switch (c) {
						case '(':
						case '*':
						case ')':
						case '\\': {
							output[cursor] = '\\';
							cursor++;
							encoded = true;
							break;
						}
					}
					output[cursor] = c;
					cursor++;
				}
				return encoded ? org.eclipse.vjet.vjo.java.lang.StringFactory.build(output, 0, cursor) : value;
			},

			valueOf : function(target, value2) {
				do {
					var method;
					try {
						method = target.getMethod("valueOf", vjo.Object.clazz);
					}
					catch (e) {
						break;
					}
					if (this.vj$.Modifier.isStatic(method.getModifiers()) && target.isAssignableFrom(method.getReturnType())) {
						setAccessible(method);
						try {
							return method.invoke(null, org.eclipse.vjet.vjo.java.lang.StringUtil.trim(value2));
						}
						catch (e) {
							if (e instanceof this.vj$.IllegalAccessException) {
								return null;
							}
							else if (e instanceof this.vj$.InvocationTargetException) {
								return null;
							}
						}
					}
				}
				while (false);
				do {
					var constructor;
					try {
						constructor = target.getConstructor(vjo.Object.clazz);
					}
					catch (e) {
						break;
					}
					setAccessible(constructor);
					try {
						return constructor.newInstance(org.eclipse.vjet.vjo.java.lang.StringUtil.trim(value2));
					}
					catch (e) {
						if (e instanceof this.vj$.IllegalAccessException) {
							return null;
						}
						else if (e instanceof this.vj$.InvocationTargetException) {
							return null;
						}
						else if (e instanceof this.vj$.InstantiationException) {
							return null;
						}
					}
				}
				while (false);
				return null;
			},

			setAccessible : function(accessible) {
				if (!accessible.isAccessible()) {
					this.vj$.AccessController.doPrivileged(new this.vj$.FrameworkUtil.SetAccessibleAction(accessible));
				}
			},

			approxString : function(input) {
				var changed = false;
				var output = org.eclipse.vjet.vjo.java.lang.StringUtil.toCharArray(input);
				var cursor = 0;
				for (var c, _$i16 = 0; _$i16 < output.length; _$i16++) {
					c = output[_$i16];
					if (this.vj$.Character.isWhitespace(c)) {
						changed = true;
						continue;
					}
					output[cursor] = c;
					cursor++;
				}
				return changed ? org.eclipse.vjet.vjo.java.lang.StringFactory.build(output, 0, cursor) : input;
			}
		})
		
		.protos({
		
			op : 0,
			
			attr : null,
			
			value : null,
			
			filterString : null,

			constructs : function(operation, attr, value) {
				this.op = operation;
				this.attr = attr;
				this.value = value;
				this.filterString = null;
			},

			match : function(reference) {
				if (arguments.length === 1) {
					if (osgi.framework.ServiceReference.clazz.isInstance(arguments[0])) {
						return this.match_1_0_FilterImpl_ovld(arguments[0]);
					}
					else if (arguments[0] instanceof java.util.Dictionary) {
						return this.match_1_1_FilterImpl_ovld(arguments[0]);
					}
				}
			},

			match_1_0_FilterImpl_ovld : function(reference) {
				return matches(new this.vj$.FrameworkUtil.ServiceReferenceMap(reference));
			},

			match_1_1_FilterImpl_ovld : function(dictionary) {
				return matches(new this.vj$.FrameworkUtil.CaseInsensitiveMap(dictionary));
			},

			matchCase : function(dictionary) {
				switch (this.op) {
					case this.vj$.FilterImpl.AND: {
						var filters = this.value;
						for (var f, _$i0 = 0; _$i0 < filters.length; _$i0++) {
							f = filters[_$i0];
							if (!f.matchCase(dictionary)) {
								return false;
							}
						}
						return true;
					}
					case this.vj$.FilterImpl.OR: {
						var filters = this.value;
						for (var f, _$i1 = 0; _$i1 < filters.length; _$i1++) {
							f = filters[_$i1];
							if (f.matchCase(dictionary)) {
								return true;
							}
						}
						return false;
					}
					case this.vj$.FilterImpl.NOT: {
						var filter = this.value;
						return !filter.matchCase(dictionary);
					}
					case this.vj$.FilterImpl.SUBSTRING:
					case this.vj$.FilterImpl.EQUAL:
					case this.vj$.FilterImpl.GREATER:
					case this.vj$.FilterImpl.LESS:
					case this.vj$.FilterImpl.APPROX: {
						var prop = (dictionary === null) ? null : dictionary.get(this.attr);
						return this.compare(this.op, prop, this.value);
					}
					case this.vj$.FilterImpl.PRESENT: {
						var prop = (dictionary === null) ? null : dictionary.get(this.attr);
						return prop !== null;
					}
				}
				return false;
			},

			matches : function(map) {
				switch (this.op) {
					case this.vj$.FilterImpl.AND: {
						var filters = this.value;
						for (var f, _$i2 = 0; _$i2 < filters.length; _$i2++) {
							f = filters[_$i2];
							if (!f.matches(map)) {
								return false;
							}
						}
						return true;
					}
					case this.vj$.FilterImpl.OR: {
						var filters = this.value;
						for (var f, _$i3 = 0; _$i3 < filters.length; _$i3++) {
							f = filters[_$i3];
							if (f.matches(map)) {
								return true;
							}
						}
						return false;
					}
					case this.vj$.FilterImpl.NOT: {
						var filter = this.value;
						return !filter.matches(map);
					}
					case this.vj$.FilterImpl.SUBSTRING:
					case this.vj$.FilterImpl.EQUAL:
					case this.vj$.FilterImpl.GREATER:
					case this.vj$.FilterImpl.LESS:
					case this.vj$.FilterImpl.APPROX: {
						var prop = (map === null) ? null : map.get(this.attr);
						return this.compare(this.op, prop, this.value);
					}
					case this.vj$.FilterImpl.PRESENT: {
						var prop = (map === null) ? null : map.get(this.attr);
						return prop !== null;
					}
				}
				return false;
			},

			toString : function() {
				var result = this.filterString;
				if (result === null) {
					this.filterString = result = this.normalize().toString();
				}
				return result;
			},

			normalize : function() {
				var sb = new this();
				sb.append('(');
				switch (this.op) {
					case this.vj$.FilterImpl.AND: {
						sb.append('&');
						var filters = this.value;
						for (var f, _$i4 = 0; _$i4 < filters.length; _$i4++) {
							f = filters[_$i4];
							sb.append(f.normalize());
						}
						break;
					}
					case this.vj$.FilterImpl.OR: {
						sb.append('|');
						var filters = this.value;
						for (var f, _$i5 = 0; _$i5 < filters.length; _$i5++) {
							f = filters[_$i5];
							sb.append(f.normalize());
						}
						break;
					}
					case this.vj$.FilterImpl.NOT: {
						sb.append('!');
						var filter = this.value;
						sb.append(filter.normalize());
						break;
					}
					case this.vj$.FilterImpl.SUBSTRING: {
						sb.append(this.attr);
						sb.append('=');
						var substrings = this.value;
						for (var substr, _$i6 = 0; _$i6 < substrings.length; _$i6++) {
							substr = substrings[_$i6];
							if (substr === null) {
								sb.append('*');
							}
							else {
								sb.append(this.vj$.FilterImpl.encodeValue(substr));
							}
						}
						break;
					}
					case this.vj$.FilterImpl.EQUAL: {
						sb.append(this.attr);
						sb.append('=');
						sb.append(this.vj$.FilterImpl.encodeValue(/* >> */this.value));
						break;
					}
					case this.vj$.FilterImpl.GREATER: {
						sb.append(this.attr);
						sb.append(">=");
						sb.append(this.vj$.FilterImpl.encodeValue(/* >> */this.value));
						break;
					}
					case this.vj$.FilterImpl.LESS: {
						sb.append(this.attr);
						sb.append("<=");
						sb.append(this.vj$.FilterImpl.encodeValue(/* >> */this.value));
						break;
					}
					case this.vj$.FilterImpl.APPROX: {
						sb.append(this.attr);
						sb.append("~=");
						sb.append(this.vj$.FilterImpl.encodeValue(this.vj$.FilterImpl.approxString(/* >> */this.value)));
						break;
					}
					case this.vj$.FilterImpl.PRESENT: {
						sb.append(this.attr);
						sb.append("=*");
						break;
					}
				}
				sb.append(')');
				return sb;
			},

			equals : function(obj) {
				if (obj === this) {
					return true;
				}
				if (!(osgi.framework.Filter.isInstance(obj))) {
					return false;
				}
				return vjo.java.lang.ObjectUtil.equals(this.toString(), obj.toString());
			},

			hashCode : function() {
				return vjo.java.lang.ObjectUtil.hashCode(this.toString());
			},

			compare : function(operation, value1, value2) {
				if (value1 === null) {
					return false;
				}
				if (value1 instanceof String) {
					return this.compare_String(operation,/* >> */value1, value2);
				}
				var clazz = value1.getClass();
				if (clazz.isArray()) {
					var type = clazz.getComponentType();
					if (type.isPrimitive()) {
						return this.compare_PrimitiveArray(operation, type, value1, value2);
					}
					return this.compare_ObjectArray(operation,/* >> */value1, value2);
				}
				if (vjo.getType('org.eclipse.vjet.vjo.java.util.Collection').isInstance(value1)) {
					return this.compare_Collection(operation,/* >> */value1, value2);
				}
				if (vjo.getType('org.eclipse.vjet.vjo.java.lang.Integer').isInstance(value1)) {
					return this.compare_Integer(operation, value1.intValue(), value2);
				}
				if (vjo.getType('org.eclipse.vjet.vjo.java.lang.Long').isInstance(value1)) {
					return this.compare_Long(operation, value1.longValue(), value2);
				}
				if (vjo.getType('org.eclipse.vjet.vjo.java.lang.Byte').isInstance(value1)) {
					return this.compare_Byte(operation, value1.byteValue(), value2);
				}
				if (vjo.getType('org.eclipse.vjet.vjo.java.lang.Short').isInstance(value1)) {
					return this.compare_Short(operation, value1.shortValue(), value2);
				}
				if (vjo.getType('org.eclipse.vjet.vjo.java.lang.Character').isInstance(value1)) {
					return compare_Character(operation, value1.charValue(), value2);
				}
				if (vjo.getType('org.eclipse.vjet.vjo.java.lang.Float').isInstance(value1)) {
					return this.compare_Float(operation, value1.floatValue(), value2);
				}
				if (vjo.getType('org.eclipse.vjet.vjo.java.lang.Double').isInstance(value1)) {
					return this.compare_Double(operation, value1.doubleValue(), value2);
				}
				if (value1 instanceof Boolean) {
					return this.compare_Boolean(operation, org.eclipse.vjet.vjo.java.lang.BooleanUtil.booleanValue(value1), value2);
				}
				if (Comparable.isInstance(value1)) {
					var comparable = value1;
					return this.compare_Comparable(operation, comparable, value2);
				}
				return this.compare_Unknown(operation, value1, value2);
			},

			compare_Collection : function(operation, collection, value2) {
				for (var value1, _$itr = collection.iterator(); _$itr.hasNext();) {
					value1 = _$itr.next();
					if (this.compare(operation, value1, value2)) {
						return true;
					}
				}
				return false;
			},

			compare_ObjectArray : function(operation, array, value2) {
				for (var value1, _$i7 = 0; _$i7 < array.length; _$i7++) {
					value1 = array[_$i7];
					if (this.compare(operation, value1, value2)) {
						return true;
					}
				}
				return false;
			},

			compare_PrimitiveArray : function(operation, type, primarray, value2) {
				if (this.vj$.Integer.TYPE.isAssignableFrom(type)) {
					var array = primarray;
					for (var value1, _$i8 = 0; _$i8 < array.length; _$i8++) {
						value1 = array[_$i8];
						if (this.compare_Integer(operation, value1, value2)) {
							return true;
						}
					}
					return false;
				}
				if (this.vj$.Long.TYPE.isAssignableFrom(type)) {
					var array = primarray;
					for (var value1, _$i9 = 0; _$i9 < array.length; _$i9++) {
						value1 = array[_$i9];
						if (this.compare_Long(operation, value1, value2)) {
							return true;
						}
					}
					return false;
				}
				if (this.vj$.Byte.TYPE.isAssignableFrom(type)) {
					var array = primarray;
					for (var value1, _$i10 = 0; _$i10 < array.length; _$i10++) {
						value1 = array[_$i10];
						if (this.compare_Byte(operation, value1, value2)) {
							return true;
						}
					}
					return false;
				}
				if (this.vj$.Short.TYPE.isAssignableFrom(type)) {
					var array = primarray;
					for (var value1, _$i11 = 0; _$i11 < array.length; _$i11++) {
						value1 = array[_$i11];
						if (this.compare_Short(operation, value1, value2)) {
							return true;
						}
					}
					return false;
				}
				if (this.vj$.Character.TYPE.isAssignableFrom(type)) {
					var array = primarray;
					for (var value1, _$i12 = 0; _$i12 < array.length; _$i12++) {
						value1 = array[_$i12];
						if (this.compare_Character(operation, value1, value2)) {
							return true;
						}
					}
					return false;
				}
				if (this.vj$.Float.TYPE.isAssignableFrom(type)) {
					var array = primarray;
					for (var value1, _$i13 = 0; _$i13 < array.length; _$i13++) {
						value1 = array[_$i13];
						if (this.compare_Float(operation, value1, value2)) {
							return true;
						}
					}
					return false;
				}
				if (this.vj$.Double.TYPE.isAssignableFrom(type)) {
					var array = primarray;
					for (var value1, _$i14 = 0; _$i14 < array.length; _$i14++) {
						value1 = array[_$i14];
						if (this.compare_Double(operation, value1, value2)) {
							return true;
						}
					}
					return false;
				}
				if (org.eclipse.vjet.vjo.java.lang.BooleanUtil.TRUE.isAssignableFrom(type)) {
					var array = primarray;
					for (var value1, _$i15 = 0; _$i15 < array.length; _$i15++) {
						value1 = array[_$i15];
						if (this.compare_Boolean(operation, value1, value2)) {
							return true;
						}
					}
					return false;
				}
				return false;
			},

			compare_String : function(operation, string, value2) {
				switch (operation) {
					case this.vj$.FilterImpl.SUBSTRING: {
						var substrings = value2;
						var pos = 0;
						for (var i = 0, size = substrings.length; i < size; i++) {
							var substr = substrings[i];
							if (i + 1 < size) {
								if (substr === null) {
									var substr2 = substrings[i + 1];
									if (substr2 === null) {
										continue;
									}
									var index = string.indexOf(substr2, pos);
									if (index === -1) {
										return false;
									}
									pos = index + substr2.length;
									if (i + 2 < size) {
										i++;
									}
								}
								else {
									var len = substr.length;
									if (org.eclipse.vjet.vjo.java.lang.StringUtil.regionMatches(string, pos, substr, 0, len)) {
										pos += len;
									}
									else {
										return false;
									}
								}
							}
							else {
								if (substr === null) {
									return true;
								}
								return org.eclipse.vjet.vjo.java.lang.StringUtil.endsWith(string, substr);
							}
						}
						return true;
					}
					case this.vj$.FilterImpl.EQUAL: {
						return vjo.java.lang.ObjectUtil.equals(string, value2);
					}
					case this.vj$.FilterImpl.APPROX: {
						string = this.vj$.FilterImpl.approxString(string);
						var string2 = this.vj$.FilterImpl.approxString(/* >> */value2);
						return org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(string, string2);
					}
					case this.vj$.FilterImpl.GREATER: {
						return org.eclipse.vjet.vjo.java.lang.StringUtil.compareTo(string,/* >> */value2) >= 0;
					}
					case this.vj$.FilterImpl.LESS: {
						return org.eclipse.vjet.vjo.java.lang.StringUtil.compareTo(string,/* >> */value2) <= 0;
					}
				}
				return false;
			},

			compare_Integer : function(operation, intval, value2) {
				if (operation === this.vj$.FilterImpl.SUBSTRING) {
					return false;
				}
				var intval2;
				try {
					intval2 = this.vj$.Integer.parseInt(org.eclipse.vjet.vjo.java.lang.StringUtil.trim(value2));
				}
				catch (e) {
					return false;
				}
				switch (operation) {
					case this.vj$.FilterImpl.APPROX:
					case this.vj$.FilterImpl.EQUAL: {
						return intval === intval2;
					}
					case this.vj$.FilterImpl.GREATER: {
						return intval >= intval2;
					}
					case this.vj$.FilterImpl.LESS: {
						return intval <= intval2;
					}
				}
				return false;
			},

			compare_Long : function(operation, longval, value2) {
				if (operation === this.vj$.FilterImpl.SUBSTRING) {
					return false;
				}
				var longval2;
				try {
					longval2 = this.vj$.Long.parseLong(org.eclipse.vjet.vjo.java.lang.StringUtil.trim(value2));
				}
				catch (e) {
					return false;
				}
				switch (operation) {
					case this.vj$.FilterImpl.APPROX:
					case this.vj$.FilterImpl.EQUAL: {
						return longval === longval2;
					}
					case this.vj$.FilterImpl.GREATER: {
						return longval >= longval2;
					}
					case this.vj$.FilterImpl.LESS: {
						return longval <= longval2;
					}
				}
				return false;
			},

			compare_Byte : function(operation, byteval, value2) {
				if (operation === this.vj$.FilterImpl.SUBSTRING) {
					return false;
				}
				var byteval2;
				try {
					byteval2 = this.vj$.Byte.parseByte(org.eclipse.vjet.vjo.java.lang.StringUtil.trim(value2));
				}
				catch (e) {
					return false;
				}
				switch (operation) {
					case this.vj$.FilterImpl.APPROX:
					case this.vj$.FilterImpl.EQUAL: {
						return byteval === byteval2;
					}
					case this.vj$.FilterImpl.GREATER: {
						return byteval >= byteval2;
					}
					case this.vj$.FilterImpl.LESS: {
						return byteval <= byteval2;
					}
				}
				return false;
			},

			compare_Short : function(operation, shortval, value2) {
				if (operation === this.vj$.FilterImpl.SUBSTRING) {
					return false;
				}
				var shortval2;
				try {
					shortval2 = this.vj$.Short.parseShort(org.eclipse.vjet.vjo.java.lang.StringUtil.trim(value2));
				}
				catch (e) {
					return false;
				}
				switch (operation) {
					case this.vj$.FilterImpl.APPROX:
					case this.vj$.FilterImpl.EQUAL: {
						return shortval === shortval2;
					}
					case this.vj$.FilterImpl.GREATER: {
						return shortval >= shortval2;
					}
					case this.vj$.FilterImpl.LESS: {
						return shortval <= shortval2;
					}
				}
				return false;
			},

			compare_Character : function(operation, charval, value2) {
				if (operation === this.vj$.FilterImpl.SUBSTRING) {
					return false;
				}
				var charval2;
				try {
					charval2 = value2.charAt(0);
				}
				catch (e) {
					return false;
				}
				switch (operation) {
					case this.vj$.FilterImpl.EQUAL: {
						return charval === charval2;
					}
					case this.vj$.FilterImpl.APPROX: {
						return (charval === charval2) || (this.vj$.Character.toUpperCase(charval) === this.vj$.Character.toUpperCase(charval2)) || (this.vj$.Character.toLowerCase(charval) === this.vj$.Character.toLowerCase(charval2));
					}
					case this.vj$.FilterImpl.GREATER: {
						return charval >= charval2;
					}
					case this.vj$.FilterImpl.LESS: {
						return charval <= charval2;
					}
				}
				return false;
			},

			compare_Boolean : function(operation, boolval, value2) {
				if (operation === this.vj$.FilterImpl.SUBSTRING) {
					return false;
				}
				var boolval2 = org.eclipse.vjet.vjo.java.lang.BooleanUtil.booleanValue(org.eclipse.vjet.vjo.java.lang.BooleanUtil.valueOf_(org.eclipse.vjet.vjo.java.lang.StringUtil.trim(value2)));
				switch (operation) {
					case this.vj$.FilterImpl.APPROX:
					case this.vj$.FilterImpl.EQUAL:
					case this.vj$.FilterImpl.GREATER:
					case this.vj$.FilterImpl.LESS: {
						return boolval === boolval2;
					}
				}
				return false;
			},

			compare_Float : function(operation, floatval, value2) {
				if (operation === this.vj$.FilterImpl.SUBSTRING) {
					return false;
				}
				var floatval2;
				try {
					floatval2 = this.vj$.Float.parseFloat(org.eclipse.vjet.vjo.java.lang.StringUtil.trim(value2));
				}
				catch (e) {
					return false;
				}
				switch (operation) {
					case this.vj$.FilterImpl.APPROX:
					case this.vj$.FilterImpl.EQUAL: {
						return this.vj$.Float.compare(floatval, floatval2) === 0;
					}
					case this.vj$.FilterImpl.GREATER: {
						return this.vj$.Float.compare(floatval, floatval2) >= 0;
					}
					case this.vj$.FilterImpl.LESS: {
						return this.vj$.Float.compare(floatval, floatval2) <= 0;
					}
				}
				return false;
			},

			compare_Double : function(operation, doubleval, value2) {
				if (operation === this.vj$.FilterImpl.SUBSTRING) {
					return false;
				}
				var doubleval2;
				try {
					doubleval2 = this.vj$.Double.parseDouble(org.eclipse.vjet.vjo.java.lang.StringUtil.trim(value2));
				}
				catch (e) {
					return false;
				}
				switch (operation) {
					case this.vj$.FilterImpl.APPROX:
					case this.vj$.FilterImpl.EQUAL: {
						return this.vj$.Double.compare(doubleval, doubleval2) === 0;
					}
					case this.vj$.FilterImpl.GREATER: {
						return this.vj$.Double.compare(doubleval, doubleval2) >= 0;
					}
					case this.vj$.FilterImpl.LESS: {
						return this.vj$.Double.compare(doubleval, doubleval2) <= 0;
					}
				}
				return false;
			},

			compare_Comparable : function(operation, value1, value2) {
				if (operation === this.vj$.FilterImpl.SUBSTRING) {
					return false;
				}
				value2 = valueOf(value1.getClass(),/* >> */value2);
				if (value2 === null) {
					return false;
				}
				try {
					switch (operation) {
						case this.vj$.FilterImpl.APPROX:
						case this.vj$.FilterImpl.EQUAL: {
							return value1.compareTo(value2) === 0;
						}
						case this.vj$.FilterImpl.GREATER: {
							return value1.compareTo(value2) >= 0;
						}
						case this.vj$.FilterImpl.LESS: {
							return value1.compareTo(value2) <= 0;
						}
					}
				}
				catch (e) {
					return false;
				}
				return false;
			},

			compare_Unknown : function(operation, value1, value2) {
				if (operation === this.vj$.FilterImpl.SUBSTRING) {
					return false;
				}
				value2 = valueOf(value1.getClass(),/* >> */value2);
				if (value2 === null) {
					return false;
				}
				try {
					switch (operation) {
						case this.vj$.FilterImpl.APPROX:
						case this.vj$.FilterImpl.EQUAL:
						case this.vj$.FilterImpl.GREATER:
						case this.vj$.FilterImpl.LESS: {
							return org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(value1, value2);
						}
					}
				}
				catch (e) {
					return false;
				}
				return false;
			}
		
		})
		
		.endType(),
		
		CaseInsensitiveMap : vjo.ctype()
		
		.inherits('java.util.AbstractMap<String,Object>')
		
		.satisfies('org.eclipse.vjet.vjo.java.util.Map<String,Object>')
		
		.protos({
		
			dictionary : null,
			
			keys : null,

			constructs : function(dictionary) {
				this.base();
				if (dictionary === null) {
					this.dictionary = null;
					this.keys = vjo.createArray(null, 0);
					return;
				}
				this.dictionary = dictionary;
				var keyList = new ArrayList(dictionary.size());
				for (var e = dictionary.keys(); e.hasMoreElements();) {
					var k = e.nextElement();
					if (k instanceof String) {
						var key = k;
						for (var i, _$itr = keyList.iterator(); _$itr.hasNext();) {
							i = _$itr.next();
							if (org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(key, i)) {
								throw new IllegalArgumentException();
							}
						}
						keyList.add(key);
					}
				}
				this.keys = keyList.toArray(vjo.createArray(null, keyList.size()));
			},

			get : function(o) {
				var k = o;
				for (var key, _$i0 = 0; _$i0 < this.keys.length; _$i0++) {
					key = this.keys[_$i0];
					if (org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(key, k)) {
						return this.dictionary.get(key);
					}
				}
				return null;
			},

			entrySet : function() {
				throw new UnsupportedOperationException();
			}
			
		})
		.endType(),
		
		ServiceReferenceMap : vjo.ctype().inherits(""java.util.AbstractMap<String,Object>")
		
		.satisfies("org.eclipse.vjet.vjo.java.util.Map<String,Object>")
		
		.protos({
		
			reference : null,

			constructs : function(reference) {
				this.base();
				this.reference = reference;
			},

			get : function(key) {
				if (this.reference === null) {
					return null;
				}
				return this.reference.getProperty(/* >> */key);
			},

			entrySet : function() {
				throw new UnsupportedOperationException();
			}
		}).endType(),
		SetAccessibleAction : vjo.ctype().satisfies('java.security.PrivilegedAction<Object>').protos({
			accessible : null,

			constructs : function(accessible) {
				this.accessible = accessible;
			},

			run : function() {
				this.accessible.setAccessible(true);
				return null;
			}
		
		})
		
		.endType(),
		
		DNChainMatching : vjo.ctype()
		
		.props({
		
			MINUS_WILDCARD : "-",
			
			STAR_WILDCARD : "*",

			rdnmatch : function(rdn, rdnPattern) {
				if (rdn.size() !== rdnPattern.size()) {
					return false;
				}
				for (var i = 0; i < rdn.size(); i++) {
					var rdnNameValue = rdn.get(i);
					var patNameValue = rdnPattern.get(i);
					var rdnNameEnd = rdnNameValue.indexOf('=');
					var patNameEnd = patNameValue.indexOf('=');
					if (rdnNameEnd !== patNameEnd || !org.eclipse.vjet.vjo.java.lang.StringUtil.regionMatches(rdnNameValue, 0, patNameValue, 0, rdnNameEnd)) {
						return false;
					}
					var patValue = patNameValue.substring(patNameEnd);
					var rdnValue = rdnNameValue.substring(rdnNameEnd);
					if (!vjo.java.lang.ObjectUtil.equals(rdnValue, patValue) && !vjo.java.lang.ObjectUtil.equals(patValue, "=*") && !vjo.java.lang.ObjectUtil.equals(patValue, "=#16012a")) {
						return false;
					}
				}
				return true;
			},

			dnmatch : function(dn, dnPattern) {
				var dnStart = 0;
				var patStart = 0;
				var patLen = dnPattern.size();
				if (patLen === 0) {
					return false;
				}
				if (dnPattern.get(0).equals(this.STAR_WILDCARD)) {
					patStart = 1;
					patLen--;
				}
				if (dn.size() < patLen) {
					return false;
				}
				else {
					if (dn.size() > patLen) {
						if (!dnPattern.get(0).equals(this.STAR_WILDCARD)) {
							return false;
						}
						dnStart = dn.size() - patLen;
					}
				}
				for (var i = 0; i < patLen; i++) {
					if (!this.rdnmatch(dn.get(i + dnStart), dnPattern.get(i + patStart))) {
						return false;
					}
				}
				return true;
			},

			parseDNchainPattern : function(dnChain) {
				if (dnChain === null) {
					throw new IllegalArgumentException("The DN chain must not be null.");
				}
				var parsed = new ArrayList();
				var startIndex = 0;
				startIndex = this.skipSpaces(dnChain, startIndex);
				while (startIndex < dnChain.length) {
					var endIndex = startIndex;
					var inQuote = false;
					out: while (endIndex < dnChain.length) {
						var c = dnChain.charAt(endIndex);
						switch (c) {
							case '"':
								inQuote = !inQuote;
								break;
							case '\\':
								endIndex++;
								break;
							case ';':
								if (!inQuote) {
									break out;
								}
						}
						endIndex++;
					}
					if (endIndex > dnChain.length) {
						throw new IllegalArgumentException("unterminated escape");
					}
					parsed.add(dnChain.substring(startIndex, endIndex));
					startIndex = endIndex + 1;
					startIndex = this.skipSpaces(dnChain, startIndex);
				}
				for (var i = 0; i < parsed.size(); i++) {
					var dn = parsed.get(i);
					if (vjo.java.lang.ObjectUtil.equals(dn, this.STAR_WILDCARD) || vjo.java.lang.ObjectUtil.equals(dn, this.MINUS_WILDCARD)) {
						continue;
					}
					var rdns = new ArrayList();
					if (dn.charAt(0) === '*') {
						if (dn.charAt(1) !== ',') {
							throw new IllegalArgumentException("invalid wildcard prefix");
						}
						rdns.add(this.STAR_WILDCARD);
						dn = (new this.vj$.X500Principal(dn.substring(2))).getName(this.vj$.X500Principal.CANONICAL);
					}
					else {
						dn = (new this.vj$.X500Principal(dn)).getName(this.vj$.X500Principal.CANONICAL);
					}
					this.parseDN(dn, rdns);
					parsed.set(i, rdns);
				}
				if (parsed.size() === 0) {
					throw new IllegalArgumentException("empty DN chain");
				}
				return parsed;
			},

			parseDNchain : function(chain) {
				if (chain === null) {
					throw new IllegalArgumentException("DN chain must not be null.");
				}
				var result = new ArrayList(chain.size());
				for (var dn, _$itr = chain.iterator(); _$itr.hasNext();) {
					dn = _$itr.next();
					dn = (new this.vj$.X500Principal(dn)).getName(this.vj$.X500Principal.CANONICAL);
					var rdns = new ArrayList();
					this.parseDN(dn, rdns);
					result.add(rdns);
				}
				if (result.size() === 0) {
					throw new IllegalArgumentException("empty DN chain");
				}
				return result;
			},

			skipSpaces : function(dnChain, startIndex) {
				while (startIndex < dnChain.length && dnChain.charAt(startIndex) === ' ') {
					startIndex++;
				}
				return startIndex;
			},

			parseDN : function(dn, rdn) {
				var startIndex = 0;
				var c = '\0';
				var nameValues = new ArrayList();
				while (startIndex < dn.length) {
					var endIndex;
					for (endIndex = startIndex; endIndex < dn.length; endIndex++) {
						c = dn.charAt(endIndex);
						if (c === ',' || c === '+') {
							break;
						}
						if (c === '\\') {
							endIndex++;
						}
					}
					if (endIndex > dn.length) {
						throw new IllegalArgumentException("unterminated escape " + dn);
					}
					nameValues.add(dn.substring(startIndex, endIndex));
					if (c !== '+') {
						rdn.add(nameValues);
						if (endIndex !== dn.length) {
							nameValues = new ArrayList();
						}
						else {
							nameValues = null;
						}
					}
					startIndex = endIndex + 1;
				}
				if (nameValues !== null) {
					throw new IllegalArgumentException("improperly terminated DN " + dn);
				}
			},

			skipWildCards : function(dnChainPattern, dnChainPatternIndex) {
				var i;
				for (i = dnChainPatternIndex; i < dnChainPattern.size(); i++) {
					var dnPattern = dnChainPattern.get(i);
					if (dnPattern instanceof String) {
						if (!org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(dnPattern, this.STAR_WILDCARD) && !org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(dnPattern, this.MINUS_WILDCARD)) {
							throw new IllegalArgumentException("expected wildcard in DN pattern");
						}
					}
					else {
						if (vjo.getType('org.eclipse.vjet.vjo.java.util.List').isInstance(dnPattern)) {
							break;
						}
						else {
							throw new IllegalArgumentException("expected String or List in DN Pattern");
						}
					}
				}
				return i;
			},

			dnChainMatch : function(dnChain, dnChainIndex, dnChainPattern, dnChainPatternIndex) {
				if (dnChainIndex >= dnChain.size()) {
					return false;
				}
				if (dnChainPatternIndex >= dnChainPattern.size()) {
					return false;
				}
				var dnPattern = dnChainPattern.get(dnChainPatternIndex);
				if (dnPattern instanceof String) {
					if (!org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(dnPattern, this.STAR_WILDCARD) && !org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(dnPattern, this.MINUS_WILDCARD)) {
						throw new IllegalArgumentException("expected wildcard in DN pattern");
					}
					if (org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(dnPattern, this.MINUS_WILDCARD)) {
						dnChainPatternIndex = this.skipWildCards(dnChainPattern, dnChainPatternIndex);
					}
					else {
						dnChainPatternIndex++;
					}
					if (dnChainPatternIndex >= dnChainPattern.size()) {
						return org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(dnPattern, this.MINUS_WILDCARD) ? true : dnChain.size() - 1 === dnChainIndex;
					}
					if (org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(dnPattern, this.STAR_WILDCARD)) {
						return this.dnChainMatch(dnChain, dnChainIndex, dnChainPattern, dnChainPatternIndex) || this.dnChainMatch(dnChain, dnChainIndex + 1, dnChainPattern, dnChainPatternIndex);
					}
					for (var i = dnChainIndex; i < dnChain.size(); i++) {
						if (this.dnChainMatch(dnChain, i, dnChainPattern, dnChainPatternIndex)) {
							return true;
						}
					}
				}
				else {
					if (vjo.getType('org.eclipse.vjet.vjo.java.util.List').isInstance(dnPattern)) {
						do {
							if (!this.dnmatch(dnChain.get(dnChainIndex), dnPattern)) {
								return false;
							}
							dnChainIndex++;
							dnChainPatternIndex++;
							if ((dnChainIndex >= dnChain.size()) && (dnChainPatternIndex >= dnChainPattern.size())) {
								return true;
							}
							if (dnChainIndex >= dnChain.size()) {
								dnChainPatternIndex = this.skipWildCards(dnChainPattern, dnChainPatternIndex);
								return dnChainPatternIndex >= dnChainPattern.size();
							}
							if (dnChainPatternIndex >= dnChainPattern.size()) {
								return false;
							}
							dnPattern = dnChainPattern.get(dnChainPatternIndex);
							if (dnPattern instanceof String) {
								if (!org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(dnPattern, this.STAR_WILDCARD) && !org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(dnPattern, this.MINUS_WILDCARD)) {
									throw new IllegalArgumentException("expected wildcard in DN pattern");
								}
								return this.dnChainMatch(dnChain, dnChainIndex, dnChainPattern, dnChainPatternIndex);
							}
							else {
								if (!(vjo.getType('org.eclipse.vjet.vjo.java.util.List').isInstance(dnPattern))) {
									throw new IllegalArgumentException("expected String or List in DN Pattern");
								}
							}
						}
						while (true);
					}
					else {
						throw new IllegalArgumentException("expected String or List in DN Pattern");
					}
				}
				return false;
			},

			match : function(pattern, dnChain) {
				var parsedDNChain;
				var parsedDNPattern;
				try {
					parsedDNChain = this.parseDNchain(dnChain);
				}
				catch (e) {
					var iae = new IllegalArgumentException("Invalid DN chain: " + this.toString(dnChain));
					iae.initCause(e);
					throw iae;
				}
				try {
					parsedDNPattern = this.parseDNchainPattern(pattern);
				}
				catch (e) {
					var iae = new IllegalArgumentException("Invalid match pattern: " + pattern);
					iae.initCause(e);
					throw iae;
				}
				return this.dnChainMatch(parsedDNChain, 0, parsedDNPattern, 0);
			},

			toString : function(dnChain) {
				if (dnChain === null) {
					return null;
				}
				var sb = new StringBuffer();
				for (var iChain = dnChain.iterator(); iChain.hasNext();) {
					sb.append(iChain.next());
					if (iChain.hasNext()) {
						sb.append("; ");
					}
				}
				return sb.toString();
			}
			
		}).endType(),

		createFilter : function(filter) {
			return this.FilterImpl.newInstance(filter);
		},

		matchDistinguishedNameChain : function(matchPattern, dnChain) {
			return this.DNChainMatching.match(matchPattern, dnChain);
		},

		getBundle : function(classFromBundle) {
			var cl = this.vj$.AccessController.doPrivileged(vjo.make(this, this.vj$.PrivilegedAction).protos({
				run : function() {
					return classFromBundle.getClassLoader();
				}
			}).endType());
			if (osgi.framework.BundleReference.isInstance(cl)) {
				return cl.getBundle();
			}
			return null;
		}
	})
	
	.protos({

		constructs : function() {
		}
	
	})
	
	.endType();

})