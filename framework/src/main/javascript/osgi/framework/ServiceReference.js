define([ "vjo", "./Comparable" ], function(vjo, Comparable) {

	return vjo.itype("osgi.framework.ServiceReference")

	.inherits(Comparable).protos({

		getProperty : function(key) {
		},

		getPropertyKeys : function() {
		},

		getBundle : function() {
		},

		getUsingBundles : function() {
		},

		isAssignableTo : function(bundle, className) {
		},

		compareTo : function(reference) {
		}

	})

	.endType();

})