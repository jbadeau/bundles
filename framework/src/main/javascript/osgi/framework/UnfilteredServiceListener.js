define([ "vjo", "./ServiceListener" ], function(vjo, ServiceListener) {

	return vjo.itype("osgi.framework.UnfilteredServiceListener")

	.inherits(ServiceListener)

	.endType();

})