define([ "vjo", "./EventListener" ], function(vjo, EventListener) {

	return vjo.itype("osgi.framework.FrameworkListener")

	.inherits(EventListener)

	.protos({

		frameworkEvent : function(event) {
		}

	})

	.endType();

})