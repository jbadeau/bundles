define([ "vjo", "./EventObject" ], function(vjo, EventObject) {

	return vjo.ctype("osgi.framework.BundleEvent")

	.inherits(EventObject)

	.props({

		INSTALLED : 0x00000001,

		STARTED : 0x00000002,

		STOPPED : 0x00000004,

		UPDATED : 0x00000008,

		UNINSTALLED : 0x00000010,

		RESOLVED : 0x00000020,

		UNRESOLVED : 0x00000040,

		STARTING : 0x00000080,

		STOPPING : 0x00000100,

		LAZY_ACTIVATION : 0x00000200

	})

	.protos({

		bundle : null,

		type : 0,

		origin : null,

		constructs : function() {
			if (arguments.length === 3) {
				this.constructs_3_0_BundleEvent_ovld(arguments[0], arguments[1], arguments[2]);
			}
			else if (arguments.length === 2) {
				this.constructs_2_0_BundleEvent_ovld(arguments[0], arguments[1]);
			}
		},

		constructs_3_0_BundleEvent_ovld : function(type, bundle, origin) {
			this.base(bundle);
			if (origin === null) {
				throw new this.vj$.IllegalArgumentException("null origin");
			}
			this.bundle = bundle;
			this.type = type;
			this.origin = origin;
		},

		constructs_2_0_BundleEvent_ovld : function(type, bundle) {
			this.base(bundle);
			this.bundle = bundle;
			this.type = type;
			this.origin = bundle;
		},

		getBundle : function() {
			return this.bundle;
		},

		getType : function() {
			return this.type;
		},

		getOrigin : function() {
			return this.origin;
		},

		toString : function() {
			return getClass().getName() + "[type = " + this.type + ", source=" + source + "]";
		}

	})

	.endType();

})