define([ "vjo", "./ServiceListener" ], function(vjo, ServiceListener) {

	return vjo.itype("osgi.framework.AllServiceListener")

	.inherits(ServiceListener)
	
	.endType();

})