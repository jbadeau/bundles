define([ "vjo", "./BundleReference" ], function(vjo, BundleReference) {

	return vjo.itype("osgi.framework.BundleContext")

	.inherits(BundleReference)

	.protos({

		getProperty : function(key) {
		},

		installBundle : function(location, input) {
			if (arguments.length === 2) {
				if ((arguments[0] instanceof String || typeof arguments[0] == "string") && arguments[1] instanceof org.eclipse.vjet.vjo.java.io.InputStream) {
					return this.installBundle_2_0_BundleContext_ovld(arguments[0], arguments[1]);
				}
				else if (this.base && this.base.installBundle) {
					return this.base.installBundle.apply(this, arguments);
				}
			}
			else if (arguments.length === 1) {
				if (arguments[0] instanceof String || typeof arguments[0] == "string") {
					return this.installBundle_1_0_BundleContext_ovld(arguments[0]);
				}
				else if (this.base && this.base.installBundle) {
					return this.base.installBundle.apply(this, arguments);
				}
			}
			else if (this.base && this.base.installBundle) {
				return this.base.installBundle.apply(this, arguments);
			}
		},

		installBundle_2_0_BundleContext_ovld : function(location, input) {
		},

		installBundle_1_0_BundleContext_ovld : function(location) {
		},

		getBundle : function() {
			if (arguments.length === 0) {
				if (arguments.length == 0) {
					return this.getBundle_0_0_BundleContext_ovld();
				}
				else if (this.base && this.base.getBundle) {
					return this.base.getBundle.apply(this, arguments);
				}
			}
			else if (arguments.length === 1) {
				if (typeof arguments[0] == "number") {
					return this.getBundle_1_0_BundleContext_ovld(arguments[0]);
				}
				else if (arguments[0] instanceof String || typeof arguments[0] == "string") {
					return this.getBundle_1_1_BundleContext_ovld(arguments[0]);
				}
				else if (this.base && this.base.getBundle) {
					return this.base.getBundle.apply(this, arguments);
				}
			}
			else if (this.base && this.base.getBundle) {
				return this.base.getBundle.apply(this, arguments);
			}
		},

		getBundle_0_0_BundleContext_ovld : function() {
		},

		getBundle_1_0_BundleContext_ovld : function(id) {
		},

		getBundles : function() {
		},

		addServiceListener : function(listener, filter) {
			if (arguments.length === 2) {
				if (osgi.framework.ServiceListener.clazz.isInstance(arguments[0]) && (arguments[1] instanceof String || typeof arguments[1] == "string")) {
					this.addServiceListener_2_0_BundleContext_ovld(arguments[0], arguments[1]);
				}
				else if (this.base && this.base.addServiceListener) {
					this.base.addServiceListener.apply(this, arguments);
				}
			}
			else if (arguments.length === 1) {
				if (osgi.framework.ServiceListener.clazz.isInstance(arguments[0])) {
					this.addServiceListener_1_0_BundleContext_ovld(arguments[0]);
				}
				else if (this.base && this.base.addServiceListener) {
					this.base.addServiceListener.apply(this, arguments);
				}
			}
			else if (this.base && this.base.addServiceListener) {
				this.base.addServiceListener.apply(this, arguments);
			}
		},

		addServiceListener_2_0_BundleContext_ovld : function(listener, filter) {
		},

		addServiceListener_1_0_BundleContext_ovld : function(listener) {
		},

		removeServiceListener : function(listener) {
		},

		addBundleListener : function(listener) {
		},

		removeBundleListener : function(listener) {
		},

		addFrameworkListener : function(listener) {
		},

		removeFrameworkListener : function(listener) {
		},

		registerService : function(clazzes, service, properties) {
			if (arguments.length === 3) {
				if (arguments[0] instanceof Array && arguments[1] instanceof Object && arguments[2] instanceof java.util.Dictionary) {
					return this.registerService_3_0_BundleContext_ovld(arguments[0], arguments[1], arguments[2]);
				}
				else if ((arguments[0] instanceof String || typeof arguments[0] == "string") && arguments[1] instanceof Object && arguments[2] instanceof java.util.Dictionary) {
					return this.registerService_3_1_BundleContext_ovld(arguments[0], arguments[1], arguments[2]);
				}
				else if (arguments[0] instanceof vjo.Class && arguments[1] instanceof Object && arguments[2] instanceof java.util.Dictionary) {
					return this.registerService_3_2_BundleContext_ovld(arguments[0], arguments[1], arguments[2]);
				}
				else if (this.base && this.base.registerService) {
					return this.base.registerService.apply(this, arguments);
				}
			}
			else if (this.base && this.base.registerService) {
				return this.base.registerService.apply(this, arguments);
			}
		},

		registerService_3_0_BundleContext_ovld : function(clazzes, service, properties) {
		},

		registerService_3_1_BundleContext_ovld : function(clazz, service, properties) {
		},

		registerService_3_2_BundleContext_ovld : function(clazz, service, properties) {
		},

		getAllServiceReferences : function(clazz, filter) {
		},

		getServiceReference : function(clazz) {
			if (arguments.length === 1) {
				if (arguments[0] instanceof String || typeof arguments[0] == "string") {
					return this.getServiceReference_1_0_BundleContext_ovld(arguments[0]);
				}
				else if (arguments[0] instanceof vjo.Class) {
					return this.getServiceReference_1_1_BundleContext_ovld(arguments[0]);
				}
				else if (this.base && this.base.getServiceReference) {
					return this.base.getServiceReference.apply(this, arguments);
				}
			}
			else if (this.base && this.base.getServiceReference) {
				return this.base.getServiceReference.apply(this, arguments);
			}
		},

		getServiceReference_1_0_BundleContext_ovld : function(clazz) {
		},

		getServiceReference_1_1_BundleContext_ovld : function(clazz) {
		},

		getServiceReferences : function(clazz, filter) {
			if (arguments.length === 2) {
				if ((arguments[0] instanceof String || typeof arguments[0] == "string") && (arguments[1] instanceof String || typeof arguments[1] == "string")) {
					return this.getServiceReferences_2_0_BundleContext_ovld(arguments[0], arguments[1]);
				}
				else if (arguments[0] instanceof vjo.Class && (arguments[1] instanceof String || typeof arguments[1] == "string")) {
					return this.getServiceReferences_2_1_BundleContext_ovld(arguments[0], arguments[1]);
				}
				else if (this.base && this.base.getServiceReferences) {
					return this.base.getServiceReferences.apply(this, arguments);
				}
			}
			else if (this.base && this.base.getServiceReferences) {
				return this.base.getServiceReferences.apply(this, arguments);
			}
		},

		getServiceReferences_2_0_BundleContext_ovld : function(clazz, filter) {
		},

		getServiceReferences_2_1_BundleContext_ovld : function(clazz, filter) {
		},

		getService : function(reference) {
		},

		ungetService : function(reference) {
		},

		getDataFile : function(filename) {
		},

		createFilter : function(filter) {
		},

		getBundle_1_1_BundleContext_ovld : function(location) {
		}

	})

	.endType();

})