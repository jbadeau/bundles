define([ "vjo", "../Bundle" ], function(vjo, Bundle) {

	vjo.itype("osgi.framework.launch.Framework")

	.inherits(Bundle).protos({

		init : function() {
		},

		waitForStop : function(timeout) {
		},

		start : function() {
			if (arguments.length === 0) {
				if (arguments.length == 0) {
					this.start_0_0_Framework_ovld();
				}
				else if (this.base && this.base.start) {
					this.base.start.apply(this, arguments);
				}
			}
			else if (arguments.length === 1) {
				if (typeof arguments[0] == "number") {
					this.start_1_0_Framework_ovld(arguments[0]);
				}
				else if (this.base && this.base.start) {
					this.base.start.apply(this, arguments);
				}
			}
			else if (this.base && this.base.start) {
				this.base.start.apply(this, arguments);
			}
		},

		start_0_0_Framework_ovld : function() {
		},

		start_1_0_Framework_ovld : function(options) {
		},

		stop : function() {
			if (arguments.length === 0) {
				if (arguments.length == 0) {
					this.stop_0_0_Framework_ovld();
				}
				else if (this.base && this.base.stop) {
					this.base.stop.apply(this, arguments);
				}
			}
			else if (arguments.length === 1) {
				if (typeof arguments[0] == "number") {
					this.stop_1_0_Framework_ovld(arguments[0]);
				}
				else if (this.base && this.base.stop) {
					this.base.stop.apply(this, arguments);
				}
			}
			else if (this.base && this.base.stop) {
				this.base.stop.apply(this, arguments);
			}
		},

		stop_0_0_Framework_ovld : function() {
		},

		stop_1_0_Framework_ovld : function(options) {
		},

		uninstall : function() {
		},

		update : function() {
			if (arguments.length === 0) {
				if (arguments.length == 0) {
					this.update_0_0_Framework_ovld();
				}
				else if (this.base && this.base.update) {
					this.base.update.apply(this, arguments);
				}
			}
			else if (arguments.length === 1) {
				if (arguments[0] instanceof org.eclipse.vjet.vjo.java.io.InputStream) {
					this.update_1_0_Framework_ovld(arguments[0]);
				}
				else if (this.base && this.base.update) {
					this.base.update.apply(this, arguments);
				}
			}
			else if (this.base && this.base.update) {
				this.base.update.apply(this, arguments);
			}
		},

		update_0_0_Framework_ovld : function() {
		},

		update_1_0_Framework_ovld : function(in_) {
		},

		getBundleId : function() {
		},

		getLocation : function() {
		},

		getSymbolicName : function() {
		},

		getEntryPaths : function(path) {
		},

		getEntry : function(path) {
		},

		findEntries : function(path, filePattern, recurse) {
		},

		adapt : function(type) {
		}

	})

	.endType();

})