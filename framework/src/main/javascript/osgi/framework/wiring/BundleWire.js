define([ "vjo", '../../resource/Wire' ], function(vjo, Wire) {

	return vjo.itype("osgi.framework.wiring.BundleWire")

	.inherits(Wire)

	.protos({

		getCapability : function() {
		},

		getRequirement : function() {
		},

		getProviderWiring : function() {
		},

		getRequirerWiring : function() {
		},

		getProvider : function() {
		},

		getRequirer : function() {
		}

	}).endType();

})