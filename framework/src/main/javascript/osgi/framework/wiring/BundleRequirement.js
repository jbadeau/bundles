define([ "vjo", "osgi/resource/Requirement" ], function(vjo, Requirement) {

	return vjo.itype("osgi.framework.wiring.BundleRequirement")

	.inherits(Requirement)

	.protos({

		getRevision : function() {
		},

		matches : function(capability) {
		},

		getNamespace : function() {
		},

		getDirectives : function() {
		},

		getAttributes : function() {
		},

		getResource : function() {
		}

	})

	.endType();

})