define([ "vjo", "../../resource/Capability" ], function(vjo, Capability) {

	return vjo.itype("osgi.framework.wiring.BundleCapability")

	.inherits(Capability)
	
	.protos({

		getRevision : function() {
		},

		getNamespace : function() {
		},

		getDirectives : function() {
		},

		getAttributes : function() {
		},

		getResource : function() {
		}

	})

	.endType();

})