define([ "vjo", '../BundleReference', "../../resource/Wiring" ], function(vjo, BundleReference, Wiring) {

	return vjo.itype("osgi.framework.wiring.BundleWiring")

	.inherits([ BundleReference, Wiring ])

	.props({

		FINDENTRIES_RECURSE : 0x00000001,

		LISTRESOURCES_RECURSE : 0x00000001,

		LISTRESOURCES_LOCAL : 0x00000002

	})

	.protos({

		isCurrent : function() {
		},

		isInUse : function() {
		},

		getCapabilities : function(namespace) {
		},

		getRequirements : function(namespace) {
		},

		getProvidedWires : function(namespace) {
		},

		getRequiredWires : function(namespace) {
		},

		getRevision : function() {
		},

		getClassLoader : function() {
		},

		findEntries : function(path, filePattern, options) {
		},

		listResources : function(path, filePattern, options) {
		},

		getResourceCapabilities : function(namespace) {
		},

		getResourceRequirements : function(namespace) {
		},

		getProvidedResourceWires : function(namespace) {
		},

		getRequiredResourceWires : function(namespace) {
		},

		getResource : function() {
		}

	})

	.endType();

})