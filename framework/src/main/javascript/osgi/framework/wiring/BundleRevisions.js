define([ "vjo", "../BundleReference", ], function(vjo, BundleReference) {

	return vjo.itype("osgi.framework.wiring.BundleRevisions")

	.inherits(BundleReference)

	.protos({

		getRevisions : function() {
		}

	})

	.endType();

})