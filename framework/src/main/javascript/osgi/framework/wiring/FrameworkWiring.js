define([ "vjo", "../BundleReference" ], function(vjo, BundleReference) {

	return vjo.itype("osgi.framework.wiring.FrameworkWiring")

	.inherits(BundleReference)

	.protos({

		refreshBundles : function(bundles) {
			var listeners;
			if (arguments.length == 2 && arguments[1] instanceof Array) {
				listeners = arguments[1];
			}
			else {
				listeners = [];
				for (var i = 1; i < arguments.length; i++) {
					listeners[i - 1] = arguments[i];
				}
			}
		},

		resolveBundles : function(bundles) {
		},

		getRemovalPendingBundles : function() {
		},

		getDependencyClosure : function(bundles) {
		}

	})

	.endType();

})