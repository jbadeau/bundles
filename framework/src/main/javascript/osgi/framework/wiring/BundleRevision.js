define([ "vjo", "../BundleReference", "../../resource/Resource", "../namespace/PackageNamespace", "../namespace/BundleNamespace", "../namespace/HostNamespace" ], function(vjo, BundleReference, Resource, PackageNamespace, BundleNamespace, HostNamespace) {

	return vjo.itype("osgi.framework.wiring.BundleRevision")

	.inherits([ BundleReference, Resource ])

	.props({

		PACKAGE_NAMESPACE : null,

		BUNDLE_NAMESPACE : null,

		HOST_NAMESPACE : null,

		TYPE_FRAGMENT : 0x00000001

	})

	.protos({

		getSymbolicName : function() {
		},

		getVersion : function() {
		},

		getDeclaredCapabilities : function(namespace) {
		},

		getDeclaredRequirements : function(namespace) {
		},

		getTypes : function() {
		},

		getWiring : function() {
		},

		getCapabilities : function(namespace) {
		},

		getRequirements : function(namespace) {
		}

	})

	.inits(function() {
		this.vj$.BundleRevision.PACKAGE_NAMESPACE = PackageNamespace.PACKAGE_NAMESPACE;
		this.vj$.BundleRevision.BUNDLE_NAMESPACE = BundleNamespace.BUNDLE_NAMESPACE;
		this.vj$.BundleRevision.HOST_NAMESPACE = HostNamespace.HOST_NAMESPACE;
	})

	.endType();

})