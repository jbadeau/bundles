define([ "vjo", "./EventListener" ], function(vjo, EventListener) {

	return vjo.itype("osgi.framework.BundleListener")

	.inherits(EventListener)

	.protos({

		bundleChanged : function(event) {
		}

	})

	.endType();

})