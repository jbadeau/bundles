define([ "vjo", "../../framework/Exception" ], function(vjo, Exception) {

	return vjo.ctype("osgi.service.resolver.ResolutionException")

	.inherits(Exception)

	.props({

		emptyCollection : function() {
			return this.vj$.Collections.EMPTY_LIST;
		}

	})
	
	.protos({

		unresolvedRequirements : null,

		constructs : function() {
			if (arguments.length === 3) {
				this.constructs_3_0_ResolutionException_ovld(arguments[0], arguments[1], arguments[2]);
			}
			else if (arguments.length === 1) {
				if (arguments[0] instanceof String || typeof arguments[0] == "string") {
					this.constructs_1_0_ResolutionException_ovld(arguments[0]);
				}
				else if (arguments[0] instanceof vjo.java.lang.Throwable) {
					this.constructs_1_1_ResolutionException_ovld(arguments[0]);
				}
			}
		},

		constructs_3_0_ResolutionException_ovld : function(message, cause, unresolvedRequirements) {
			this.base(message, cause);
			if ((unresolvedRequirements === null) || unresolvedRequirements.isEmpty()) {
				this.unresolvedRequirements = this.vj$.ResolutionException.emptyCollection();
			}
			else {
				this.unresolvedRequirements = this.vj$.Collections.unmodifiableCollection(new this.vj$.ArrayList(unresolvedRequirements));
			}
		},

		constructs_1_0_ResolutionException_ovld : function(message) {
			this.base(message);
			this.unresolvedRequirements = this.vj$.ResolutionException.emptyCollection();
		},

		constructs_1_1_ResolutionException_ovld : function(cause) {
			this.base(cause);
			this.unresolvedRequirements = this.vj$.ResolutionException.emptyCollection();
		},

		getUnresolvedRequirements : function() {
			return this.unresolvedRequirements;
		}

	})

	.endType();
})