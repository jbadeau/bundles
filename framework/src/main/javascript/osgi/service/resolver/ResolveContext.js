define([ "vjo" ], function(vjo) {

	return vjo.ctype("osgi.service.resolver.ResolveContext")

	.props({

		emptyCollection : function() {
			return this.vj$.Collections.EMPTY_LIST;
		}

	})

	.protos({

		getMandatoryResources : function() {
			return this.vj$.ResolveContext.emptyCollection();
		},

		getOptionalResources : function() {
			return this.vj$.ResolveContext.emptyCollection();
		},

		findProviders : function(requirement) {
		},

		insertHostedCapability : function(capabilities, hostedCapability) {
		},

		isEffective : function(requirement) {
		},

		getWirings : function() {
		}

	})

	.endType();

})