define([ "vjo", "../../resource/Capability" ], function(vjo, Capability) {

	return vjo.itype("osgi.service.resolver.HostedCapability")

	.inherits(osgi.resource.Capability).protos({

		getResource : function() {
		},

		getDeclaredCapability : function() {
		}

	})

	.endType();

})