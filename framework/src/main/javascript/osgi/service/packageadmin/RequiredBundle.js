define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.service.packageadmin.RequiredBundle")

	.protos({

		getSymbolicName : function() {
		},

		getBundle : function() {
		},

		getRequiringBundles : function() {
		},

		getVersion : function() {
		},

		isRemovalPending : function() {
		}

	})

	.endType();

})