define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.service.packageadmin.ExportedPackage")

	.protos({

		getName : function() {
		},

		getExportingBundle : function() {
		},

		getImportingBundles : function() {
		},

		getSpecificationVersion : function() {
		},

		getVersion : function() {
		},

		isRemovalPending : function() {
		}

	})

	.endType();

})