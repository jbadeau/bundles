define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.service.packageadmin.PackageAdmin")

	.props({

		BUNDLE_TYPE_FRAGMENT : 0x00000001

	})

	.protos({

		getExportedPackages : function(bundle) {
			if (arguments.length === 1) {
				if (osgi.framework.Bundle.clazz.isInstance(arguments[0])) {
					return this.getExportedPackages_1_0_PackageAdmin_ovld(arguments[0]);
				}
				else if (arguments[0] instanceof String || typeof arguments[0] == "string") {
					return this.getExportedPackages_1_1_PackageAdmin_ovld(arguments[0]);
				}
			}
		},

		getExportedPackages_1_0_PackageAdmin_ovld : function(bundle) {
		},

		getExportedPackages_1_1_PackageAdmin_ovld : function(name) {
		},

		getExportedPackage : function(name) {
		},

		refreshPackages : function(bundles) {
		},

		resolveBundles : function(bundles) {
		},

		getRequiredBundles : function(symbolicName) {
		},

		getBundles : function(symbolicName, versionRange) {
		},

		getFragments : function(bundle) {
		},

		getHosts : function(bundle) {
		},

		getBundle : function(clazz) {
		},

		getBundleType : function(bundle) {
		}

	})

	.endType();

})