define([ "vjo", "../../framework/ServiceReference" ], function(vjo, ServiceReference) {

	return vjo.itype("osgi.service.log.LogService")

	.props({

		LOG_ERROR : 1,

		LOG_WARNING : 2,

		LOG_INFO : 3,

		LOG_DEBUG : 4

	})

	.protos({

		log : function(level, message) {
			if (arguments.length === 2) {
				this.log_2_0_LogService_ovld(arguments[0], arguments[1]);
			}
			else if (arguments.length === 3) {
				if (typeof arguments[0] == "number" && (arguments[1] instanceof String || typeof arguments[1] == "string") && arguments[2] instanceof vjo.java.lang.Throwable) {
					this.log_3_0_LogService_ovld(arguments[0], arguments[1], arguments[2]);
				}
				else if (osgi.framework.ServiceReference.clazz.isInstance(arguments[0]) && typeof arguments[1] == "number" && (arguments[2] instanceof String || typeof arguments[2] == "string")) {
					this.log_3_1_LogService_ovld(arguments[0], arguments[1], arguments[2]);
				}
			}
			else if (arguments.length === 4) {
				this.log_4_0_LogService_ovld(arguments[0], arguments[1], arguments[2], arguments[3]);
			}
		},

		log_2_0_LogService_ovld : function(level, message) {
		},

		log_3_0_LogService_ovld : function(level, message, exception) {
		},

		log_3_1_LogService_ovld : function(sr, level, message) {
		},

		log_4_0_LogService_ovld : function(sr, level, message, exception) {
		}

	})

	.endType();

})