define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.service.log.LogReaderService")

	.protos({

		addLogListener : function(listener) {
		},

		removeLogListener : function(listener) {
		},

		getLog : function() {
		}

	})

	.endType();

})