define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.service.log.LogEntry")

	.protos({

		getBundle : function() {
		},

		getServiceReference : function() {
		},

		getLevel : function() {
		},

		getMessage : function() {
		},

		getException : function() {
		},

		getTime : function() {
		}

	})

	.endType();

})