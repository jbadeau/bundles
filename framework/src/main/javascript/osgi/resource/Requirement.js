define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.resource.Requirement")

	.protos({

		getNamespace : function() {
		},

		getDirectives : function() {
		},

		getAttributes : function() {
		},

		getResource : function() {
		},

		equals : function(obj) {
		},

		hashCode : function() {
		}

	})

	.endType();

})