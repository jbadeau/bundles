define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.resource.Wire")

	.protos({

		getCapability : function() {
		},

		getRequirement : function() {
		},

		getProvider : function() {
		},

		getRequirer : function() {
		},

		equals : function(obj) {
		},

		hashCode : function() {
		}

	})

	.endType();

})