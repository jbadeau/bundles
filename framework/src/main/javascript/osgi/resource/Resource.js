define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.resource.Resource")

	.protos({

		getCapabilities : function(namespace) {
		},

		getRequirements : function(namespace) {
		},

		equals : function(obj) {
		},

		hashCode : function() {
		}

	})

	.endType();

})