define([ "vjo" ], function(vjo) {

	return vjo.itype("osgi.resource.Wiring")

	.protos({

		getResourceCapabilities : function(namespace) {
		},

		getResourceRequirements : function(namespace) {
		},

		getProvidedResourceWires : function(namespace) {
		},

		getRequiredResourceWires : function(namespace) {
		},

		getResource : function() {
		}

	})

	.endType();

})