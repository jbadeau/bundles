define([ "vjo" ], function(vjo) {

	return vjo.ctype("osgi.resource.Namespace")

	.props({

		CAPABILITY_USES_DIRECTIVE : "uses",

		CAPABILITY_EFFECTIVE_DIRECTIVE : "effective",

		REQUIREMENT_FILTER_DIRECTIVE : "filter",

		REQUIREMENT_RESOLUTION_DIRECTIVE : "resolution",

		RESOLUTION_MANDATORY : "mandatory",

		RESOLUTION_OPTIONAL : "optional",

		REQUIREMENT_EFFECTIVE_DIRECTIVE : "effective",

		EFFECTIVE_RESOLVE : "resolve",

		EFFECTIVE_ACTIVE : "active",

		REQUIREMENT_CARDINALITY_DIRECTIVE : "cardinality",

		CARDINALITY_MULTIPLE : "multiple",

		CARDINALITY_SINGLE : "single"

	})

	.protos({

		constructs : function() {
		}
	})

	.endType();

})