/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleClassLoader') 
.needs(['org.eclipse.vjet.vjo.java.util.ArrayList','vjo.java.lang.Throwable',
    'org.eclipse.vjet.vjo.java.util.HashSet','org.eclipse.vjet.vjo.java.util.Vector',
    'bundles.framework.BundleClassPath','org.eclipse.vjet.vjo.java.lang.Integer',
    'java.lang.ClassNotFoundException','java.lang.reflect.Proxy',
    'osgi.framework.Bundle','java.io.IOException',
    'bundles.framework.WeavingHooks','java.io.File',
    'org.eclipse.vjet.vjo.java.lang.System','bundles.framework.Util',
    'org.eclipse.vjet.vjo.java.lang.StringUtil'])
.needs('bundles.framework.Debug','')

.inherits('ClassLoader')
.satisfies('osgi.framework.BundleReference')
.props({
    ONLY_FIRST:1, 
    LIST:2, 
    ONLY_RECURSE:4, 
    RECURSE:256, 
    LOCAL:512, 
    tlBundlesToActivate:null, 
    dexFileClassLoadDexMethod:null, 
    dexFileClassLoadClassMethod:null, 
    bDalvik:false, 
    smex:null, 
    classSearch:null, 
    resourceSearch:null, 
    listSearch:null, 
    SecurityManagerExposer:vjo.ctype() 
    .inherits('SecurityManager')
    .protos({
        
        getClassContext:function(){
            return getClassContext();
        }
    })
    .endType(),
    SearchAction:vjo.itype() 
    .protos({
        
        get:function(items,path,name,pkg,options,requestor,cl){
        }
    })
    .endType(),
    
    isSystemBundle:function(bundle){
        return bundle===bundle.fwCtx.systemBundle;
    }
})
.protos({
    fwCtx:null, 
    secure:null, 
    protectionDomain:null, 
    archive:null, 
    bpkgs:null, 
    classPath:null, 
    dexFile:null, 
    debug:null, 
    
    constructs:function(gen){
        this.base(gen.bundle.fwCtx.parentClassLoader);
        this.fwCtx=gen.bundle.fwCtx;
        bundles.framework.Debug=this.fwCtx.debug;
        this.secure=this.fwCtx.perm;
        this.protectionDomain=gen.getProtectionDomain();
        this.bpkgs=gen.bpkgs;
        this.archive=gen.archive;
        this.classPath=new this.vj$.BundleClassPath(this.archive,gen.fragments,this.fwCtx);
        this.fwCtx.bundleClassLoaderCreated(this);
        if(this.classLoader){
            bundles.framework.Debug.println(this+" Created new classloader");
        }
    },
    
    findClass:function(name){
        if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(name,"java.")){
            return this.fwCtx.parentClassLoader.loadClass(name);
        }
        if(this.fwCtx.isBootDelegated(name)){
            try {
                var bootDelegationCls=this.fwCtx.parentClassLoader.loadClass(name); 
                if(this.classLoader&&bootDelegationCls!==null){
                    bundles.framework.Debug.println(this+" findClass: "+name+" boot delegation: "+bootDelegationCls);
                }
                return bootDelegationCls;
            }
            catch(e){
            }
        }
        var path; 
        var pkg; 
        var pos=name.lastIndexOf('.'); 
        if(pos!== -1){
            path=org.eclipse.vjet.vjo.java.lang.StringUtil.replace(name,'.','/');
            pkg=name.substring(0,pos);
        }else {
            path=name;
            pkg=null;
        }
        var res=this.secure.callSearchFor(this,name,pkg,path+".class",this.vj$.BundleClassLoader.classSearch,this.vj$.BundleClassLoader.ONLY_FIRST,this,null); 
        if(res!==null){
            return res;
        }
        if(!this.fwCtx.props.STRICTBOOTCLASSLOADING){
            if(this.isBootClassContext(name)){
                if(this.classLoader){
                    bundles.framework.Debug.println(this+" trying parent loader for class="+name+", since it was loaded on the system loader itself");
                }
                res=this.fwCtx.parentClassLoader.loadClass(name);
                if(res!==null){
                    if(this.classLoader){
                        bundles.framework.Debug.println(this+" loaded "+name+" from "+this.fwCtx.parentClassLoader);
                    }
                }
                return res;
            }
        }
        throw new this.vj$.ClassNotFoundException(name);
    },
    
    findLibrary:function(name){
        var res=this.secure.callFindLibrary0(this,name); 
        if(this.classLoader){
            bundles.framework.Debug.println(this+" Find library: "+name+(res!==null?" OK":" FAIL"));
        }
        return res;
    },
    
    findResources:function(name){
        return this.getBundleResources(name,false);
    },
    
    findResource:function(name){
        var res=this.getBundleResources(name,true); 
        if(res!==null){
            return res.nextElement();
        }else {
            return null;
        }
    },
    
    isNonBundleClass:function(cls){
        return (this.getClass().getClassLoader()!==cls.getClassLoader())&& !this.vj$.ClassLoader.clazz.isAssignableFrom(cls)&& !vjo.Class.clazz.equals(cls)&& !this.vj$.Proxy.clazz.equals(cls);
    },
    
    isBootClassContext:function(name){
        var classStack=this.vj$.BundleClassLoader.smex.getClassContext(); 
        if(classStack===null){
            try {
                var classNames=(new this.vj$.Throwable()).getStackTrace(); 
                classStack=vjo.createArray(null, classNames.length);
                for (var i=1;i<classNames.length;i++){
                    classStack[i]=vjo.Class.forName(classNames[i].getClassName());
                }
            }
            catch(e){
                return false;
            }
        }
        for (var i=1;i<classStack.length;i++){
            var currentCls=classStack[i]; 
            if(this.isNonBundleClass(currentCls)){
                var currentCL=currentCls.getClassLoader(); 
                for (var cl=currentCL;cl!==null && cl!==cl.getClass().getClassLoader();cl=cl.getClass().getClassLoader()){
                    if(this.vj$.BundleClassLoader.clazz.isInstance(cl)){
                        return false;
                    }
                }
                return !this.vj$.Bundle.clazz.isInstance(classStack[i-1]);
            }
        }
        return false;
    },
    
    loadClass:function(name,resolve){
        var c=findLoadedClass(name); 
        if(c===null){
            c=this.findClass(name);
        }else if(this.secure.getClassLoaderOf(c)===this){
            var b=this.getBundle(); 
            if(b.triggersActivationCls(name)){
                if(this.lazy_activation){
                    bundles.framework.Debug.println(this+" lazy activation of #"+b.id+" triggered by loadClass("+name+")");
                }
                var bundlesToActivate=this.vj$.BundleClassLoader.tlBundlesToActivate.get(); 
                if(null===bundlesToActivate){
                    if(this.lazy_activation){
                        bundles.framework.Debug.println(this+" requesting lazy activation of #"+b.id);
                    }
                    try {
                        this.secure.callFinalizeActivation(b);
                    }
                    catch(e){
                        this.fwCtx.frameworkError(b,e);
                    }
                }else {
                    var bundlePresent=false; 
                    for (var i=0,size=bundlesToActivate.size();i<size;i++){
                        var tmp=bundlesToActivate.get(i); 
                        if(tmp.id===b.id){
                            bundlePresent=true;
                            break;
                        }
                    }
                    if(!bundlePresent){
                        bundlesToActivate.add(b);
                        if(this.lazy_activation){
                            bundles.framework.Debug.println(this+" added #"+b.id+" to list of bundles to be activated.");
                        }
                    }
                }
            }
        }
        if(resolve){
            resolveClass(c);
        }
        return c;
    },
    
    getResource:function(name){
        if(this.classLoader){
            bundles.framework.Debug.println(this+" getResource: "+name);
        }
        var res=null; 
        if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(name,"java/")){
            res=this.fwCtx.parentClassLoader.getResource(name);
            if(this.classLoader){
                bundles.framework.Debug.println(this+" getResource: "+name+" file in java pkg: "+res);
            }
            return res;
        }
        if(this.fwCtx.isBootDelegatedResource(name)){
            res=this.fwCtx.parentClassLoader.getResource(name);
            if(res!==null){
                if(this.classLoader){
                    bundles.framework.Debug.println(this+" getResource: "+name+" boot delegation: "+res);
                }
                return res;
            }
        }
        res=this.findResource(name);
        if(this.classLoader){
            bundles.framework.Debug.println(this+" getResource: "+name+" bundle space: "+res);
        }
        return res;
    },
    
    getResourcesOSGi:function(name){
        if(this.classLoader){
            bundles.framework.Debug.println(this+" getResources: "+name);
        }
        var start=org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(name,"/")?1:0; 
        if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(name.substring(start),"java/")){
            return this.fwCtx.parentClassLoader.getResources(name);
        }
        var res=null; 
        if(this.fwCtx.isBootDelegatedResource(name)){
            res=this.fwCtx.parentClassLoader.getResources(name);
        }
        if(res===null|| !res.hasMoreElements()){
            res=this.findResources(name);
        }
        return res;
    },
    
    getResourceAsStream:function(name){
        try {
            var url=this.getResource(name); 
            if(url!==null){
                return url.openStream();
            }
        }
        catch(ignore){
        }
        return null;
    },
    
    toString:function(){
        return "BundleClassLoader("+"id="+this.bpkgs.bg.bundle.id+",gen="+this.bpkgs.bg.generation+")";
    },
    
    getBundle:function(){
        return this.bpkgs.bg.bundle;
    },
    
    close:function(){
        this.archive=null;
        this.fwCtx.bundleClassLoaderClosed(this);
        if(this.classLoader){
            bundles.framework.Debug.println(this+" Cleared archives");
        }
    },
    
    getBundleResources:function(name,onlyFirst){
        if(this.classLoader){
            bundles.framework.Debug.println(this+" Find bundle resource"+(onlyFirst?"":"s")+": "+name);
        }
        var pkg=null; 
        var pos=name.lastIndexOf('/'); 
        if(pos>0){
            var start=org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(name,"/")?1:0; 
            pkg=org.eclipse.vjet.vjo.java.lang.StringUtil.replace(name.substring(start,pos),'/','.');
        }else {
            pkg=null;
        }
        var res=this.secure.callSearchFor(this,null,pkg,name,this.vj$.BundleClassLoader.resourceSearch,onlyFirst?this.vj$.BundleClassLoader.ONLY_FIRST:0,this,null); 
        return res;
    },
    
    getBpkgs:function(){
        return this.bpkgs;
    },
    
    attachFragment:function(gen){
        if(this.classLoader){
            bundles.framework.Debug.println(this+" fragment attached update classpath");
        }
        this.classPath.attachFragment(gen);
    },
    
    listResources:function(path,filePattern,options){
        if(this.classLoader){
            bundles.framework.Debug.println(this+" List bundle resources: "+path+", pattern="+filePattern);
        }
        var start=org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(path,"/")?1:0; 
        var end=org.eclipse.vjet.vjo.java.lang.StringUtil.endsWith(path,"/")?-1:0; 
        if(start!==0||end!==0){
            path=path.substring(start,path.length+end);
        }
        var pkg=org.eclipse.vjet.vjo.java.lang.StringUtil.replace(path,'/','.'); 
        var res=this.secure.callSearchFor(this,filePattern,pkg,path,this.vj$.BundleClassLoader.listSearch,(options<<8)|this.vj$.BundleClassLoader.LIST,this,null); 
        return res;
    },
    
    searchFor:function(name,pkg,path,action,options,requestor,visited){
        try {
            var b=this.getBundle(); 
            var initiator=false; 
            var bundlesToActivate=null; 
            if(action===this.vj$.BundleClassLoader.classSearch){
                var bundlePresent=false; 
                bundlesToActivate=this.vj$.BundleClassLoader.tlBundlesToActivate.get();
                initiator=bundlesToActivate===null;
                if(initiator){
                    bundlesToActivate=new this.vj$.ArrayList();
                    this.vj$.BundleClassLoader.tlBundlesToActivate.set(bundlesToActivate);
                }else {
                    bundlePresent=bundlesToActivate.contains(b);
                }
                if(!bundlePresent&&b.triggersActivationPkg(pkg)){
                    bundlesToActivate.add(b);
                    if(this.lazy_activation){
                        bundles.framework.Debug.println(this+" lazy activation of #"+b.id+" triggered by searchFor("+name+")");
                    }
                }
            }
            var res=this.searchFor0(name,pkg,path,action,options,requestor,visited);
            if(initiator){
                this.vj$.BundleClassLoader.tlBundlesToActivate.set(null);
                for (var i=bundlesToActivate.size()-1;i>=0;i--){
                    var tmp=bundlesToActivate.get(i); 
                    if(this.lazy_activation){
                        bundles.framework.Debug.println(this+" requesting lazy activation of #"+tmp.id);
                    }
                    try {
                        tmp.finalizeActivation();
                    }
                    catch(e){
                        this.fwCtx.frameworkError(tmp,e);
                    }
                }
            }
            return res;
        }
        catch(te){
            this.vj$.BundleClassLoader.tlBundlesToActivate.set(null);
            throw te;
        }
    },
    
    searchFor0:function(name,pkg,path,action,options,requestor,visited){
        var pbp; 
        var ep; 
        if(action===this.vj$.BundleClassLoader.classSearch&&requestor!==this){
            var c=findLoadedClass(name); 
            if(c!==null){
                return c;
            }
        }
        var list=(options&this.vj$.BundleClassLoader.LIST)!==0; 
        var local=(options&this.vj$.BundleClassLoader.LOCAL)!==0; 
        var recurse=(options&this.vj$.BundleClassLoader.RECURSE)!==0; 
        var answer=null;
        if(this.classLoader){
            bundles.framework.Debug.println(this+" Search for: "+path);
        }
        if(pkg!==null){
            pbp=this.bpkgs.getProviderBundlePackages(pkg);
            if(pbp!==null){
                var cl=pbp.getClassLoader(); 
                if(!local||cl===this){
                    if(this.vj$.BundleClassLoader.isSystemBundle(pbp.bg.bundle)){
                        answer=this.frameworkSearchFor(cl,name,path,action);
                        if(!recurse){
                            return answer;
                        }
                    }else {
                        var bcl=cl; 
                        if(bcl!==this&&(visited===null||(bcl!==null&& !visited.contains(bcl)))){
                            if(bcl!==null){
                                if(this.classLoader){
                                    bundles.framework.Debug.println(this+" Import search: "+path+" from #"+pbp.bg.bundle.id);
                                }
                                answer=this.secure.callSearchFor(bcl,name,pkg,path,action,options& ~this.vj$.BundleClassLoader.RECURSE,requestor,visited);
                            }else {
                                if(this.classLoader){
                                    bundles.framework.Debug.println(this+" No import found: "+path);
                                }
                            }
                            if(!recurse){
                                return answer;
                            }
                        }
                    }
                }
                options|=this.vj$.BundleClassLoader.ONLY_RECURSE;
            }else if(!local){
                var pl=this.bpkgs.getRequiredBundleGenerations(pkg); 
                if(pl!==null){
                    if(visited===null){
                        visited=new this.vj$.HashSet();
                    }
                    visited.add(this);
                    for (var pbg,_$itr=pl.iterator();_$itr.hasNext();){
                        pbg=_$itr.next();
                        var cl=pbg.getClassLoader(); 
                        if(bundles.framework.BundleClassLoader.isInstance(cl)){
                            var bcl=cl; 
                            if(bcl!==null&& !visited.contains(bcl)){
                                if(this.classLoader){
                                    bundles.framework.Debug.println(this+" Required bundle search: "+path+" from #"+pbg.bundle.id);
                                }
                                answer=this.secure.callSearchFor(bcl,name,pkg,path,action,options,requestor,visited);
                            }
                        }else {
                            answer=this.frameworkSearchFor(cl,name,path,action);
                        }
                        if(answer!==null){
                            if(list||recurse){
                                break;
                            }else {
                                return answer;
                            }
                        }
                    }
                    if(this.classLoader&&answer===null){
                        bundles.framework.Debug.println(this+" Required bundle search: "+"Not found, continuing with local search.");
                    }
                }
            }
            ep=this.bpkgs.getExports(pkg);
        }else {
            ep=null;
        }
        if(this!==requestor&&ep!==null){
            if(action===this.vj$.BundleClassLoader.classSearch){
                var blocked=true; 
                while(ep.hasNext()){
                    if(ep.next().checkFilter(name)){
                        blocked=false;
                        break;
                    }
                }
                if(blocked){
                    if(this.classLoader){
                        bundles.framework.Debug.println(this+" Filter check blocked search for: "+name);
                    }
                    return null;
                }
            }
        }
        var av=this.classPath.componentExists(path,(options&this.vj$.BundleClassLoader.ONLY_FIRST)!==0,(options&this.vj$.BundleClassLoader.LIST)!==0); 
        if(av!==null||recurse){
            try {
                var res=action.get(av,path,name,pkg,options,requestor,this);
                if(answer!==null){
                    if(res!==null){
                        var ca=answer; 
                        var cr=res; 
                        ca.addAll(cr);
                    }
                }else {
                    answer=res;
                }
                return answer;
            }
            catch(e){
                if(e instanceof this.vj$.ClassFormatError){
                    var cfe=e;
                    throw cfe;
                }else if(e instanceof this.vj$.IOException){
                    var ioe=e;
                    this.fwCtx.frameworkError(this.bpkgs.bg.bundle,ioe);
                    return null;
                }
            }
        }
        if(ep!==null||(options&this.vj$.BundleClassLoader.LIST)!==0){
            return null;
        }
        if(pkg!==null){
            pbp=this.bpkgs.getDynamicProviderBundlePackages(pkg);
            if(pbp!==null){
                if(this.vj$.BundleClassLoader.isSystemBundle(pbp.bg.bundle)){
                    try {
                        return this.fwCtx.systemBundle.getClassLoader().loadClass(name);
                    }
                    catch(e){
                    }
                }else {
                    var cl=pbp.getClassLoader(); 
                    if(cl!==null){
                        if(this.classLoader){
                            bundles.framework.Debug.println(this+" Dynamic import search: "+path+" from #"+pbp.bg.bundle.id);
                        }
                        return this.secure.callSearchFor(cl,name,pkg,path,action,options,requestor,visited);
                    }
                }
            }
            if(this.classLoader){
                bundles.framework.Debug.println(this+" No dynamic import: "+path);
            }
        }
        return null;
    },
    
    frameworkSearchFor:function(cl,name,path,action){
        if(action===this.vj$.BundleClassLoader.classSearch){
            try {
                return cl.loadClass(name);
            }
            catch(e){
            }
        }else if(action===this.vj$.BundleClassLoader.resourceSearch){
            try {
                return cl.getResources(path);
            }
            catch(e){
            }
        }else if(action===this.vj$.BundleClassLoader.listSearch){
            throw new UnsupportedOperationException("listResources not available on system bundle");
        }
        return null;
    },
    
    walkAndAddJars:function(dexlist,path){
        var root=new this.vj$.File(path); 
        var list=root.listFiles(); 
        for (var f,_$i0=0;_$i0<list.length;_$i0++){
            f=list[_$i0];
            if(f.isDirectory()){
                walkAndAddJars(dexlist,f.getAbsolutePath());
            }else {
                if(f.getAbsolutePath().endsWith(".jar")){
                    if(this.classLoader){
                        bundles.framework.Debug.println("creating DexFile from "+f.getAbsolutePath());
                    }
                    var dex=this.vj$.BundleClassLoader.dexFileClassLoadDexMethod.invoke(null,[f.getAbsolutePath(),f.getAbsolutePath()+".dexopt",new this.vj$.Integer(0)]);
                    dexlist.add(dex);
                }
            }
        }
    },
    
    getDexFileClass:function(name){
        if(this.classLoader){
            bundles.framework.Debug.println("loading dex class "+name);
        }
        if(this.dexFile===null){
            this.dexFile=new this.vj$.ArrayList();
            var f=new this.vj$.File(this.archive.getJarLocation()); 
            if(!f.isDirectory()){
                if(this.classLoader){
                    bundles.framework.Debug.println("creating DexFile from "+f);
                }
                var dex=this.vj$.BundleClassLoader.dexFileClassLoadDexMethod.invoke(null,[f.getAbsolutePath(),f.getAbsolutePath()+".dexopt",new this.vj$.Integer(0)]);
                this.dexFile.add(dex);
                if(this.classLoader){
                    bundles.framework.Debug.println("created DexFile from "+f);
                }
            }else {
                if(this.classLoader){
                    this.vj$.System.err.println("creating DexFile from "+f+"/classes.dex");
                }
                var dex=this.vj$.BundleClassLoader.dexFileClassLoadDexMethod.invoke(null,[f.getAbsolutePath()+"/classes.dex",f.getAbsolutePath()+".dexopt",new this.vj$.Integer(0)]);
                this.dexFile.add(dex);
                if(this.classLoader){
                    bundles.framework.Debug.println("created DexFile from "+f+this.vj$.File.pathSeparatorChar+"classes.dex");
                }
                walkAndAddJars(this.dexFile,f.getAbsolutePath());
            }
        }
        var path=org.eclipse.vjet.vjo.java.lang.StringUtil.replace(name,'.','/'); 
        var i=this.dexFile.iterator(); 
        while(i.hasNext()){
            var dex=i.next();
            if(this.classLoader){
                bundles.framework.Debug.println("trying to load "+path+" from "+dex);
            }
            try {
                var clz=this.vj$.BundleClassLoader.dexFileClassLoadClassMethod.invoke(dex,[path,this]); 
                if(clz!==null){
                    if(this.classLoader){
                        bundles.framework.Debug.println("loaded "+path+" from "+dex);
                    }
                    return clz;
                }
            }
            catch(e){
            }
            if(this.classLoader){
                bundles.framework.Debug.println("failed to load "+path+" from "+dex);
            }
        }
        throw new this.vj$.ClassNotFoundException("could not find dex class "+path);
    },
    
    findLibrary0:function(name){
        return this.classPath.getNativeLibrary(name);
    }
})
.inits(function(){
    this.vj$.BundleClassLoader.tlBundlesToActivate=new this.vj$.ThreadLocal();
    {
        try {
            var dexFileClass=null; 
            try {
                dexFileClass=vjo.Class.forName("android.dalvik.DexFile");
            }
            catch(ex){
                dexFileClass=vjo.Class.forName("dalvik.system.DexFile");
            }
            this.dexFileClassLoadDexMethod=dexFileClass.getMethod("loadDex",[vjo.Object.clazz,vjo.Object.clazz,this.vj$.Integer.TYPE]);
            this.dexFileClassLoadClassMethod=dexFileClass.getMethod("loadClass",[vjo.Object.clazz,this.vj$.ClassLoader.clazz]);
            this.bDalvik=true;
        }
        catch(e){
            this.dexFileClassLoadDexMethod=null;
            this.dexFileClassLoadClassMethod=null;
        }
    }
    this.vj$.BundleClassLoader.smex=new this.SecurityManagerExposer();
    this.vj$.BundleClassLoader.classSearch=
        vjo.make(this,this.SearchAction)
        .protos({
            get:function(items,path,name,pkg,options,requestor,cl){
                var bytes=items.get(0).getClassBytes(path); 
                if(bytes!==null){
                    if(cl.debug.classLoader){
                        cl.debug.println("classLoader(#"+cl.bpkgs.bg.bundle.id+") - load class: "+name);
                    }
                    {
                        var c=cl.findLoadedClass(name); 
                        if(c===null){
                            if(pkg!==null){
                                if(cl.getPackage(pkg)===null){
                                    cl.definePackage(pkg,null,null,null,null,null,null,null);
                                }
                            }
                            if(this.vj$.BundleClassLoader.bDalvik){
                                try {
                                    c=cl.getDexFileClass(name);
                                }
                                catch(e){
                                    throw new this.vj$.IOException("Failed to load dex class '"+name+"', "+e);
                                }
                            }
                            if(c===null){
                                var wc=null; 
                                if(cl!==null&&cl.bpkgs!==null&&cl.bpkgs.bg!==null&&cl.bpkgs.bg.bundle!==null){
                                    wc=new this.vj$.WeavingHooks.WovenClassImpl(cl.bpkgs.bg.bundle,name,bytes);
                                    try {
                                        cl.fwCtx.weavingHooks.callHooks(wc);
                                        if(wc.hasAdditionalDynamicImports()){
                                            cl.bpkgs.parseDynamicImports(wc.getDynamicImportsAsString());
                                        }
                                        bytes=wc.getBytes();
                                    }
                                    catch(e){
                                        if(e instanceof this.vj$.ClassFormatError){
                                            var cfe=e;
                                            throw cfe;
                                        }else if(e instanceof this.vj$.Throwable){
                                            var t=e;
                                            var cfe=new ClassFormatError("Failed to call WeavingHooks for "+name); 
                                            cfe.initCause(t);
                                            throw cfe;
                                        }
                                    }
                                }
                                if(cl.protectionDomain===null){
                                    c=cl.defineClass(name,bytes,0,bytes.length);
                                }else {
                                    c=cl.defineClass(name,bytes,0,bytes.length,cl.protectionDomain);
                                }
                                if(wc!==null){
                                    wc.setDefinedClass(c);
                                }
                            }
                        }
                        return c;
                    }
                }
                return null;
            }
        })
        .endType();
    this.vj$.BundleClassLoader.resourceSearch=
        vjo.make(this,this.SearchAction)
        .protos({
            get:function(items,path,name,pkg,options,requestor,cl){
                var answer=new this.vj$.Vector(); 
                for (var i=0;i<items.size();i++){
                    var fa=items.elementAt(i); 
                    var url=fa.getBundleGeneration().getURL(fa.getSubId(),path); 
                    if(url!==null){
                        if(cl.debug.classLoader){
                            cl.debug.println("classLoader(#"+cl.bpkgs.bg.bundle.id+") - found: "+path+" -> "+url);
                        }
                        answer.addElement(url);
                    }else {
                        return null;
                    }
                }
                return answer.elements();
            }
        })
        .endType();
    this.vj$.BundleClassLoader.listSearch=
        vjo.make(this,this.SearchAction)
        .protos({
            get:function(items,path,name,pkg,options,requestor,cl){
                var answer=new this.vj$.parent(); 
                var onlyRecurse=(options&this.vj$.BundleClassLoader.ONLY_RECURSE)!==0; 
                var scanned=new this.vj$.parent(); 
                for (var subPkg,_$itr=cl.bpkgs.getSubProvider(pkg).iterator();_$itr.hasNext();){
                    subPkg=_$itr.next();
                    if((options&this.vj$.BundleClassLoader.RECURSE)!==0){
                        var next=path.length>0?path+"/"+subPkg:subPkg; 
                        var subAnswer=cl.searchFor(name,org.eclipse.vjet.vjo.java.lang.StringUtil.replace(next,'/','.'),next,this.vj$.BundleClassLoader.listSearch,options& ~this.vj$.BundleClassLoader.ONLY_RECURSE,requestor,null); 
                        if(subAnswer!==null){
                            answer.addAll(subAnswer);
                        }
                    }
                    if(!onlyRecurse&&(name===null||this.vj$.Util.filterMatch(name,subPkg))){
                        answer.add(path+"/"+subPkg);
                    }
                    scanned.add(subPkg+"/");
                }
                if(items!==null){
                    for (var fa,_$itr=items.iterator();_$itr.hasNext();){
                        fa=_$itr.next();
                        for (var e,_$itr=fa.listDir(path).iterator();_$itr.hasNext();){
                            e=_$itr.next();
                            if(scanned.contains(e)){
                                continue;
                            }
                            if(org.eclipse.vjet.vjo.java.lang.StringUtil.endsWith(e,"/")){
                                e=e.substring(0,e.length-1);
                                if((options&this.vj$.BundleClassLoader.RECURSE)!==0){
                                    var next=path.length>0?path+"/"+e:e; 
                                    var subAnswer=cl.searchFor(name,org.eclipse.vjet.vjo.java.lang.StringUtil.replace(next,'/','.'),next,this.vj$.BundleClassLoader.listSearch,options& ~this.vj$.BundleClassLoader.ONLY_RECURSE,requestor,null); 
                                    if(subAnswer!==null){
                                        answer.addAll(subAnswer);
                                    }
                                }
                            }
                            if(!onlyRecurse&&(name===null||this.vj$.Util.filterMatch(name,e))){
                                answer.add(path+"/"+e);
                            }
                        }
                    }
                }
                return answer;
            }
        })
        .endType();
})
.endType();