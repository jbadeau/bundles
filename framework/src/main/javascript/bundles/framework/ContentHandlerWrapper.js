/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.ContentHandlerWrapper') 
.needs(['org.eclipse.vjet.vjo.java.lang.Exception','org.eclipse.vjet.vjo.java.lang.IllegalArgumentException',
    'org.eclipse.vjet.vjo.java.lang.Integer','org.eclipse.vjet.vjo.java.lang.Long',
    'org.eclipse.vjet.vjo.java.lang.IllegalStateException','org.eclipse.vjet.vjo.java.lang.StringBuffer',
    'bundles.framework.FrameworkContext','osgi.framework.ServiceReference',
    'osgi.framework.Constants','osgi.service.url.URLConstants',
    'osgi.framework.ServiceListener','osgi.framework.ServiceEvent',
    'java.net.URLConnection'])
.inherits('java.net.ContentHandler')
.protos({
    framework:null, 
    mimetype:null, 
    filter:null, 
    best:null, 
    
    constructs:function(framework,mimetype){
        this.base();
        this.framework=framework;
        this.mimetype=mimetype;
        this.filter="(&"+"("+this.vj$.Constants.OBJECTCLASS+"="+this.vj$.ContentHandler.clazz.getName()+")"+"("+this.vj$.URLConstants.URL_CONTENT_MIMETYPE+"="+mimetype+")"+")";
        var serviceListener=
            vjo.make(this,this.vj$.ServiceListener)
            .protos({
                serviceChanged:function(evt){
                    var ref=evt.getServiceReference(); 
                    switch(evt.getType()){
                        case this.vj$.ServiceEvent.MODIFIED:
                        case this.vj$.ServiceEvent.REGISTERED:
                        if(this.vj$.parent.best===null){
                            updateBest();
                            return;
                        }
                        if(compare(this.vj$.parent.best,ref)>0){
                            this.vj$.parent.best=ref;
                        }
                            break;
                        case this.vj$.ServiceEvent.MODIFIED_ENDMATCH:
                        case this.vj$.ServiceEvent.UNREGISTERING:
                        if(this.vj$.parent.best.equals(ref)){
                            this.vj$.parent.best=null;
                        }
                    }
                }
            })
            .endType(); 
        try {
            framework.systemBundle.bundleContext.addServiceListener(serviceListener,this.filter);
        }
        catch(e){
            throw new this.vj$.IllegalArgumentException("Could not register service listener for content handler: "+e);
        }
        if(framework.debug.url){
            framework.debug.println("created wrapper for "+mimetype+", filter="+this.filter);
        }
    },
    
    compare:function(ref1,ref2){
        var tmp1=ref1.getProperty(this.vj$.Constants.SERVICE_RANKING);
        var tmp2=ref2.getProperty(this.vj$.Constants.SERVICE_RANKING);
        var r1=(tmp1 instanceof this.vj$.Integer)?tmp1.intValue():0; 
        var r2=(tmp2 instanceof this.vj$.Integer)?tmp2.intValue():0; 
        if(r2===r1){
            var i1=ref1.getProperty(this.vj$.Constants.SERVICE_ID); 
            var i2=ref2.getProperty(this.vj$.Constants.SERVICE_ID); 
            return i1.compareTo(i2);
        }else {
            return r2-r1;
        }
    },
    
    updateBest:function(){
        try {
            var refs=this.framework.systemBundle.bundleContext.getServiceReferences(this.vj$.ContentHandler.clazz.getName(),this.filter); 
            if(refs!==null){
                this.best=refs[0];
                for (var i=1;i<refs.length;i++){
                    if(this.compare(this.best,refs[i])>0){
                        this.best=refs[i];
                    }
                }
            }
        }
        catch(e){
            throw new this.vj$.IllegalArgumentException("Could not register url handler: "+e);
        }
    },
    
    getService:function(){
        var obj; 
        try {
            if(this.best===null){
                this.updateBest();
            }
            if(this.best===null){
                throw new this.vj$.IllegalStateException("null: Lost service for protocol="+this.mimetype);
            }
            obj=this.framework.systemBundle.bundleContext.getService(this.best);
            if(obj===null){
                throw new this.vj$.IllegalStateException("null: Lost service for protocol="+this.mimetype);
            }
        }
        catch(e){
            throw new this.vj$.IllegalStateException("null: Lost service for protocol="+this.mimetype);
        }
        return obj;
    },
    
    
    getContent:function(urlc){
        if(arguments.length===1){
            if(arguments[0] instanceof java.net.URLConnection){
                return this.getContent_1_0_ContentHandlerWrapper_ovld(arguments[0]);
            }else if(this.base && this.base.getContent){
                return this.base.getContent.apply(this,arguments);
            }
        }else if(arguments.length===2){
            if(arguments[0] instanceof java.net.URLConnection && arguments[1] instanceof Array){
                return this.getContent_2_0_ContentHandlerWrapper_ovld(arguments[0],arguments[1]);
            }else if(this.base && this.base.getContent){
                return this.base.getContent.apply(this,arguments);
            }
        }else if(this.base && this.base.getContent){
            return this.base.getContent.apply(this,arguments);
        }
    },
    
    getContent_1_0_ContentHandlerWrapper_ovld:function(urlc){
        return this.getService().getContent(urlc);
    },
    
    getContent_2_0_ContentHandlerWrapper_ovld:function(urlc,classes){
        return this.getService().getContent(urlc,classes);
    },
    
    toString:function(){
        var sb=new this.vj$.StringBuffer(); 
        sb.append("ContentHandlerWrapper[");
        var ref=this.best; 
        sb.append("mimetype="+this.mimetype);
        if(ref!==null){
            sb.append(", id="+ref.getProperty(this.vj$.Constants.SERVICE_ID));
            sb.append(", rank="+ref.getProperty(this.vj$.Constants.SERVICE_RANKING));
        }else {
            sb.append(" no service tracked");
        }
        sb.append("]");
        return sb.toString();
    }
})
.endType();