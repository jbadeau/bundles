/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.ServiceURLStreamHandlerFactory') 
.needs(['org.eclipse.vjet.vjo.java.util.Vector','org.eclipse.vjet.vjo.java.util.Hashtable',
    'org.eclipse.vjet.vjo.java.util.HashMap','org.eclipse.vjet.vjo.java.lang.RuntimeException',
    'bundles.framework.BundleURLStreamHandler','bundles.framework.Util',
    'bundles.framework.ReferenceURLStreamHandler','osgi.service.url.URLConstants',
    'osgi.service.url.URLStreamHandlerService','bundles.framework.URLStreamHandlerWrapper',
    'org.eclipse.vjet.vjo.java.lang.StringUtil'])
.needs('bundles.framework.Debug','')

.satisfies('java.net.URLStreamHandlerFactory')
.protos({
    framework:null, 
    handlers:null, 
    jvmPkgs:null, 
    wrapMap:null, 
    bundleHandler:null, 
    debug:null, 
    
    constructs:function(fw){
        this.framework=new this.vj$.Vector(2);
        this.handlers=new this.vj$.Hashtable();
        this.wrapMap=new this.vj$.HashMap();
        var s=fw.props.getProperty("java.protocol.handler.pkgs"); 
        bundles.framework.Debug=fw.debug;
        if(s!==null){
            this.jvmPkgs=this.vj$.Util.splitwords(s,"|");
            for (var i=0;i<this.jvmPkgs.length;i++){
                this.jvmPkgs[i]=org.eclipse.vjet.vjo.java.lang.StringUtil.trim(this.jvmPkgs[i]);
                if(this.url){
                    bundles.framework.Debug.println("JVMClassPath - URLHandler jvmPkgs["+i+"]="+this.jvmPkgs[i]);
                }
            }
        }
        this.setURLStreamHandler(this.vj$.ReferenceURLStreamHandler.PROTOCOL,new this.vj$.ReferenceURLStreamHandler());
        this.bundleHandler=new this.vj$.BundleURLStreamHandler();
        this.setURLStreamHandler(this.vj$.BundleURLStreamHandler.PROTOCOL,this.bundleHandler);
    },
    
    createURLStreamHandler:function(protocol){
        if(this.url){
            bundles.framework.Debug.println("createURLStreamHandler protocol="+protocol);
        }
        var handler=this.getJVMClassPathHandler(protocol); 
        if(handler!==null){
            if(this.url){
                bundles.framework.Debug.println("using JVMClassPath handler for "+protocol);
            }
            return handler;
        }
        handler=this.handlers.get(protocol); 
        if(handler!==null){
            if(this.url){
                bundles.framework.Debug.println("using predefined handler for "+protocol);
            }
            return handler;
        }
        handler=this.getServiceHandler(protocol);
        if(handler!==null){
            if(this.url){
                bundles.framework.Debug.println("Using service URLHandler for "+protocol);
            }
            return handler;
        }
        if(this.url){
            bundles.framework.Debug.println("Using default URLHandler for "+protocol);
        }
        return null;
    },
    
    setURLStreamHandler:function(protocol,handler){
        this.handlers.put(protocol,handler);
    },
    
    addFramework:function(fw){
        this.bundleHandler.addFramework(fw);
        this.framework.add(fw);
    },
    
    removeFramework:function(fw){
        this.framework.remove(fw);
        this.bundleHandler.removeFramework(fw);
        {
            for (var i=this.wrapMap.entrySet().iterator();i.hasNext();){
                var e=i.next(); 
                if((e.getValue()).removeFramework(fw)){
                    i.remove();
                }
            }
        }
    },
    
    getServiceHandler:function(protocol){
        try {
            var filter="("+this.vj$.URLConstants.URL_HANDLER_PROTOCOL+"="+protocol+")"; 
            var sfws=this.framework.clone(); 
            for (var sfw,_$itr=sfws.iterator();_$itr.hasNext();){
                sfw=_$itr.next();
                var srl=sfw.services.get(this.vj$.URLStreamHandlerService.clazz.getName(),filter,null); 
                if(srl!==null&&srl.length>0){
                    {
                        var wrapper=this.wrapMap.get(protocol); 
                        if(wrapper===null){
                            wrapper=new this.vj$.URLStreamHandlerWrapper(sfw,protocol);
                            this.wrapMap.put(protocol,wrapper);
                        }else {
                            wrapper.addFramework(sfw);
                        }
                        return wrapper;
                    }
                }
            }
        }
        catch(e){
            throw new this.vj$.RuntimeException("Failed to get service: "+e);
        }
        return null;
    },
    
    getJVMClassPathHandler:function(protocol){
        if(this.jvmPkgs!==null){
            for (var jvmPkg,_$i0=0;_$i0<this.jvmPkgs.length;_$i0++){
                jvmPkg=this.jvmPkgs[_$i0];
                var className=jvmPkg+"."+protocol+".Handler"; 
                try {
                    if(this.url){
                        bundles.framework.Debug.println("JVMClassPath - trying URLHandler class="+className);
                    }
                    var clazz=vjo.Class.forName(className); 
                    var handler=clazz.newInstance(); 
                    if(this.url){
                        bundles.framework.Debug.println("JVMClassPath - created URLHandler class="+className);
                    }
                    return handler;
                }
                catch(t){
                    if(this.url){
                        bundles.framework.Debug.println("JVMClassPath - no URLHandler class "+className);
                    }
                }
            }
        }
        if(this.url){
            bundles.framework.Debug.println("JVMClassPath - no URLHandler for "+protocol);
        }
        return null;
    }
})
.endType();