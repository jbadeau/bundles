define([ "vjo", "./Util", "osgi/util/StringUtil" ], function(vjo, Util, StringUtil) {

	return vjo.ctype("bundles.framework.Alias")

	.props({

		processorAliases : null,

		osNameAliases : null,

		unifyProcessor : function(name) {
			var lname = StringUtil.toLowerCase(name);
			for (var i = 0; i < this.processorAliases.length; i++) {
				for (var j = 1; j < this.processorAliases[i].length; j++) {
					if (Util.filterMatch(this.processorAliases[i][j], lname)) {
						return this.processorAliases[i][0];
					}
				}
			}
			return name;
		},

		unifyOsName : function(name) {
			var lname = StringUtil.toLowerCase(name);
			for (var i = 0; i < this.osNameAliases.length; i++) {
				for (var j = 1; j < this.osNameAliases[i].length; j++) {
					if (Util.filterMatch(this.osNameAliases[i][j], lname)) {
						return this.osNameAliases[i][0];
					}
				}
			}
			return name;
		}

	})

	.inits(function() {
		this.vj$.Alias.processorAliases = [ [ "arm_le", "armv*l" ], [ "arm_be", "armv*" ], [ "Ignite", "psc1k" ], [ "PowerPC", "power", "ppc", "ppcbe" ], [ "x86", "pentium", "i386", "i486", "i586", "i686" ], [ "x86-64", "amd64", "em64t", "x86_64" ] ];
		this.vj$.Alias.osNameAliases = [ [ "Epoc32", "symbianos" ], [ "HPUX", "hp-ux" ], [ "MacOS", "mac os" ], [ "MacOSX", "mac os x" ], [ "OS2", "os/2" ], [ "QNX", "procnto" ], [ "Windows95", "win*95", "win32" ], [ "Windows98", "win*98", "win32" ], [ "WindowsNT", "win*nt", "win32" ], [ "WindowsCE", "win*ce", "win32" ], [ "Windows2000", "win*2000", "win32" ], [ "WindowsXP", "win*xp", "win32" ], [ "Windows2003", "win*2003", "win32" ], [ "WindowsVista", "win*vista", "win32" ], [ "Windows7", "win*7", "win32" ], [ "WindowsServer2008", "win*2008", "win32" ], [ "Windows8", "win*8", "win32" ], [ "WindowsServer2012", "win*2012", "win32" ] ];
	})

	.endType();

});