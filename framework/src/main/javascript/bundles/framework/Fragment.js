/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.Fragment') 
.needs(['org.eclipse.vjet.vjo.java.util.Vector','org.eclipse.vjet.vjo.java.lang.IllegalArgumentException',
    'org.eclipse.vjet.vjo.java.util.Collections','org.eclipse.vjet.vjo.java.lang.StringBuffer',
    'bundles.framework.BundleGeneration','osgi.framework.VersionRange',
    'osgi.framework.Constants','osgi.framework.wiring.BundleRevision',
    'osgi.framework.FrameworkUtil','org.eclipse.vjet.vjo.java.lang.System',
    'vjo.java.lang.ObjectUtil'])

.satisfies('osgi.framework.wiring.BundleRequirement')
.protos({
    gen:null, 
    hostName:null, 
    extension:null, 
    versionRange:null, 
    attributes:null, 
    directives:null, 
    hosts:null, 
    
    constructs:function(gen,headerEntry){
        this.hosts=new this.vj$.Vector(2);
        this.gen=gen;
        this.hostName=headerEntry.getKey();
        if(gen.archive.getAttribute(this.vj$.Constants.BUNDLE_ACTIVATOR)!==null){
            throw new this.vj$.IllegalArgumentException("A fragment bundle can not have a Bundle-Activator.");
        }
        var dirs=headerEntry.getDirectives(); 
        var extension=dirs.get(this.vj$.Constants.EXTENSION_DIRECTIVE); 
        if(vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.EXTENSION_FRAMEWORK,extension)||vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.EXTENSION_BOOTCLASSPATH,extension)){
            if(!vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.SYSTEM_BUNDLE_SYMBOLICNAME,this.hostName)&& !vjo.java.lang.ObjectUtil.equals(this.vj$.BundleGeneration.BUNDLES_SYMBOLICNAME,this.hostName)){
                throw new this.vj$.IllegalArgumentException("An extension bundle must target "+"the system bundle("+this.vj$.Constants.SYSTEM_BUNDLE_SYMBOLICNAME+" or "+this.vj$.BundleGeneration.BUNDLES_SYMBOLICNAME+")");
            }
            if(gen.archive.getAttribute(this.vj$.Constants.IMPORT_PACKAGE)!==null||gen.archive.getAttribute(this.vj$.Constants.REQUIRE_BUNDLE)!==null||gen.archive.getAttribute(this.vj$.Constants.BUNDLE_NATIVECODE)!==null||gen.archive.getAttribute(this.vj$.Constants.DYNAMICIMPORT_PACKAGE)!==null||gen.archive.getAttribute(this.vj$.Constants.BUNDLE_ACTIVATOR)!==null){
                throw new this.vj$.IllegalArgumentException("An extension bundle cannot specify: "+this.vj$.Constants.IMPORT_PACKAGE+", "+this.vj$.Constants.REQUIRE_BUNDLE+", "+this.vj$.Constants.BUNDLE_NATIVECODE+", "+this.vj$.Constants.DYNAMICIMPORT_PACKAGE+" or "+this.vj$.Constants.BUNDLE_ACTIVATOR);
            }
            if(!gen.bundle.fwCtx.props.getBooleanProperty(this.vj$.Constants.SUPPORTS_FRAMEWORK_EXTENSION)&&vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.EXTENSION_FRAMEWORK,extension)){
                throw new UnsupportedOperationException("Framework extension bundles are not supported "+"by this framework. Consult the documentation");
            }
            if(!gen.bundle.fwCtx.props.getBooleanProperty(this.vj$.Constants.SUPPORTS_BOOTCLASSPATH_EXTENSION)&&vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.EXTENSION_BOOTCLASSPATH,extension)){
                throw new UnsupportedOperationException("Bootclasspath extension bundles are not supported "+"by this framework. Consult the documentation");
            }
        }else {
            if(extension!==null){
                throw new this.vj$.IllegalArgumentException("Did not recognize directive "+this.vj$.Constants.EXTENSION_DIRECTIVE+":="+extension+".");
            }
        }
        this.extension=extension;
        var range=headerEntry.getAttributes().remove(this.vj$.Constants.BUNDLE_VERSION_ATTRIBUTE); 
        this.versionRange=range===null?null:new this.vj$.VersionRange(range);
        this.attributes=headerEntry.getAttributes();
        var filter=this.toFilter(); 
        if(null!==filter){
            dirs.put(this.vj$.Constants.FILTER_DIRECTIVE,filter.toString());
        }
        this.directives=this.vj$.Collections.unmodifiableMap(dirs);
    },
    
    addHost:function(host){
        this.hosts.add(host);
    },
    
    removeHost:function(host){
        if(host===null){
            this.hosts.clear();
        }else {
            this.hosts.remove(host);
        }
    },
    
    isHost:function(host){
        return this.hosts.contains(host);
    },
    
    getHosts:function(){
        return this.hosts.clone(); 
    },
    
    hasHosts:function(){
        return !this.hosts.isEmpty();
    },
    
    isTarget:function(bg){
        return vjo.java.lang.ObjectUtil.equals(this.hostName,bg.symbolicName)&&(this.versionRange===null||this.versionRange.includes(bg.version))&&bg.bsnAttrMatch(this.attributes);
    },
    
    targets:function(){
        var lbg=this.gen.bundle.fwCtx.bundles.getBundles(this.hostName,this.versionRange); 
        for (var i=lbg.iterator();i.hasNext();){
            var tbg=i.next(); 
            if(vjo.java.lang.ObjectUtil.equals(tbg.attachPolicy,this.vj$.Constants.FRAGMENT_ATTACHMENT_NEVER)|| !this.isTarget(tbg)){
                i.remove();
            }
        }
        return lbg;
    },
    
    getNamespace:function(){
        return this.vj$.BundleRevision.HOST_NAMESPACE;
    },
    
    getDirectives:function(){
        return this.directives;
    },
    
    toFilter:function(){
        var sb=new this.vj$.StringBuffer(80); 
        var multipleConditions=false; 
        sb.append('(');
        sb.append(this.vj$.BundleRevision.HOST_NAMESPACE);
        sb.append('=');
        if(vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.SYSTEM_BUNDLE_SYMBOLICNAME,this.hostName)){
            sb.append(this.vj$.BundleGeneration.BUNDLES_SYMBOLICNAME);
        }else {
            sb.append(this.hostName);
        }
        sb.append(')');
        if(this.versionRange!==null){
            sb.append(this.versionRange.toFilterString(this.vj$.Constants.BUNDLE_VERSION_ATTRIBUTE));
            multipleConditions=true;
        }
        for (var entry,_$itr=this.attributes.entrySet().iterator();_$itr.hasNext();){
            entry=_$itr.next();
            sb.append('(');
            sb.append(entry.getKey());
            sb.append('=');
            sb.append(entry.getValue().toString());
            sb.append(')');
            multipleConditions|=true;
        }
        if(multipleConditions){
            sb.insert(0,"(&");
            sb.append(')');
        }
        try {
            return this.vj$.FrameworkUtil.createFilter(sb.toString());
        }
        catch(_ise){
            this.vj$.System.err.println("createFilter: '"+sb.toString()+"': "+_ise.getMessage());
            return null;
        }
    },
    
    getAttributes:function(){
        return this.vj$.Collections.EMPTY_MAP;
    },
    
    getRevision:function(){
        return this.gen.bundleRevision;
    },
    
    getResource:function(){
        return this.gen.bundleRevision;
    },
    
    matches:function(capability){
        if(vjo.java.lang.ObjectUtil.equals(this.vj$.BundleRevision.HOST_NAMESPACE,capability.getNamespace())){
            return this.toFilter().matches(capability.getAttributes());
        }
        return false;
    }
})
.endType();