define([ "vjo", "bundles/framework/BundleImpl", "osgi/framework/Bundle", "osgi/framework/launch/Framework", "osgi/lang/IllegalStateException" ], function(vjo, BundleImpl, Bundle, Framework, IllegalStateException) {

	return vjo.ctype("bundles.framework.SystemBundle")

	.inherits([BundleImpl])

	.satisfies([Framework])

	.props({

		BOOT_CLASSPATH_FILE : "boot_cp"

	})

	.protos({

		exportPackageString : null,

		provideCapabilityString : null,

		stopEvent : null,

		shutdownThread : null,

		lock : null,

		bootClassPathHasChanged : false,

		fwWiring : null,

		constructs : function(fw) {
			this.lock = Object();
			this.base(fw);
		},

		init : function() {
			this.secure.checkExecuteAdminPerm(this);

				this.waitOnOperation(this.lock, "Framework.init", true);
				switch (this.state) {
					case Bundle.INSTALLED:
					case Bundle.RESOLVED:
						break;
					case Bundle.STARTING:
					case Bundle.ACTIVE:
						return;
					default:
						throw new IllegalStateException("INTERNAL ERROR, Illegal state, " + this.state);
				}
				this.doInit();
		},

		waitForStop : function(timeout) {
			{
				if (((Bundle.INSTALLED | Bundle.RESOLVED) & this.state) === 0) {
					this.stopEvent = null;
					while (true) {
						var st = this.vj$.Util.timeMillis();
						try {
							this.lock.wait(timeout);
							if (this.stopEvent !== null) {
								break;
							}
						}
						catch (_) {
						}
						if (timeout > 0) {
							timeout = timeout - (this.vj$.Util.timeMillis() - st);
							if (timeout <= 0) {
								break;
							}
						}
					}
					if (this.stopEvent === null) {
						return new this.vj$.FrameworkEvent(this.vj$.FrameworkEvent.WAIT_TIMEDOUT, this, null);
					}
				}
				else if (this.stopEvent === null) {
					this.stopEvent = new this.vj$.FrameworkEvent(this.vj$.FrameworkEvent.STOPPED, this, null);
				}
				return this.stopEvent;
			}
		},

		uninstall : function() {
			this.vj$.secure.checkLifecycleAdminPerm(this);
			throw new this.vj$.BundleException("Uninstall of System bundle is not allowed", this.vj$.BundleException.INVALID_OPERATION);
		},

		hasPermission : function(permission) {
			return true;
		},

		getHeaders : function() {
			if (arguments.length === 0) {
				if (arguments.length == 0) {
					return this.getHeaders_0_0_SystemBundle_ovld();
				}
				else if (this.base && this.base.getHeaders) {
					return this.base.getHeaders.apply(this, arguments);
				}
			}
			else if (arguments.length === 1) {
				if (arguments[0] instanceof String || typeof arguments[0] == "string") {
					return this.getHeaders_1_0_SystemBundle_ovld(arguments[0]);
				}
				else if (this.base && this.base.getHeaders) {
					return this.base.getHeaders.apply(this, arguments);
				}
			}
			else if (this.base && this.base.getHeaders) {
				return this.base.getHeaders.apply(this, arguments);
			}
		},

		getHeaders_0_0_SystemBundle_ovld : function() {
			return this.getHeaders(null);
		},

		getHeaders_1_0_SystemBundle_ovld : function(locale) {
			this.vj$.secure.checkMetadataAdminPerm(this);
			var headers = new this.vj$.Hashtable();
			headers.put(this.vj$.Constants.BUNDLE_SYMBOLICNAME, this.getSymbolicName());
			headers.put(this.vj$.Constants.BUNDLE_NAME, this.location);
			headers.put(this.vj$.Constants.EXPORT_PACKAGE, this.exportPackageString);
			headers.put(this.vj$.Constants.BUNDLE_VERSION, this.getVersion().toString());
			headers.put(this.vj$.Constants.BUNDLE_MANIFESTVERSION, "2");
			headers.put(this.vj$.Constants.BUNDLE_REQUIREDEXECUTIONENVIRONMENT, this.fwCtx.props.getProperty(this.vj$.Constants.FRAMEWORK_EXECUTIONENVIRONMENT));
			headers.put("Bundle-Icon", "icon.png;size=32,icon64.png;size=64");
			headers.put(this.vj$.Constants.BUNDLE_VENDOR, "Bundles");
			headers.put(this.vj$.Constants.BUNDLE_DESCRIPTION, "Bundles System Bundle");
			headers.put(this.vj$.Constants.PROVIDE_CAPABILITY, this.provideCapabilityString);
			return headers;
		},

		findEntries : function(path, filePattern, recurse) {
			return null;
		},

		getEntry : function(name) {
			if (this.vj$.secure.okResourceAdminPerm(this)) {
				return this.getClassLoader().getResource(name);
			}
			return null;
		},

		getEntryPaths : function(path) {
			return null;
		},

		adapt : function(type) {
			this.vj$.secure.checkAdaptPerm(this, type);
			var res = null;
			if (this.vj$.FrameworkWiring.clazz.equals(type)) {
				res = this.fwWiring;
			}
			else if (this.vj$.FrameworkStartLevel.clazz.equals(type)) {
				if (this.fwCtx.startLevelController !== null) {
					res = this.fwCtx.startLevelController.frameworkStartLevel(this);
				}
			}
			else if (this.vj$.Framework.clazz.equals(type)) {
				res = this;
			}
			else {
				res = this.adaptSecure(type);
			}
			return res;
		},

		getClassLoader : function() {
			return this.getClass().getClassLoader();
		},

		systemShuttingdown : function(restart) {
		},

		systemShuttingdownDone : function(fe) {
			{
				if (this.state !== Bundle.INSTALLED) {
					this.state = Bundle.RESOLVED;
					this.operation = this.vj$.BundleImpl.IDLE;
					this.lock.notifyAll();
				}
				this.stopEvent = fe;
			}
		},

		attachExtension : function(extension) {
			if (extension.isBootClassPathExtension()) {
				if (this.getClassLoader() === null) {
					this.current().attachFragment(extension);
				}
				else {
					throw new UnsupportedOperationException("Bootclasspath extension can not be dynamicly activated");
				}
			}
			else {
				try {
					this.addClassPathURL(new this.vj$.URL("file:" + extension.archive.getJarLocation()));
					this.current().attachFragment(extension);
					this.handleExtensionActivator(extension);
				}
				catch (e) {
					throw new UnsupportedOperationException("Framework extension could not be dynamicly activated, " + e);
				}
			}
		},

		readLocalization : function(locale, localization_entries, baseName) {
			var fragments = this.current().fragments.clone();
			if (fragments === null) {
				return;
			}
			if (baseName === null) {
				baseName = this.vj$.Constants.BUNDLE_LOCALIZATION_DEFAULT_BASENAME;
			}
			if (!vjo.java.lang.ObjectUtil.equals(locale, "")) {
				locale = "_" + locale;
			}
			while (true) {
				var l = baseName + locale + ".properties";
				for (var i = fragments.size() - 1; i >= 0; i--) {
					var bg = fragments.get(i);
					var tmp = bg.archive.getLocalizationEntries(l);
					if (tmp !== null) {
						localization_entries.putAll(tmp);
						return;
					}
				}
				var pos = locale.lastIndexOf('_');
				if (pos === -1) {
					break;
				}
				locale = locale.substring(0, pos);
			}
		},

		initSystemBundle : function() {
			this.vj$.bundleContext = new this.vj$.BundleContextImpl(this);
			var sp = new this.vj$.StringBuffer(this.fwCtx.props.getProperty(this.vj$.Constants.FRAMEWORK_SYSTEMPACKAGES));
			if (sp.length() === 0) {
				addSysPackagesFromFile(sp, this.fwCtx.props.getProperty(this.vj$.FrameworkProperties.SYSTEM_PACKAGES_FILE_PROP), null);
				if (sp.length() === 0) {
					sp.append(this.fwCtx.props.getProperty(this.vj$.FrameworkProperties.SYSTEM_PACKAGES_BASE_PROP));
					if (sp.length() === 0) {
						var jver = this.fwCtx.props.getProperty(this.vj$.FrameworkProperties.SYSTEM_PACKAGES_VERSION_PROP);
						var jv;
						if (jver === null) {
							jv = new this.vj$.Version(this.vj$.FrameworkProperties.javaVersionMajor, this.vj$.FrameworkProperties.javaVersionMinor, 0);
						}
						else {
							try {
								jv = new this.vj$.Version(jver);
							}
							catch (_ignore) {
								if (this.fwCtx.debug.framework) {
									this.fwCtx.debug.println("No built in list of Java packages to be exported " + "by the system bundle for JRE with version '" + jver + "', using the list for 1.7.");
								}
								jv = new this.vj$.Version(1, 7, 0);
							}
						}
						this.addSysPackagesFromFile(sp, "packages.txt", jv);
					}
					else {
						if (sp.charAt(sp.length() - 1) === ',') {
							sp.deleteCharAt(sp.length() - 1);
						}
					}
					this.addSysPackagesFromFile(sp, "exports", null);
				}
			}
			var extraPkgs = this.fwCtx.props.getProperty(this.vj$.Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA);
			if (extraPkgs.length > 0) {
				sp.append(",").append(extraPkgs);
			}
			this.exportPackageString = sp.toString();
			sp.setLength(0);
			sp.append(this.fwCtx.props.getProperty(this.vj$.Constants.FRAMEWORK_SYSTEMCAPABILITIES));
			if (sp.length() === 0) {
				this.addSysCapabilitiesFromEE(sp);
			}
			var epc = this.fwCtx.props.getProperty(this.vj$.Constants.FRAMEWORK_SYSTEMCAPABILITIES_EXTRA);
			if (epc.length > 0) {
				if (sp.length() > 0) {
					sp.append(',');
				}
				sp.append(epc);
			}
			this.provideCapabilityString = sp.toString();
			var gen = new this.vj$.BundleGeneration(this, this.exportPackageString, this.provideCapabilityString);
			this.vj$.generations.add(gen);
			gen.bpkgs.registerPackages();
			try {
				gen.bpkgs.resolvePackages(null);
			}
			catch (_ignore) {
			}
			gen.setWired();
			this.fwWiring = new this.vj$.FrameworkWiringImpl(this.vj$.fwCtx);
		},

		uninitSystemBundle : function() {
			this.vj$.bundleContext.invalidate();
			this.vj$.bundleContext = null;
		},

		doInit : function() {
		    this.state = Bundle.STARTING;
		    this.bootClassPathHasChanged = false;
		    this.fwCtx.init();
		},

		addSysPackagesFromFile : function(sp, sysPkgFile, guard) {
		},

		addSysCapabilitiesFromEE : function(sp) {
		},

		addSysCapabilityEE : function(eeNameVersions, name, v) {
		},

		addSysCapabilityForEE : function(sb, eeNameVersions) {
		},

		shutdown : function(restart) {
		},

		shutdown0 : function(restart, wasActive) {
		},

		shutdownDone : function(restart) {
		},

		stopAllBundles : function() {
		},

		saveClasspaths : function() {
		},

		saveStringBuffer : function(f, content) {
		},

		addClassPathURL : function(url) {
		},

		handleExtensionActivator : function(extension) {
		},

		start : function(options) {
			if (arguments.length === 1) {
				if (typeof arguments[0] == "number") {
					this.start_1_0_SystemBundle_ovld(arguments[0]);
				}
				else if (this.base && this.base.start) {
					this.base.start.apply(this, arguments);
				}
			}
			else if (this.base && this.base.start) {
				this.base.start.apply(this, arguments);
			}
		},

		start_1_0_SystemBundle_ovld : function(options) {
			var bundlesToStart = null;
			{
				this.waitOnOperation(this.lock, "Framework.start", true);
				switch (this.state) {
					case Bundle.INSTALLED:
					case Bundle.RESOLVED:
						this.doInit();
					case Bundle.STARTING:
						this.operation = this.vj$.BundleImpl.ACTIVATING;
						break;
					case Bundle.ACTIVE:
						return;
					default:
						throw new IllegalStateException("INTERNAL ERROR, Illegal state, " + this.state);
				}
				if (this.fwCtx.startLevelController === null) {
					bundlesToStart = this.fwCtx.storage.getStartOnLaunchBundles();
				}
			}
			if (this.fwCtx.startLevelController !== null) {
				this.fwCtx.startLevelController.open();
			}
			else {
				var i = bundlesToStart.iterator();
				while (i.hasNext()) {
					var b = this.fwCtx.bundles.getBundle(i.next());
					try {
						var autostartSetting = b.current().archive.getAutostartSetting();
						var option = Bundle.START_TRANSIENT;
						if (Bundle.START_ACTIVATION_POLICY === autostartSetting) {
							option |= Bundle.START_ACTIVATION_POLICY;
						}
						b.start(option);
					}
					catch (be) {
						this.fwCtx.frameworkError(b, be);
					}
				}
			}
			{
				this.state = Bundle.ACTIVE;
				this.operation = this.vj$.BundleImpl.IDLE;
				this.lock.notifyAll();
				this.fwCtx.listeners.frameworkEvent(new this.vj$.FrameworkEvent(this.vj$.FrameworkEvent.STARTED, this, null));
			}
		},

		stop : function(options) {
			if (arguments.length === 1) {
				if (typeof arguments[0] == "number") {
					this.stop_1_0_SystemBundle_ovld(arguments[0]);
				}
				else if (this.base && this.base.stop) {
					this.base.stop.apply(this, arguments);
				}
			}
			else if (this.base && this.base.stop) {
				this.base.stop.apply(this, arguments);
			}
		},

		stop_1_0_SystemBundle_ovld : function(options) {
			this.vj$.secure.checkExecuteAdminPerm(this);
			this.vj$.secure.callShutdown(this, false);
		},

		update : function(in_) {
			if (arguments.length === 1) {
				if (arguments[0] instanceof org.eclipse.vjet.vjo.java.io.InputStream) {
					this.update_1_0_SystemBundle_ovld(arguments[0]);
				}
				else if (this.base && this.base.update) {
					this.base.update.apply(this, arguments);
				}
			}
			else if (this.base && this.base.update) {
				this.base.update.apply(this, arguments);
			}
		},

		update_1_0_SystemBundle_ovld : function(in_) {
			this.vj$.secure.checkLifecycleAdminPerm(this);
			if (in_ !== null) {
				try {
					in_.close();
				}
				catch (ignore) {
				}
			}
			this.vj$.secure.callShutdown(this, true);
		}

	})

	.endType();

})