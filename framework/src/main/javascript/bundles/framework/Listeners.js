/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.Listeners') 
.needs(['org.eclipse.vjet.vjo.java.util.HashSet','org.eclipse.vjet.vjo.java.util.LinkedList',
    'org.eclipse.vjet.vjo.java.util.HashMap','org.eclipse.vjet.vjo.java.lang.Integer',
    'org.eclipse.vjet.vjo.java.lang.NumberFormatException','org.eclipse.vjet.vjo.java.util.Collection',
    'org.eclipse.vjet.vjo.java.util.Set','vjo.java.lang.Throwable',
    'org.eclipse.vjet.vjo.java.lang.IllegalStateException','org.eclipse.vjet.vjo.java.util.Iterator',
    'org.eclipse.vjet.vjo.java.lang.Exception','bundles.framework.ListenerEntry',
    'bundles.framework.ServiceListenerState','bundles.framework.PermissionOps',
    'bundles.framework.FrameworkContext','bundles.framework.FrameworkProperties',
    'bundles.framework.BundleContextImpl','osgi.framework.BundleListener',
    'osgi.framework.SynchronousBundleListener','osgi.framework.FrameworkListener',
    'osgi.framework.ServiceListener','osgi.framework.BundleEvent',
    'osgi.framework.FrameworkEvent','bundles.framework.ServiceListenerEntry',
    'osgi.framework.ServiceEvent','bundles.framework.ServiceReferenceImpl',
    'osgi.framework.Constants','osgi.framework.AllServiceListener',
    'osgi.framework.ServiceReference','osgi.framework.BundleContext',
    'java.util.EventObject'])
.protos({
    bundleListeners:null, 
    syncBundleListeners:null, 
    frameworkListeners:null, 
    serviceListeners:null, 
    asyncEventQueue:null, 
    threads:null, 
    activeListeners:null, 
    secure:null, 
    fwCtx:null, 
    nocacheldap:false, 
    quit:false, 
    AsyncEvent:vjo.ctype() 
    .protos({
        le:null, 
        evt:null, 
        
        constructs:function(le,evt){
            this.le=le;
            this.evt=evt;
        }
    })
    .endType(),
    AsyncEventThread:vjo.ctype() 
    .inherits('Thread')
    .protos({
        
        constructs:function(i){
            this.base(this.vj$.outer.fwCtx.threadGroup,"AsyncEventThread#"+i);
        },
        
        run:function(){
            while(true){
                var ae; 
                {
                    while(!this.vj$.outer.quit && this.vj$.outer.asyncEventQueue.isEmpty()){
                        try {
                            this.vj$.outer.asyncEventQueue.wait();
                        }
                        catch(ignored){
                        }
                    }
                    if(this.vj$.outer.quit){
                        break;
                    }
                    ae=this.vj$.outer.asyncEventQueue.removeFirst();
                }
                if(this.vj$.outer.activeListeners!==null){
                    {
                        while(this.vj$.outer.activeListeners.containsKey(ae.le)){
                            try {
                                this.vj$.outer.activeListeners.wait();
                            }
                            catch(ignore){
                            }
                        }
                        this.vj$.outer.activeListeners.put(ae.le,this.vj$.Thread.currentThread());
                    }
                }
                if(ae.le.bc===null||ae.le.bc.isValid()){
                    if(osgi.framework.BundleEvent.isInstance(ae.evt)){
                        this.vj$.outer.bundleChanged(ae.le,/*>>*/ae.evt);
                    }else {
                        this.vj$.outer.frameworkEvent(ae.le,/*>>*/ae.evt);
                    }
                }
                if(this.vj$.outer.activeListeners!==null){
                    {
                        this.vj$.outer.activeListeners.remove(ae.le);
                        this.vj$.outer.activeListeners.notifyAll();
                    }
                }
            }
        }
    })
    .endType(),
    
    constructs:function(framework,perm){
        this.bundleListeners=new this.vj$.HashSet();
        this.syncBundleListeners=new this.vj$.HashSet();
        this.frameworkListeners=new this.vj$.HashSet();
        this.fwCtx=framework;
        this.secure=perm;
        this.nocacheldap=framework.props.getBooleanProperty(this.vj$.FrameworkProperties.LDAP_NOCACHE_PROP);
        this.serviceListeners=new this.vj$.ServiceListenerState(this);
        var ets=framework.props.getProperty(this.vj$.FrameworkProperties.LISTENER_N_THREADS_PROP); 
        var n_threads=1; 
        if(ets!==null){
            try {
                n_threads=this.vj$.Integer.parseInt(ets);
            }
            catch(nfe){
            }
        }
        if(n_threads>0){
            this.asyncEventQueue=new this.vj$.LinkedList();
            this.threads=vjo.createArray(null, n_threads);
            for (var i=0;i<n_threads;i++){
                this.threads[i]=new this.AsyncEventThread(i);
                this.threads[i].start();
            }
            if(n_threads>1){
                this.activeListeners=new this.vj$.HashMap();
            }
        }
    },
    
    clear:function(){
        this.bundleListeners.clear();
        this.syncBundleListeners.clear();
        this.frameworkListeners.clear();
        this.serviceListeners.clear();
        this.secure=null;
        this.fwCtx=null;
    },
    
    addBundleListener:function(bc,listener){
        var le=new this.vj$.ListenerEntry(bc,listener); 
        if(osgi.framework.SynchronousBundleListener.isInstance(listener)){
            this.secure.checkListenerAdminPerm(bc.bundle);
            {
                this.syncBundleListeners.add(le);
            }
        }else {
            {
                this.bundleListeners.add(le);
            }
        }
    },
    
    removeBundleListener:function(bc,listener){
        var le=new this.vj$.ListenerEntry(bc,listener); 
        if(osgi.framework.SynchronousBundleListener.isInstance(listener)){
            {
                this.secure.checkListenerAdminPerm(bc.bundle);
                this.syncBundleListeners.remove(le);
            }
        }else {
            {
                this.bundleListeners.remove(le);
            }
        }
    },
    
    addFrameworkListener:function(bc,listener){
        var le=new this.vj$.ListenerEntry(bc,listener); 
        {
            this.frameworkListeners.add(le);
        }
    },
    
    removeFrameworkListener:function(bc,listener){
        {
            this.frameworkListeners.remove(new this.vj$.ListenerEntry(bc,listener));
        }
    },
    
    addServiceListener:function(bc,listener,filter){
        this.serviceListeners.add(bc,listener,filter);
    },
    
    removeServiceListener:function(bc,listener){
        this.serviceListeners.remove(bc,listener);
    },
    
    serviceChanged:function(receivers,evt,matchBefore){
        var sr=evt.getServiceReference(); 
        var classes=sr.getProperty(this.vj$.Constants.OBJECTCLASS); 
        var n=0; 
        if(matchBefore!==null){
            for (var l,_$itr=receivers.iterator();_$itr.hasNext();){
                l=_$itr.next();
                matchBefore.remove(l);
            }
        }
        this.fwCtx.serviceHooks.filterServiceEventReceivers(evt,receivers);
        for (var l,_$itr=receivers.iterator();_$itr.hasNext();){
            l=_$itr.next();
            try {
                if(!l.isRemoved()){
                    var testAssignable=!(osgi.framework.AllServiceListener.isInstance(l.listener)); 
                    for (var i=0;i<classes.length;i++){
                        if(testAssignable&& !sr.isAssignableTo(l.bc.bundle,classes[i])){
                            continue;
                        }
                        try {
                            l.listener.serviceChanged(evt);
                        }
                        catch(pe){
                            this.fwCtx.frameworkError(l.bc,pe);
                        }
                        break;
                    }
                }
            }
            catch(ignore){
            }
        }
        if(this.fwCtx.debug.ldap){
            this.fwCtx.debug.println("Notified "+n+" listeners");
        }
    },
    
    getMatchingServiceListeners:function(sr){
        return this.serviceListeners.getMatchingListeners(/*>>*/sr);
    },
    
    
    removeAllListeners:function(bc){
        if(arguments.length===1){
            this.removeAllListeners_1_0_Listeners_ovld(arguments[0]);
        }else if(arguments.length===2){
            this.removeAllListeners_2_0_Listeners_ovld(arguments[0],arguments[1]);
        }
    },
    
    removeAllListeners_1_0_Listeners_ovld:function(bc){
        removeAllListeners(this.syncBundleListeners,bc);
        removeAllListeners(this.bundleListeners,bc);
        removeAllListeners(this.frameworkListeners,bc);
        this.serviceListeners.removeAll(bc);
    },
    
    removeAllListeners_2_0_Listeners_ovld:function(s,bc){
        {
            for (var i=s.iterator();i.hasNext();){
                if(i.next().bc===bc){
                    i.remove();
                }
            }
        }
    },
    
    
    bundleChanged:function(evt){
        if(arguments.length===1){
            this.bundleChanged_1_0_Listeners_ovld(arguments[0]);
        }else if(arguments.length===2){
            this.bundleChanged_2_0_Listeners_ovld(arguments[0],arguments[1]);
        }
    },
    
    bundleChanged_1_0_Listeners_ovld:function(evt){
        var filteredSyncBundleListeners=new this.vj$.HashSet(); 
        var filteredBundleListeners=null; 
        var type=evt.getType(); 
        if(type!==this.vj$.BundleEvent.LAZY_ACTIVATION&&type!==this.vj$.BundleEvent.STARTING&&type!==this.vj$.BundleEvent.STOPPING){
            filteredBundleListeners=new this.vj$.HashSet();
        }
        this.fwCtx.bundleHooks.filterBundleEventReceivers(evt,filteredSyncBundleListeners,filteredBundleListeners);
        for (var le,_$itr=filteredSyncBundleListeners.iterator();_$itr.hasNext();){
            le=_$itr.next();
            this.bundleChanged(le,evt);
        }
        if(filteredBundleListeners!==null){
            if(this.asyncEventQueue!==null){
                {
                    for (var le,_$itr=filteredBundleListeners.iterator();_$itr.hasNext();){
                        le=_$itr.next();
                        this.asyncEventQueue.addLast(new this.AsyncEvent(le,evt));
                    }
                    this.asyncEventQueue.notify();
                }
            }else {
                for (var le,_$itr=filteredBundleListeners.iterator();_$itr.hasNext();){
                    le=_$itr.next();
                    this.bundleChanged(le,evt);
                }
            }
        }
    },
    
    bundleChanged_2_0_Listeners_ovld:function(le,evt){
        try {
            le.listener.bundleChanged(evt);
        }
        catch(pe){
            this.fwCtx.frameworkError(le.bc,pe);
        }
    },
    
    
    frameworkEvent:function(evt){
        var oneTimeListeners;
        if (arguments.length == 2 && arguments[1]  instanceof Array){
            oneTimeListeners=arguments[1];
        }
        else {
            oneTimeListeners=[];
            for (var i=1; i<arguments.length; i++){
                oneTimeListeners[i-1]=arguments[i];
            }
        }
        if(arguments.length===2){
            if(arguments[0] instanceof osgi.framework.FrameworkEvent && osgi.framework.FrameworkListener.clazz.isInstance(arguments[1])){
                this.frameworkEvent_2_0_Listeners_ovld(arguments[0],arguments[1]);
            }else if(arguments[0] instanceof bundles.framework.ListenerEntry && arguments[1] instanceof osgi.framework.FrameworkEvent){
                this.frameworkEvent_2_1_Listeners_ovld(arguments[0],arguments[1]);
            }
        }
    },
    
    frameworkEvent_2_0_Listeners_ovld:function(evt){
        var oneTimeListeners;
        if (arguments.length == 2 && arguments[1]  instanceof Array){
            oneTimeListeners=arguments[1];
        }
        else {
            oneTimeListeners=[];
            for (var i=1; i<arguments.length; i++){
                oneTimeListeners[i-1]=arguments[i];
            }
        }
        if(this.fwCtx.debug.errors){
            if(evt.getType()===this.vj$.FrameworkEvent.ERROR){
                this.fwCtx.debug.println("errors - FrameworkErrorEvent bundle #"+evt.getBundle().getBundleId());
                this.fwCtx.debug.printStackTrace("errors - FrameworkErrorEvent throwable: ",evt.getThrowable());
            }
        }
        if(this.fwCtx.debug.warnings){
            if(evt.getType()===this.vj$.FrameworkEvent.WARNING){
                this.fwCtx.debug.println("warnings - FrameworkErrorEvent bundle #"+evt.getBundle().getBundleId());
                this.fwCtx.debug.printStackTrace("warnings - FrameworkErrorEvent throwable: ",evt.getThrowable());
            }
        }
        if(this.fwCtx.debug.startlevel){
            if(evt.getType()===this.vj$.FrameworkEvent.STARTLEVEL_CHANGED){
                this.fwCtx.debug.println("startlevel: FrameworkEvent Startlevel Changed");
            }else if(evt.getType()===this.vj$.FrameworkEvent.STARTED){
                this.fwCtx.debug.println("startlevel: FrameworkEvent Started");
            }
        }
        if(this.asyncEventQueue!==null){
            {
                if(oneTimeListeners!==null){
                    for (var fl,_$i0=0;_$i0<oneTimeListeners.length;_$i0++){
                        fl=oneTimeListeners[_$i0];
                        this.asyncEventQueue.addLast(new this.AsyncEvent(new this.vj$.ListenerEntry(null,fl),evt));
                    }
                }
                {
                    for (var listenerEntry,_$itr=this.frameworkListeners.iterator();_$itr.hasNext();){
                        listenerEntry=_$itr.next();
                        this.asyncEventQueue.addLast(new this.AsyncEvent(listenerEntry,evt));
                    }
                }
                this.asyncEventQueue.notify();
            }
        }else {
            if(oneTimeListeners!==null){
                for (var ofl,_$i1=0;_$i1<oneTimeListeners.length;_$i1++){
                    ofl=oneTimeListeners[_$i1];
                    this.frameworkEvent(new this.vj$.ListenerEntry(null,ofl),evt);
                }
            }
            var fl; 
            {
                fl=vjo.createArray(null, this.frameworkListeners.size());
                this.frameworkListeners.toArray(fl);
            }
            for (var element,_$i2=0;_$i2<fl.length;_$i2++){
                element=fl[_$i2];
                this.frameworkEvent(element,evt);
            }
        }
    },
    
    frameworkEvent_2_1_Listeners_ovld:function(le,evt){
        try {
            le.listener.frameworkEvent(evt);
        }
        catch(pe){
            if(evt.getType()!==this.vj$.FrameworkEvent.ERROR){
                this.fwCtx.frameworkError(le.bc,pe);
            }
        }
    }
})
.endType();