/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.ServiceRegistrationImpl<S>') 
.needs(['org.eclipse.vjet.vjo.java.util.Hashtable','org.eclipse.vjet.vjo.java.lang.Integer',
    'org.eclipse.vjet.vjo.java.util.HashMap','org.eclipse.vjet.vjo.java.util.HashSet',
    'org.eclipse.vjet.vjo.java.lang.IllegalStateException','bundles.framework.ServiceReferenceImpl',
    'bundles.framework.PropertiesDictionary','osgi.framework.Constants',
    'osgi.framework.ServiceEvent','osgi.framework.ServiceFactory',
    'osgi.framework.ServiceException'])

.satisfies('osgi.framework.ServiceRegistration<S>')
.protos({
    fwCtx:null, 
    bundle:null, 
    service:null, 
    reference:null, 
    properties:null, 
    dependents:null, 
    serviceInstances:null, 
    ungetInProgress:null, 
    available:false, 
    eventLock:null, 
    unregistering:false, 
    factoryThread:null, 
    
    constructs:function(b,s,props){
        this.dependents=new this.vj$.Hashtable();
        this.serviceInstances=new this.vj$.HashMap();
        this.ungetInProgress=new this.vj$.HashSet();
        this.eventLock=Object();
        this.fwCtx=b.fwCtx;
        this.bundle=b;
        this.service=s;
        this.properties=props;
        this.reference=new this.vj$.ServiceReferenceImpl(this);
        this.available=true;
    },
    
    getReference:function(){
        var res=this.reference; 
        if(res!==null){
            return res;
        }else {
            throw new this.vj$.IllegalStateException("Service is unregistered");
        }
    },
    
    setProperties:function(props){
        {
            if(this.available){
                var before; 
                {
                    {
                        var old_rank=this.properties.get(this.vj$.Constants.SERVICE_RANKING);
                        before=this.fwCtx.listeners.getMatchingServiceListeners(this.reference);
                        var classes=this.properties.get(this.vj$.Constants.OBJECTCLASS); 
                        var sid=this.properties.get(this.vj$.Constants.SERVICE_ID); 
                        this.properties=new this.vj$.PropertiesDictionary(props,classes,sid);
                        var new_rank=this.properties.get(this.vj$.Constants.SERVICE_RANKING);
                        if(old_rank!==new_rank&&vjo.getType('org.eclipse.vjet.vjo.java.lang.Integer').isInstance(new_rank)&& !new_rank.equals(old_rank)){
                            this.fwCtx.services.updateServiceRegistrationOrder(this,classes);
                        }
                    }
                }
                this.fwCtx.perm.callServiceChanged(this.fwCtx,this.fwCtx.listeners.getMatchingServiceListeners(this.reference),new this.vj$.ServiceEvent(this.vj$.ServiceEvent.MODIFIED,this.reference),before);
                if(!before.isEmpty()){
                    this.fwCtx.perm.callServiceChanged(this.fwCtx,before,new this.vj$.ServiceEvent(this.vj$.ServiceEvent.MODIFIED_ENDMATCH,this.reference),null);
                }
            }else {
                throw new this.vj$.IllegalStateException("Service is unregistered");
            }
        }
    },
    
    unregister:function(){
        if(this.unregistering){
            return;
        }
        {
            if(this.unregistering){
                return;
            }
            this.unregistering=true;
            if(this.available){
                if(null!==this.bundle){
                    this.fwCtx.services.removeServiceRegistration(this);
                }
            }else {
                throw new this.vj$.IllegalStateException("Service is unregistered");
            }
        }
        if(null!==this.bundle){
            this.fwCtx.perm.callServiceChanged(this.fwCtx,this.fwCtx.listeners.getMatchingServiceListeners(this.reference),new this.vj$.ServiceEvent(this.vj$.ServiceEvent.UNREGISTERING,this.reference),null);
        }
        {
            this.available=false;
            var using=this.getUsingBundles(); 
            if(using!==null){
                for (var element,_$i0=0;_$i0<using.length;_$i0++){
                    element=using[_$i0];
                    this.ungetService(element,false);
                }
            }
            {
                this.bundle=null;
                this.dependents=null;
                this.reference=null;
                this.service=null;
                this.serviceInstances=null;
                this.unregistering=false;
            }
        }
    },
    
    getProperties:function(){
        return this.properties;
    },
    
    getProperty:function(key){
        return this.properties.get(key);
    },
    
    getService:function(b){
        var ref; 
        var sBundle=null; 
        {
            if(this.available){
                ref=this.dependents.get(b);
                if(osgi.framework.ServiceFactory.isInstance(this.service)){
                    if(ref===null){
                        this.dependents.put(b,new this.vj$.Integer(0));
                        this.factoryThread=this.vj$.Thread.currentThread();
                    }else if(this.factoryThread!==null){
                        if(this.factoryThread.equals(this.vj$.Thread.currentThread())){
                            throw new this.vj$.IllegalStateException("Recursive call of getService");
                        }
                    }
                }else {
                    this.dependents.put(b,new this.vj$.Integer(ref!==null?ref.intValue()+1:1));
                    var res=this.service; 
                    return res;
                }
                sBundle=this.bundle;
            }else {
                return null;
            }
        }
        var s=null; 
        if(ref===null){
            try {
                s=sBundle.fwCtx.perm.callGetService(this,b);
                if(s===null){
                    sBundle.fwCtx.frameworkWarning(sBundle,new this.vj$.ServiceException("ServiceFactory produced null",this.vj$.ServiceException.FACTORY_ERROR));
                }
            }
            catch(pe){
                sBundle.fwCtx.frameworkError(sBundle,new this.vj$.ServiceException("ServiceFactory throw an exception",this.vj$.ServiceException.FACTORY_EXCEPTION,pe));
            }
            if(s!==null){
                var classes=this.getProperty(this.vj$.Constants.OBJECTCLASS); 
                for (var classe,_$i1=0;_$i1<classes.length;_$i1++){
                    classe=classes[_$i1];
                    var cls=classe; 
                    if(!sBundle.fwCtx.services.checkServiceClass(s,cls)){
                        sBundle.fwCtx.frameworkError(sBundle,new this.vj$.ServiceException("ServiceFactory produced an object "+"that did not implement: "+cls,this.vj$.ServiceException.FACTORY_ERROR));
                        s=null;
                        break;
                    }
                }
            }
            if(s===null){
                {
                    if(this.dependents!==null){
                        this.dependents.remove(b);
                    }
                    this.factoryThread=null;
                    return null;
                }
            }
        }
        var recall=false; 
        {
            if(s===null){
                while(true){
                    s=this.serviceInstances.get(b);
                    if(s!==null){
                        break;
                    }
                    try {
                        this.properties.wait(500);
                    }
                    catch(ie){
                    }
                    if(this.dependents===null|| !this.dependents.containsKey(b)){
                        break;
                    }
                }
            }else {
                this.factoryThread=null;
                this.serviceInstances.put(b,s);
                this.properties.notifyAll();
            }
            if(s!==null){
                ref=this.dependents!==null?this.dependents.get(b):null;
                if(ref!==null){
                    this.dependents.put(b,new this.vj$.Integer(ref.intValue()+1));
                }else {
                    recall=true;
                }
            }
        }
        if(recall){
            try {
                sBundle.fwCtx.perm.callUngetService(this,b,s);
            }
            catch(e){
                sBundle.fwCtx.frameworkError(sBundle,e);
            }
            return null;
        }else {
            return s;
        }
    },
    
    getUsingBundles:function(){
        var d=this.dependents; 
        if(d!==null){
            {
                var size=d.size()+this.ungetInProgress.size(); 
                if(size>0){
                    var res=vjo.createArray(null, size); 
                    for (var e=d.keys();e.hasMoreElements();){
                        res[--size]=e.nextElement();
                    }
                    for (var b,_$itr=this.ungetInProgress.iterator();_$itr.hasNext();){
                        b=_$itr.next();
                        res[--size]=b;
                    }
                    return res;
                }
            }
        }
        return null;
    },
    
    isUsedByBundle:function(b){
        var deps=this.dependents; 
        if(deps!==null){
            return deps.containsKey(b);
        }else {
            return false;
        }
    },
    
    ungetService:function(b,checkRefCounter){
        var serviceToRemove=null; 
        var deps; 
        var sBundle; 
        {
            if(this.dependents===null){
                return false;
            }
            var countInteger=this.dependents.get(b);
            if(countInteger===null){
                return false;
            }
            var count=countInteger.intValue(); 
            if(checkRefCounter&&count>1){
                this.dependents.put(b,new this.vj$.Integer(count-1));
            }else {
                {
                    this.ungetInProgress.add(b);
                    this.dependents.remove(b);
                }
                serviceToRemove=this.serviceInstances.remove(b);
            }
            deps=this.dependents;
            sBundle=this.bundle;
        }
        if(serviceToRemove!==null){
            if(osgi.framework.ServiceFactory.isInstance(this.service)){
                try {
                    sBundle.fwCtx.perm.callUngetService(this,b,serviceToRemove);
                }
                catch(e){
                    sBundle.fwCtx.frameworkError(sBundle,e);
                }
            }
        }
        {
            this.ungetInProgress.remove(b);
        }
        return true;
    }
})
.endType();