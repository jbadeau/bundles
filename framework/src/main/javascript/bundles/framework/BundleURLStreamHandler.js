/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleURLStreamHandler') 
.needs(['org.eclipse.vjet.vjo.java.util.ArrayList','org.eclipse.vjet.vjo.java.lang.Long',
    'org.eclipse.vjet.vjo.java.lang.Character','org.eclipse.vjet.vjo.java.lang.IllegalArgumentException',
    'org.eclipse.vjet.vjo.java.lang.System','org.eclipse.vjet.vjo.java.lang.StringBuffer',
    'org.eclipse.vjet.vjo.java.lang.Integer','java.io.IOException',
    'bundles.framework.BundleURLConnection','org.eclipse.vjet.vjo.java.lang.StringUtil',
    'vjo.java.lang.ObjectUtil'])
.needs('org.eclipse.vjet.vjo.java.lang.StringFactory','')

.inherits('java.net.URLStreamHandler')
.props({
    PROTOCOL:"bundle", 
    PERM_OK:"P", 
    
    getId:function(host){
        var e=host.indexOf("."); 
        if(e=== -1){
            e=host.indexOf("!");
        }
        if(e>=0){
            host=host.substring(0,e);
        }
        return this.vj$.Long.parseLong(host);
    }
})
.protos({
    framework:null, 
    
    constructs:function(){
        this.framework=new this.vj$.ArrayList(2);
    },
    
    openConnection:function(u){
        var h=u.getHost(); 
        var fw=this.getFramework(h); 
        if(fw===null){
            throw new this.vj$.IOException("Framework associated with URL is not active");
        }
        if(u.getAuthority()!==this.vj$.BundleURLStreamHandler.PERM_OK){
            fw.perm.checkResourceAdminPerm(fw.bundles.getBundle(this.vj$.BundleURLStreamHandler.getId(h)));
        }
        return new this.vj$.BundleURLConnection(u,fw);
    },
    
    parseURL:function(u,s,start,limit){
        var path=u.getPath(); 
        var host=u.getHost(); 
        var id=-1; 
        var cpElem=u.getPort(); 
        if(limit>start){
            var len=limit-start; 
            var sc=vjo.createArray("", len); 
            org.eclipse.vjet.vjo.java.lang.StringUtil.getChars(s,start,limit,sc,0);
            var pos=0; 
            if(len>=2&&sc[0]==='/'&&sc[1]==='/'){
                for (pos=2;pos<len;pos++){
                    if(sc[pos]===':'||sc[pos]==='/'){
                        break;
                    }else if(sc[pos]==='!'||sc[pos]==='.'){
                        if(id=== -1){
                            id=this.vj$.Long.parseLong(org.eclipse.vjet.vjo.java.lang.StringFactory.build(sc,2,pos-2));
                        }
                    }else if(!this.vj$.Character.isDigit(sc[pos])){
                        throw new this.vj$.IllegalArgumentException("Illegal chars in bundle id specification");
                    }
                }
                host=org.eclipse.vjet.vjo.java.lang.StringFactory.build(sc,2,pos-2);
                if(pos<len&&sc[pos]===':'){
                    ++pos;
                    cpElem=0;
                    while(pos<len){
                        if(sc[pos]==='/'){
                            break;
                        }else if(!this.vj$.Character.isDigit(sc[pos])){
                            throw new this.vj$.IllegalArgumentException("Illegal chars in bundle port specification");
                        }
                        cpElem=10*cpElem+(sc[pos++]-'0');
                    }
                }else {
                    cpElem=-1;
                }
            }
            if(pos<len){
                var pstart; 
                if(sc[pos]!=='/'){
                    if(path!==null){
                        var dirend=path.lastIndexOf('/')+1; 
                        if(dirend>0){
                            var plen=len-pos; 
                            pstart=org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(path,"/")?0:1;
                            len=dirend+plen+pstart;
                            if(len>sc.length){
                                var newsc=vjo.createArray("", len); 
                                this.vj$.System.arraycopy(sc,pos,newsc,dirend+pstart,plen);
                                sc=newsc;
                            }else if(pos!==dirend){
                                this.vj$.System.arraycopy(sc,pos,sc,dirend+pstart,plen);
                            }
                            org.eclipse.vjet.vjo.java.lang.StringUtil.getChars(path,1-pstart,dirend,sc,1);
                        }else {
                            len=1;
                        }
                    }else {
                        len=1;
                    }
                    sc[0]='/';
                    pstart=0;
                    pos=0;
                }else {
                    pstart=pos;
                }
                var dots=0; 
                var ipos=pstart-1; 
                var slash=false; 
                for (;pos<len;pos++){
                    if(sc[pos]==='/'){
                        if(slash){
                            continue;
                        }
                        slash=true;
                        if(dots===1){
                            dots=0;
                            continue;
                        }else if(dots===2){
                            if(ipos>pstart){
                                dots=0;
                                while(ipos>pstart && sc[--ipos]!=='/'){
                                }
                                continue;
                            }
                        }
                    }else if(sc[pos]==='.'){
                        if(slash){
                            dots=1;
                            slash=false;
                            continue;
                        }else if(dots===1){
                            dots=2;
                            continue;
                        }
                    }else {
                        slash=false;
                    }
                    while(dots-->0){
                        sc[++ipos]='.';
                    }
                    if(++ipos!==pos){
                        sc[ipos]=sc[pos];
                    }
                }
                if(dots===2){
                    if(ipos>pstart){
                        while(ipos>pstart && sc[--ipos]!=='/'){
                        }
                    }else {
                        while(dots-->0){
                            sc[++ipos]='.';
                        }
                        sc[++ipos]='/';
                    }
                }
                path=org.eclipse.vjet.vjo.java.lang.StringFactory.build(sc,pstart,ipos-pstart+1);
            }
        }
        if(id=== -1){
            id=this.vj$.BundleURLStreamHandler.getId(host);
        }
        var fw=this.getFramework(host); 
        if(fw===null){
            throw new this.vj$.IllegalArgumentException("Framework associated with URL is not active");
        }
        fw.perm.checkResourceAdminPerm(fw.bundles.getBundle(id));
        setURL(u,this.vj$.BundleURLStreamHandler.PROTOCOL,host,cpElem,this.vj$.BundleURLStreamHandler.PERM_OK,null,path,null,null);
    },
    
    equals:function(u1,u2){
        return this.sameFile(u1,u2);
    },
    
    hashCode:function(u){
        var h=0; 
        if(vjo.java.lang.ObjectUtil.equals(this.vj$.BundleURLStreamHandler.PROTOCOL,u.getProtocol())){
            var host=u.getHost(); 
            if(host!==null){
                h=vjo.java.lang.ObjectUtil.hashCode(host);
            }
            var file=u.getFile(); 
            if(file!==null){
                h+=vjo.java.lang.ObjectUtil.hashCode(file);
            }
            h+=u.getPort();
        }else {
            h=u.hashCode();
        }
        return h;
    },
    
    sameFile:function(u1,u2){
        var p1=u1.getProtocol(); 
        if(vjo.java.lang.ObjectUtil.equals(this.vj$.BundleURLStreamHandler.PROTOCOL,p1)){
            if(!vjo.java.lang.ObjectUtil.equals(p1,u2.getProtocol())){
                return false;
            }
            if(!this.hostsEqual(u1,u2)){
                return false;
            }
            if(!(u1.getFile()===u2.getFile()||(u1.getFile()!==null&&u1.getFile().equals(u2.getFile())))){
                return false;
            }
            if(u1.getPort()!==u2.getPort()){
                return false;
            }
            return true;
        }else {
            return u1.equals(u2);
        }
    },
    
    hostsEqual:function(u1,u2){
        var s1=u1.getHost(); 
        var s2=u2.getHost(); 
        return (s1===s2)||(s1!==null&&vjo.java.lang.ObjectUtil.equals(s1,s2));
    },
    
    toExternalForm:function(url){
        var res=new this.vj$.StringBuffer(url.getProtocol()); 
        res.append("://");
        res.append(url.getHost());
        var port=url.getPort(); 
        if(port>=0){
            res.append(":").append(port);
        }
        res.append(url.getPath());
        return res.toString();
    },
    
    getHostAddress:function(url){
        return null;
    },
    
    addFramework:function(fw){
        this.framework.add(fw);
    },
    
    removeFramework:function(fw){
        this.framework.remove(fw);
    },
    
    getFramework:function(host){
        var i=this.framework.iterator(); 
        var e=host.indexOf("!"); 
        var fwId; 
        if(e=== -1){
            fwId=0;
        }else {
            try {
                fwId=this.vj$.Integer.parseInt(host.substring(e+1));
            }
            catch(_){
                return null;
            }
        }
        while(i.hasNext()){
            var fw=i.next(); 
            if(fw.id===fwId){
                return fw;
            }
        }
        return null;
    }
})
.endType();