/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.AutoManifest') 
.needs(['org.eclipse.vjet.vjo.java.util.Set','org.eclipse.vjet.vjo.java.util.TreeSet',
    'org.eclipse.vjet.vjo.java.util.Map','org.eclipse.vjet.vjo.java.lang.NullPointerException',
    'org.eclipse.vjet.vjo.java.util.TreeMap','org.eclipse.vjet.vjo.java.io.InputStream',
    'org.eclipse.vjet.vjo.java.io.OutputStream','org.eclipse.vjet.vjo.java.util.Hashtable',
    'org.eclipse.vjet.vjo.java.util.Enumeration','org.eclipse.vjet.vjo.java.lang.StringBuffer',
    'org.eclipse.vjet.vjo.java.lang.Exception','org.eclipse.vjet.vjo.java.util.HashMap',
    'org.eclipse.vjet.vjo.java.util.Properties','org.eclipse.vjet.vjo.java.lang.RuntimeException',
    'bundles.framework.FrameworkContext','java.util.jar.Attributes',
    'bundles.framework.FrameworkProperties','osgi.framework.Constants',
    'java.io.File','java.util.zip.ZipFile',
    'java.util.zip.ZipEntry','java.net.URL',
    'bundles.framework.LDAPExpr','vjo.java.lang.ObjectUtil',
    'org.eclipse.vjet.vjo.java.lang.StringUtil'])
.inherits('java.util.jar.Manifest')
.props({
    configSource:null, 
    configs:null, 
    AutoInfo:vjo.ctype() 
    .protos({
        filter:null, 
        exportFilter:null, 
        fileNameFilter:null, 
        headers:null, 
        version:null, 
        
        constructs:function(filter,export_,nameFilter,headers){
        },
        
        toString:function(){
        }
    })
    .endType()
})
.protos({
    fwCtx:null, 
    mf:null, 
    location:null, 
    autoInfo:null, 
    mainAttrs:null, 
    packages:null, 
    fileProps:null, 
    
    constructs:function(fwCtx,mf,location){
        this.packages=new this.vj$.TreeSet();
        this.fileProps=new this.vj$.Hashtable();
        this.base();
        if(mf===null){
            throw new this.vj$.NullPointerException("Manifest cannot be null");
        }
        if(location===null){
            throw new this.vj$.NullPointerException("location cannot be null");
        }
        this.fwCtx=fwCtx;
        this.mf=mf;
        this.location=location;
        if(this.vj$.AutoManifest.configs===null){
            if(fwCtx.props.getBooleanProperty(this.vj$.FrameworkProperties.AUTOMANIFEST_PROP)){
                this.vj$.AutoManifest.configSource=fwCtx.props.getProperty(this.vj$.FrameworkProperties.AUTOMANIFEST_CONFIG_PROP);
                this.vj$.AutoManifest.configs=this.loadConfig(this.vj$.AutoManifest.configSource);
                if(fwCtx.debug.automanifest){
                    fwCtx.debug.println("Loaded auto manifest config from "+this.vj$.AutoManifest.configSource);
                }
            }else {
                this.vj$.AutoManifest.configs=new this.vj$.TreeMap();
            }
        }
        this.autoInfo=this.findConfig();
        if(this.isAuto()&&fwCtx.debug.automanifest){
            fwCtx.debug.println("Using auto manifest for bundlelocation "+location);
        }
    },
    
    isAuto:function(){
        return this.autoInfo!==null;
    },
    
    clear:function(){
        this.mf.clear();
        this.mainAttrs=null;
    },
    
    getAttributes:function(name){
        return this.mf.getAttributes(name);
    },
    
    getEntries:function(){
        return this.mf.getEntries();
    },
    
    read:function(is){
        this.mf.read(is);
        this.mainAttrs=null;
    },
    
    write:function(out){
        this.mf.write(out);
    },
    
    getManifest:function(){
        return this.mf;
    },
    
    getLocation:function(){
        return this.location;
    },
    
    equals:function(obj){
        if(obj===null|| !(bundles.framework.AutoManifest.isInstance(obj))){
            return false;
        }
        var af=obj; 
        return org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(this.mf,af.mf)&&vjo.java.lang.ObjectUtil.equals(this.location,af.location);
    },
    
    hashCode:function(){
        return org.eclipse.vjet.vjo.java.lang.ObjectUtil.hashCode(this.mf)+17*vjo.java.lang.ObjectUtil.hashCode(this.location);
    },
    
    getMainAttributes:function(){
        if(this.mainAttrs===null){
            this.mainAttrs=this.mf.getMainAttributes();
            if(this.autoInfo!==null){
                this.mainAttrs.putValue("Bundle-AutoManifest-config",this.vj$.AutoManifest.configSource);
                for (var key,_$itr=this.autoInfo.headers.keySet().iterator();_$itr.hasNext();){
                    key=_$itr.next();
                    var val=this.autoInfo.headers.get(key); 
                    if(vjo.java.lang.ObjectUtil.equals("[remove]",val)){
                        this.mainAttrs.remove(new this.vj$.Attributes.Name(key));
                    }else if(vjo.java.lang.ObjectUtil.equals("[autoexport]",val)){
                        var exports=this.getExports(); 
                        if(this.fwCtx.debug.automanifest){
                            this.fwCtx.debug.println("Auto exports for "+this.location+": "+exports);
                        }
                        if(exports.length>0){
                            this.mainAttrs.putValue(this.vj$.Constants.EXPORT_PACKAGE,exports);
                        }else {
                            this.mainAttrs.remove(new this.vj$.Attributes.Name(this.vj$.Constants.EXPORT_PACKAGE));
                        }
                    }else {
                        this.mainAttrs.putValue(key,val);
                    }
                }
            }
        }
        return this.mainAttrs;
    },
    
    
    addFile:function(file){
        if(arguments.length===1){
            if(arguments[0] instanceof java.io.File){
                this.addFile_1_0_AutoManifest_ovld(arguments[0]);
            }else if(this.base && this.base.addFile){
                this.base.addFile.apply(this,arguments);
            }
        }else if(arguments.length===2){
            if((arguments[0] instanceof String || typeof arguments[0]=="string") && arguments[1] instanceof java.io.File){
                this.addFile_2_0_AutoManifest_ovld(arguments[0],arguments[1]);
            }else if(this.base && this.base.addFile){
                this.base.addFile.apply(this,arguments);
            }
        }else if(this.base && this.base.addFile){
            this.base.addFile.apply(this,arguments);
        }
    },
    
    addFile_1_0_AutoManifest_ovld:function(file){
        addFile(file.getAbsolutePath(),file);
    },
    
    addFile_2_0_AutoManifest_ovld:function(prefix,file){
        var f=prefix.length<file.getAbsolutePath().length()?file.getAbsolutePath().substring(prefix.length+1):file.getAbsolutePath(); 
        if(org.eclipse.vjet.vjo.java.lang.StringUtil.endsWith(f,".jar")){
            this.addZipFile(new this.vj$.ZipFile(file));
        }else if(this.isValidFileName(f)){
            this.addFileName(f);
        }else if(file.isDirectory()){
            var files=file.list(); 
            for (var file2,_$i0=0;_$i0<files.length;_$i0++){
                file2=files[_$i0];
                this.addFile(prefix,new this.vj$.File(file.getAbsolutePath(),file2));
            }
        }
    },
    
    isValidFileName:function(f){
        if(this.autoInfo!==null&&this.autoInfo.fileNameFilter!==null){
            this.fileProps.put("file",f);
            return this.autoInfo.fileNameFilter.evaluate(this.fileProps,true);
        }else {
            return org.eclipse.vjet.vjo.java.lang.StringUtil.endsWith(f,".class");
        }
    },
    
    addZipFile:function(jar){
        for (var e=jar.entries();e.hasMoreElements();){
            var ze=e.nextElement(); 
            var f=ze.getName(); 
            if(this.isValidFileName(f)){
                this.addFileName(f);
            }
        }
    },
    
    addFileName:function(f){
        f=org.eclipse.vjet.vjo.java.lang.StringUtil.replace(f,'\\','/');
        var ix=f.lastIndexOf("/"); 
        if(ix!== -1){
            f=f.substring(0,ix);
        }
        f=org.eclipse.vjet.vjo.java.lang.StringUtil.replace(f,'/','.');
        if(this.autoInfo!==null){
            var props=new this.vj$.Hashtable(); 
            props.put("pkg",f);
            if(this.autoInfo.exportFilter.evaluate(props,true)){
                if(!this.packages.contains(f)){
                    this.packages.add(f);
                }
            }
        }
        this.mainAttrs=null;
    },
    
    getExports:function(){
        var sb=new this.vj$.StringBuffer(); 
        for (var pkg,_$itr=this.packages.iterator();_$itr.hasNext();){
            pkg=_$itr.next();
            if(sb.length()>0){
                sb.append(",");
            }
            sb.append(pkg);
            if(this.autoInfo.version!==null){
                sb.append(";");
                sb.append(this.autoInfo.version);
            }
        }
        return sb.toString();
    },
    
    findConfig:function(){
        var props=new this.vj$.Hashtable(); 
        props.put("location",this.location);
        var attrs=this.mf.getMainAttributes(); 
        for (var key,_$itr=attrs.keySet().iterator();_$itr.hasNext();){
            key=_$itr.next();
            var val=attrs.getValue(key.toString());
            props.put(key.toString(),val.toString());
        }
        for (var id,_$itr=this.vj$.AutoManifest.configs.keySet().iterator();_$itr.hasNext();){
            id=_$itr.next();
            var ai=this.vj$.AutoManifest.configs.get(id); 
            if(ai.filter.evaluate(props,true)){
                return ai;
            }
        }
        return null;
    },
    
    loadConfig:function(urlS){
        if(urlS!==null&& !vjo.java.lang.ObjectUtil.equals("",urlS)){
            var url=null; 
            var is=null; 
            try {
                if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(urlS,"!!")){
                    url=this.vj$.AutoManifest.clazz.getResource(urlS.substring(2));
                }else {
                    url=new this.vj$.URL(urlS);
                }
                is=url.openStream();
                return this.loadConfigFromInputStream(is);
            }
            catch(e){
                this.fwCtx.debug.printStackTrace("Failed to load autoimportexport conf from "+url,e);
            }
            finally {
                try {
                    is.close();
                }
                catch(ignored){
                }
            }
        }
        return new this.vj$.HashMap();
    },
    
    loadConfigFromInputStream:function(is){
        var props=new this.vj$.Properties(); 
        props.load(is);
        var configMap=new this.vj$.TreeMap(); 
    }
})
.endType();