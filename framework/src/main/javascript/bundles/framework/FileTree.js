/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.FileTree') 
.needs(['java.io.BufferedInputStream','java.io.BufferedOutputStream',
    'org.eclipse.vjet.vjo.java.lang.BooleanUtil'])
.needs('org.eclipse.vjet.vjo.java.io.InputStream','')
.needs('org.eclipse.vjet.vjo.java.io.OutputStream','')
.needs('org.eclipse.vjet.vjo.java.io.FileInputStream','')
.needs('org.eclipse.vjet.vjo.java.io.FileOutputStream','')
.inherits('java.io.File')
.props({
    serialVersionUID:3396907770563704920 
})
.protos({
    
    
    
    
    constructs:function(){
        if(arguments.length===1){
            this.constructs_1_0_FileTree_ovld(arguments[0]);
        }else if(arguments.length===2){
            if(arguments[0] instanceof java.io.File && (arguments[1] instanceof String || typeof arguments[1]=="string")){
                this.constructs_2_0_FileTree_ovld(arguments[0],arguments[1]);
            }else if((arguments[0] instanceof String || typeof arguments[0]=="string") && (arguments[1] instanceof String || typeof arguments[1]=="string")){
                this.constructs_2_1_FileTree_ovld(arguments[0],arguments[1]);
            }
        }
    },
    
    constructs_1_0_FileTree_ovld:function(name){
        this.base(name);
    },
    
    constructs_2_0_FileTree_ovld:function(file,name){
        this.base(file,name);
    },
    
    constructs_2_1_FileTree_ovld:function(n1,n2){
        this.base(n1,n2);
    },
    
    copyTo:function(copyFile){
        if(isDirectory()){
            copyFile.mkdirs();
            var dirs=list(); 
            for (var i=dirs.length-1;i>=0;i--){
                (new this.vj$.FileTree(this,dirs[i])).copyTo(new this.vj$.File(copyFile,dirs[i]));
            }
        }else {
            var is=null; 
            var os=null; 
            try {
                is=new this.vj$.BufferedInputStream(new org.eclipse.vjet.vjo.java.io.FileInputStream(this));
                os=new this.vj$.BufferedOutputStream(new org.eclipse.vjet.vjo.java.io.FileOutputStream(copyFile));
                var buf=vjo.createArray(0, 4096); 
                for (;;){
                    var n=is.read(buf); 
                    if(n<0){
                        break;
                    }
                    os.write(buf,0,n);
                }
            }
            finally {
                try {
                    if(is!==null){
                        is.close();
                    }
                }
                finally {
                    if(os!==null){
                        os.close();
                    }
                }
            }
        }
    },
    
    delete_:function(){
        var allDeleted=true; 
        if(isDirectory()){
            var dirs=list(); 
            if(dirs!==null){
                for (var i=dirs.length-1;i>=0;i--){
                    allDeleted&=(new this.vj$.FileTree(this,dirs[i])).delete_();
                }
            }
        }
        var thisDeleted=delete_(); 
        return org.eclipse.vjet.vjo.java.lang.BooleanUtil.booleanValue(allDeleted)&org.eclipse.vjet.vjo.java.lang.BooleanUtil.booleanValue(thisDeleted);
    }
})
.endType();