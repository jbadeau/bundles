/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.StartLevelController') 
.needs(['org.eclipse.vjet.vjo.java.lang.Exception','org.eclipse.vjet.vjo.java.lang.NumberFormatException',
    'org.eclipse.vjet.vjo.java.lang.IllegalArgumentException','org.eclipse.vjet.vjo.java.util.Vector',
    'org.eclipse.vjet.vjo.java.util.List','org.eclipse.vjet.vjo.java.lang.IllegalStateException',
    'vjo.java.lang.Throwable','org.eclipse.vjet.vjo.java.lang.Integer',
    'bundles.framework.Queue','bundles.framework.FrameworkContext',
    'bundles.framework.FileTree','bundles.framework.FrameworkProperties',
    'bundles.framework.Util','java.io.File',
    'osgi.framework.Constants','osgi.framework.FrameworkListener',
    'osgi.framework.FrameworkEvent','bundles.framework.BundleImpl',
    'osgi.framework.Bundle','bundles.framework.BundleGeneration',
    'bundles.framework.BundleArchive','osgi.service.startlevel.StartLevel',
    'osgi.framework.ServiceRegistration','osgi.framework.startlevel.BundleStartLevel',
    'osgi.framework.startlevel.FrameworkStartLevel'])
.satisfies('Runnable')
.satisfies('osgi.framework.ServiceFactory<StartLevel>')
.props({
    SPEC_VERSION:"1.1", 
    API_SPEC_VERSION:"1.0", 
    START_MIN:0, 
    START_MAX:0, 
    LEVEL_FILE:"currentlevel", 
    INITIAL_LEVEL_FILE:"initiallevel", 
    BSComparator:null, 
    StartLevelImpl:vjo.ctype() 
    .satisfies('osgi.service.startlevel.StartLevel')
    .protos({
        st:null, 
        
        constructs:function(st){
            this.st=st;
        },
        
        getBundleStartLevel:function(bundle){
            return this.st.getBundleStartLevel(this.checkBundle(bundle));
        },
        
        getInitialBundleStartLevel:function(){
            return this.st.getInitialBundleStartLevel();
        },
        
        getStartLevel:function(){
            return this.st.getStartLevel();
        },
        
        isBundleActivationPolicyUsed:function(bundle){
            return this.st.isBundleActivationPolicyUsed(this.getBundleArchive(bundle));
        },
        
        isBundlePersistentlyStarted:function(bundle){
            return this.st.isBundlePersistentlyStarted(this.getBundleArchive(bundle));
        },
        
        setBundleStartLevel:function(bundle,startlevel){
            this.st.setBundleStartLevel(this.checkBundle(bundle),startlevel);
        },
        
        setInitialBundleStartLevel:function(startlevel){
            this.st.setInitialBundleStartLevel(startlevel);
        },
        
        setStartLevel:function(startlevel){
            this.st.setStartLevel(startlevel);
        },
        
        checkBundle:function(b){
            if(bundles.framework.BundleImpl.isInstance(b)){
                var res=b; 
                if(res.fwCtx===this.st.fwCtx){
                    if(res.state!==this.vj$.Bundle.UNINSTALLED){
                        return res;
                    }
                    throw new IllegalArgumentException("Bundle is in UNINSTALLED state");
                }
            }
            throw new IllegalArgumentException("Bundle doesn't belong to the same framework as the StartLevel service");
        },
        
        getBundleArchive:function(b){
            var bi=this.checkBundle(b); 
            var res=bi.current().archive; 
            if(res===null&&bi.id!==0){
                throw new IllegalArgumentException("Bundle is in UNINSTALLED state");
            }
            return res;
        }
    })
    .endType(),
    BundleStartLevelImpl:vjo.ctype() 
    .satisfies('osgi.framework.startlevel.BundleStartLevel')
    .protos({
        st:null, 
        bi:null, 
        
        constructs:function(st,bi){
            this.st=st;
            this.bi=bi;
        },
        
        getBundle:function(){
            return this.bi;
        },
        
        getStartLevel:function(){
            return this.st.getBundleStartLevel(this.bi);
        },
        
        setStartLevel:function(startlevel){
            this.st.setBundleStartLevel(this.bi,startlevel);
        },
        
        isPersistentlyStarted:function(){
            return this.st.isBundlePersistentlyStarted(this.getBundleArchive());
        },
        
        isActivationPolicyUsed:function(){
            return this.st.isBundleActivationPolicyUsed(this.getBundleArchive());
        },
        
        getBundleArchive:function(){
            var res=this.bi.current().archive; 
            if(res===null&&this.bi.id!==0){
                throw new IllegalArgumentException("Bundle is in UNINSTALLED state");
            }
            return res;
        }
    })
    .endType(),
    FrameworkStartLevelImpl:vjo.ctype() 
    .satisfies('osgi.framework.startlevel.FrameworkStartLevel')
    .protos({
        st:null, 
        bi:null, 
        
        constructs:function(startLevelController,bi){
            this.st=startLevelController;
            this.bi=bi;
        },
        
        getBundle:function(){
            return this.bi;
        },
        
        getStartLevel:function(){
            return this.st.getStartLevel();
        },
        
        setStartLevel:function(startlevel){
            var listeners;
            if (arguments.length == 2 && arguments[1]  instanceof Array){
                listeners=arguments[1];
            }
            else {
                listeners=[];
                for (var i=1; i<arguments.length; i++){
                    listeners[i-1]=arguments[i];
                }
            }
            this.st.setStartLevel(startlevel,listeners);
        },
        
        getInitialBundleStartLevel:function(){
            return this.st.getInitialBundleStartLevel();
        },
        
        setInitialBundleStartLevel:function(startlevel){
            this.st.setInitialBundleStartLevel(startlevel);
        }
    })
    .endType()
})
.protos({
    wc:null, 
    wcDelay:2000, 
    bRun:false, 
    jobQueue:null, 
    currentLevel:0, 
    initStartLevel:1, 
    targetStartLevel:0, 
    acceptChanges:true, 
    fwCtx:null, 
    storage:null, 
    bCompat:false, 
    lock:null, 
    
    constructs:function(fwCtx){
        this.jobQueue=new this.vj$.Queue(100);
        this.targetStartLevel=this.currentLevel;
        this.lock=Object();
        this.fwCtx=fwCtx;
        this.bCompat=fwCtx.props.getBooleanProperty(this.vj$.FrameworkProperties.STARTLEVEL_COMPAT_PROP);
        this.storage=this.vj$.Util.getFileStorage(fwCtx,"startlevel");
    },
    
    open:function(){
        if(this.fwCtx.debug.startlevel){
            this.fwCtx.debug.println("startlevel: open");
        }
        var lastJob=this.jobQueue.lastElement(); 
        this.wc=new Thread(this.fwCtx.threadGroup,this,"startlevel job");
        {
            this.bRun=true;
            this.wc.start();
            if(!this.acceptChanges){
                this.acceptChanges=true;
            }
            try {
                lastJob.wait();
            }
            catch(_ignore){
            }
        }
    },
    
    restoreState:function(){
        if(this.fwCtx.debug.startlevel){
            this.fwCtx.debug.println("startlevel: restoreState");
        }
        if(this.storage!==null){
            var startLevel=-1; 
            try {
                var s=this.vj$.Util.getContent(new this.vj$.File(this.storage,this.vj$.StartLevelController.LEVEL_FILE)); 
                if(s!==null){
                    startLevel=this.vj$.Integer.parseInt(s);
                    if(this.fwCtx.debug.startlevel){
                        this.fwCtx.debug.println("startlevel: restored level "+startLevel);
                    }
                }
            }
            catch(_ignored){
            }
            if(startLevel=== -1){
                var sBeginningLevel=this.fwCtx.props.getProperty(this.vj$.Constants.FRAMEWORK_BEGINNING_STARTLEVEL); 
                try {
                    startLevel=this.vj$.Integer.parseInt(sBeginningLevel);
                    if(this.fwCtx.debug.startlevel){
                        this.fwCtx.debug.println("startlevel: beginning level "+startLevel);
                    }
                }
                catch(nfe){
                    this.fwCtx.debug.printStackTrace("Invalid number '"+sBeginningLevel+"' in value of property named '"+this.vj$.Constants.FRAMEWORK_BEGINNING_STARTLEVEL+"'.",nfe);
                }
            }
            if(startLevel<0){
                startLevel=1;
            }
            this.setStartLevel0(startLevel,false,false,true);
            try {
                var s=this.vj$.Util.getContent(new this.vj$.File(this.storage,this.vj$.StartLevelController.INITIAL_LEVEL_FILE)); 
                if(s!==null){
                    this.setInitialBundleStartLevel0(this.vj$.Integer.parseInt(s),false);
                }
            }
            catch(_ignored){
            }
        }
    },
    
    close:function(){
        if(this.fwCtx.debug.startlevel){
            this.fwCtx.debug.println("*** closing startlevel service");
        }
        this.bRun=false;
        this.jobQueue.insert(
            vjo.make(this,Runnable)
            .protos({
                run:function(){
                    this.vj$.parent.jobQueue.close();
                }
            })
            .endType());
        if(this.wc!==null){
            try {
                this.wc.join(this.wcDelay*2);
            }
            catch(ignored){
            }
            this.wc=null;
        }
    },
    
    shutdown:function(){
        this.acceptChanges=false;
        {
            this.setStartLevel0(0,false,true,false);
            while(this.currentLevel>0){
                try {
                    this.wc.wait();
                }
                catch(e){
                }
            }
        }
        this.close();
    },
    
    run:function(){
        while(this.bRun){
            try {
                var job=this.jobQueue.removeWait(this.vj$.Util.cast((this.wcDelay/1000.0),'float')); 
                if(job!==null){
                    job.run();
                    {
                        job.notify();
                    }
                }
            }
            catch(ignored){
                ignored.printStackTrace();
            }
        }
    },
    
    getStartLevel:function(){
        return this.currentLevel;
    },
    
    
    setStartLevel:function(startLevel){
        if(arguments.length===1){
            this.setStartLevel_1_0_StartLevelController_ovld(arguments[0]);
        }else if(arguments.length===2){
            this.setStartLevel_2_0_StartLevelController_ovld(arguments[0],arguments[1]);
        }
    },
    
    setStartLevel_1_0_StartLevelController_ovld:function(startLevel){
        setStartLevel(startLevel,/*>>*/null);
    },
    
    setStartLevel_2_0_StartLevelController_ovld:function(startLevel){
        var listeners;
        if (arguments.length == 2 && arguments[1]  instanceof Array){
            listeners=arguments[1];
        }
        else {
            listeners=[];
            for (var i=1; i<arguments.length; i++){
                listeners[i-1]=arguments[i];
            }
        }
        this.fwCtx.perm.checkStartLevelAdminPerm();
        if(startLevel<=0){
            throw new this.vj$.IllegalArgumentException("Initial start level must be > 0, is "+startLevel);
        }
        if(this.acceptChanges){
            setStartLevel0(startLevel,this.bRun,false,true,listeners);
        }
    },
    
    setStartLevel0:function(startLevel,notifyFw,notifyWC,storeLevel){
        var listeners;
        if (arguments.length == 5 && arguments[4]  instanceof Array){
            listeners=arguments[4];
        }
        else {
            listeners=[];
            for (var i=4; i<arguments.length; i++){
                listeners[i-4]=arguments[i];
            }
        }
        if(this.fwCtx.debug.startlevel){
            this.fwCtx.debug.println("startlevel: setStartLevel "+startLevel);
        }
        this.jobQueue.insert(
            vjo.make(this,Runnable)
            .protos({
                run:function(){
                    var sl=this.vj$.parent.bCompat?1:startLevel; 
                    this.vj$.parent.targetStartLevel=sl;
                    while(this.vj$.parent.targetStartLevel>this.vj$.parent.currentLevel){
                        increaseStartLevel();
                    }
                    while(this.vj$.parent.targetStartLevel<this.vj$.parent.currentLevel){
                        decreaseStartLevel();
                    }
                    if(storeLevel&&this.vj$.parent.storage!==null){
                        try {
                            this.vj$.Util.putContent(new this.vj$.File(this.vj$.parent.storage,this.vj$.StartLevelController.LEVEL_FILE),this.vj$.Integer.toString(this.vj$.parent.currentLevel));
                        }
                        catch(e){
                            e.printStackTrace();
                        }
                    }
                    if(notifyFw){
                        var event=new this.vj$.FrameworkEvent(this.vj$.FrameworkEvent.STARTLEVEL_CHANGED,this.vj$.parent.fwCtx.systemBundle,null); 
                        this.vj$.parent.fwCtx.listeners.frameworkEvent(event,this.vj$.Listeners);
                    }
                    if(notifyWC&&this.vj$.parent.wc!==null){
                        {
                            this.vj$.parent.wc.notifyAll();
                        }
                    }
                }
            })
            .endType());
    },
    
    increaseStartLevel:function(){
        {
            this.currentLevel++;
            if(this.fwCtx.debug.startlevel){
                this.fwCtx.debug.println("startlevel: increaseStartLevel currentLevel="+this.currentLevel);
            }
            var set=new this.vj$.Vector(); 
            var bundles=this.fwCtx.bundles.getBundles(); 
            for (var bs,_$itr=bundles.iterator();_$itr.hasNext();){
                bs=_$itr.next();
                if(this.canStart(bs)){
                    if(bs.getStartLevel()===this.currentLevel){
                        if(bs.current().archive.getAutostartSetting()!== -1){
                            set.addElement(bs);
                        }
                    }
                }
            }
            this.vj$.Util.sort(set,this.vj$.StartLevelController.BSComparator,false);
            for (var i=0;i<set.size();i++){
                var bs=set.elementAt(i); 
                try {
                    if(bs.current().archive.getAutostartSetting()!== -1){
                        if(this.fwCtx.debug.startlevel){
                            this.fwCtx.debug.println("startlevel: start "+bs);
                        }
                        var startOptions=this.vj$.Bundle.START_TRANSIENT; 
                        if(this.isBundleActivationPolicyUsed(bs.current().archive)){
                            startOptions|=this.vj$.Bundle.START_ACTIVATION_POLICY;
                        }
                        bs.start(startOptions);
                    }
                }
                catch(e){
                    if(e instanceof this.vj$.IllegalStateException){
                        var ignore=e;
                    }else if(e instanceof this.vj$.Exception){
                        this.fwCtx.frameworkError(bs,e);
                    }
                }
            }
        }
    },
    
    decreaseStartLevel:function(){
        {
            this.currentLevel--;
            var set=new this.vj$.Vector(); 
            var bundles=this.fwCtx.bundles.getBundles(); 
            for (var bs,_$itr=bundles.iterator();_$itr.hasNext();){
                bs=_$itr.next();
                if(bs.getState()===this.vj$.Bundle.ACTIVE||(bs.getState()===this.vj$.Bundle.STARTING&&bs.current().lazyActivation)){
                    if(bs.getStartLevel()===this.currentLevel+1){
                        set.addElement(bs);
                    }
                }
            }
            this.vj$.Util.sort(set,this.vj$.StartLevelController.BSComparator,true);
            {
                for (var i=0;i<set.size();i++){
                    var bs=set.elementAt(i); 
                    if(bs.getState()===this.vj$.Bundle.ACTIVE||(bs.getState()===this.vj$.Bundle.STARTING&&bs.current().lazyActivation)){
                        if(this.fwCtx.debug.startlevel){
                            this.fwCtx.debug.println("startlevel: stop "+bs);
                        }
                        try {
                            bs.stop(this.vj$.Bundle.STOP_TRANSIENT);
                        }
                        catch(t){
                            this.fwCtx.frameworkError(bs,t);
                        }
                    }
                }
            }
        }
    },
    
    canStart:function(b){
        return b.getState()!==this.vj$.Bundle.UNINSTALLED;
    },
    
    getBundleStartLevel:function(bundle){
        if(bundle.getBundleId()===0){
            return 0;
        }
        return bundle.getStartLevel();
    },
    
    setBundleStartLevel:function(bundle,startLevel){
        this.fwCtx.perm.checkExecuteAdminPerm(bundle);
        if(startLevel<=0){
            throw new this.vj$.IllegalArgumentException("Initial start level must be > 0, is "+startLevel);
        }
        if(bundle.getBundleId()===0){
            throw new this.vj$.IllegalArgumentException("System bundle start level cannot be changed");
        }
        this.fwCtx.perm.callSetStartLevel(bundle,this.bCompat?1:startLevel);
        this.jobQueue.insert(
            vjo.make(this,Runnable)
            .protos({
                run:function(){
                    syncStartLevel(bundle);
                }
            })
            .endType());
    },
    
    syncStartLevel:function(bs){
        try {
            if(this.fwCtx.debug.startlevel){
                this.fwCtx.debug.println("syncstartlevel: "+bs);
            }
            {
                {
                    if(bs.getStartLevel()<=this.currentLevel){
                        var current=bs.current(); 
                        if((bs.getState()&(this.vj$.Bundle.INSTALLED|this.vj$.Bundle.RESOLVED|this.vj$.Bundle.STOPPING))!==0&&current.archive.getAutostartSetting()!==-1){
                            if(this.fwCtx.debug.startlevel){
                                this.fwCtx.debug.println("startlevel: start "+bs);
                            }
                            var startOptions=this.vj$.Bundle.START_TRANSIENT; 
                            if(this.isBundleActivationPolicyUsed(current.archive)){
                                startOptions|=this.vj$.Bundle.START_ACTIVATION_POLICY;
                            }
                            bs.start(startOptions);
                        }
                    }else if(bs.getStartLevel()>this.currentLevel){
                        if((bs.getState()&(this.vj$.Bundle.ACTIVE|this.vj$.Bundle.STARTING))!==0){
                            if(this.fwCtx.debug.startlevel){
                                this.fwCtx.debug.println("startlevel: stop "+bs);
                            }
                            bs.stop(this.vj$.Bundle.STOP_TRANSIENT);
                        }
                    }
                }
            }
        }
        catch(t){
            this.fwCtx.frameworkError(bs,t);
        }
    },
    
    getInitialBundleStartLevel:function(){
        return this.initStartLevel;
    },
    
    setInitialBundleStartLevel:function(startLevel){
        this.fwCtx.perm.checkStartLevelAdminPerm();
        this.fwCtx.perm.callSetInitialBundleStartLevel0(this,startLevel);
    },
    
    setInitialBundleStartLevel0:function(startLevel,save){
        if(startLevel<=0){
            throw new this.vj$.IllegalArgumentException("Initial start level must be > 0, is "+startLevel);
        }
        this.initStartLevel=this.bCompat?1:startLevel;
        if(this.storage!==null&&save){
            try {
                this.vj$.Util.putContent(new this.vj$.File(this.storage,this.vj$.StartLevelController.INITIAL_LEVEL_FILE),this.vj$.Integer.toString(this.initStartLevel));
            }
            catch(e){
                e.printStackTrace();
            }
        }
    },
    
    isBundlePersistentlyStarted:function(archive){
        return archive===null||archive.getAutostartSetting()!== -1;
    },
    
    isBundleActivationPolicyUsed:function(archive){
        return archive!==null&&archive.getAutostartSetting()===this.vj$.Bundle.START_ACTIVATION_POLICY;
    },
    
    getService:function(bundle,registration){
        return new this.vj$.StartLevelController.StartLevelImpl(this);
    },
    
    ungetService:function(bundle,registration,service){
    },
    
    bundleStartLevel:function(bi){
        return new this.vj$.StartLevelController.BundleStartLevelImpl(this,bi);
    },
    
    frameworkStartLevel:function(bi){
        return new this.vj$.StartLevelController.FrameworkStartLevelImpl(this,bi);
    }
})
.inits(function(){
    this.vj$.StartLevelController.START_MAX=this.vj$.Integer.MAX_VALUE;
    this.vj$.StartLevelController.BSComparator=
        vjo.make(this,this.vj$.Util.Comparator)
        .protos({
            compare:function(b1,b2){
                var res=b1.getStartLevel()-b2.getStartLevel(); 
                if(res===0){
                    res=this.vj$.Util.cast((b1.getBundleId()-b2.getBundleId()),'int');
                }
                return res;
            }
        })
        .endType();
})
.endType();