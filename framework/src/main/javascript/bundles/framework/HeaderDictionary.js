/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.HeaderDictionary') 
.needs(['org.eclipse.vjet.vjo.java.util.Hashtable','org.eclipse.vjet.vjo.java.util.Map',
    'org.eclipse.vjet.vjo.java.util.Enumeration','java.util.jar.Attributes'])
.inherits('java.util.Dictionary<String,String>')
.satisfies('Cloneable')
.protos({
    headers:null, 
    
    
    
    constructs:function(){
        if(arguments.length===1){
            if(arguments[0] instanceof java.util.jar.Attributes){
                this.constructs_1_0_HeaderDictionary_ovld(arguments[0]);
            }else if(arguments[0] instanceof org.eclipse.vjet.vjo.java.util.Hashtable){
                this.constructs_1_1_HeaderDictionary_ovld(arguments[0]);
            }
        }
    },
    
    constructs_1_0_HeaderDictionary_ovld:function(in_){
        this.base();
        this.headers=new this.vj$.Hashtable();
        for (var e,_$itr=in_.entrySet().iterator();_$itr.hasNext();){
            e=_$itr.next();
            this.headers.put(/*>>*/e.getKey(),/*>>*/e.getValue());
        }
    },
    
    constructs_1_1_HeaderDictionary_ovld:function(t){
        this.base();
        this.headers=t;
    },
    
    elements:function(){
        return this.headers.elements();
    },
    
    get:function(key){
        return this.headers.get(new this.vj$.Attributes.Name(/*>>*/key));
    },
    
    isEmpty:function(){
        return this.headers.isEmpty();
    },
    
    keys:function(){
        var keys=this.headers.keys(); 
        return vjo.make(this,this.vj$.Enumeration)
            .protos({
                hasMoreElements:function(){
                    return keys.hasMoreElements();
                },
                nextElement:function(){
                    return keys.nextElement().toString();
                }
            })
            .endType();
    },
    
    put:function(key,value){
        return this.headers.put(new this.vj$.Attributes.Name(key),value);
    },
    
    remove:function(key){
        return this.headers.remove(new this.vj$.Attributes.Name(/*>>*/key));
    },
    
    size:function(){
        return this.headers.size();
    },
    
    clone:function(){
        return new this.vj$.HeaderDictionary(/*>>*/this.headers.clone());
    },
    
    cloneHD:function(){
        return new this.vj$.HeaderDictionary(/*>>*/this.headers.clone());
    },
    
    toString:function(){
        return this.headers.toString();
    }
})
.endType();