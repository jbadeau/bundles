/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleContextImpl') 
.needs(['org.eclipse.vjet.vjo.java.io.InputStream','org.eclipse.vjet.vjo.java.util.List',
    'org.eclipse.vjet.vjo.java.lang.NullPointerException','org.eclipse.vjet.vjo.java.util.Collection',
    'org.eclipse.vjet.vjo.java.util.Arrays','org.eclipse.vjet.vjo.java.lang.IllegalStateException',
    'bundles.framework.BundleImpl','osgi.framework.Bundle',
    'java.io.IOException','osgi.framework.ServiceListener',
    'osgi.framework.InvalidSyntaxException','osgi.framework.BundleListener',
    'osgi.framework.FrameworkListener','osgi.framework.ServiceRegistration',
    'java.util.Dictionary','osgi.framework.ServiceReference',
    'bundles.framework.ServiceReferenceImpl','java.io.File',
    'osgi.framework.Filter','osgi.framework.FrameworkUtil',
    'org.eclipse.vjet.vjo.java.util.Collections'])
.satisfies('osgi.framework.BundleContext')
.protos({
    bundle:null, 
    valid:true, 
    
    constructs:function(bundle){
        this.bundle=bundle;
    },
    
    getProperty:function(key){
        this.checkValid();
        return this.bundle.fwCtx.props.getProperty(key);
    },
    
    
    installBundle:function(location){
        if(arguments.length===1){
            return this.installBundle_1_0_BundleContextImpl_ovld(arguments[0]);
        }else if(arguments.length===2){
            return this.installBundle_2_0_BundleContextImpl_ovld(arguments[0],arguments[1]);
        }
    },
    
    installBundle_1_0_BundleContextImpl_ovld:function(location){
        this.checkValid();
        return this.bundle.fwCtx.bundles.install(location,null,this.bundle);
    },
    
    installBundle_2_0_BundleContextImpl_ovld:function(location,in_){
        try {
            this.checkValid();
            return this.bundle.fwCtx.bundles.install(location,in_,this.bundle);
        }
        finally {
            if(in_!==null){
                try {
                    in_.close();
                }
                catch(ignore){
                }
            }
        }
    },
    
    
    
    getBundle:function(){
        if(arguments.length===0){
            return this.getBundle_0_0_BundleContextImpl_ovld();
        }else if(arguments.length===1){
            if(typeof arguments[0]=="number"){
                return this.getBundle_1_0_BundleContextImpl_ovld(arguments[0]);
            }else if(arguments[0] instanceof String || typeof arguments[0]=="string"){
                return this.getBundle_1_1_BundleContextImpl_ovld(arguments[0]);
            }
        }
    },
    
    getBundle_0_0_BundleContextImpl_ovld:function(){
        this.checkValid();
        return this.bundle;
    },
    
    getBundle_1_0_BundleContextImpl_ovld:function(id){
        return this.bundle.fwCtx.bundleHooks.filterBundle(this,this.bundle.fwCtx.bundles.getBundle(id));
    },
    
    getBundles:function(){
        var bl=this.bundle.fwCtx.bundles.getBundles(); 
        this.bundle.fwCtx.bundleHooks.filterBundles(this,bl);
        return bl.toArray(vjo.createArray(null, bl.size()));
    },
    
    
    addServiceListener:function(listener,filter){
        if(arguments.length===2){
            this.addServiceListener_2_0_BundleContextImpl_ovld(arguments[0],arguments[1]);
        }else if(arguments.length===1){
            this.addServiceListener_1_0_BundleContextImpl_ovld(arguments[0]);
        }
    },
    
    addServiceListener_2_0_BundleContextImpl_ovld:function(listener,filter){
        this.checkValid();
        this.bundle.fwCtx.listeners.addServiceListener(this,listener,filter);
    },
    
    addServiceListener_1_0_BundleContextImpl_ovld:function(listener){
        this.checkValid();
        try {
            this.bundle.fwCtx.listeners.addServiceListener(this,listener,null);
        }
        catch(neverHappens){
        }
    },
    
    removeServiceListener:function(listener){
        this.checkValid();
        this.bundle.fwCtx.listeners.removeServiceListener(this,listener);
    },
    
    addBundleListener:function(listener){
        this.checkValid();
        this.bundle.fwCtx.listeners.addBundleListener(this,listener);
    },
    
    removeBundleListener:function(listener){
        this.checkValid();
        this.bundle.fwCtx.listeners.removeBundleListener(this,listener);
    },
    
    addFrameworkListener:function(listener){
        this.checkValid();
        this.bundle.fwCtx.listeners.addFrameworkListener(this,listener);
    },
    
    removeFrameworkListener:function(listener){
        this.checkValid();
        this.bundle.fwCtx.listeners.removeFrameworkListener(this,listener);
    },
    
    
    
    registerService:function(clazzes,service,properties){
        if(arguments.length===3){
            if((arguments[0] instanceof String || typeof arguments[0]=="string") && arguments[1] instanceof Object && arguments[2] instanceof java.util.Dictionary){
                return this.registerService_3_1_BundleContextImpl_ovld(arguments[0],arguments[1],arguments[2]);
            }else if(arguments[0] instanceof Array && arguments[1] instanceof Object && arguments[2] instanceof java.util.Dictionary){
                return this.registerService_3_0_BundleContextImpl_ovld(arguments[0],arguments[1],arguments[2]);
            }else if(arguments[0] instanceof vjo.Class && arguments[1] instanceof Object && arguments[2] instanceof java.util.Dictionary){
                return this.registerService_3_2_BundleContextImpl_ovld(arguments[0],arguments[1],arguments[2]);
            }
        }
    },
    
    registerService_3_0_BundleContextImpl_ovld:function(clazzes,service,properties){
        this.checkValid();
        var classes=clazzes.clone(); 
        return this.bundle.fwCtx.services.register(this.bundle,classes,service,properties);
    },
    
    registerService_3_1_BundleContextImpl_ovld:function(clazz,service,properties){
        this.checkValid();
        var classes=[clazz]; 
        return this.bundle.fwCtx.services.register(this.bundle,classes,service,properties);
    },
    
    getAllServiceReferences:function(clazz,filter){
        this.checkValid();
        return this.bundle.fwCtx.services.get(clazz,filter,null);
    },
    
    getService:function(reference){
        this.checkValid();
        if(reference===null){
            throw new this.vj$.NullPointerException("null ServiceReference is not valid input to getService()");
        }
        var sri=reference; 
        return sri.getService(this.bundle);
    },
    
    ungetService:function(reference){
        this.checkValid();
        if(reference===null){
            throw new this.vj$.NullPointerException("null ServiceReference is not valid input to ungetService()");
        }
        return reference.ungetService(this.bundle);
    },
    
    getDataFile:function(filename){
        this.checkValid();
        var dataRoot=this.bundle.getDataRoot(); 
        if(dataRoot!==null){
            if(!dataRoot.exists()){
                dataRoot.mkdirs();
            }
            return new this.vj$.File(dataRoot,filename);
        }else {
            return null;
        }
    },
    
    createFilter:function(filter){
        this.checkValid();
        return this.vj$.FrameworkUtil.createFilter(filter);
    },
    
    registerService_3_2_BundleContextImpl_ovld:function(clazz,service,properties){
        var res=registerService(clazz===null?null:clazz.getName(),service,properties); 
        return res;
    },
    
    
    getServiceReference:function(clazz){
        if(arguments.length===1){
            if(arguments[0] instanceof String || typeof arguments[0]=="string"){
                return this.getServiceReference_1_0_BundleContextImpl_ovld(arguments[0]);
            }else if(arguments[0] instanceof vjo.Class){
                return this.getServiceReference_1_1_BundleContextImpl_ovld(arguments[0]);
            }
        }
    },
    
    getServiceReference_1_0_BundleContextImpl_ovld:function(clazz){
        this.checkValid();
        return this.bundle.fwCtx.services.get(this.bundle,clazz);
    },
    
    getServiceReference_1_1_BundleContextImpl_ovld:function(clazz){
        var res=getServiceReference(clazz===null?null:clazz.getName()); 
        return res;
    },
    
    
    getServiceReferences:function(clazz,filter){
        if(arguments.length===2){
            if((arguments[0] instanceof String || typeof arguments[0]=="string") && (arguments[1] instanceof String || typeof arguments[1]=="string")){
                return this.getServiceReferences_2_0_BundleContextImpl_ovld(arguments[0],arguments[1]);
            }else if(arguments[0] instanceof vjo.Class && (arguments[1] instanceof String || typeof arguments[1]=="string")){
                return this.getServiceReferences_2_1_BundleContextImpl_ovld(arguments[0],arguments[1]);
            }
        }
    },
    
    getServiceReferences_2_0_BundleContextImpl_ovld:function(clazz,filter){
        this.checkValid();
        return this.bundle.fwCtx.services.get(clazz,filter,this.bundle);
    },
    
    getServiceReferences_2_1_BundleContextImpl_ovld:function(clazz,filter){
        var srs=getServiceReferences(clazz===null?null:clazz.getName(),filter); 
        if(srs===null){
            var res=this.vj$.Collections.EMPTY_LIST; 
            return res;
        }else {
            return this.vj$.Arrays.asList(srs);
        }
    },
    
    getBundle_1_1_BundleContextImpl_ovld:function(location){
        return this.bundle.fwCtx.bundles.getBundle(location);
    },
    
    invalidate:function(){
        this.valid=false;
    },
    
    isValid:function(){
        return this.valid;
    },
    
    checkValid:function(){
        if(!this.valid){
            throw new this.vj$.IllegalStateException("This bundle context is no longer valid");
        }
    }
})
.endType();