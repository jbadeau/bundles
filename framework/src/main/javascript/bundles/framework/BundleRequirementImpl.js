/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleRequirementImpl') 
.needs(['org.eclipse.vjet.vjo.java.util.Map','org.eclipse.vjet.vjo.java.util.Arrays',
    'org.eclipse.vjet.vjo.java.lang.IllegalArgumentException','org.eclipse.vjet.vjo.java.util.Collections',
    'org.eclipse.vjet.vjo.java.lang.StringBuffer','org.eclipse.vjet.vjo.java.lang.Character',
    'org.eclipse.vjet.vjo.java.lang.RuntimeException','bundles.framework.BundleGeneration',
    'osgi.framework.Filter','bundles.framework.BundleWireImpl',
    'bundles.framework.Util','osgi.framework.wiring.BundleRevision',
    'osgi.framework.Constants','osgi.framework.FrameworkUtil',
    'osgi.framework.InvalidSyntaxException','osgi.framework.namespace.ExecutionEnvironmentNamespace',
    'osgi.framework.Version','osgi.framework.wiring.BundleCapability',
    'bundles.framework.BundleCapabilityImpl','vjo.java.lang.ObjectUtil',
    'org.eclipse.vjet.vjo.java.lang.StringUtil'])
.satisfies('osgi.framework.wiring.BundleRequirement')
.protos({
    gen:null, 
    nameSpace:null, 
    attributes:null, 
    directives:null, 
    filter:null, 
    wire:null, 
    
    
    
    constructs:function(){
        if(arguments.length===2){
            if(arguments[0] instanceof bundles.framework.BundleGeneration && arguments[1] instanceof bundles.framework.Util.HeaderEntry){
                this.constructs_2_0_BundleRequirementImpl_ovld(arguments[0],arguments[1]);
            }else if(arguments[0] instanceof bundles.framework.BundleGeneration && (arguments[1] instanceof String || typeof arguments[1]=="string")){
                this.constructs_2_1_BundleRequirementImpl_ovld(arguments[0],arguments[1]);
            }
        }
    },
    
    constructs_2_0_BundleRequirementImpl_ovld:function(gen,he){
        this.gen=gen;
        this.nameSpace=he.getKey();
        for (var ns,_$itr=this.vj$.Arrays.asList([this.vj$.BundleRevision.BUNDLE_NAMESPACE,this.vj$.BundleRevision.HOST_NAMESPACE,this.vj$.BundleRevision.PACKAGE_NAMESPACE]).iterator();_$itr.hasNext();){
            ns=_$itr.next();
            if(vjo.java.lang.ObjectUtil.equals(ns,this.nameSpace)){
                throw new this.vj$.IllegalArgumentException("Capability with name-space '"+ns+"' must not be required in the "+this.vj$.Constants.REQUIRE_CAPABILITY+" manifest header.");
            }
        }
        var filterStr=he.getDirectives().remove("filter"); 
        if(null!==filterStr&&filterStr.length>0){
            try {
                this.filter=this.vj$.FrameworkUtil.createFilter(filterStr);
                he.getDirectives().put("filter",this.filter.toString());
            }
            catch(ise){
                var msg="Invalid filter '"+filterStr+"' in "+this.vj$.Constants.REQUIRE_CAPABILITY+" for name-space "+this.nameSpace+": "+ise; 
                throw (new this.vj$.IllegalArgumentException(msg)).initCause(ise);
            }
        }else {
            this.filter=null;
        }
        this.directives=this.vj$.Collections.unmodifiableMap(he.getDirectives());
        this.attributes=this.vj$.Collections.unmodifiableMap(he.getAttributes());
    },
    
    constructs_2_1_BundleRequirementImpl_ovld:function(gen,ee){
        this.gen=gen;
        this.nameSpace=this.vj$.ExecutionEnvironmentNamespace.EXECUTION_ENVIRONMENT_NAMESPACE;
        var filterStrB=new this.vj$.StringBuffer(); 
        var l=this.vj$.Util.splitwords(ee,","); 
        if(l.length>1){
            filterStrB.append("(|");
        }
        for (var e,_$i0=0;_$i0<l.length;_$i0++){
            e=l[_$i0];
            var es=this.vj$.Util.splitwords(e,"-"); 
            try {
                if(es.length===2){
                    var si=es[1].indexOf('/'); 
                    new this.vj$.Version(si===-1?es[1]:es[1].substring(0,si));
                    filterStrB.append("(&(").append(this.nameSpace).append('=');
                    if(org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(es[0],"J2SE")){
                        es[0]="JavaSE";
                    }
                    filterStrB.append(es[0]);
                    if(si!== -1){
                        filterStrB.append(es[1].substring(si));
                    }
                    filterStrB.append(")(version=").append(es[1]).append("))");
                    continue;
                }else if(es.length>2){
                    var esStrB=new this.vj$.StringBuffer(es[0]); 
                    var v=null; 
                    for (var i=1;i<es.length;i++){
                        if(this.vj$.Character.isDigit(es[i].charAt(0))){
                            if(v===null){
                                var si=es[i].indexOf('/'); 
                                v=new this.vj$.Version(si===-1?es[i]:es[i].substring(0,si));
                                if(si!== -1){
                                    esStrB.append(es[1].substring(si));
                                }else if(i!==es.length-1){
                                    throw new this.vj$.IllegalArgumentException("Version not at end");
                                }
                            }else {
                                if(org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(v,new this.vj$.Version(es[i]))&&i===es.length-1){
                                    break;
                                }
                                throw new this.vj$.IllegalArgumentException("Version mismatch");
                            }
                        }else {
                            esStrB.append('-').append(es[i]);
                        }
                    }
                    if(v!==null){
                        filterStrB.append("(&(").append(this.nameSpace).append('=');
                        filterStrB.append(esStrB).append(")(version=");
                        filterStrB.append(v).append("))");
                        continue;
                    }
                }
            }
            catch(_ignore){
            }
            filterStrB.append('(').append(this.nameSpace).append('=');
            filterStrB.append(e).append(')');
        }
        if(l.length>1){
            filterStrB.append(')');
        }
        try {
            this.filter=this.vj$.FrameworkUtil.createFilter(filterStrB.toString());
        }
        catch(ise){
            throw new this.vj$.RuntimeException("Internal error");
        }
        this.directives=this.vj$.Collections.singletonMap("filter",this.filter.toString());
        this.attributes=this.vj$.Collections.EMPTY_MAP;
    },
    
    getNamespace:function(){
        return this.nameSpace;
    },
    
    getDirectives:function(){
        return this.directives;
    },
    
    getAttributes:function(){
        return this.attributes;
    },
    
    getRevision:function(){
        return this.gen.bundleRevision;
    },
    
    getResource:function(){
        return this.gen.bundleRevision;
    },
    
    matches:function(capability){
        if(vjo.java.lang.ObjectUtil.equals(this.nameSpace,capability.getNamespace())){
            return null===this.filter?true:this.filter.matches(capability.getAttributes());
        }
        return false;
    },
    
    toString:function(){
        var sb=new this.vj$.StringBuffer(40); 
        sb.append("[").append(this.vj$.BundleRequirement.clazz.getName()).append(": ").append(this.nameSpace).append(" directives: ").append(this.directives.toString()).append("]");
        return sb.toString();
    },
    
    getBundleGeneration:function(){
        return this.gen;
    },
    
    getWire:function(){
        return this.wire;
    },
    
    resetWire:function(){
        if(this.wire!==null){
            (this.wire.getCapability()).removeWire(this.wire);
            this.wire=null;
        }
    },
    
    setWire:function(wire){
        (wire.getCapability()).addWire(wire);
        this.wire=wire;
    },
    
    isOptional:function(){
        var resolution=this.directives.get(this.vj$.Constants.RESOLUTION_DIRECTIVE); 
        return vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.RESOLUTION_OPTIONAL,resolution);
    },
    
    shouldResolve:function(){
        var effective=this.directives.get(this.vj$.Constants.EFFECTIVE_DIRECTIVE); 
        return effective===null||vjo.java.lang.ObjectUtil.equals(effective,this.vj$.Constants.EFFECTIVE_RESOLVE);
    },
    
    isWired:function(){
        return this.wire!==null;
    }
})
.endType();