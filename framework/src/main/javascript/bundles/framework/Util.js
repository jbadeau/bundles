define([ "vjo", "osgi/framework/launch/FrameworkFactory", "./FrameworkContext", "osgi/util/StringUtil" ], function(vjo, FrameworkFactory, FrameworkContext, StringUtil) {

	return vjo.ctype("bundles.framework.Util")

	.props({
		
		DOUBLE_TYPE : "Double",
		
		LONG_TYPE : "Long",
		
		LIST_TYPE : "List",
		
		STRING_TYPE : "String",
		
		VERSION_TYPE : "Version",
		
		FWDIR_PROP : "org.osgi.framework.dir",
		
		FWDIR_DEFAULT : "fwdir",
		
		nanoTimeMethod : null,
		
		WHITESPACE : " \t\n\r",
		
		CITCHAR : '"',
		
		encTab : null,
		
		Comparator : vjo.itype()
		
		.protos({

			compare : function(a, b) {
			}
		
		})
		
		.endType(),
		
		AttributeTokenizer : vjo.ctype()
		
		.protos({
		
			s : null,
			
			length : 0,
			
			pos : 0,

			constructs : function(input) {
				this.s = input;
				this.length = this.s.length;
			},

			getWord : function(keepEscapse, valueWord) {
				this.skipWhite();
				var backslash = false;
				var quote = false;
				var val = new StringBuffer();
				var end = 0;
				loop: for (; this.pos < this.length; this.pos++) {
					if (backslash) {
						backslash = false;
						if (keepEscapse) {
							val.append('\\');
						}
						val.append(this.s.charAt(this.pos));
						end = val.length();
					}
					else {
						var c = this.s.charAt(this.pos);
						switch (c) {
							case '"':
								quote = !quote;
								end = val.length();
								break;
							case '\\':
								backslash = true;
								break;
							case ':':
							case ',':
							case ';':
							case '=':
								if (!quote && !(valueWord && (c === ':' || c === '='))) {
									break loop;
								}
							default:
								val.append(c);
								if (!this.vj$.Character.isWhitespace(c)) {
									end = val.length();
								}
								break;
						}
					}
				}
				if (quote || backslash || end === 0) {
					return null;
				}
				val.setLength(end);
				return val.toString();
			},

			getKey : function(singleKey) {
				if (this.pos >= this.length) {
					return null;
				}
				var save = this.pos;
				if (this.s.charAt(this.pos) === ';') {
					this.pos++;
				}
				var res = this.getWord(false, singleKey);
				if (res !== null) {
					if (this.pos === this.length) {
						return res;
					}
					var c = this.s.charAt(this.pos);
					if (c === ';' || c === ',') {
						return res;
					}
				}
				this.pos = save;
				return null;
			},

			getParam : function() {
				if (this.pos === this.length || this.s.charAt(this.pos) !== ';') {
					return null;
				}
				var save = this.pos++;
				var res = this.getWord(false, false);
				if (res !== null) {
					if (this.pos < this.length && this.s.charAt(this.pos) === '=') {
						return res;
					}
					if (this.pos < this.length && this.s.charAt(this.pos) === ':') {
						return res;
					}
				}
				this.pos = save;
				return null;
			},

			isDirective : function() {
				if (this.pos + 1 < this.length && this.s.charAt(this.pos) === ':' && this.s.charAt(this.pos + 1) === '=') {
					this.pos++;
					return true;
				}
				else {
					return false;
				}
			},

			getParamType : function() {
				if (this.pos === this.length || this.s.charAt(this.pos) !== ':') {
					return null;
				}
				var save = this.pos++;
				var res = this.getWord(false, false);
				if (res !== null) {
					if (this.pos < this.length && this.s.charAt(this.pos) === '=') {
						return res;
					}
				}
				this.pos = save;
				return null;
			},

			getValue : function() {
				if (arguments.length === 0) {
					return this.getValue_0_0_AttributeTokenizer_ovld();
				}
				else if (arguments.length === 1) {
					return this.getValue_1_0_AttributeTokenizer_ovld(arguments[0]);
				}
			},

			getValue_0_0_AttributeTokenizer_ovld : function() {
				return this.getValue(false);
			},

			getValue_1_0_AttributeTokenizer_ovld : function(keepEscapes) {
				if (this.s.charAt(this.pos) !== '=') {
					return null;
				}
				var save = this.pos++;
				this.skipWhite();
				var val = this.getWord(keepEscapes, true);
				if (val === null) {
					this.pos = save;
					return null;
				}
				return val;
			},

			getEntryEnd : function() {
				var save = this.pos;
				this.skipWhite();
				if (this.pos === this.length) {
					return true;
				}
				else if (this.s.charAt(this.pos) === ',') {
					this.pos++;
					return true;
				}
				else {
					this.pos = save;
					return false;
				}
			},

			getEnd : function() {
				var save = this.pos;
				this.skipWhite();
				if (this.pos === this.length) {
					return true;
				}
				else {
					this.pos = save;
					return false;
				}
			},

			getRest : function() {
				var res = StringUtil.trim(this.s.substring(this.pos));
				return res.length === 0 ? "<END OF LINE>" : res;
			},

			skipWhite : function() {
				for (; this.pos < this.length; this.pos++) {
					if (!this.vj$.Character.isWhitespace(this.s.charAt(this.pos))) {
						break;
					}
				}
			}
			
		})
		
		.endType(),
		
		HeaderEntry : vjo.ctype()
		
		.protos({
		
			headerName : null,
			
			singleKey : false,
			
			keys : null,
			
			attributes : null,
			
			directives : null,

			constructs : function(headerName, singleKey) {
				this.keys = new ArrayList();
				this.attributes = new HashMap();
				this.directives = new HashMap();
				this.headerName = headerName;
				this.singleKey = singleKey;
			},

			getKey : function() {
				if (this.singleKey) {
					return this.keys.get(0);
				}
				throw new IllegalArgumentException("Requesting single key for multi key header clause");
			},

			getKeys : function() {
				return this.keys;
			},

			getAttributes : function() {
				return this.attributes;
			},

			getDirectives : function() {
				return this.directives;
			}
			
		})
		
		.endType(),

		getFrameworkDir : function(props) {
			if (arguments.length === 1) {
				if (org.eclipse.vjet.vjo.java.util.Map.clazz.isInstance(arguments[0])) {
					return this.vj$.Util.getFrameworkDir_1_0_Util_ovld(arguments[0]);
				}
				else if (arguments[0] instanceof bundles.framework.FrameworkContext) {
					return this.vj$.Util.getFrameworkDir_1_1_Util_ovld(arguments[0]);
				}
			}
		},

		getFrameworkDir_1_0_Util_ovld : function(props) {
			var s = props.get(this.vj$.Constants.FRAMEWORK_STORAGE);
			if (s === null || s.length === 0) {
				s = props.get(this.FWDIR_PROP);
			}
			if (s === null || s.length === 0) {
				s = this.vj$.System.getProperty(this.FWDIR_PROP);
			}
			if (s === null || s.length === 0) {
				s = this.FWDIR_DEFAULT;
			}
			return s;
		},

		getFrameworkDir_1_1_Util_ovld : function(ctx) {
			var s = ctx.props.getProperty(this.vj$.Constants.FRAMEWORK_STORAGE);
			if (s === null || s.length === 0) {
				s = ctx.props.getProperty(this.FWDIR_PROP);
			}
			if (s === null || s.length === 0) {
				s = this.FWDIR_DEFAULT;
			}
			return s;
		},

		getFileStorage : function(ctx, name) {
			var fwdir = this.getFrameworkDir(ctx);
			if (fwdir === null) {
				return null;
			}
			var dir = new this.vj$.FileTree((new this.vj$.File(fwdir)).getAbsoluteFile(), name);
			if (dir !== null) {
				if (dir.exists()) {
					if (!dir.isDirectory()) {
						throw new this.vj$.RuntimeException("Not a directory: " + dir);
					}
				}
				else {
					if (!dir.mkdirs()) {
						throw new this.vj$.RuntimeException("Cannot create directory: " + dir);
					}
				}
			}
			return dir;
		},

		compareStringVersion : function(ver1, ver2) {
			var i1, i2;
			while (ver1 !== null || ver2 !== null) {
				if (ver1 !== null) {
					var d1 = ver1.indexOf(".");
					if (d1 === -1) {
						i1 = this.vj$.Integer.parseInt(StringUtil.trim(ver1));
						ver1 = null;
					}
					else {
						i1 = this.vj$.Integer.parseInt(StringUtil.trim(ver1.substring(0, d1)));
						ver1 = ver1.substring(d1 + 1);
					}
				}
				else {
					i1 = 0;
				}
				if (ver2 !== null) {
					var d2 = ver2.indexOf(".");
					if (d2 === -1) {
						i2 = this.vj$.Integer.parseInt(StringUtil.trim(ver2));
						ver2 = null;
					}
					else {
						i2 = this.vj$.Integer.parseInt(StringUtil.trim(ver2.substring(0, d2)));
						ver2 = ver2.substring(d2 + 1);
					}
				}
				else {
					i2 = 0;
				}
				if (i1 < i2) {
					return -1;
				}
				if (i1 > i2) {
					return 1;
				}
			}
			return 0;
		},

		parseEnumeration : function(d, s) {
			var result = new this.vj$.HashSet();
			if (s !== null) {
				var at = new this.AttributeTokenizer(s);
				do {
					var key = at.getKey(true);
					if (key === null) {
						throw new this.vj$.IllegalArgumentException("Directive " + d + ", unexpected character at: " + at.getRest());
					}
					if (!at.getEntryEnd()) {
						throw new this.vj$.IllegalArgumentException("Directive " + d + ", expected end of entry at: " + at.getRest());
					}
					result.add(key);
				}
				while (!at.getEnd());
				return result;
			}
			else {
				return null;
			}
		},

		parseManifestHeader : function(a, s, single, unique, single_entry) {
			var res = new this.vj$.ArrayList();
			if (s !== null) {
				var at = new this.AttributeTokenizer(s);
				do {
					var he = new this.HeaderEntry(a, single);
					var key = at.getKey(single);
					if (key === null) {
						var msg = "Definition, " + a + ", expected key at: " + at.getRest() + ". Key values are terminated " + "by a ';' or a ',' and may not " + "contain unquoted ':', '=' if multiple keys are allowed.";
						throw new this.vj$.IllegalArgumentException(msg);
					}
					he.keys.add(key);
					if (!single) {
						while ((key = at.getKey(false)) !== null) {
							he.keys.add(key);
						}
					}
					var param;
					while ((param = at.getParam()) !== null) {
						var is_directive = at.isDirective();
						if (is_directive) {
							if (he.directives.containsKey(param)) {
								var msg = "Definition, " + a + ", duplicate directive: " + param;
								throw new this.vj$.IllegalArgumentException(msg);
							}
							var valueStr = at.getValue(false);
							if (valueStr === null) {
								var msg = "Definition, " + a + ", expected value for " + " directive " + param + " at: " + at.getRest();
								throw new this.vj$.IllegalArgumentException(msg);
							}
							he.directives.put(param, valueStr);
						}
						else {
							var old = he.attributes.get(param);
							if (old !== null && unique) {
								var msg = "Definition, " + a + ", duplicate attribute: " + param;
								throw new this.vj$.IllegalArgumentException(msg);
							}
							var paramType = at.getParamType();
							var keepEscape = paramType !== null && StringUtil.startsWith(paramType, "List");
							var valueStr = at.getValue(keepEscape);
							if (valueStr === null) {
								var msg = "Definition, " + a + ", expected value for " + " attribute " + param + " at: " + at.getRest();
								throw new this.vj$.IllegalArgumentException(msg);
							}
							var value = this.toValue(a, param, paramType, valueStr);
							if (unique) {
								he.attributes.put(param, value);
							}
							else {
								var oldValues = old;
								if (oldValues === null) {
									oldValues = new this.vj$.ArrayList();
									he.attributes.put(param, oldValues);
								}
								oldValues.add(value);
							}
						}
					}
					if (at.getEntryEnd()) {
						res.add(he);
					}
					else {
						throw new this.vj$.IllegalArgumentException("Definition, " + a + ", expected end of entry at: " + at.getRest());
					}
					if (single_entry && !at.getEnd()) {
						throw new this.vj$.IllegalArgumentException("Definition, " + a + ", expected end of single entry at: " + at.getRest());
					}
				}
				while (!at.getEnd());
			}
			return res;
		},

		toValue : function(a, param, type, value) {
			var res;
			type = type === null ? this.STRING_TYPE : StringUtil.intern(type);
			if (this.STRING_TYPE === type) {
				res = value;
			}
			else if (this.LONG_TYPE === type) {
				try {
					res = new this.vj$.Long(StringUtil.trim(value));
				}
				catch (e) {
					throw (new this.vj$.IllegalArgumentException("Definition, " + a + ", expected value of type Long but found '" + value + "' for attribute '" + param + "'.")).initCause(e);
				}
			}
			else if (this.DOUBLE_TYPE === type) {
				try {
					res = new this.vj$.Double(StringUtil.trim(value));
				}
				catch (e) {
					throw (new this.vj$.IllegalArgumentException("Definition, " + a + ", expected value of type Double but found '" + value + "' for attribute '" + param + "'.")).initCause(e);
				}
			}
			else if (this.VERSION_TYPE === type) {
				try {
					res = new this.vj$.Version(value);
				}
				catch (e) {
					throw (new this.vj$.IllegalArgumentException("Definition, " + a + ", expected value of type Version but found '" + value + "' for attribute '" + param + "'.")).initCause(e);
				}
			}
			else if (StringUtil.startsWith(type, this.LIST_TYPE)) {
				var elemType = type.substring(this.LIST_TYPE.length).trim();
				if (elemType.length > 0) {
					if ('<' !== elemType.charAt(0) || elemType.charAt(elemType.length - 1) !== '>') {
						throw new this.vj$.IllegalArgumentException("Definition, " + a + ", expected List type definition '" + type + "' for attribute '" + param + "'.");
					}
					elemType = StringUtil.trim(elemType.substring(1, elemType.length - 1)).intern();
				}
				if (elemType.length === 0) {
					elemType = this.STRING_TYPE;
				}
				try {
					var elements = splitWords(value, ',', this.STRING_TYPE !== elemType);
					var l = new this.vj$.ArrayList(elements.size());
					for (var elem, _$itr = elements.iterator(); _$itr.hasNext();) {
						elem = _$itr.next();
						l.add(this.toValue(a, param, elemType, elem));
					}
					res = l;
				}
				catch (e) {
					throw (new this.vj$.IllegalArgumentException("Definition, " + a + ", expected '" + type + "' value but found '" + value + "' for attribute '" + param + "'.")).initCause(e);
				}
			}
			else {
				throw new this.vj$.IllegalArgumentException("Definition, " + a + ", unknown type '" + type + "' for attribute '" + param + "'.");
			}
			return res;
		},

		readResource : function(name) {
			if (arguments.length === 1) {
				return this.vj$.Util.readResource_1_0_Util_ovld(arguments[0]);
			}
			else if (arguments.length === 3) {
				return this.vj$.Util.readResource_3_0_Util_ovld(arguments[0], arguments[1], arguments[2]);
			}
		},

		readResource_1_0_Util_ovld : function(name) {
			var buf = vjo.createArray(0, 1024);
			var in_ = this.clazz.getResourceAsStream(name);
			try {
				var bout = new this.vj$.ByteArrayOutputStream();
				var n;
				while ((n = in_.read(buf)) > 0) {
					bout.write(buf, 0, n);
				}
				return bout.toByteArray();
			}
			finally {
				try {
					in_.close();
				}
				catch (ignored) {
				}
			}
		},

		readResource_3_0_Util_ovld : function(file, defaultValue, encoding) {
			try {
				return StringUtil.trim((org.eclipse.vjet.vjo.java.lang.StringFactory.build(this.readResource(file), encoding)));
			}
			catch (e) {
				return defaultValue;
			}
		},

		readFrameworkVersion : function() {
			return this.readResource("/version", "0.0.0", "UTF-8");
		},

		readTstampYear : function() {
			var year = this.readResource("/tstamp", "2013", "UTF-8");
			var pos = year.indexOf(',');
			if (pos > -1) {
				year = year.substring(0, pos);
				pos = year.lastIndexOf(' ');
				if (pos > -1) {
					year = year.substring(pos + 1);
				}
			}
			return year;
		},

		splitwords : function(s) {
			if (arguments.length === 1) {
				return this.vj$.Util.splitwords_1_0_Util_ovld(arguments[0]);
			}
			else if (arguments.length === 2) {
				return this.vj$.Util.splitwords_2_0_Util_ovld(arguments[0], arguments[1]);
			}
			else if (arguments.length === 3) {
				return this.vj$.Util.splitwords_3_0_Util_ovld(arguments[0], arguments[1], arguments[2]);
			}
		},

		splitwords_1_0_Util_ovld : function(s) {
			return this.splitwords(s, this.WHITESPACE);
		},

		splitwords_2_0_Util_ovld : function(s, whiteSpace) {
			return this.splitwords(s, whiteSpace, this.CITCHAR);
		},

		splitwords_3_0_Util_ovld : function(s, whiteSpace, citChar) {
			var bCit = false;
			var v = new this.vj$.Vector();
			var buf = new this.vj$.StringBuffer();
			var i = 0;
			while (i < s.length) {
				var c = s.charAt(i);
				if (bCit || whiteSpace.indexOf(c) === -1) {
					if (c === citChar) {
						bCit = !bCit;
					}
					else {
						if (buf === null) {
							buf = new this.vj$.StringBuffer();
						}
						buf.append(c);
					}
					i++;
				}
				else {
					if (buf !== null) {
						v.addElement(buf.toString());
						buf = null;
					}
					while ((i < s.length) && (-1 !== whiteSpace.indexOf(s.charAt(i)))) {
						i++;
					}
				}
			}
			if (buf !== null) {
				v.addElement(buf.toString());
			}
			var r = vjo.createArray(null, v.size());
			v.copyInto(r);
			return r;
		},

		splitWords : function(s, sepChar, trim) {
			var res = new this.vj$.ArrayList();
			var buf = new this.vj$.StringBuffer();
			var pos = 0;
			var length = s.length;
			var esc = false;
			var end = 0;
			for (; pos < length; pos++) {
				if (esc) {
					esc = false;
					buf.append(s.charAt(pos));
					end = buf.length();
				}
				else {
					var c = s.charAt(pos);
					if (c === '\\') {
						esc = true;
					}
					else if (c === sepChar) {
						if (trim) {
							buf.setLength(end);
						}
						res.add(buf.toString());
						buf.setLength(0);
						end = 0;
					}
					else if (this.vj$.Character.isWhitespace(c)) {
						if (buf.length() > 0 || !trim) {
							buf.append(c);
						}
					}
					else {
						buf.append(c);
						end = buf.length();
					}
				}
			}
			if (esc) {
				throw new this.vj$.IllegalArgumentException("Value ends on escape character");
			}
			if (trim) {
				buf.setLength(end);
			}
			res.add(buf.toString());
			return res;
		},

		replace : function(s, v1, v2) {
			if (s === null || v1 === null || v2 === null || v1.length === 0 || vjo.java.lang.ObjectUtil.equals(v1, v2)) {
				return s;
			}
			var ix = 0;
			var v1Len = v1.length;
			var n = 0;
			while (-1 !== (ix = s.indexOf(v1, ix))) {
				n++;
				ix += v1Len;
			}
			if (n === 0) {
				return s;
			}
			var start = 0;
			var v2Len = v2.length;
			var r = vjo.createArray("", s.length + n * (v2Len - v1Len));
			var rPos = 0;
			while (-1 !== (ix = s.indexOf(v1, start))) {
				while (start < ix) {
					r[rPos++] = s.charAt(start++);
				}
				for (var j = 0; j < v2Len; j++) {
					r[rPos++] = v2.charAt(j);
				}
				start += v1Len;
			}
			ix = s.length;
			while (start < ix) {
				r[rPos++] = s.charAt(start++);
			}
			return org.eclipse.vjet.vjo.java.lang.StringFactory.build(r);
		},

		getContent : function(f) {
			var in_ = null;
			try {
				in_ = new this.vj$.DataInputStream(new this.vj$.FileInputStream(f));
				return in_.readUTF();
			}
			catch (ignore) {
			}
			finally {
				if (in_ !== null) {
					try {
						in_.close();
					}
					catch (ignore) {
					}
				}
			}
			return null;
		},

		putContent : function(f, content) {
			if (arguments.length === 2) {
				this.vj$.Util.putContent_2_0_Util_ovld(arguments[0], arguments[1]);
			}
			else if (arguments.length === 3) {
				this.vj$.Util.putContent_3_0_Util_ovld(arguments[0], arguments[1], arguments[2]);
			}
		},

		putContent_2_0_Util_ovld : function(f, content) {
			this.putContent(f, content, true);
		},

		putContent_3_0_Util_ovld : function(f, content, useUTF8) {
			var out = null;
			try {
				out = new this.vj$.DataOutputStream(new this.vj$.FileOutputStream(f));
				if (useUTF8) {
					out.writeUTF(content);
				}
				else {
					out.writeChars(content);
				}
			}
			finally {
				if (out !== null) {
					out.close();
				}
			}
		},

		sort : function(a, cf, bReverse) {
			if (arguments.length === 3) {
				this.vj$.Util.sort_3_0_Util_ovld(arguments[0], arguments[1], arguments[2]);
			}
			else if (arguments.length === 5) {
				this.vj$.Util.sort_5_0_Util_ovld(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
			}
		},

		sort_3_0_Util_ovld : function(a, cf, bReverse) {
			this.sort(a, 0, a.size() - 1, cf, bReverse ? -1 : 1);
		},

		sort_5_0_Util_ovld : function(a, lo0, hi0, cf, k) {
			var lo = lo0;
			var hi = hi0;
			var mid;
			if (hi0 > lo0) {
				mid = a.get(parseInt((lo0 + hi0) / 2));
				while (lo <= hi) {
					while ((lo < hi0) && (k * cf.compare(a.get(lo), mid) < 0)) {
						++lo;
					}
					while ((hi > lo0) && (k * cf.compare(a.get(hi), mid) > 0)) {
						--hi;
					}
					if (lo <= hi) {
						this.swap(a, lo, hi);
						++lo;
						--hi;
					}
				}
				if (lo0 < hi) {
					this.sort(a, lo0, hi, cf, k);
				}
				if (lo < hi0) {
					this.sort(a, lo, hi0, cf, k);
				}
			}
		},

		swap : function(a, i, j) {
			var tmp = a.get(i);
			a.set(i, a.get(j));
			a.set(j, tmp);
		},

		binarySearch : function(pl, c, k) {
			var l = 0;
			var u = pl.size() - 1;
			while (l <= u) {
				var m = parseInt((l + u) / 2);
				var v = c.compare(pl.get(m), k);
				if (v > 0) {
					l = m + 1;
				}
				else if (v < 0) {
					u = m - 1;
				}
				else {
					return m;
				}
			}
			return -(l + 1);
		},

		base64Encode : function(s) {
			return encode(s.getBytes(), 0);
		},

		encode : function(in_, len) {
			if (arguments.length === 2) {
				return this.vj$.Util.encode_2_0_Util_ovld(arguments[0], arguments[1]);
			}
			else if (arguments.length === 3) {
				this.vj$.Util.encode_3_0_Util_ovld(arguments[0], arguments[1], arguments[2]);
			}
		},

		encode_2_0_Util_ovld : function(in_, len) {
			var baos = null;
			var bais = null;
			try {
				baos = new this.vj$.ByteArrayOutputStream();
				bais = new this.vj$.ByteArrayInputStream(in_);
				encode(bais, baos, len);
				return (org.eclipse.vjet.vjo.java.lang.StringFactory.build(baos.toByteArray()));
			}
			finally {
				if (baos !== null) {
					baos.close();
				}
				if (bais !== null) {
					bais.close();
				}
			}
		},

		encode_3_0_Util_ovld : function(in_, out, len) {
			if (len % 4 !== 0) {
				throw new this.vj$.IllegalArgumentException("Length must be a multiple of 4");
			}
			var bits = 0;
			var nbits = 0;
			var nbytes = 0;
			var b;
			while ((b = in_.read()) !== -1) {
				bits = (bits << 8) | b;
				nbits += 8;
				while (nbits >= 6) {
					nbits -= 6;
					out.write(this.encTab[0x3f & (bits >> nbits)]);
					nbytes++;
					if (len !== 0 && nbytes >= len) {
						out.write(0x0d);
						out.write(0x0a);
						nbytes -= len;
					}
				}
			}
			switch (nbits) {
				case 2:
					out.write(this.encTab[0x3f & (bits << 4)]);
					out.write(0x3d);
					out.write(0x3d);
					break;
				case 4:
					out.write(this.encTab[0x3f & (bits << 2)]);
					out.write(0x3d);
					break;
			}
			if (len !== 0) {
				if (nbytes !== 0) {
					out.write(0x0d);
					out.write(0x0a);
				}
				out.write(0x0d);
				out.write(0x0a);
			}
		},

		mergeDictionaries : function(target, extra) {
			for (var e = extra.keys(); e.hasMoreElements();) {
				var key = e.nextElement();
				if (target.get(key) === null) {
					target.put(key, extra.get(key));
				}
			}
		},

		filterMatch : function(filter, s) {
			return this.patSubstr(StringUtil.toCharArray(s), 0, StringUtil.toCharArray(filter), 0);
		},

		patSubstr : function(s, si, pat, pi) {
			if (pat.length - pi === 0) {
				return s.length - si === 0;
			}
			if (pat[pi] === '*') {
				pi++;
				for (;;) {
					if (this.patSubstr(s, si, pat, pi)) {
						return true;
					}
					if (s.length - si === 0) {
						return false;
					}
					si++;
				}
			}
			else {
				if (s.length - si === 0) {
					return false;
				}
				if (s[si] !== pat[pi]) {
					return false;
				}
				return this.patSubstr(s, ++si, pat, ++pi);
			}
		},

		getMethod : function(c, name, args) {
			var m = null;
			while (true) {
				try {
					m = c.getDeclaredMethod(name, args);
					break;
				}
				catch (e) {
					c = org.eclipse.vjet.vjo.java.lang.ClassUtil.getSuperclass(c);
					if (c === null) {
						return null;
					}
				}
			}
			m.setAccessible(true);
			return m;
		},

		timeMillis : function() {
			if (this.nanoTimeMethod !== null) {
				try {
					var res = this.nanoTimeMethod.invoke(null, []);
					if (res !== null) {
						return parseInt(res.longValue() / 1000000);
					}
				}
				catch (_ignore) {
					if (_ignore instanceof this.vj$.IllegalAccessException) {
					}
					else if (_ignore instanceof this.vj$.InvocationTargetException) {
					}
				}
			}
			return Data.now();
		}
	
	})
	
	.inits(function() {
		//this.vj$.Util.nanoTimeMethod = getMethod(vjo.getType('org.eclipse.vjet.vjo.java.lang.System').clazz, "nanoTime", []);
		this.vj$.Util.encTab = [ 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x2b, 0x2f ];
	})
	
	.endType();

});