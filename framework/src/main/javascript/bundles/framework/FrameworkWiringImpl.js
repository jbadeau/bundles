/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.FrameworkWiringImpl') 
.needs('org.eclipse.vjet.vjo.java.util.HashSet')

.satisfies('osgi.framework.wiring.FrameworkWiring')
.props({
    SPEC_VERSION:"1.0" 
})
.protos({
    fwCtx:null, 
    
    constructs:function(fwCtx){
        this.fwCtx=fwCtx;
    },
    
    getBundle:function(){
        return this.fwCtx.systemBundle;
    },
    
    refreshBundles:function(bundles){
        var listeners;
        if (arguments.length == 2 && arguments[1]  instanceof Array){
            listeners=arguments[1];
        }
        else {
            listeners=[];
            for (var i=1; i<arguments.length; i++){
                listeners[i-1]=arguments[i];
            }
        }
        var bs=bundles!==null?bundles.toArray(vjo.createArray(null, bundles.size())):null; 
        this.fwCtx.packageAdmin.refreshPackages(bs,listeners);
    },
    
    resolveBundles:function(bundles){
        var bs=bundles!==null?bundles.toArray(vjo.createArray(null, bundles.size())):null; 
        return this.fwCtx.packageAdmin.resolveBundles(bs);
    },
    
    getRemovalPendingBundles:function(){
        var res=new this.vj$.HashSet(); 
        this.fwCtx.bundles.getRemovalPendingBundles(res);
        return res;
    },
    
    getDependencyClosure:function(bundles){
        var res=new this.vj$.HashSet(); 
        for (var b,_$itr=bundles.iterator();_$itr.hasNext();){
            b=_$itr.next();
            this.fwCtx.checkOurBundle(b);
            res.add(b);
        }
        this.fwCtx.resolver.closure(res);
        return res;
    }
})
.endType();