/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.ExportPkg') 
.needs(['org.eclipse.vjet.vjo.java.util.Set','org.eclipse.vjet.vjo.java.util.Map',
    'org.eclipse.vjet.vjo.java.lang.IllegalArgumentException','org.eclipse.vjet.vjo.java.util.Collections',
    'org.eclipse.vjet.vjo.java.util.Iterator','org.eclipse.vjet.vjo.java.util.List',
    'org.eclipse.vjet.vjo.java.util.ArrayList','org.eclipse.vjet.vjo.java.lang.StringBuffer',
    'org.eclipse.vjet.vjo.java.util.HashMap','bundles.framework.BundlePackages',
    'osgi.framework.Version','bundles.framework.Pkg',
    'bundles.framework.Util','osgi.framework.Constants',
    'bundles.framework.ImportPkg','osgi.framework.wiring.BundleRevision',
    'org.eclipse.vjet.vjo.java.lang.StringUtil','org.eclipse.vjet.vjo.java.lang.ObjectUtil'])
.satisfies('osgi.framework.wiring.BundleCapability')
.satisfies('Comparable<ExportPkg>')
.props({
    exportPkgCount:0 
})
.protos({
    orderal:0, 
    name:null, 
    bpkgs:null, 
    uses:null, 
    mandatory:null, 
    include:null, 
    exclude:null, 
    version:null, 
    attributes:null, 
    zombie:false, 
    pkg:null, 
    
    
    
    
    constructs:function(){
        this.orderal=++this.vj$.ExportPkg.exportPkgCount;
        if(arguments.length===3){
            this.constructs_3_0_ExportPkg_ovld(arguments[0],arguments[1],arguments[2]);
        }else if(arguments.length===2){
            if(arguments[0] instanceof bundles.framework.ExportPkg && (arguments[1] instanceof String || typeof arguments[1]=="string")){
                this.constructs_2_0_ExportPkg_ovld(arguments[0],arguments[1]);
            }else if(arguments[0] instanceof bundles.framework.ExportPkg && arguments[1] instanceof bundles.framework.BundlePackages){
                this.constructs_2_1_ExportPkg_ovld(arguments[0],arguments[1]);
            }
        }
    },
    
    constructs_3_0_ExportPkg_ovld:function(name,he,b){
        this.bpkgs=b;
        this.name=name;
        if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(name,"java.")){
            throw new this.vj$.IllegalArgumentException("You can not export a java.* package");
        }
        this.uses=this.vj$.Util.parseEnumeration(this.vj$.Constants.USES_DIRECTIVE,he.getDirectives().get(this.vj$.Constants.USES_DIRECTIVE));
        this.mandatory=this.vj$.Util.parseEnumeration(this.vj$.Constants.MANDATORY_DIRECTIVE,he.getDirectives().get(this.vj$.Constants.MANDATORY_DIRECTIVE));
        this.include=this.vj$.Util.parseEnumeration(this.vj$.Constants.INCLUDE_DIRECTIVE,he.getDirectives().get(this.vj$.Constants.INCLUDE_DIRECTIVE));
        this.exclude=this.vj$.Util.parseEnumeration(this.vj$.Constants.EXCLUDE_DIRECTIVE,he.getDirectives().get(this.vj$.Constants.EXCLUDE_DIRECTIVE));
        var versionStr=he.getAttributes().remove(this.vj$.Constants.VERSION_ATTRIBUTE); 
        var SPEC_VERSION=this.vj$.Constants.PACKAGE_SPECIFICATION_VERSION; 
        var specVersionStr=he.getAttributes().remove(SPEC_VERSION); 
        if(specVersionStr!==null){
            this.version=new this.vj$.Version(specVersionStr);
            if(versionStr!==null&& !org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(this.version,new this.vj$.Version(versionStr))){
                throw new this.vj$.IllegalArgumentException("Both "+this.vj$.Constants.VERSION_ATTRIBUTE+" and "+SPEC_VERSION+" are specified, and differs");
            }
        }else if(versionStr!==null){
            this.version=new this.vj$.Version(versionStr);
        }else {
            this.version=this.vj$.Version.emptyVersion;
        }
        if(he.getAttributes().containsKey(this.vj$.Constants.BUNDLE_VERSION_ATTRIBUTE)){
            throw new this.vj$.IllegalArgumentException("Export definition illegally contains attribute, "+this.vj$.Constants.BUNDLE_VERSION_ATTRIBUTE);
        }
        if(he.getAttributes().containsKey(this.vj$.Constants.BUNDLE_SYMBOLICNAME_ATTRIBUTE)){
            throw new this.vj$.IllegalArgumentException("Export definition illegally contains attribute, "+this.vj$.Constants.BUNDLE_SYMBOLICNAME_ATTRIBUTE);
        }
        this.attributes=this.vj$.Collections.unmodifiableMap(he.getAttributes());
    },
    
    constructs_2_0_ExportPkg_ovld:function(ep,name){
        this.name=name;
        this.bpkgs=ep.bpkgs;
        this.uses=ep.uses;
        this.mandatory=ep.mandatory;
        this.include=ep.include;
        this.exclude=ep.exclude;
        this.version=ep.version;
        this.attributes=ep.attributes;
    },
    
    constructs_2_1_ExportPkg_ovld:function(ep,b){
        this.name=ep.name;
        this.bpkgs=b;
        this.uses=ep.uses;
        this.mandatory=ep.mandatory;
        this.include=ep.include;
        this.exclude=ep.exclude;
        this.version=ep.version;
        this.attributes=ep.attributes;
    },
    
    attachPkg:function(p){
        this.vj$.Pkg=p;
    },
    
    detachPkg:function(){
        this.vj$.Pkg=null;
        this.zombie=false;
    },
    
    checkFilter:function(fullClassName){
        var clazz=null; 
        var ok=true; 
        if(fullClassName!==null){
            if(this.include!==null){
                clazz=fullClassName.substring(this.name.length+1);
                for (var i=this.include.iterator();i.hasNext();){
                    if(this.vj$.Util.filterMatch(i.next(),clazz)){
                        break;
                    }
                    if(!i.hasNext()){
                        ok=false;
                    }
                }
            }
            if(ok&&this.exclude!==null){
                if(clazz===null){
                    clazz=fullClassName.substring(this.name.length+1);
                }
                for (var string,_$itr=this.exclude.iterator();_$itr.hasNext();){
                    string=_$itr.next();
                    if(this.vj$.Util.filterMatch(string,clazz)){
                        ok=false;
                        break;
                    }
                }
            }
        }
        return ok;
    },
    
    isProvider:function(){
        var p=this.vj$.Pkg; 
        if(p!==null){
            {
                return p.providers.contains(this)||this.bpkgs.isRequired();
            }
        }
        return false;
    },
    
    isExported:function(){
        var bp=this.bpkgs; 
        if(this.checkPermission()&&this.vj$.Pkg!==null&&(bp.bg.bundle.isResolved()||this.zombie)){
            var pbp=bp.getProviderBundlePackages(this.name); 
            return pbp===null||pbp.bg.bundle===this.bpkgs.bg.bundle;
        }
        return false;
    },
    
    getPackageImporters:function(){
        var p=this.vj$.Pkg; 
        if(p!==null){
            var res=new this.vj$.ArrayList(); 
            {
                for (var ip,_$itr=p.importers.iterator();_$itr.hasNext();){
                    ip=_$itr.next();
                    if(ip.provider===this&&ip.bpkgs!==this.bpkgs){
                        res.add(ip);
                    }
                }
            }
            return res;
        }
        return null;
    },
    
    checkPermission:function(){
        return this.bpkgs.bg.bundle.fwCtx.perm.hasExportPackagePermission(this);
    },
    
    pkgEquals:function(o){
        if(this===o){
            return true;
        }
        if(null===o){
            return false;
        }
        var ep=o; 
        return vjo.java.lang.ObjectUtil.equals(this.name,ep.name)&&org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(this.version,ep.version)&&(this.uses===null?ep.uses===null:this.uses.equals(ep.uses))&&(this.mandatory===null?ep.mandatory===null:this.mandatory.equals(ep.mandatory))&&(this.include===null?ep.include===null:this.include.equals(ep.include))&&(this.exclude===null?ep.exclude===null:this.exclude.equals(ep.exclude))&&this.attributes.equals(ep.attributes);
    },
    
    pkgString:function(){
        if(this.version!==this.vj$.Version.emptyVersion){
            return this.name+";"+this.vj$.Constants.VERSION_ATTRIBUTE+"="+this.version;
        }else {
            return this.name;
        }
    },
    
    toString:function(){
        var sb=new this.vj$.StringBuffer(this.pkgString()); 
        sb.append(' ');
        if(this.zombie){
            sb.append("Zombie");
        }
        sb.append("Bundle");
        sb.append(this.bpkgs.bundleGenInfo());
        return sb.toString();
    },
    
    getNamespace:function(){
        return this.vj$.BundleRevision.PACKAGE_NAMESPACE;
    },
    
    getDirectives:function(){
        var res=new this.vj$.HashMap(1); 
        if(this.uses!==null){
            var sb=new this.vj$.StringBuffer(this.uses.size()*30); 
            for (var pkg,_$itr=this.uses.iterator();_$itr.hasNext();){
                pkg=_$itr.next();
                if(sb.length()>0){
                    sb.append(',');
                }
                sb.append(pkg);
            }
            res.put(this.vj$.Constants.USES_DIRECTIVE,sb.toString());
        }
        return res;
    },
    
    getAttributes:function(){
        var res=new this.vj$.HashMap(4+this.attributes.size()); 
        res.put(this.vj$.BundleRevision.PACKAGE_NAMESPACE,this.name);
        res.put(this.vj$.Constants.VERSION_ATTRIBUTE,this.version);
        res.put(this.vj$.Constants.BUNDLE_SYMBOLICNAME_ATTRIBUTE,this.bpkgs.bg.symbolicName);
        res.put(this.vj$.Constants.BUNDLE_VERSION_ATTRIBUTE,this.bpkgs.bg.version);
        res.putAll(this.attributes);
        return this.vj$.Collections.unmodifiableMap(res);
    },
    
    getRevision:function(){
        return this.bpkgs.bg.bundleRevision;
    },
    
    getResource:function(){
        return this.bpkgs.bg.bundleRevision;
    },
    
    compareTo:function(o){
        return this.orderal-o.orderal;
    }
})
.endType();