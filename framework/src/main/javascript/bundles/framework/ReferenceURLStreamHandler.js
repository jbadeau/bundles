/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.ReferenceURLStreamHandler') 
.needs(['org.eclipse.vjet.vjo.java.lang.IllegalArgumentException','java.net.URL',
    'java.io.IOException','org.eclipse.vjet.vjo.java.lang.StringUtil'])

.inherits('java.net.URLStreamHandler')
.props({
    PROTOCOL:"reference", 
    
    getActual:function(u){
        var s=u.toString(); 
        if(!org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(s,this.PROTOCOL+":")){
            throw new this.vj$.IllegalArgumentException("URL "+u+" does not start with "+this.PROTOCOL+":");
        }
        return s.substring(this.PROTOCOL.length+1);
    }
})
.protos({
    
    constructs:function(){
        this.base();
    },
    
    openConnection:function(url){
        var actual=new this.vj$.URL(this.vj$.ReferenceURLStreamHandler.getActual(url)); 
        if(!vjo.java.lang.ObjectUtil.equals("file",actual.getProtocol())){
            throw new this.vj$.IOException("Only file: URLs are allowed as references, got "+url);
        }
        return actual.openConnection();
    }
})
.endType();