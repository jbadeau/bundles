/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.Capabilities') 
.needs(['org.eclipse.vjet.vjo.java.util.HashMap','org.eclipse.vjet.vjo.java.util.ArrayList',
    'org.eclipse.vjet.vjo.java.lang.RuntimeException'])

.protos({
    namespaceCapabilties:null, 
    
    constructs:function(){
        this.namespaceCapabilties=new this.vj$.HashMap();
    },
    
    getCapabilities:function(namespace){
        return this.namespaceCapabilties.get(namespace);
    },
    
    addCapabilities:function(capabilities){
        for (var e,_$itr=capabilities.entrySet().iterator();_$itr.hasNext();){
            e=_$itr.next();
            var ns=e.getKey(); 
            var bcl=this.namespaceCapabilties.get(ns); 
            if(bcl===null){
                bcl=new this.vj$.ArrayList();
                this.namespaceCapabilties.put(ns,bcl);
            }
            bcl.addAll(e.getValue());
        }
    },
    
    removeCapabilities:function(capabilities){
        for (var e,_$itr=capabilities.entrySet().iterator();_$itr.hasNext();){
            e=_$itr.next();
            var ns=e.getKey(); 
            var bcl=this.namespaceCapabilties.get(ns); 
            if(bcl!==null){
                var before=bcl.size(); 
                bcl.removeAll(e.getValue());
                if(bcl.isEmpty()){
                    this.namespaceCapabilties.remove(ns);
                }
                if(before!==bcl.size()+e.getValue().size()){
                    throw new this.vj$.RuntimeException("Internal error, tried to remove unknown capabilities");
                }
            }else {
                throw new this.vj$.RuntimeException("Internal error, tried to remove unknown name space with capabilities");
            }
        }
    },
    
    getAll:function(){
        return this.namespaceCapabilties.values();
    }
})
.endType();