/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.WeavingHooks') 
.needs(['org.eclipse.vjet.vjo.java.lang.RuntimeException','vjo.java.lang.Throwable',
    'org.eclipse.vjet.vjo.java.util.List','org.eclipse.vjet.vjo.java.lang.System',
    'org.eclipse.vjet.vjo.java.lang.IllegalStateException','org.eclipse.vjet.vjo.java.lang.NullPointerException',
    'org.eclipse.vjet.vjo.java.lang.StringBuffer','org.eclipse.vjet.vjo.java.util.ArrayList',
    'org.eclipse.vjet.vjo.java.util.ListIterator','osgi.util.tracker.ServiceTracker',
    'osgi.framework.hooks.weaving.WeavingHook','osgi.util.tracker.ServiceTrackerCustomizer',
    'osgi.framework.hooks.weaving.WeavingException','osgi.framework.hooks.weaving.WovenClass'])

.props({
    TrackedWeavingHook:vjo.ctype() 
    .satisfies('osgi.framework.hooks.weaving.WeavingHook')
    .protos({
        tracked:null, 
        reference:null, 
        blacklisted:false, 
        
        constructs:function(tracked,reference){
            this.tracked=tracked;
            this.reference=reference;
        },
        
        weave:function(wovenClass){
            this.tracked.weave(wovenClass);
        },
        
        blacklist:function(){
            this.blacklisted=true;
        },
        
        isBlackListed:function(){
            return this.blacklisted;
        }
    })
    .endType(),
    WovenClassImpl:vjo.ctype() 
    .satisfies('osgi.framework.hooks.weaving.WovenClass')
    .protos({
        bundle:null, 
        name:null, 
        current:null, 
        complete:false, 
        c:null, 
        dynamicImports:null, 
        
        constructs:function(bundle,name,initial){
            this.dynamicImports=new this.vj$.WeavingHooks.DynamicImportList(this);
            this.bundle=bundle;
            this.name=name;
            this.current=initial;
        },
        
        getBytes:function(){
            this.bundle.fwCtx.perm.checkWeaveAdminPerm(this.bundle);
            if(this.complete){
                var r=vjo.createArray(0, this.current.length); 
                this.vj$.System.arraycopy(this.current,0,r,0,this.current.length);
                return r;
            }else {
                return this.current;
            }
        },
        
        setBytes:function(newBytes){
            this.bundle.fwCtx.perm.checkWeaveAdminPerm(this.bundle);
            if(this.complete){
                throw new IllegalStateException("Trying to call WovenClass.setBytes(byte[]) after weaving is complete");
            }
            if(newBytes===null){
                throw new NullPointerException("Trying to call WovenClass.setBytes(byte[]) with null newBytes");
            }
            this.current=newBytes;
        },
        
        getDynamicImports:function(){
            return this.dynamicImports;
        },
        
        isWeavingComplete:function(){
            return this.complete;
        },
        
        getClassName:function(){
            return this.name;
        },
        
        getProtectionDomain:function(){
            return this.c===null?null:this.c.getProtectionDomain();
        },
        
        getDefinedClass:function(){
            return this.c;
        },
        
        getBundleWiring:function(){
            return this.bundle.current().bundleRevision.getWiring();
        },
        
        markAsComplete:function(){
            this.complete=true;
        },
        
        setDefinedClass:function(c){
            this.markAsComplete();
            this.c=c;
        },
        
        getDynamicImportsAsString:function(){
            var sb=new StringBuffer(); 
            for (var s,_$itr=this.dynamicImports.iterator();_$itr.hasNext();){
                s=_$itr.next();
                if(sb.length()>0){
                    sb.append(", ");
                }
                sb.append(s);
            }
            return sb.toString();
        },
        
        
        toString:function(){
            if(arguments.length===0){
                return this.toString_0_0_WovenClassImpl_ovld();
            }else if(arguments.length===1){
                return this.toString_1_0_WovenClassImpl_ovld(arguments[0]);
            }
        },
        
        toString_0_0_WovenClassImpl_ovld:function(){
            return "WovenClass["+this.name+", "+this.toString(this.dynamicImports)+", byte["+this.current.length+"]="+this.current+"]";
        },
        
        toString_1_0_WovenClassImpl_ovld:function(sl){
            var sb=new StringBuffer(); 
            sb.append("(");
            for (var s,_$itr=sl.iterator();_$itr.hasNext();){
                s=_$itr.next();
                if(sb.length()>1){
                    sb.append(", ");
                }
                sb.append(s);
            }
            sb.append(")");
            return sb.toString();
        },
        
        hasAdditionalDynamicImports:function(){
            return !this.dynamicImports.isEmpty();
        }
    })
    .endType(),
    DynamicImportList:vjo.ctype() 
    .satisfies('org.eclipse.vjet.vjo.java.util.List<E>')
    .protos({
        org:null, 
        parent:null, 
        
        
        
        constructs:function(){
            if(arguments.length===1){
                this.constructs_1_0_DynamicImportList_ovld(arguments[0]);
            }else if(arguments.length===2){
                this.constructs_2_0_DynamicImportList_ovld(arguments[0],arguments[1]);
            }
        },
        
        constructs_1_0_DynamicImportList_ovld:function(parent){
            this.parent=parent;
            this.org=new ArrayList();
        },
        
        constructs_2_0_DynamicImportList_ovld:function(parent,subList){
            this.parent=parent;
            this.org=subList;
        },
        
        
        
        add:function(elem){
            if(arguments.length===1){
                return this.add_1_0_DynamicImportList_ovld(arguments[0]);
            }else if(arguments.length===2){
                this.add_2_0_DynamicImportList_ovld(arguments[0],arguments[1]);
            }
        },
        
        add_1_0_DynamicImportList_ovld:function(elem){
            this.checkChangeAllowed();
            return this.org.add(elem);
        },
        
        add_2_0_DynamicImportList_ovld:function(index,elem){
            this.checkChangeAllowed();
            this.org.add(index,elem);
        },
        
        
        
        addAll:function(elems){
            if(arguments.length===1){
                return this.addAll_1_0_DynamicImportList_ovld(arguments[0]);
            }else if(arguments.length===2){
                return this.addAll_2_0_DynamicImportList_ovld(arguments[0],arguments[1]);
            }
        },
        
        addAll_1_0_DynamicImportList_ovld:function(elems){
            this.checkChangeAllowed();
            return this.org.addAll(elems);
        },
        
        addAll_2_0_DynamicImportList_ovld:function(index,elems){
            this.checkChangeAllowed();
            return this.org.addAll(index,elems);
        },
        
        clear:function(){
            this.checkChangeAllowed();
            this.org.clear();
        },
        
        contains:function(elem){
            return this.org.contains(elem);
        },
        
        containsAll:function(elems){
            return this.org.containsAll(elems);
        },
        
        get:function(index){
            return this.org.get(index);
        },
        
        indexOf:function(elem){
            return this.org.indexOf(elem);
        },
        
        isEmpty:function(){
            return this.org.isEmpty();
        },
        
        iterator:function(){
            return new this.vj$.WeavingHooks.DynamicListIterator(this.parent,this.org.listIterator());
        },
        
        lastIndexOf:function(elem){
            return this.org.lastIndexOf(elem);
        },
        
        
        listIterator:function(){
            if(arguments.length===0){
                return this.listIterator_0_0_DynamicImportList_ovld();
            }else if(arguments.length===1){
                return this.listIterator_1_0_DynamicImportList_ovld(arguments[0]);
            }
        },
        
        listIterator_0_0_DynamicImportList_ovld:function(){
            return new this.vj$.WeavingHooks.DynamicListIterator(this.parent,this.org.listIterator());
        },
        
        listIterator_1_0_DynamicImportList_ovld:function(index){
            return new this.vj$.WeavingHooks.DynamicListIterator(this.parent,this.org.listIterator(index));
        },
        
        
        remove:function(index){
            if(arguments.length===1){
                if(typeof arguments[0]=="number"){
                    return this.remove_1_0_DynamicImportList_ovld(arguments[0]);
                }else if(arguments[0] instanceof Object){
                    return this.remove_1_1_DynamicImportList_ovld(arguments[0]);
                }
            }
        },
        
        remove_1_0_DynamicImportList_ovld:function(index){
            this.checkChangeAllowed();
            return this.org.remove(index);
        },
        
        remove_1_1_DynamicImportList_ovld:function(elem){
            this.checkChangeAllowed();
            return this.org.remove(elem);
        },
        
        removeAll:function(elems){
            this.checkChangeAllowed();
            return this.org.removeAll(elems);
        },
        
        retainAll:function(elems){
            this.checkChangeAllowed();
            return this.org.removeAll(elems);
        },
        
        set:function(index,elem){
            this.checkChangeAllowed();
            return this.org.set(index,elem);
        },
        
        size:function(){
            return this.org.size();
        },
        
        subList:function(from,to){
            return new this.vj$.DynamicImportList(this.parent,this.org.subList(from,to));
        },
        
        
        toArray:function(){
            if(arguments.length===0){
                return this.toArray_0_0_DynamicImportList_ovld();
            }else if(arguments.length===1){
                return this.toArray_1_0_DynamicImportList_ovld(arguments[0]);
            }
        },
        
        toArray_0_0_DynamicImportList_ovld:function(){
            return this.org.toArray();
        },
        
        toArray_1_0_DynamicImportList_ovld:function(a){
            return this.org.toArray(a);
        },
        
        checkChangeAllowed:function(){
            if(this.parent.isWeavingComplete()){
                throw new IllegalStateException("Parent WovenClass is frozen");
            }
            this.parent.bundle.fwCtx.perm.checkWeaveAdminPerm(this.parent.bundle);
        }
    })
    .endType(),
    DynamicListIterator:vjo.ctype() 
    .satisfies('org.eclipse.vjet.vjo.java.util.ListIterator<E>')
    .protos({
        parent:null, 
        org:null, 
        
        constructs:function(parent,org){
            this.parent=parent;
            this.org=org;
        },
        
        add:function(elem){
            this.checkChangeAllowed();
            this.org.add(elem);
        },
        
        hasNext:function(){
            return this.org.hasNext();
        },
        
        hasPrevious:function(){
            return this.org.hasPrevious();
        },
        
        next:function(){
            return this.org.next();
        },
        
        nextIndex:function(){
            return this.org.nextIndex();
        },
        
        previous:function(){
            return this.org.previous();
        },
        
        previousIndex:function(){
            return this.org.previousIndex();
        },
        
        remove:function(){
            this.checkChangeAllowed();
            this.org.remove();
        },
        
        set:function(elem){
            this.checkChangeAllowed();
            this.org.set(elem);
        },
        
        checkChangeAllowed:function(){
            if(this.parent.isWeavingComplete()){
                throw new IllegalStateException("Parent WovenClass is frozen");
            }
            this.parent.bundle.fwCtx.perm.checkWeaveAdminPerm(this.parent.bundle);
        }
    })
    .endType()
})
.protos({
    fwCtx:null, 
    weavingHookTracker:null, 
    
    constructs:function(fwCtx){
        this.fwCtx=fwCtx;
    },
    
    open:function(){
        if(this.fwCtx.debug.hooks){
            this.fwCtx.debug.println("Begin Tracking Weaving Hooks");
        }
        this.weavingHookTracker=new this.vj$.ServiceTracker(this.fwCtx.systemBundle.bundleContext,this.vj$.WeavingHook.clazz,
            vjo.make(this,this.vj$.ServiceTrackerCustomizer)
            .protos({
                addingService:function(reference){
                    return new this.vj$.WeavingHooks.TrackedWeavingHook(this.vj$.parent.fwCtx.systemBundle.bundleContext.getService(reference),reference);
                    return new this.vj$.WeavingHooks.TrackedWeavingHook(this.vj$.parent.fwCtx.systemBundle.bundleContext.getService(reference),reference);
                },
                modifiedService:function(reference,service){
                },
                removedService:function(reference,service){
                }
            })
            .endType());
        this.weavingHookTracker.open();
    },
    
    close:function(){
        this.weavingHookTracker.close();
        this.weavingHookTracker=null;
    },
    
    isOpen:function(){
        return this.weavingHookTracker!==null;
    },
    
    callHooks:function(wc){
        if(!this.isOpen()){
            return;
        }
        if(wc.isWeavingComplete()){
            throw new this.vj$.RuntimeException("ERROR!!");
        }
        try {
            var hooks=this.weavingHookTracker.getTracked(); 
            for (var twh,_$itr=hooks.values().iterator();_$itr.hasNext();){
                twh=_$itr.next();
                if(twh.isBlackListed()){
                    continue;
                }
                try {
                    twh.weave(wc);
                }
                catch(e){
                    if(e instanceof this.vj$.WeavingException){
                        var we=e;
                        this.fwCtx.frameworkError(twh.reference.getBundle(),we);
                        var cfe=new ClassFormatError("WeavingException thrown: "+we.getMessage()+" by hook "+twh.getClass().getName()); 
                        cfe.initCause(we);
                        throw cfe;
                    }else if(e instanceof this.vj$.Throwable){
                        var t=e;
                        this.fwCtx.frameworkError(twh.reference.getBundle(),t);
                        twh.blacklist();
                        var cfe=new ClassFormatError("Exception throw: "+t+" while calling hook "+twh.getClass().getName()); 
                        cfe.initCause(t);
                        throw cfe;
                    }
                }
            }
        }
        finally {
            wc.markAsComplete();
        }
    }
})
.endType();