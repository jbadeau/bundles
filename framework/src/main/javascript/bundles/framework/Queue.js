/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.Queue<E>') 
.needs(['org.eclipse.vjet.vjo.java.lang.IndexOutOfBoundsException','org.eclipse.vjet.vjo.java.lang.Math'])
.inherits('org.eclipse.vjet.vjo.java.util.Vector<E>')
.props({
    serialVersionUID:1 
})
.protos({
    m_nMaxSize:-1, 
    queueClosed:false, 
    
    constructs:function(size){
        this.base();
        this.m_nMaxSize=size;
    },
    
    insert:function(item){
        if(this.m_nMaxSize>0&&size()>=this.m_nMaxSize){
            throw new this.vj$.IndexOutOfBoundsException("Queue full");
        }
        addElement(item);
        notify();
    },
    
    insertFirst:function(item){
        insertElementAt(item,0);
        notify();
    },
    
    removeWait:function(timeout){
        var obj=null; 
        if(isEmpty()&& !this.queueClosed){
            try {
                if(timeout>0){
                    wait(this.vj$.Math.round(timeout*1000.0));
                }else {
                    wait();
                }
            }
            catch(e){
            }
        }
        if(this.queueClosed||isEmpty()){
            return null;
        }
        obj=firstElement();
        removeElementAt(0);
        return obj;
    },
    
    remove:function(){
        return this.removeWait(0);
    },
    
    close:function(){
        this.queueClosed=true;
        notifyAll();
    }
})
.endType();