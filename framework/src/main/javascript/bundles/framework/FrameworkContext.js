define([ "vjo", "./SystemBundle", "./FrameworkProperties", "osgi/lang/ClassLoader", "./PermissionOps", "osgi/framework/Constants", "./Util", "osgi/util/ObjectUtil", "osgi/util/StringUtil" ], function(vjo, SystemBundle, FrameworkProperties, ClassLoader, PermissionOps, Constants, Util, ObjectUtil, StringUtil) {

	return vjo.ctype('bundles.framework.FrameworkContext')

	.props({

		CONDITIONAL_PERMISSION_SECURITY_MANAGER : "org.bundles.framework.permissions.ConditionalPermissionSecurityManager",

		KF_SECURITY_MANAGER : "org.bundles.framework.permissions.KFSecurityManager",

		SECURE_PERMISSON_OPS : "org.bundles.framework.SecurePermissionOps",

		systemUrlStreamHandlerFactory : null,

		systemContentHandlerFactory : null,

		globalFwLock : null,

		globalId : 0,

		smUse : 0,

		BCLoader : vjo.ctype()

		.inherits(ClassLoader)

		.protos({

			constructs : function() {
				this.base(null);
			}

		})

		.endType()

	})

	.protos({

		debug : null,

		bundles : null,

		listeners : null,

		serviceHooks : null,

		bundleHooks : null,

		resolverHooks : null,

		weavingHooks : null,

		resolver : null,

		services : null,

		perm : null,

		systemBundle : null,

		storage : null,

		validator : null,

		dataStorage : null,

		startLevelController : null,

		urlStreamHandlerFactory : null,

		contentHandlerFactory : null,

		bootDelegationPatterns : null,

		bootDelegationUsed : false,

		parentClassLoader : null,

		firstInit : true,

		id : 0,

		initCount : 0,

		props : null,

		threadGroup : null,

		bundleThreads : null,

		packageAdmin : null,

		bsnversionMode : null,

		extCtxs : null,

		constructs : function(initProps) {
			this.extCtxs = [];
			this.id = this.vj$.FrameworkContext.globalId++;
			// this.threadGroup = new ThreadGroup("FW#" + this.id);
			this.props = new FrameworkProperties(initProps, this);
			this.perm = new PermissionOps();
			this.systemBundle = new SystemBundle(this);
			this.log("created");
		},

		getClassLoader : function(clazz) {
			if (clazz !== null) {
				var pos = clazz.lastIndexOf('.');
				if (pos !== -1) {
					var p = this.resolver.getPkg(clazz.substring(0, pos));
					if (p !== null) {
						var ep = p.getBestProvider();
						if (ep !== null) {
							return ep.bpkgs.bg.getClassLoader();
						}
					}
				}
			}
			return systemBundle.getClassLoader();
		},

		init : function() {
			this.log("initializing");
			this.initCount++;
			if (this.firstInit && ObjectUtil.equals(Constants.FRAMEWORK_STORAGE_CLEAN_ONFIRSTINIT, this.props.getProperty(Constants.FRAMEWORK_STORAGE_CLEAN))) {
				this.deleteFWDir();
				this.firstInit = false;
			}
			this.buildBootDelegationPatterns();
//			this.selectBootDelegationParentClassLoader();
//			if (this.setSecurityManager()) {
//			this.perm =
//			this.doNew(this.vj$.FrameworkContext.SECURE_PERMISSON_OPS);
//			bundles.framework.SystemBundle.secure = this.perm;
//			}
//			this.perm.init();

//			var v = this.props.getProperty(this.vj$.FrameworkProperties.VALIDATOR_PROP);
//			if (!StringUtil.equalsIgnoreCase(v, "none") && !StringUtil.equalsIgnoreCase(v, "null")) {
//			this.validator = new this.vj$.ArrayList();
//			for (var start = 0; start < v.length;) {
//			var end = v.indexOf(',', start);
//			if (end === -1) {
//			end = v.length;
//			}
//			var vs = "org.bundles.framework.validator." + StringUtil.trim(v.substring(start, end));
//			this.validator.add(/* >> */this.doNew(vs));
//			start = end + 1;
//			}
//			}
//			if (null === this.urlStreamHandlerFactory) {
//			if (this.props.REGISTERSERVICEURLHANDLER) {
//			if (this.vj$.FrameworkContext.systemUrlStreamHandlerFactory !== null) {
//			this.urlStreamHandlerFactory = this.vj$.FrameworkContext.systemUrlStreamHandlerFactory;
//			this.contentHandlerFactory = this.vj$.FrameworkContext.systemContentHandlerFactory;
//			}
//			else {
//			this.urlStreamHandlerFactory = new this.vj$.ServiceURLStreamHandlerFactory(this);
//			this.contentHandlerFactory = new this.vj$.ServiceContentHandlerFactory(this);
//			try {
//			this.vj$.URL.setURLStreamHandlerFactory(this.urlStreamHandlerFactory);
//			this.vj$.URLConnection.setContentHandlerFactory(this.contentHandlerFactory);
//			this.vj$.FrameworkContext.systemContentHandlerFactory = this.contentHandlerFactory;
//			this.vj$.FrameworkContext.systemUrlStreamHandlerFactory = this.urlStreamHandlerFactory;
//			}
//			catch (e) {
//			bundles.framework.Debug.printStackTrace("Cannot set global URL handlers, " + "continuing without OSGi service URL handler (" + e + ")", e);
//			}
//			}
//			}
//			else {
//			this.urlStreamHandlerFactory = new this.vj$.ServiceURLStreamHandlerFactory(this);
//			this.contentHandlerFactory = new this.vj$.ServiceContentHandlerFactory(this);
//			}
//			}
//			this.props.props.set(Constants.FRAMEWORK_UUID, this.getUUID());
//			var bsnProp = StringUtil.trim(this.props.getProperty(Constants.FRAMEWORK_BSNVERSION)).toLowerCase();
//			if (ObjectUtil.equals(bsnProp, Constants.FRAMEWORK_BSNVERSION_MANAGED) || ObjectUtil.equals(bsnProp, Constants.FRAMEWORK_BSNVERSION_MULTIPLE) || ObjectUtil.equals(bsnProp, Constants.FRAMEWORK_BSNVERSION_SINGLE)) {
//				this.bsnversionMode = bsnProp;
//			}
//			else {
//				this.bsnversionMode = Constants.FRAMEWORK_BSNVERSION_MANAGED;
//				bundles.framework.Debug.println("Unknown property value: " + Constants.FRAMEWORK_BSNVERSION + " = " + bsnProp);
//			}
//			var storageClass = "org.bundles.framework.bundlestorage." + this.props.getProperty(this.vj$.FrameworkProperties.BUNDLESTORAGE_PROP) + ".BundleStorageImpl";
//			try {
//				var storageImpl = vjo.Class.forName(storageClass);
//				var cons = storageImpl.getConstructor([ this.vj$.FrameworkContext.clazz ]);
//				this.storage = cons.newInstance([ this ]);
//			}
//			catch (e) {
//				var cause = e;
//				if (java.lang.reflect.InvocationTargetException.isInstance(e)) {
//					cause = e.getTargetException();
//				}
//				throw new this.vj$.RuntimeException("Failed to initialize storage " + storageClass, cause);
//			}
//			this.dataStorage = this.vj$.Util.getFileStorage(this, "data");
//			this.vj$.BundleThread.checkWarnStopActionNotSupported(this);
			this.bundleThreads = [];
			this.resolver = new this.vj$.Resolver(this);
			this.vj$.Listeners = new this.vj$.Listeners(this, this.perm);
			this.services = new this.vj$.Services(this, this.perm);
			this.urlStreamHandlerFactory.addFramework(this);
			this.serviceHooks = new this.vj$.ServiceHooks(this);
			this.bundleHooks = new this.vj$.BundleHooks(this);
			this.resolverHooks = new this.vj$.ResolverHooks(this);
			this.weavingHooks = new this.vj$.WeavingHooks(this);
			bundles.framework.SystemBundle.initSystemBundle();
			this.vj$.Bundles = new this.vj$.Bundles(this);
			this.serviceHooks.open();
			this.resolverHooks.open();
			this.weavingHooks.open();
			this.perm.registerService();
			this.packageAdmin = new this.vj$.PackageAdminImpl(this);
			var classes = [ osgi.service.packageadmin.PackageAdmin.clazz.getName() ];
			this.services.register(bundles.framework.SystemBundle, classes, this.packageAdmin, null);
			this.registerStartLevel();
			this.vj$.Bundles.load();
			this.log("inited");
			this.log("Installed bundles:");
			var allBAs = this.storage.getAllBundleArchives();
			for (var ba, _$i0 = 0; _$i0 < allBAs.length; _$i0++) {
				ba = allBAs[_$i0];
				var b = this.vj$.Bundles.getBundle(ba.getBundleLocation());
				this.log(" #" + b.getBundleId() + " " + b.getSymbolicName() + ":" + b.getVersion() + " location:" + b.getLocation());
			}
		},

		doNew : function(clazz) {
			try {
				var n = vjo.Class.forName(clazz);
				var nc = n.getConstructor([ this.vj$.FrameworkContext.clazz ]);
				return nc.newInstance([ this ]);
			}
			catch (e) {
				if (e instanceof this.vj$.InvocationTargetException) {
					var ite = e;
					throw new this.vj$.RuntimeException(clazz + ", constructor failed with, " + ite.getTargetException(), ite);
				}
				else if (e instanceof this.vj$.NoSuchMethodException) {
					throw new this.vj$.RuntimeException(clazz + ", found no such class", e);
				}
				else if (e instanceof this.vj$.NoClassDefFoundError) {
					var ncdfe = e;
					throw new this.vj$.RuntimeException(clazz + ", class not supported by JVM", ncdfe);
				}
				else if (e instanceof this.vj$.Exception) {
					throw new this.vj$.RuntimeException(clazz + ", constructor failed", e);
				}
			}
		},

		uninit : function() {
			this.log("uninit");
			this.startLevelController = null;
			bundles.framework.SystemBundle.uninitSystemBundle();
			this.resolverHooks = null;
			if (this.props.REGISTERSERVICEURLHANDLER) {
				this.urlStreamHandlerFactory.removeFramework(this);
			}
			else {
				this.urlStreamHandlerFactory = null;
				this.contentHandlerFactory = null;
			}
			this.vj$.Bundles.clear();
			this.vj$.Bundles = null;
			this.services.clear();
			this.services = null;
			this.vj$.Listeners.clear();
			this.vj$.Listeners = null;
			this.resolver.clear();
			this.resolver = null;
			{
				while (!this.bundleThreads.isEmpty()) {
					this.bundleThreads.remove(0).quit();
				}
			}
			this.bundleThreads = null;
			this.dataStorage = null;
			this.storage.close();
			this.storage = null;
			this.perm = new this.vj$.PermissionOps();
			{
				if (--this.vj$.FrameworkContext.smUse === 0) {
					this.vj$.System.setSecurityManager(null);
				}
			}
			this.parentClassLoader = null;
			this.bootDelegationPatterns = null;
		},

		setSecurityManager : function() {
			{
				var current = this.vj$.System.getSecurityManager();
				var osgiSecurity = this.props.getProperty(Constants.FRAMEWORK_SECURITY);
				if (osgiSecurity.length > 0) {
					if (!ObjectUtil.equals(Constants.FRAMEWORK_SECURITY_OSGI, osgiSecurity)) {
						throw new SecurityException("Unknown OSGi security, " + osgiSecurity);
					}
					if (current === null) {
						var POLICY_PROPERTY = "java.security.policy";
						var defaultPolicy = this.getClass().getResource("/framework.policy").toString();
						var policy = this.vj$.System.getProperty(POLICY_PROPERTY, defaultPolicy);
						if (this.framework) {
							bundles.framework.Debug.println("Installing OSGi security manager, policy=" + policy);
						}
						this.vj$.System.setProperty(POLICY_PROPERTY, policy);
						java.security.Policy.getPolicy().refresh();
						current = this.doNew(this.vj$.FrameworkContext.KF_SECURITY_MANAGER);
						this.vj$.System.setSecurityManager(current);
						this.vj$.FrameworkContext.smUse = 1;
					}
					else {
						var cpsmc;
						try {
							cpsmc = vjo.Class.forName(this.vj$.FrameworkContext.CONDITIONAL_PERMISSION_SECURITY_MANAGER);
						}
						catch (e) {
							throw new this.vj$.RuntimeException("Missing class", e);
						}
						if (cpsmc.isInstance(current)) {
							if (this.vj$.FrameworkContext.smUse === 0) {
								this.vj$.FrameworkContext.smUse = 2;
							}
							else {
								this.vj$.FrameworkContext.smUse++;
							}
						}
						else {
							throw new SecurityException("Incompatible security manager installed");
						}
					}
				}
				return current !== null;
			}
		},

		deleteFWDir : function() {
			var d = this.vj$.Util.getFrameworkDir(this);
			var dir = (d !== null) ? new this.vj$.FileTree(d) : null;
			if (dir !== null) {
				if (dir.exists()) {
					this.log("deleting old framework directory.");
					var bOK = dir.delete_();
					if (!bOK) {
						bundles.framework.Debug.println("Failed to remove existing fwdir " + dir.getAbsolutePath());
					}
				}
			}
		},

		getUUID : function() {
			// TODO use real UUID
			return "4e524769-3136-4b46-8000-00000000";
		},

		registerStartLevel : function() {
			if (this.props.getBooleanProperty(this.vj$.FrameworkProperties.STARTLEVEL_USE_PROP)) {
				if (this.startlevel) {
					bundles.framework.Debug.println("[using startlevel service]");
				}
				this.startLevelController = new this.vj$.StartLevelController(this);
				this.startLevelController.restoreState();
				var clsName = [ osgi.service.startlevel.StartLevel.clazz.getName() ];
				this.services.register(bundles.framework.SystemBundle, clsName, this.startLevelController, null);
			}
		},

		getDataStorage : function(id) {
			if (this.dataStorage !== null) {
				return new this.vj$.FileTree(this.dataStorage, this.vj$.Long.toString(id));
			}
			return null;
		},

		checkOurBundle : function(b) {
			if (b === null || !(bundles.framework.BundleImpl.isInstance(b)) || this !== b.fwCtx) {
				throw new this.vj$.IllegalArgumentException("Bundle does not belong to this framework: " + b);
			}
		},

		buildBootDelegationPatterns : function() {
			var bootDelegationString = this.props.getProperty(Constants.FRAMEWORK_BOOTDELEGATION);
			this.bootDelegationUsed = bootDelegationString.length > 0;
			this.bootDelegationPatterns = null; // delegate all '*'
			/*
			 * if (this.bootDelegationUsed) { try { for (var he, _$itr =
			 * Util.parseManifestHeader(Constants.FRAMEWORK_BOOTDELEGATION,
			 * bootDelegationString, true, true, false).iterator();
			 * _$itr.hasNext();) { he = _$itr.next(); var key = he.getKey(); if
			 * (ObjectUtil.equals(key, "*")) { this.bootDelegationPatterns =
			 * null; break; } else if
			 * (StringUtil.endsWith(key, ".*")) {
			 * this.bootDelegationPatterns.add(key.substring(0, key.length -
			 * 1)); } else if
			 * (StringUtil.endsWith(key, ".")) {
			 * frameworkError(bundles.framework.SystemBundle, new
			 * this.vj$.IllegalArgumentException(Constants.FRAMEWORK_BOOTDELEGATION + "
			 * entry ends with '.': " + key)); } else if (key.indexOf("*") !==
			 * -1) { frameworkError(bundles.framework.SystemBundle, new
			 * this.vj$.IllegalArgumentException(Constants.FRAMEWORK_BOOTDELEGATION + "
			 * entry contains a '*': " + key)); } else {
			 * this.bootDelegationPatterns.add(key); } } } catch (e) {
			 * bundles.framework.Debug.printStackTrace("Failed to parse " +
			 * Constants.FRAMEWORK_BOOTDELEGATION, e); } }
			 */
		},

		isBootDelegatedResource : function(name) {
			var pos = name.lastIndexOf('/');
			return pos !== -1 ? this.isBootDelegated(StringUtil.replace(name.substring(0, pos), '/', '.') + ".X") : false;
		},

		isBootDelegated : function(className) {
			if (!this.bootDelegationUsed) {
				return false;
			}
			if (this.bootDelegationPatterns === null) {
				return true;
			}
			var pos = className.lastIndexOf('.');
			if (pos !== -1) {
				var classPackage = className.substring(0, pos);
				for (var ps, _$itr = this.bootDelegationPatterns.iterator(); _$itr.hasNext();) {
					ps = _$itr.next();
					if ((StringUtil.endsWith(ps, ".") && StringUtil.regionMatches(classPackage, 0, ps, 0, ps.length - 1)) || ObjectUtil.equals(classPackage, ps)) {
						return true;
					}
				}
			}
			return false;
		},

		selectBootDelegationParentClassLoader : function() {
			var s = this.props.getProperty(Constants.FRAMEWORK_BUNDLE_PARENT);
			if (ObjectUtil.equals(Constants.FRAMEWORK_BUNDLE_PARENT_EXT, s)) {
				this.parentClassLoader = this.vj$.ClassLoader.getSystemClassLoader();
				if (this.parentClassLoader !== null) {
					this.parentClassLoader = this.parentClassLoader.getParent();
				}
			}
			else if (ObjectUtil.equals(Constants.FRAMEWORK_BUNDLE_PARENT_APP, s)) {
				this.parentClassLoader = this.vj$.ClassLoader.getSystemClassLoader();
			}
			else if (ObjectUtil.equals(Constants.FRAMEWORK_BUNDLE_PARENT_FRAMEWORK, s)) {
				this.parentClassLoader = this.getClass().getClassLoader();
			}
			else {
				this.parentClassLoader = vjo.Object.clazz.getClassLoader();
			}
			if (this.parentClassLoader === null) {
				this.parentClassLoader = new this.vj$.FrameworkContext.BCLoader();
			}
		},

		activateExtension : function(extension) {
			this.extCtxs.add(new this.vj$.ExtensionContext(this, extension));
		},

		bundleClassLoaderCreated : function(bcl) {
			for (var extCtx, _$itr = this.extCtxs.iterator(); _$itr.hasNext();) {
				extCtx = _$itr.next();
				extCtx.bundleClassLoaderCreated(bcl);
			}
		},

		bundleClassLoaderClosed : function(bcl) {
			for (var extCtx, _$itr = this.extCtxs.iterator(); _$itr.hasNext();) {
				extCtx = _$itr.next();
				extCtx.bundleClassLoaderClosed(bcl);
			}
		},

		frameworkError : function(b, t) {
			var oneTimeListeners;
			if (arguments.length == 3 && arguments[2] instanceof Array) {
				oneTimeListeners = arguments[2];
			}
			else {
				oneTimeListeners = [];
				for (var i = 2; i < arguments.length; i++) {
					oneTimeListeners[i - 2] = arguments[i];
				}
			}
			if (arguments.length === 3) {
				if (osgi.framework.Bundle.clazz.isInstance(arguments[0]) && arguments[1] instanceof vjo.java.lang.Throwable && osgi.framework.FrameworkListener.clazz.isInstance(arguments[2])) {
					this.frameworkError_3_0_FrameworkContext_ovld(arguments[0], arguments[1], arguments[2]);
				}
				else if (arguments[0] instanceof bundles.framework.BundleContextImpl && arguments[1] instanceof vjo.java.lang.Throwable && osgi.framework.FrameworkListener.clazz.isInstance(arguments[2])) {
					this.frameworkError_3_1_FrameworkContext_ovld(arguments[0], arguments[1], arguments[2]);
				}
			}
		},

		frameworkError_3_0_FrameworkContext_ovld : function(b, t) {
			var oneTimeListeners;
			if (arguments.length == 3 && arguments[2] instanceof Array) {
				oneTimeListeners = arguments[2];
			}
			else {
				oneTimeListeners = [];
				for (var i = 2; i < arguments.length; i++) {
					oneTimeListeners[i - 2] = arguments[i];
				}
			}
			this.vj$.Listeners.frameworkEvent(new this.vj$.FrameworkEvent(this.vj$.FrameworkEvent.ERROR, b, t), oneTimeListeners);
		},

		frameworkError_3_1_FrameworkContext_ovld : function(bc, t) {
			var oneTimeListeners;
			if (arguments.length == 3 && arguments[2] instanceof Array) {
				oneTimeListeners = arguments[2];
			}
			else {
				oneTimeListeners = [];
				for (var i = 2; i < arguments.length; i++) {
					oneTimeListeners[i - 2] = arguments[i];
				}
			}
			this.vj$.Listeners.frameworkEvent(new this.vj$.FrameworkEvent(this.vj$.FrameworkEvent.ERROR, bc.bundle, t), oneTimeListeners);
		},

		frameworkInfo : function(b, t) {
			var oneTimeListeners;
			if (arguments.length == 3 && arguments[2] instanceof Array) {
				oneTimeListeners = arguments[2];
			}
			else {
				oneTimeListeners = [];
				for (var i = 2; i < arguments.length; i++) {
					oneTimeListeners[i - 2] = arguments[i];
				}
			}
			this.vj$.Listeners.frameworkEvent(new this.vj$.FrameworkEvent(this.vj$.FrameworkEvent.INFO, b, t), oneTimeListeners);
		},

		frameworkWarning : function(b, t) {
			var oneTimeListeners;
			if (arguments.length == 3 && arguments[2] instanceof Array) {
				oneTimeListeners = arguments[2];
			}
			else {
				oneTimeListeners = [];
				for (var i = 2; i < arguments.length; i++) {
					oneTimeListeners[i - 2] = arguments[i];
				}
			}
			if (arguments.length === 3) {
				if (osgi.framework.Bundle.clazz.isInstance(arguments[0]) && arguments[1] instanceof vjo.java.lang.Throwable && osgi.framework.FrameworkListener.clazz.isInstance(arguments[2])) {
					this.frameworkWarning_3_0_FrameworkContext_ovld(arguments[0], arguments[1], arguments[2]);
				}
				else if (arguments[0] instanceof bundles.framework.BundleGeneration && arguments[1] instanceof vjo.java.lang.Throwable && osgi.framework.FrameworkListener.clazz.isInstance(arguments[2])) {
					this.frameworkWarning_3_1_FrameworkContext_ovld(arguments[0], arguments[1], arguments[2]);
				}
			}
		},

		frameworkWarning_3_0_FrameworkContext_ovld : function(b, t) {
			var oneTimeListeners;
			if (arguments.length == 3 && arguments[2] instanceof Array) {
				oneTimeListeners = arguments[2];
			}
			else {
				oneTimeListeners = [];
				for (var i = 2; i < arguments.length; i++) {
					oneTimeListeners[i - 2] = arguments[i];
				}
			}
			this.vj$.Listeners.frameworkEvent(new this.vj$.FrameworkEvent(this.vj$.FrameworkEvent.WARNING, b, t), oneTimeListeners);
		},

		frameworkWarning_3_1_FrameworkContext_ovld : function(bg, t) {
			var oneTimeListeners;
			if (arguments.length == 3 && arguments[2] instanceof Array) {
				oneTimeListeners = arguments[2];
			}
			else {
				oneTimeListeners = [];
				for (var i = 2; i < arguments.length; i++) {
					oneTimeListeners[i - 2] = arguments[i];
				}
			}
			frameworkWarning(bg.bundle, t, oneTimeListeners);
		},

		log : function(msg) {
			if (arguments.length === 1) {
				this.log_1_0_FrameworkContext_ovld(arguments[0]);
			}
			else if (arguments.length === 2) {
				this.log_2_0_FrameworkContext_ovld(arguments[0], arguments[1]);
			}
		},

		log_1_0_FrameworkContext_ovld : function(msg) {
			if (this.framework) {
				bundles.framework.Debug.println("Framework instance " + org.eclipse.vjet.ObjectUtil.hashCode(this) + ": " + msg);
			}
		},

		log_2_0_FrameworkContext_ovld : function(msg, t) {
			if (this.framework) {
				bundles.framework.Debug.printStackTrace("Framework instance " + org.eclipse.vjet.ObjectUtil.hashCode(this) + ": " + msg, t);
			}
		}

	})

	.inits(function() {
		this.vj$.FrameworkContext.globalFwLock = Object();
	})

	.endType();
})