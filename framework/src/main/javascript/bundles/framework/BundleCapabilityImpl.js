/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleCapabilityImpl') 
.needs(['org.eclipse.vjet.vjo.java.util.Map','org.eclipse.vjet.vjo.java.util.Vector',
    'org.eclipse.vjet.vjo.java.util.Arrays','org.eclipse.vjet.vjo.java.lang.IllegalArgumentException',
    'org.eclipse.vjet.vjo.java.util.Collections','org.eclipse.vjet.vjo.java.util.HashMap',
    'org.eclipse.vjet.vjo.java.lang.StringBuffer','org.eclipse.vjet.vjo.java.util.List',
    'org.eclipse.vjet.vjo.java.util.Set','bundles.framework.BundleGeneration',
    'bundles.framework.BundleWireImpl','bundles.framework.Util',
    'osgi.framework.wiring.BundleRevision','osgi.framework.namespace.IdentityNamespace',
    'osgi.framework.Constants','bundles.framework.BundleImpl',
    'vjo.java.lang.ObjectUtil'])
.satisfies('osgi.framework.wiring.BundleCapability')
.protos({
    gen:null, 
    owner:null, 
    nameSpace:null, 
    attributes:null, 
    directives:null, 
    wires:null, 
    
    
    
    
    constructs:function(){
        this.wires=new this.vj$.Vector(2);
        if(arguments.length===2){
            if(arguments[0] instanceof bundles.framework.BundleGeneration && arguments[1] instanceof bundles.framework.Util.HeaderEntry){
                this.constructs_2_0_BundleCapabilityImpl_ovld(arguments[0],arguments[1]);
            }else if(osgi.framework.wiring.BundleCapability.clazz.isInstance(arguments[0]) && arguments[1] instanceof bundles.framework.BundleGeneration){
                this.constructs_2_1_BundleCapabilityImpl_ovld(arguments[0],arguments[1]);
            }
        }else if(arguments.length===1){
            this.constructs_1_0_BundleCapabilityImpl_ovld(arguments[0]);
        }
    },
    
    constructs_2_0_BundleCapabilityImpl_ovld:function(gen,he){
        this.gen=gen;
        this.owner=gen;
        this.nameSpace=he.getKey();
        for (var ns,_$itr=this.vj$.Arrays.asList([this.vj$.BundleRevision.BUNDLE_NAMESPACE,this.vj$.BundleRevision.HOST_NAMESPACE,this.vj$.BundleRevision.PACKAGE_NAMESPACE,this.vj$.IdentityNamespace.IDENTITY_NAMESPACE]).iterator();_$itr.hasNext();){
            ns=_$itr.next();
            if(vjo.java.lang.ObjectUtil.equals(ns,this.nameSpace)){
                throw new this.vj$.IllegalArgumentException("Capability with name-space '"+ns+"' must not be provided in the "+this.vj$.Constants.PROVIDE_CAPABILITY+" manifest header.");
            }
        }
        this.attributes=this.vj$.Collections.unmodifiableMap(he.getAttributes());
        this.directives=this.vj$.Collections.unmodifiableMap(he.getDirectives());
    },
    
    constructs_2_1_BundleCapabilityImpl_ovld:function(bc,bg){
        this.gen=bg;
        this.owner=bc.owner;
        this.nameSpace=bc.getNamespace();
        this.attributes=bc.getAttributes();
        this.directives=bc.getDirectives();
    },
    
    constructs_1_0_BundleCapabilityImpl_ovld:function(bg){
        this.gen=bg;
        this.owner=this.gen;
        this.nameSpace=this.vj$.IdentityNamespace.IDENTITY_NAMESPACE;
        var attrs=new this.vj$.HashMap(); 
        attrs.put(this.vj$.IdentityNamespace.IDENTITY_NAMESPACE,this.gen.symbolicName);
        attrs.put(this.vj$.IdentityNamespace.CAPABILITY_TYPE_ATTRIBUTE,this.gen.fragment!==null?this.vj$.IdentityNamespace.TYPE_FRAGMENT:this.vj$.IdentityNamespace.TYPE_BUNDLE);
        attrs.put(this.vj$.IdentityNamespace.CAPABILITY_VERSION_ATTRIBUTE,this.gen.version);
        if(this.gen.archive!==null){
            var a=this.gen.archive.getAttribute(this.vj$.Constants.BUNDLE_COPYRIGHT); 
            if(a!==null){
                attrs.put(this.vj$.IdentityNamespace.CAPABILITY_COPYRIGHT_ATTRIBUTE,a);
            }
            a=this.gen.archive.getAttribute(this.vj$.Constants.BUNDLE_DESCRIPTION);
            if(a!==null){
                attrs.put(this.vj$.IdentityNamespace.CAPABILITY_DESCRIPTION_ATTRIBUTE,a);
            }
            a=this.gen.archive.getAttribute(this.vj$.Constants.BUNDLE_DOCURL);
            if(a!==null){
                attrs.put(this.vj$.IdentityNamespace.CAPABILITY_DOCUMENTATION_ATTRIBUTE,a);
            }
            a=this.gen.archive.getAttribute("Bundle-License");
            if(a!==null){
                var sb=new this.vj$.StringBuffer(); 
                try {
                    var lic=this.vj$.Util.parseManifestHeader("Bundle-License",a,true,true,false); 
                    for (var he,_$itr=lic.iterator();_$itr.hasNext();){
                        he=_$itr.next();
                        if(sb.length()>0){
                            sb.append(", ");
                        }
                        sb.append(he.getKey());
                    }
                }
                catch(iae){
                    this.gen.bundle.fwCtx.frameworkInfo(this.gen.bundle,iae);
                    sb.append(a);
                }
                attrs.put(this.vj$.IdentityNamespace.CAPABILITY_LICENSE_ATTRIBUTE,sb.toString());
            }
        }
        this.attributes=this.vj$.Collections.unmodifiableMap(attrs);
        var dirs=new this.vj$.HashMap(); 
        dirs.put(this.vj$.Constants.SINGLETON_DIRECTIVE,this.gen.singleton?"true":"false");
        this.directives=this.vj$.Collections.unmodifiableMap(dirs);
    },
    
    getNamespace:function(){
        return this.nameSpace;
    },
    
    getDirectives:function(){
        return this.vj$.Collections.unmodifiableMap(this.directives);
    },
    
    getAttributes:function(){
        return this.attributes;
    },
    
    getRevision:function(){
        return this.owner.bundleRevision;
    },
    
    getResource:function(){
        return this.owner.bundleRevision;
    },
    
    toString:function(){
        return "BundleCapability[nameSpace="+this.nameSpace+", attributes="+this.attributes+", directives="+this.directives+", revision="+this.getRevision()+"]";
    },
    
    getBundleGeneration:function(){
        return this.gen;
    },
    
    isEffectiveResolve:function(){
        var effective=this.directives.get(this.vj$.Constants.EFFECTIVE_DIRECTIVE); 
        return effective===null||vjo.java.lang.ObjectUtil.equals(effective,this.vj$.Constants.EFFECTIVE_RESOLVE);
    },
    
    isZombie:function(){
        return !this.gen.isCurrent();
    },
    
    addWire:function(bw){
        this.wires.add(bw);
    },
    
    getWires:function(res){
        {
            res.addAll(this.wires);
        }
    },
    
    removeWire:function(wire){
        this.wires.remove(wire);
    },
    
    removeWires:function(){
        this.wires.clear();
    },
    
    isWired:function(){
        return !this.wires.isEmpty();
    },
    
    checkPermission:function(){
        return this.gen.bundle.fwCtx.perm.hasProvidePermission(this);
    },
    
    getUses:function(){
        try {
            return this.vj$.Util.parseEnumeration(this.vj$.Constants.USES_DIRECTIVE,this.directives.get(this.vj$.Constants.USES_DIRECTIVE));
        }
        catch(iae){
            var b=this.gen.bundle; 
            b.fwCtx.frameworkError(b,iae);
        }
        return null;
    }
})
.endType();