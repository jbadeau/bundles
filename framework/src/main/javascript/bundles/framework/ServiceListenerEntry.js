/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.ServiceListenerEntry') 
.needs(['org.eclipse.vjet.vjo.java.util.List','osgi.framework.hooks.service.ListenerHook',
    'bundles.framework.LDAPExpr','bundles.framework.BundleContextImpl',
    'java.util.EventListener','osgi.framework.UnfilteredServiceListener',
    'osgi.framework.BundleContext'])
.inherits('bundles.framework.ListenerEntry')
.satisfies('osgi.framework.hooks.service.ListenerHook.ListenerInfo')
.protos({
    ldap:null, 
    noFiltering:false, 
    local_cache:null, 
    
    
    
    constructs:function(){
        if(arguments.length===3){
            this.constructs_3_0_ServiceListenerEntry_ovld(arguments[0],arguments[1],arguments[2]);
        }else if(arguments.length===2){
            this.constructs_2_0_ServiceListenerEntry_ovld(arguments[0],arguments[1]);
        }
    },
    
    constructs_3_0_ServiceListenerEntry_ovld:function(bc,l,filter){
        this.base(bc,l);
        if(filter!==null){
            this.ldap=new this.vj$.LDAPExpr(filter);
            this.noFiltering=osgi.framework.UnfilteredServiceListener.isInstance(l);
        }else {
            this.ldap=null;
            this.noFiltering=true;
        }
    },
    
    constructs_2_0_ServiceListenerEntry_ovld:function(bc,l){
        this.base(bc,l);
        this.ldap=null;
        this.noFiltering=true;
    },
    
    getBundleContext:function(){
        return this.bc;
    },
    
    getFilter:function(){
        return this.ldap!==null?this.ldap.toString():null;
    }
})
.endType();