/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.Main') 
.needs(['org.eclipse.vjet.vjo.java.util.Map','org.eclipse.vjet.vjo.java.util.HashMap',
    'org.eclipse.vjet.vjo.java.lang.System','org.eclipse.vjet.vjo.java.lang.Integer',
    'org.eclipse.vjet.vjo.java.lang.Exception','org.eclipse.vjet.vjo.java.lang.IllegalArgumentException',
    'org.eclipse.vjet.vjo.java.lang.RuntimeException','org.eclipse.vjet.vjo.java.io.PrintWriter',
    'org.eclipse.vjet.vjo.java.lang.Long','vjo.java.lang.Throwable',
    'org.eclipse.vjet.vjo.java.lang.NumberFormatException','org.eclipse.vjet.vjo.java.io.InputStream',
    'org.eclipse.vjet.vjo.java.util.List','org.eclipse.vjet.vjo.java.util.ArrayList',
    'org.eclipse.vjet.vjo.java.util.Properties','org.eclipse.vjet.vjo.java.util.Enumeration',
    'org.eclipse.vjet.vjo.java.lang.StringBuffer','osgi.framework.launch.FrameworkFactory',
    'osgi.framework.launch.Framework','bundles.framework.Util',
    'bundles.framework.FrameworkFactoryImpl','java.lang.reflect.Constructor',
    'java.net.URL','osgi.framework.Constants',
    'java.io.File','java.io.BufferedWriter',
    'java.io.FileWriter','osgi.framework.BundleException',
    'osgi.framework.Bundle','osgi.framework.FrameworkEvent',
    'osgi.framework.startlevel.FrameworkStartLevel','java.net.HttpURLConnection',
    'bundles.framework.Alias','java.io.BufferedReader',
    'java.io.FileReader','java.io.InputStreamReader',
    'java.net.MalformedURLException','java.io.IOException',
    'java.lang.reflect.Method','vjo.java.lang.ObjectUtil',
    'org.eclipse.vjet.vjo.java.lang.StringUtil'])
.needs('org.eclipse.vjet.vjo.java.lang.StringFactory','')
.props({
    main:null, 
    JARDIR_PROP:"org.bundles.gosg.jars", 
    JARDIR_DEFAULT:"file:", 
    XARGS_INIT:"init.xargs", 
    XARGS_RESTART:"restart.xargs", 
    FWPROPS_XARGS:"fwprops.xargs", 
    CMDIR_PROP:"org.bundles.bundle.cm.store", 
    CMDIR_DEFAULT:"cmdir", 
    VERBOSITY_PROP:"org.bundles.framework.main.verbosity", 
    VERBOSITY_DEFAULT:"0", 
    WRITE_FWPROPS_XARGS_PROP:"org.bundles.framework.main.write.fwprops.xargs", 
    XARGS_WRITESYSPROPS_PROP:"org.bundles.framework.main.xargs.writesysprops", 
    XARGS_DEFAULT:"default", 
    PRODVERSION_PROP:"org.bundles.prodver", 
    BOOT_TEXT_PROP:"org.bundles.framework.main.bootText", 
    
    main:function(args){
        this.main=new this();
        this.main.start(args);
        this.vj$.System.exit(0);
    },
    
    readRelease:function(){
        return this.vj$.Util.readResource("/release","0.0.0.snapshot","UTF-8");
    }
})
.protos({
    verbosity:0, 
    version:"<unknown>", 
    topDir:"", 
    bZeroArgs:false, 
    saveStartLevel:true, 
    bootText:null, 
    fwProps:null, 
    sysProps:null, 
    defaultFwProps:null, 
    ff:null, 
    framework:null, 
    
    constructs:function(){
        this.fwProps=new this.vj$.HashMap();
        this.sysProps=new this.vj$.HashMap();
        this.defaultFwProps=
            vjo.make(this,this.vj$.HashMap)
            .protos({
                constructs:function(){
                    {
                        this.put(this.vj$.Main.CMDIR_PROP,this.vj$.Main.CMDIR_DEFAULT);
                    }
                }
            })
            .endType();
        try {
            var vpv=this.vj$.System.getProperty(this.vj$.Main.VERBOSITY_PROP); 
            this.verbosity=this.vj$.Integer.parseInt(null===vpv?this.vj$.Main.VERBOSITY_DEFAULT:vpv);
        }
        catch(ignored){
        }
        this.populateSysProps();
    },
    
    start:function(args){
        this.version=this.vj$.Util.readFrameworkVersion();
        var tstampYear=this.vj$.Util.readTstampYear(); 
        this.bootText="Bundles OSGi framework launcher, version "+this.version+"\n"+"Copyright 2003-"+tstampYear+" Bundles. All Rights Reserved.\n"+"See http://www.bundles.org for more information.\n";
        this.vj$.System.out.println(this.bootText);
        this.bZeroArgs=(args.length===0);
        if(!this.bZeroArgs){
            this.bZeroArgs=true;
            for (var i=0;this.bZeroArgs && i<args.length;i++){
                if(vjo.java.lang.ObjectUtil.equals("-ff",args[i])){
                    if(null!==this.framework){
                        throw new this.vj$.IllegalArgumentException("a framework instance is already created.");
                    }
                    if(i+1<args.length){
                        i++;
                        this.ff=this.getFrameworkFactory(args[i]);
                    }else {
                        throw new this.vj$.IllegalArgumentException("No framework factory argument");
                    }
                }else {
                    this.bZeroArgs=org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(args[i],"-D")||org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(args[i],"-F")||vjo.java.lang.ObjectUtil.equals("-init",args[i]);
                }
            }
        }
        this.processProperties(args);
        if(this.bZeroArgs){
            var xargsPath=this.getDefaultXArgs(); 
            if(xargsPath!==null){
                if(0===args.length){
                    args=["-xargs",xargsPath];
                }else {
                    var newArgs=vjo.createArray(null, args.length+2); 
                    newArgs[0]="-xargs";
                    newArgs[1]=xargsPath;
                    this.vj$.System.arraycopy(args,0,newArgs,2,args.length);
                    args=newArgs;
                }
            }
        }
        args=this.expandArgs(args);
        this.handleArgs(args);
    },
    
    writeSysProps:function(){
        var val=this.fwProps.get(this.vj$.Main.XARGS_WRITESYSPROPS_PROP);
        if(val===null){
            val=this.sysProps.get(this.vj$.Main.XARGS_WRITESYSPROPS_PROP);
        }
        this.println("writeSysProps? '"+val+"'",2);
        return vjo.java.lang.ObjectUtil.equals("true",val);
    },
    
    
    getFrameworkFactory:function(){
        if(arguments.length===0){
            return this.getFrameworkFactory_0_0_Main_ovld();
        }else if(arguments.length===1){
            return this.getFrameworkFactory_1_0_Main_ovld(arguments[0]);
        }
    },
    
    getFrameworkFactory_0_0_Main_ovld:function(){
        var factoryClassName=this.vj$.FrameworkFactoryImpl.clazz.getName(); 
        try {
            var factoryS=org.eclipse.vjet.vjo.java.lang.StringFactory.build(this.vj$.Util.readResource("/META-INF/services/org.osgi.framework.launch.FrameworkFactory"),"UTF-8"); 
            var w=this.vj$.Util.splitwords(factoryS,"\n\r"); 
            for (var i=0;i<w.length;i++){
                if(w[i].length>0&& !org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(w[i],"#")){
                    factoryClassName=org.eclipse.vjet.vjo.java.lang.StringUtil.trim(w[i]);
                    break;
                }
            }
        }
        catch(e){
            this.println("failed to get FrameworkFactory, using default",6,e);
        }
        return this.getFrameworkFactory(factoryClassName);
    },
    
    getFrameworkFactory_1_0_Main_ovld:function(factoryClassName){
        try {
            this.println("getFrameworkFactory("+factoryClassName+")",2);
            var clazz=vjo.Class.forName(factoryClassName); 
            var cons=clazz.getConstructor([]); 
            var ff=cons.newInstance([]); 
            return ff;
        }
        catch(e){
            error("failed to create "+factoryClassName,e);
            throw new this.vj$.RuntimeException("failed to create "+factoryClassName+": "+e);
        }
    },
    
    getJarBase:function(){
        this.assertFramework();
        var jars=this.framework.getBundleContext().getProperty(this.vj$.Main.JARDIR_PROP); 
        if(jars===null){
            jars=this.vj$.Main.JARDIR_DEFAULT;
        }
        var base=this.vj$.Util.splitwords(jars,";"); 
        for (var i=0;i<base.length;i++){
            try {
                base[i]=(new this.vj$.URL(base[i])).toString();
            }
            catch(ignored){
            }
            this.println("jar base["+i+"]="+base[i],3);
        }
        return base;
    },
    
    setProperty:function(prefix,arg,props){
        var ix=arg.indexOf("="); 
        if(ix!== -1){
            var key=arg.substring(2,ix); 
            var value=arg.substring(ix+1); 
            this.println(prefix+key+"="+value,1);
            props.put(key,value);
            if(vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.FRAMEWORK_BEGINNING_STARTLEVEL,key)){
                this.saveStartLevel=false;
            }
            if(vjo.java.lang.ObjectUtil.equals(this.vj$.Main.VERBOSITY_PROP,key)){
                try {
                    var old=this.verbosity; 
                    this.verbosity=this.vj$.Integer.parseInt(value.length===0?this.vj$.Main.VERBOSITY_DEFAULT:value);
                    if(old!==this.verbosity){
                        this.println("Verbosity changed to "+this.verbosity,1);
                    }
                }
                catch(ignored){
                }
            }
        }
    },
    
    processProperties:function(args){
        for (var i=0;i<args.length;i++){
            try {
                if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(args[i],"-D")){
                    this.setProperty("-D",args[i],this.sysProps);
                }else if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(args[i],"-F")){
                    this.setProperty("-F",args[i],this.fwProps);
                }else if(vjo.java.lang.ObjectUtil.equals("-init",args[i])){
                    this.fwProps.put(this.vj$.Constants.FRAMEWORK_STORAGE_CLEAN,this.vj$.Constants.FRAMEWORK_STORAGE_CLEAN_ONFIRSTINIT);
                }else if(vjo.java.lang.ObjectUtil.equals("-launch",args[i])){
                    this.saveStartLevel=false;
                }else if(this.saveStartLevel&&i+1<args.length&&vjo.java.lang.ObjectUtil.equals("-startlevel",args[i])){
                    this.fwProps.put(this.vj$.Constants.FRAMEWORK_BEGINNING_STARTLEVEL,args[i+1]);
                }
            }
            catch(e){
                e.printStackTrace(this.vj$.System.err);
                this.error("Command \""+args[i]+"\" failed, "+e.getMessage());
            }
        }
    },
    
    save_restart_props:function(props){
        var xrwp=this.framework.getBundleContext().getProperty(this.vj$.Main.WRITE_FWPROPS_XARGS_PROP); 
        if(org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase("false",xrwp)){
            return;
        }
        var pr=null; 
        try {
            var fwDirStr=this.vj$.Util.getFrameworkDir(props); 
            var fwDir=new this.vj$.File(fwDirStr); 
            fwDir.mkdirs();
            var propsFile=new this.vj$.File(fwDir,this.vj$.Main.FWPROPS_XARGS); 
            pr=new this.vj$.PrintWriter(new this.vj$.BufferedWriter(new this.vj$.FileWriter(propsFile)));
            for (var entry,_$itr=props.entrySet().iterator();_$itr.hasNext();){
                entry=_$itr.next();
                var key=entry.getKey(); 
                var value=entry.getValue(); 
                if(!vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.FRAMEWORK_STORAGE_CLEAN,key)|| !vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.FRAMEWORK_STORAGE_CLEAN_ONFIRSTINIT,value)){
                    pr.println("-F"+key+"="+value);
                }
            }
        }
        catch(e){
            if(vjo.getType('org.eclipse.vjet.vjo.java.lang.RuntimeException').isInstance(e)){
                throw e;
            }
            throw new this.vj$.IllegalArgumentException("Failed to create "+this.vj$.Main.FWPROPS_XARGS+": "+e);
        }
        finally {
            if(null!==pr){
                pr.close();
            }
        }
    },
    
    assertFramework:function(){
        if(this.ff===null){
            this.ff=this.getFrameworkFactory();
            this.println("Created FrameworkFactory "+this.ff.getClass().getName(),1);
        }
        if(this.framework===null){
            this.finalizeProperties();
            this.framework=this.ff.newFramework(this.fwProps);
            try {
                this.framework.init();
                this.save_restart_props(this.fwProps);
                this.fwProps.put(this.vj$.Main.BOOT_TEXT_PROP,this.bootText);
            }
            catch(be){
                error("Failed to initialize the framework: "+be.getMessage(),be);
            }
            this.println("Framework class "+this.framework.getClass().getName(),1);
            this.vj$.System.out.println("Created Framework: "+this.framework.getSymbolicName()+", version="+this.framework.getVersion()+".");
        }
    },
    
    handleArgs:function(args){
        var bLaunched=false; 
        if(!vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.FRAMEWORK_STORAGE_CLEAN_ONFIRSTINIT,this.fwProps.get(this.vj$.Constants.FRAMEWORK_STORAGE_CLEAN))){
            var propsFile=new this.vj$.File(this.vj$.Util.getFrameworkDir(this.fwProps),this.vj$.Main.FWPROPS_XARGS); 
            if(propsFile.exists()){
                var newArgs=vjo.createArray(null, args.length+2); 
                newArgs[0]="-xargs";
                newArgs[1]=propsFile.getAbsolutePath();
                this.vj$.System.arraycopy(args,0,newArgs,2,args.length);
                args=this.expandArgs(newArgs);
            }
        }
        this.processProperties(args);
        if(this.verbosity>5){
            for (var i=0;i<args.length;i++){
                this.println("argv["+i+"]="+args[i],5);
            }
        }
        for (var i=0;i<args.length;i++){
            try {
                if(vjo.java.lang.ObjectUtil.equals("-exit",args[i])){
                    this.println("Exit.",0);
                    this.vj$.System.exit(0);
                }else if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(args[i],"-F")){
                }else if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(args[i],"-D")){
                }else if(vjo.java.lang.ObjectUtil.equals("-init",args[i])){
                }else if(vjo.java.lang.ObjectUtil.equals("-version",args[i])){
                    this.vj$.System.out.println("Bundles release: "+this.vj$.Main.readRelease());
                    this.vj$.System.out.println("Framework version: "+this.vj$.Util.readFrameworkVersion());
                    this.printResource("/tstamp");
                    this.vj$.System.exit(0);
                }else if(vjo.java.lang.ObjectUtil.equals("-help",args[i])){
                    this.printResource("/help.txt");
                    this.vj$.System.exit(0);
                }else if(vjo.java.lang.ObjectUtil.equals("-jvminfo",args[i])){
                    this.assertFramework();
                    this.printJVMInfo(this.framework);
                    this.vj$.System.exit(0);
                }else if(vjo.java.lang.ObjectUtil.equals("-ff",args[i])){
                    i++;
                }else if(vjo.java.lang.ObjectUtil.equals("-create",args[i])){
                    if(null!==this.framework&&(this.vj$.Bundle.RESOLVED&this.framework.getState())===0){
                        throw new this.vj$.IllegalArgumentException("a framework instance is already created."+" The '-create' command must either be the first command"+" or come directly after a '-shutdown mSEC' command.");
                    }
                    this.framework=null;
                }else if(vjo.java.lang.ObjectUtil.equals("-install",args[i])){
                    this.assertFramework();
                    if(i+1<args.length){
                        var bundle=args[i+1]; 
                        var b=this.framework.getBundleContext().installBundle(this.completeLocation(bundle),null); 
                        this.println("Installed: ",b);
                        i++;
                    }else {
                        this.error("No URL for install command");
                    }
                }else if(vjo.java.lang.ObjectUtil.equals("-istart",args[i])){
                    this.assertFramework();
                    if(i+1<args.length){
                        var bundle=args[i+1]; 
                        var b=this.framework.getBundleContext().installBundle(this.completeLocation(bundle),null); 
                        b.start(this.vj$.Bundle.START_ACTIVATION_POLICY);
                        this.println("Installed and started (policy): ",b);
                        i++;
                    }else {
                        this.error("No URL for install command");
                    }
                }else if(vjo.java.lang.ObjectUtil.equals("-launch",args[i])){
                    bLaunched=this.doLaunch();
                }else if(vjo.java.lang.ObjectUtil.equals("-shutdown",args[i])){
                    if(i+1<args.length){
                        i++;
                        var timeout=this.vj$.Long.parseLong(args[i]); 
                        try {
                            if(this.framework!==null){
                                this.framework.stop();
                                var stopEvent=this.framework.waitForStop(timeout); 
                                switch(stopEvent.getType()){
                                    case this.vj$.FrameworkEvent.STOPPED:
                                        this.println("Framework terminated",0);
                                        break;
                                    case this.vj$.FrameworkEvent.STOPPED_UPDATE:
                                        this.println("Framework stopped for update",0);
                                        break;
                                    case this.vj$.FrameworkEvent.STOPPED_BOOTCLASSPATH_MODIFIED:
                                        this.println("Framework stopped for bootclasspath update",0);
                                        break;
                                    case this.vj$.FrameworkEvent.ERROR:
                                        this.error("Fatal framework error, terminating.",stopEvent.getThrowable());
                                        break;
                                    case this.vj$.FrameworkEvent.WAIT_TIMEDOUT:
                                        this.error("Framework waitForStop("+timeout+") timed out!",stopEvent.getThrowable());
                                        break;
                                }
                                this.println("Framework shutdown",0);
                            }else {
                                throw new this.vj$.IllegalArgumentException("No framework to shutdown");
                            }
                        }
                        catch(e){
                            error("Failed to shutdown",e);
                        }
                    }else {
                        this.error("No timout for shutdown command");
                    }
                }else if(vjo.java.lang.ObjectUtil.equals("-sleep",args[i])){
                    if(i+1<args.length){
                        var t=this.vj$.Long.parseLong(args[i+1]); 
                        try {
                            this.println("Sleeping "+t+" seconds...",0);
                            this.vj$.Thread.sleep(t*1000);
                        }
                        catch(e){
                            this.error("Sleep interrupted.");
                        }
                        i++;
                    }else {
                        this.error("No time for sleep command");
                    }
                }else if(vjo.java.lang.ObjectUtil.equals("-start",args[i])){
                    i=this.doStartBundle(args,i,this.vj$.Bundle.START_ACTIVATION_POLICY,"Started (policy): ");
                }else if(vjo.java.lang.ObjectUtil.equals("-start_e",args[i])){
                    i=this.doStartBundle(args,i,0,"Started (eager): ");
                }else if(vjo.java.lang.ObjectUtil.equals("-start_et",args[i])){
                    i=this.doStartBundle(args,i,this.vj$.Bundle.START_TRANSIENT,"Started (eager,transient): ");
                }else if(vjo.java.lang.ObjectUtil.equals("-start_pt",args[i])){
                    i=this.doStartBundle(args,i,this.vj$.Bundle.START_TRANSIENT|this.vj$.Bundle.START_ACTIVATION_POLICY,"Start (policy,transient): ");
                }else if(vjo.java.lang.ObjectUtil.equals("-stop",args[i])){
                    i=this.doStopBundle(args,i,0);
                }else if(vjo.java.lang.ObjectUtil.equals("-stop_t",args[i])){
                    i=this.doStopBundle(args,i,this.vj$.Bundle.STOP_TRANSIENT);
                }else if(vjo.java.lang.ObjectUtil.equals("-uninstall",args[i])){
                    this.assertFramework();
                    if(i+1<args.length){
                        var id=this.getBundleID(this.framework,args[i+1]); 
                        var b=this.framework.getBundleContext().getBundle(id); 
                        b.uninstall();
                        this.println("Uninstalled: ",b);
                        i++;
                    }else {
                        this.error("No id for uninstall command");
                    }
                }else if(vjo.java.lang.ObjectUtil.equals("-update",args[i])){
                    this.assertFramework();
                    if(i+1<args.length){
                        var bl=null; 
                        if(vjo.java.lang.ObjectUtil.equals("ALL",args[i+1])){
                            bl=this.framework.getBundleContext().getBundles();
                        }else {
                            bl=[this.framework.getBundleContext().getBundle(this.getBundleID(this.framework,args[i+1]))];
                        }
                        for (var n=0;bl!==null && n<bl.length;n++){
                            var b=bl[n]; 
                            b.update();
                            this.println("Updated: ",b);
                        }
                        i++;
                    }else {
                        this.error("No id for update command");
                    }
                }else if(vjo.java.lang.ObjectUtil.equals("-initlevel",args[i])){
                    this.assertFramework();
                    if(i+1<args.length){
                        var n=this.vj$.Integer.parseInt(args[i+1]); 
                        var fsl=this.framework.adapt(this.vj$.FrameworkStartLevel.clazz); 
                        if(fsl!==null){
                            fsl.setInitialBundleStartLevel(n);
                        }
                        i++;
                    }else {
                        this.error("No integer level for initlevel command");
                    }
                }else if(vjo.java.lang.ObjectUtil.equals("-startlevel",args[i])){
                    this.assertFramework();
                    if(i+1<args.length){
                        var n=this.vj$.Integer.parseInt(args[i+1]); 
                        var fsl=this.framework.adapt(this.vj$.FrameworkStartLevel.clazz); 
                        if(fsl!==null){
                            fsl.setStartLevel(n);
                        }
                        i++;
                    }else {
                        this.error("No integer level for startlevel command");
                    }
                }else {
                    this.error("Unknown option: "+args[i]+"\nUse option -help to see all options");
                }
            }
            catch(e){
                if(e instanceof this.vj$.BundleException){
                    var ne=e.getNestedException(); 
                    if(ne!==null){
                        e.getNestedException().printStackTrace(this.vj$.System.err);
                    }else {
                        e.printStackTrace(this.vj$.System.err);
                    }
                    this.error("Command \""+args[i]+((i+1<args.length && !org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(args[i+1],"-"))?" "+args[i+1]:"")+"\" failed, "+e.getMessage());
                }else if(e instanceof this.vj$.Exception){
                    e.printStackTrace(this.vj$.System.err);
                    this.error("Command \""+args[i]+((i+1<args.length && !org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(args[i+1],"-"))?" "+args[i+1]:"")+"\" failed, "+e.getMessage());
                }
            }
        }
        this.assertFramework();
        if(!bLaunched){
            try {
                bLaunched=this.doLaunch();
            }
            catch(t){
                if(osgi.framework.BundleException.isInstance(t)){
                    var be=t; 
                    var ne=be.getNestedException(); 
                    if(ne!==null){
                        t=ne;
                    }
                }
                this.error("Framework launch failed, "+t.getMessage(),t);
            }
        }
        var stopEvent=null; 
        while(true){
            try {
                stopEvent=this.framework.waitForStop(0);
                switch(stopEvent.getType()){
                    case this.vj$.FrameworkEvent.STOPPED:
                        this.println("Framework terminated",0);
                        return;
                    case this.vj$.FrameworkEvent.STOPPED_UPDATE:
                        break;
                    case this.vj$.FrameworkEvent.STOPPED_BOOTCLASSPATH_MODIFIED:
                        return;
                    case this.vj$.FrameworkEvent.ERROR:
                        this.error("Fatal framework error, terminating.",stopEvent.getThrowable());
                        return;
                    case this.vj$.FrameworkEvent.WAIT_TIMEDOUT:
                        this.error("Framework waitForStop(0) timed out!",stopEvent.getThrowable());
                        break;
                }
            }
            catch(ie){
            }
        }
    },
    
    doStopBundle:function(args,i,options){
        this.assertFramework();
        if(i+1<args.length){
            var id=this.getBundleID(this.framework,args[i+1]); 
            var b=this.framework.getBundleContext().getBundle(id); 
            try {
                b.stop(options);
                this.println("Stopped: ",b);
            }
            catch(e){
                error("Failed to stop",e);
            }
            i++;
        }else {
            this.error("No ID for stop command");
        }
        return i;
    },
    
    doLaunch:function(){
        var bLaunched; 
        if(null!==this.framework&&(this.vj$.Bundle.ACTIVE&this.framework.getState())!==0){
            throw new this.vj$.IllegalArgumentException("a framework instance is already active.");
        }
        this.assertFramework();
        this.framework.start();
        bLaunched=true;
        this.closeSplash();
        this.println("Framework launched",0);
        return bLaunched;
    },
    
    doStartBundle:function(args,i,options,logMsg){
        this.assertFramework();
        if(i+1<args.length){
            var id=this.getBundleID(this.framework,args[i+1]); 
            var b=this.framework.getBundleContext().getBundle(id); 
            b.start(options);
            this.println(logMsg,b);
            i++;
        }else {
            this.error("No ID for start command");
        }
        return i;
    },
    
    getBundleID:function(fw,idLocation){
        try {
            return this.vj$.Long.parseLong(idLocation);
        }
        catch(nfe){
            var bl=fw.getBundleContext().getBundles(); 
            var loc=this.completeLocation(idLocation); 
            for (var i=0;bl!==null && i<bl.length;i++){
                if(vjo.java.lang.ObjectUtil.equals(loc,bl[i].getLocation())){
                    return bl[i].getBundleId();
                }
            }
            throw new this.vj$.IllegalArgumentException("Invalid bundle id/location: "+idLocation);
        }
    },
    
    completeLocation:function(location){
        var base=this.getJarBase(); 
        if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(location,"file:jars/")&& !vjo.java.lang.ObjectUtil.equals(this.topDir,"")){
            location=org.eclipse.vjet.vjo.java.lang.StringUtil.replace(("file:"+this.topDir+"/"+location.substring(5)),'\\','/');
            this.println("mangled bundle location to "+location,2);
        }
        var ic=location.indexOf(":"); 
        if(ic<2||ic>location.indexOf("/")){
            this.println("location="+location,2);
            for (var i=0;i<base.length;i++){
                this.println("base["+i+"]="+base[i],2);
                try {
                    var url=new this.vj$.URL(new this.vj$.URL(base[i]),location); 
                    this.println("check "+url,2);
                    if(vjo.java.lang.ObjectUtil.equals("file",url.getProtocol())){
                        var f=(new this.vj$.File(url.getFile())).getAbsoluteFile(); 
                        if(!f.exists()|| !f.canRead()){
                            continue;
                        }
                    }else if(vjo.java.lang.ObjectUtil.equals("http",url.getProtocol())){
                        var uc=url.openConnection(); 
                        uc.connect();
                        var rc=uc.getResponseCode(); 
                        uc.disconnect();
                        if(rc!==this.vj$.HttpURLConnection.HTTP_OK){
                            this.println("Can't access HTTP bundle: "+url+", response code="+rc,0);
                            continue;
                        }
                    }else {
                        var is=null; 
                        try {
                            is=url.openStream();
                        }
                        finally {
                            if(is!==null){
                                is.close();
                            }
                        }
                    }
                    location=url.toString();
                    this.println("found location="+location,5);
                    break;
                }
                catch(_e){
                }
            }
        }
        return location;
    },
    
    expandArgs:function(argv){
        var v=new this.vj$.ArrayList(); 
        var i=0; 
        while(i<argv.length){
            if(vjo.java.lang.ObjectUtil.equals("-xargs",argv[i])||vjo.java.lang.ObjectUtil.equals("--xargs",argv[i])){
                var bIgnoreException=vjo.java.lang.ObjectUtil.equals(argv[i],"--xargs"); 
                if(i+1<argv.length){
                    var xargsPath=argv[i+1]; 
                    i++;
                    try {
                        var moreArgs=this.loadArgs(xargsPath,argv); 
                        var r=this.expandArgs(moreArgs); 
                        for (var element,_$i0=0;_$i0<r.length;_$i0++){
                            element=r[_$i0];
                            v.add(element);
                        }
                    }
                    catch(e){
                        if(bIgnoreException){
                            this.println("Failed to load -xargs "+xargsPath,1,e);
                        }else {
                            throw e;
                        }
                    }
                }else {
                    throw new this.vj$.IllegalArgumentException("-xargs without argument");
                }
            }else {
                v.add(argv[i]);
            }
            i++;
        }
        var r=vjo.createArray(null, v.size()); 
        v.toArray(r);
        return r;
    },
    
    printResource:function(name){
        try {
            this.vj$.System.out.println(org.eclipse.vjet.vjo.java.lang.StringFactory.build(this.vj$.Util.readResource(name)));
        }
        catch(e){
            this.vj$.System.out.println("No resource '"+name+"' available");
        }
    },
    
    printJVMInfo:function(framework){
        try {
            var props=this.vj$.System.getProperties(); 
            this.vj$.System.out.println("--- System properties ---");
            for (var e=props.keys();e.hasMoreElements();){
                var key=e.nextElement(); 
                this.vj$.System.out.println(key+": "+props.get(key));
            }
            this.vj$.System.out.println("\n");
            this.vj$.System.out.println("--- Framework properties ---");
            var keyStr=framework.getBundleContext().getProperty("org.bundles.framework.bundleprops.keys"); 
            var keys=this.vj$.Util.splitwords(keyStr!==null?keyStr:"",","); 
            for (var key,_$i1=0;_$i1<keys.length;_$i1++){
                key=keys[_$i1];
                this.vj$.System.out.println(key+": "+framework.getBundleContext().getProperty(key));
            }
        }
        catch(e){
            e.printStackTrace();
        }
    },
    
    getDefaultXArgs:function(){
        var bInit=vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.FRAMEWORK_STORAGE_CLEAN_ONFIRSTINIT,this.fwProps.get(this.vj$.Constants.FRAMEWORK_STORAGE_CLEAN)); 
        var fwDirStr=this.vj$.Util.getFrameworkDir(this.fwProps); 
        var fwDir=new this.vj$.File((new this.vj$.File(fwDirStr)).getAbsolutePath()); 
        this.println("fwdir is "+fwDir,1);
        if(!bInit){
            bInit=!(fwDir.exists()&&fwDir.isDirectory());
            if(bInit){
                this.println("Implicit -init since fwdir does not exist.",2);
            }
        }
        this.println("init is "+bInit,2);
        var defDirStr=fwDir.getParent(); 
        var defDir=defDirStr!==null?new this.vj$.File(defDirStr):null; 
        var cwDir=new this.vj$.File((new this.vj$.File("")).getAbsolutePath()); 
        this.println("defDir="+defDir,5);
        this.println("cwDir="+cwDir,5);
        var dirs=null!==defDir?[fwDir,defDir,cwDir]:[fwDir,cwDir]; 
        for (var dir,_$i2=0;_$i2<dirs.length;_$i2++){
            dir=dirs[_$i2];
            var jarsDir=new this.vj$.File(dir,"jars"); 
            if(jarsDir.exists()&&jarsDir.isDirectory()){
                this.topDir=dir.getAbsolutePath();
                break;
            }
        }
        this.println("Bundles root directory is "+this.topDir,2);
        var osName=this.vj$.Alias.unifyOsName(this.vj$.System.getProperty("os.name")); 
        var xargNames=bInit?["init_"+osName+".xargs",this.vj$.Main.XARGS_INIT,"remote-"+this.vj$.Main.XARGS_INIT]:[this.vj$.Main.XARGS_RESTART]; 
        var xargsFound=null; 
        this.println("Searching for default xargs file:",5);
        xargsSearch:
        for (var i=0;i<dirs.length;i++){
            for (var k=0;k<xargNames.length;k++){
                var xargsFile=new this.vj$.File(dirs[i],xargNames[k]); 
                this.println("  trying "+xargsFile.getAbsolutePath(),5);
                if(xargsFile.exists()){
                    xargsFound=xargsFile.getAbsolutePath();
                    break xargsSearch;
                }
            }
        }
        this.println("default xargs file is "+xargsFound,2);
        return xargsFound;
    },
    
    addDefaultProps:function(){
        for (var entry,_$itr=this.defaultFwProps.entrySet().iterator();_$itr.hasNext();){
            entry=_$itr.next();
            var key=entry.getKey(); 
            if(!this.fwProps.containsKey(key)){
                var val=entry.getValue(); 
                this.println("Using default "+key+"="+val,1);
                this.fwProps.put(key,val);
            }else {
                this.println("framework prop "+key+"="+this.fwProps.get(key),1);
            }
        }
        if(null===this.fwProps.get(this.vj$.Main.PRODVERSION_PROP)){
            this.fwProps.put(this.vj$.Main.PRODVERSION_PROP,this.version);
        }
        var jars=this.fwProps.get(this.vj$.Main.JARDIR_PROP); 
        if(jars===null){
            jars=this.sysProps.get(this.vj$.Main.JARDIR_PROP);
        }
        if(!(jars===null||vjo.java.lang.ObjectUtil.equals("",jars))){
            this.println("old jars="+jars,1);
        }else {
            var jarBaseDir=this.topDir+this.vj$.File.separator+"jars"; 
            this.println("jarBaseDir="+jarBaseDir,2);
            var jarDir=new this.vj$.File((new this.vj$.File(jarBaseDir)).getAbsolutePath()); 
            if(jarDir.exists()&&jarDir.isDirectory()){
                var names=jarDir.list(); 
                var v=new this.vj$.ArrayList(); 
                for (var name,_$i3=0;_$i3<names.length;_$i3++){
                    name=names[_$i3];
                    var f=new this.vj$.File(jarDir,name); 
                    if(f.isDirectory()){
                        v.add(name);
                    }
                }
                var subdirs=vjo.createArray(null, v.size()); 
                v.toArray(subdirs);
                var sb=new this.vj$.StringBuffer(); 
                sb.append("file:"+jarBaseDir+"/");
                for (var subdir,_$i4=0;_$i4<subdirs.length;_$i4++){
                    subdir=subdirs[_$i4];
                    sb.append(";file:"+jarBaseDir+"/"+subdir+"/");
                }
                jars=org.eclipse.vjet.vjo.java.lang.StringUtil.replace(sb.toString(),'\\','/');
                this.fwProps.put(this.vj$.Main.JARDIR_PROP,jars);
                this.println("scanned "+this.vj$.Main.JARDIR_PROP+"="+jars,2);
            }
        }
    },
    
    populateSysProps:function(){
        var systemProperties=this.vj$.System.getProperties(); 
        var systemPropertiesNames=systemProperties.propertyNames(); 
        while(systemPropertiesNames.hasMoreElements()){
            try {
                var name=systemPropertiesNames.nextElement(); 
                var value=systemProperties.getProperty(name); 
                this.sysProps.put(name,value);
                if(vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.FRAMEWORK_BEGINNING_STARTLEVEL,name)){
                    this.saveStartLevel=false;
                }
                this.println("Initial system property: "+name+"="+value,3);
            }
            catch(e){
                this.println("Failed to process system property: "+e,1,e);
            }
        }
    },
    
    mergeSystemProperties:function(props){
        var p=this.vj$.System.getProperties(); 
        p.putAll(props);
        this.vj$.System.setProperties(p);
    },
    
    finalizeProperties:function(){
        this.expandPropValues(this.sysProps,null);
        this.expandPropValues(this.fwProps,this.sysProps);
        this.addDefaultProps();
        this.mergeSystemProperties(this.sysProps);
        if(this.writeSysProps()){
            this.mergeSystemProperties(this.fwProps);
            this.println("merged Framework to System properties "+this.fwProps,2);
        }
    },
    
    expandPropValues:function(toExpand,fallback){
        var all=null; 
        for (var entry,_$itr=toExpand.entrySet().iterator();_$itr.hasNext();){
            entry=_$itr.next();
            var value=entry.getValue(); 
            if(-1!==value.indexOf("${")){
                if(null===all){
                    all=new this.vj$.HashMap();
                    if(null!==fallback){
                        all.putAll(fallback);
                    }
                    all.putAll(toExpand);
                }
                for (var allEntry,_$itr=all.entrySet().iterator();_$itr.hasNext();){
                    allEntry=_$itr.next();
                    var rk="${"+allEntry.getKey()+"}"; 
                    var rv=allEntry.getValue(); 
                    value=this.vj$.Util.replace(value,rk,rv);
                }
                entry.setValue(value);
                this.println("Expanded property: "+entry.getKey()+"="+value,1);
            }
        }
    },
    
    addArg:function(args,arg){
        if(0===args.size()){
            args.add(arg);
        }else {
            var lastArg=args.get(args.size()-1); 
            if(vjo.java.lang.ObjectUtil.equals("-xargs",lastArg)||vjo.java.lang.ObjectUtil.equals("--xargs",lastArg)){
                var exArgs=this.expandArgs([lastArg,arg]); 
                args.remove(args.size()-1);
                for (var exArg,_$i5=0;_$i5<exArgs.length;_$i5++){
                    exArg=exArgs[_$i5];
                    args.add(exArg);
                }
            }else {
                args.add(arg);
            }
        }
    },
    
    loadArgs:function(xargsPath,oldArgs){
        if(vjo.java.lang.ObjectUtil.equals(this.vj$.Main.XARGS_DEFAULT,xargsPath)){
            this.processProperties(oldArgs);
            xargsPath=this.getDefaultXArgs();
        }
        var v=new this.vj$.ArrayList(); 
        var in_=null; 
        try {
            this.println("Searching for xargs file with '"+xargsPath+"'.",2);
            var fwDirStr=this.vj$.Util.getFrameworkDir(this.fwProps); 
            var fwDir=new this.vj$.File((new this.vj$.File(fwDirStr)).getAbsolutePath()); 
            var defDirStr=fwDir.getParent(); 
            var defDir=defDirStr!==null?new this.vj$.File(defDirStr):null; 
            if(null!==defDir){
                var f=new this.vj$.File((new this.vj$.File(defDir,xargsPath)).getAbsolutePath()); 
                this.println(" trying "+f,5);
                if(f.exists()){
                    this.println("Loading xargs file "+f,1);
                    in_=new this.vj$.BufferedReader(new this.vj$.FileReader(f));
                }
            }
            if(null===in_){
                var f=new this.vj$.File((new this.vj$.File(xargsPath)).getAbsolutePath()); 
                this.println(" trying "+f,5);
                if(f.exists()){
                    this.println("Loading xargs file "+f,1);
                    in_=new this.vj$.BufferedReader(new this.vj$.FileReader(f));
                }
            }
            if(in_===null){
                try {
                    this.println(" trying URL "+xargsPath,5);
                    var url=new this.vj$.URL(xargsPath); 
                    this.println("Loading xargs url "+url,0);
                    in_=new this.vj$.BufferedReader(new this.vj$.InputStreamReader(url.openStream()));
                }
                catch(e){
                    throw new this.vj$.IllegalArgumentException("Bad xargs URL "+xargsPath+": "+e);
                }
            }
            var contLine=new this.vj$.StringBuffer(); 
            var line=null; 
            var tmpline=null; 
            for (tmpline=in_.readLine();tmpline!==null;tmpline=in_.readLine()){
                tmpline=org.eclipse.vjet.vjo.java.lang.StringUtil.trim(tmpline);
                if(org.eclipse.vjet.vjo.java.lang.StringUtil.endsWith(tmpline,"\\")){
                    tmpline=tmpline.substring(0,tmpline.length-1);
                    if(contLine===null){
                        contLine=new this.vj$.StringBuffer(tmpline);
                    }else {
                        contLine.append(tmpline);
                    }
                    continue;
                }else {
                    if(contLine!==null){
                        contLine.append(tmpline);
                        line=contLine.toString();
                        contLine=null;
                    }else {
                        line=tmpline;
                    }
                }
                if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(line,"-D")){
                    this.addArg(v,line);
                }else if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(line,"-F")){
                    this.addArg(v,line);
                }else if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(line,"#")){
                }else if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(line,"-")){
                    var i=line.indexOf(' '); 
                    if(i!== -1){
                        this.addArg(v,line.substring(0,i));
                        line=org.eclipse.vjet.vjo.java.lang.StringUtil.trim(line.substring(i));
                        if(line.length>0){
                            this.addArg(v,line);
                        }
                    }else {
                        this.addArg(v,line);
                    }
                }else if(line.length>0){
                    this.addArg(v,line);
                }
            }
        }
        catch(e){
            if(vjo.getType('org.eclipse.vjet.vjo.java.lang.RuntimeException').isInstance(e)){
                throw e;
            }
            throw new this.vj$.IllegalArgumentException("xargs loading failed: "+e);
        }
        finally {
            if(null!==in_){
                try {
                    in_.close();
                }
                catch(ignore){
                }
            }
        }
        var args2=vjo.createArray(null, v.size()); 
        v.toArray(args2);
        return args2;
    },
    
    closeSplash:function(){
        try {
            var splashScreenCls=vjo.Class.forName("java.awt.SplashScreen"); 
            var getSplashScreenMethod=splashScreenCls.getMethod("getSplashScreen",/*>>*/null); 
            var splashScreen=getSplashScreenMethod.invoke(/*>>*/null,/*>>*/null);
            if(null!==splashScreen){
                var closeMethod=splashScreenCls.getMethod("close",/*>>*/null); 
                closeMethod.invoke(splashScreen,/*>>*/null);
            }
        }
        catch(e){
            this.println("close splash screen: ",6,e);
        }
    },
    
    
    
    println:function(s,level){
        if(arguments.length===2){
            if((arguments[0] instanceof String || typeof arguments[0]=="string") && typeof arguments[1]=="number"){
                this.println_2_0_Main_ovld(arguments[0],arguments[1]);
            }else if((arguments[0] instanceof String || typeof arguments[0]=="string") && osgi.framework.Bundle.clazz.isInstance(arguments[1])){
                this.println_2_1_Main_ovld(arguments[0],arguments[1]);
            }
        }else if(arguments.length===3){
            this.println_3_0_Main_ovld(arguments[0],arguments[1],arguments[2]);
        }
    },
    
    println_2_0_Main_ovld:function(s,level){
        this.println(s,level,null);
    },
    
    println_2_1_Main_ovld:function(s,b){
        this.println(s+b.getLocation()+" (id#"+b.getBundleId()+")",1);
    },
    
    println_3_0_Main_ovld:function(s,level,e){
        if(this.verbosity>=level){
            this.vj$.System.out.println((level>0?("#"+level+": "):"")+s);
            if(e!==null){
                e.printStackTrace();
            }
        }
    },
    
    
    error:function(s){
        if(arguments.length===1){
            this.error_1_0_Main_ovld(arguments[0]);
        }else if(arguments.length===2){
            this.error_2_0_Main_ovld(arguments[0],arguments[1]);
        }
    },
    
    error_1_0_Main_ovld:function(s){
        this.error(s,null);
    },
    
    error_2_0_Main_ovld:function(s,t){
        this.vj$.System.err.println("Error: "+s);
        if(t!==null){
            t.printStackTrace();
        }
        this.vj$.System.exit(1);
    }
})
.endType();