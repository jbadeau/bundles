/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.ServiceReferenceImpl<S>') 
.needs(['org.eclipse.vjet.vjo.java.lang.IllegalArgumentException','org.eclipse.vjet.vjo.java.lang.Integer',
    'vjo.java.lang.reflect.Array','org.eclipse.vjet.vjo.java.util.Vector',
    'org.eclipse.vjet.vjo.java.lang.IllegalStateException','org.eclipse.vjet.vjo.java.util.ArrayList',
    'osgi.framework.Constants','osgi.framework.ServiceFactory',
    'org.eclipse.vjet.vjo.java.lang.ObjectUtil','org.eclipse.vjet.vjo.java.lang.StringUtil'])

.satisfies('osgi.framework.ServiceReference<S>')
.protos({
    registration:null, 
    
    constructs:function(reg){
        this.registration=reg;
    },
    
    getProperty:function(key){
        return this.cloneObject(this.registration.getProperty(key));
    },
    
    getPropertyKeys:function(){
        return this.registration.getProperties().keyArray();
    },
    
    getBundle:function(){
        return this.registration.bundle;
    },
    
    equals:function(o){
        if(bundles.framework.ServiceReferenceImpl.isInstance(o)){
            var that=o; 
            return this.registration===that.registration;
        }
        return false;
    },
    
    compareTo:function(obj){
        var that=obj; 
        var sameFw=false; 
        if(bundles.framework.ServiceReferenceImpl.isInstance(that)){
            var thatImpl=that; 
            sameFw=this.registration.fwCtx===thatImpl.registration.fwCtx;
        }
        if(!sameFw){
            throw new this.vj$.IllegalArgumentException("Can not compare service references "+"belonging to different framework "+"instances (this="+this+", other="+that+").");
        }
        var ro1=this.getProperty(this.vj$.Constants.SERVICE_RANKING);
        var ro2=that.getProperty(this.vj$.Constants.SERVICE_RANKING);
        var r1=(ro1 instanceof this.vj$.Integer)?ro1.intValue():0; 
        var r2=(ro2 instanceof this.vj$.Integer)?ro2.intValue():0; 
        if(r1!==r2){
            return r1<r2?-1:1;
        }else {
            var id1=this.getProperty(this.vj$.Constants.SERVICE_ID); 
            var id2=that.getProperty(this.vj$.Constants.SERVICE_ID); 
            return id2.compareTo(id1);
        }
    },
    
    hashCode:function(){
        return org.eclipse.vjet.vjo.java.lang.ObjectUtil.hashCode(this.registration);
    },
    
    getUsingBundles:function(){
        return this.registration.getUsingBundles();
    },
    
    getService:function(bundle){
        bundle.fwCtx.perm.checkGetServicePerms(this);
        return this.registration.getService(bundle);
    },
    
    ungetService:function(bundle){
        return this.registration.ungetService(bundle,true);
    },
    
    getProperties:function(){
        return this.registration.getProperties();
    },
    
    cloneObject:function(val){
        if(isInstance(val)){
            val=val.clone();
            var len=this.vj$.Array.getLength(val); 
            if(len>0&&this.vj$.Array.get(val,0).getClass().isArray()){
                for (var i=0;i<len;i++){
                    this.vj$.Array.set(val,i,cloneObject(this.vj$.Array.get(val,i)));
                }
            }
        }else if(isInstance(val)){
            val=val.clone();
        }else if(isInstance(val)){
            val=val.clone();
        }else if(isInstance(val)){
            val=val.clone();
        }else if(isInstance(val)){
            val=val.clone();
        }else if(isInstance(val)){
            val=val.clone();
        }else if(isInstance(val)){
            val=val.clone();
        }else if(isInstance(val)){
            val=val.clone();
        }else if(isInstance(val)){
            val=val.clone();
        }else if(org.eclipse.vjet.vjo.java.util.Vector.isInstance(val)){
            var c=val.clone(); 
            for (var i=0;i<c.size();i++){
                c.setElementAt(cloneObject(c.elementAt(i)),i);
            }
            val=c;
        }
        return val;
    },
    
    isAssignableTo:function(bundle,className){
        var sBundle=this.registration.bundle; 
        if(sBundle===null){
            throw new this.vj$.IllegalStateException("Service is unregistered");
        }
        var fwCtx=sBundle.fwCtx; 
        if(bundle.fwCtx!==fwCtx){
            throw new this.vj$.IllegalArgumentException("Bundle is not from same framework as service");
        }
        if(fwCtx.isBootDelegated(className)){
            return true;
        }
        var pos=className.lastIndexOf('.'); 
        if(pos!== -1){
            var name=className.substring(0,pos); 
            var p=fwCtx.resolver.getPkg(name); 
            if(p!==null){
                var rbp=sBundle.current().bpkgs; 
                var pkgExporter=rbp.getProviderBundlePackages(name); 
                var pkgProvider; 
                if(pkgExporter===null){
                    pkgProvider=rbp.getRequiredBundleGenerations(name);
                }else {
                    pkgProvider=new this.vj$.ArrayList(1);
                    pkgProvider.add(pkgExporter.bg);
                }
                var bb=bundle.current().bpkgs; 
                var bbp=bb.getProviderBundlePackages(name); 
                var pkgConsumer; 
                if(bbp===null){
                    pkgConsumer=bb.getRequiredBundleGenerations(name);
                }else {
                    pkgConsumer=new this.vj$.ArrayList(1);
                    pkgConsumer.add(bbp.bg);
                }
                if(pkgConsumer===null){
                    if(bb.isExported(name)){
                        return pkgProvider!==null?pkgProvider.contains(bb.bg):true;
                    }else {
                        return true;
                    }
                }else if(pkgProvider===null){
                    var sService=this.registration.service;
                    if(sService===null){
                        throw new this.vj$.IllegalStateException("Service is unregistered");
                    }
                    if(p.providers.size()===1){
                        return true;
                    }else if(osgi.framework.ServiceFactory.isInstance(sService)){
                        return true;
                    }else {
                        var bCL=bb.getClassLoader(); 
                        if(bCL!==null){
                            try {
                                var bCls=bCL.loadClass(className); 
                                return bCls.isAssignableFrom(sService.getClass());
                            }
                            catch(e){
                                return true;
                            }
                        }
                    }
                }else {
                    for (var element,_$itr=pkgProvider.iterator();_$itr.hasNext();){
                        element=_$itr.next();
                        if(pkgConsumer.contains(element)){
                            return true;
                        }
                    }
                }
            }else {
                if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(name,"java.")||sBundle===bundle){
                    return true;
                }else {
                    return true;
                }
            }
        }
        return false;
    }
})
.endType();