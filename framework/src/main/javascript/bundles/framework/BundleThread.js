/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleThread') 
.needs(['org.eclipse.vjet.vjo.java.lang.Integer','org.eclipse.vjet.vjo.java.lang.Exception',
    'bundles.framework.FrameworkProperties','org.eclipse.vjet.vjo.java.lang.System',
    'osgi.framework.BundleException','bundles.framework.Util',
    'org.eclipse.vjet.vjo.java.lang.StringUtil'])
.needs('org.eclipse.vjet.vjo.java.lang.BooleanUtil','')

.inherits('Thread')
.props({
    OP_IDLE:0, 
    OP_BUNDLE_EVENT:1, 
    OP_START:2, 
    OP_STOP:3, 
    KEEP_ALIVE:1000, 
    ABORT_ACTION_STOP:"stop", 
    ABORT_ACTION_MINPRIO:"minprio", 
    ABORT_ACTION_IGNORE:"ignore", 
    stopMethod:null, 
    
    initStopSupported:function(){
        try {
            return this.vj$.Thread.clazz.getMethod("stop",/*>>*/null);
        }
        catch(_t){
            return null;
        }
    },
    
    checkWarnStopActionNotSupported:function(fc){
        var s=fc.props.getProperty(this.vj$.FrameworkProperties.BUNDLETHREAD_ABORT); 
        if(vjo.java.lang.ObjectUtil.equals(this.ABORT_ACTION_STOP,s)&&null===this.stopMethod){
            this.vj$.System.err.println("WARNING: Bundle thread abort action stop was "+"requested but is not supported on this execution "+"environment; using 'minprio' as abort action.");
        }
    }
})
.protos({
    fwCtx:null, 
    startStopTimeout:0, 
    lock:null, 
    be:null, 
    bundle:null, 
    operation:0, 
    res:null, 
    doRun:false, 
    
    constructs:function(fc){
        this.lock=Object();
        this.operation=this.vj$.BundleThread.OP_IDLE;
        this.base(fc.threadGroup,"BundleThread waiting");
        setDaemon(true);
        this.fwCtx=fc;
        this.doRun=true;
        var timeout=this.fwCtx.props.getProperty(this.vj$.FrameworkProperties.BUNDLETHREAD_TIMEOUT); 
        if(timeout!==null){
            try {
                this.startStopTimeout=1000*this.vj$.Integer.parseInt(timeout);
            }
            catch(nfe){
                this.fwCtx.debug.println("Property "+this.vj$.FrameworkProperties.BUNDLETHREAD_TIMEOUT+" has a non integer value, ignoring");
            }
        }
        start();
    },
    
    quit:function(){
        this.doRun=false;
        interrupt();
    },
    
    run:function(){
        while(this.doRun){
            {
                while(this.doRun && this.operation===this.vj$.BundleThread.OP_IDLE){
                    try {
                        this.lock.wait(this.vj$.BundleThread.KEEP_ALIVE);
                        if(this.operation!==this.vj$.BundleThread.OP_IDLE){
                            break;
                        }
                        {
                            if(this.fwCtx.bundleThreads.remove(this)){
                                return;
                            }
                        }
                    }
                    catch(ie){
                    }
                }
                if(!this.doRun){
                    break;
                }
                var tmpres=null;
                try {
                    switch(this.operation){
                        case this.vj$.BundleThread.OP_BUNDLE_EVENT:
                            setName("BundleChanged #"+this.be.getBundle().getBundleId());
                            this.fwCtx.listeners.bundleChanged(this.be);
                            break;
                        case this.vj$.BundleThread.OP_START:
                            setName("BundleStart #"+this.bundle.getBundleId());
                            tmpres=this.bundle.start0();
                            break;
                        case this.vj$.BundleThread.OP_STOP:
                            setName("BundleStop #"+this.bundle.getBundleId());
                            tmpres=this.bundle.stop1();
                            break;
                    }
                }
                catch(t){
                    this.fwCtx.frameworkError(this.bundle,t);
                }
                this.operation=this.vj$.BundleThread.OP_IDLE;
                this.res=tmpres;
            }
            {
                this.fwCtx.resolver.notifyAll();
            }
        }
    },
    
    bundleChanged:function(be){
        this.be=be;
        this.startAndWait(/*>>*/be.getBundle(),this.vj$.BundleThread.OP_BUNDLE_EVENT);
    },
    
    callStart0:function(b){
        return this.startAndWait(b,this.vj$.BundleThread.OP_START); 
    },
    
    callStop1:function(b){
        return this.startAndWait(b,this.vj$.BundleThread.OP_STOP); 
    },
    
    startAndWait:function(b,op){
        {
            this.res=org.eclipse.vjet.vjo.java.lang.BooleanUtil.FALSE;
            this.bundle=b;
            this.operation=op;
            this.lock.notifyAll();
        }
        var left=0; 
        if(op===this.vj$.BundleThread.OP_START||op===this.vj$.BundleThread.OP_STOP){
            b.aborted=null;
            left=this.startStopTimeout;
        }
        var timeout=false; 
        var uninstall=false; 
        var waitUntil=this.vj$.Util.timeMillis()+left; 
        do{
            try {
                this.fwCtx.resolver.wait(left);
            }
            catch(ie){
            }
            if((op===this.vj$.BundleThread.OP_START||op===this.vj$.BundleThread.OP_STOP)&&b.getState()===this.vj$.Bundle.UNINSTALLED){
                uninstall=true;
                this.res=null;
            }else if(left>0){
                left=waitUntil-this.vj$.Util.timeMillis();
                if(left<=0&&((op===this.vj$.BundleThread.OP_START&&b.getState()===this.vj$.Bundle.STARTING)||(op===this.vj$.BundleThread.OP_STOP&&b.getState()===this.vj$.Bundle.STOPPING))){
                    timeout=true;
                    this.res=null;
                }
            }
        }while(this.res===org.eclipse.vjet.vjo.java.lang.BooleanUtil.FALSE);
        if(b.aborted===null&&(timeout||uninstall)){
            b.aborted=org.eclipse.vjet.vjo.java.lang.BooleanUtil.TRUE;
            var opType=op===this.vj$.BundleThread.OP_START?"start":"stop"; 
            var reason=timeout?"Time-out during bundle "+opType+"()":"Bundle uninstalled during "+opType+"()"; 
            var s=this.fwCtx.props.getProperty(this.vj$.FrameworkProperties.BUNDLETHREAD_ABORT); 
            if(s===null){
                s=this.vj$.BundleThread.ABORT_ACTION_IGNORE;
            }
            this.fwCtx.debug.println("bundle thread aborted during "+opType+" of bundle #"+b.getBundleId()+", abort action set to '"+s+"'");
            if(timeout){
                if(op===this.vj$.BundleThread.OP_START){
                    b.startFailed();
                }else {
                    b.bactivator=null;
                    b.stop2();
                }
            }
            this.quit();
            if(org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(this.vj$.BundleThread.ABORT_ACTION_STOP,s)){
                if(null!==this.vj$.BundleThread.stopMethod){
                    try {
                        this.vj$.BundleThread.stopMethod.invoke(this,/*>>*/null);
                    }
                    catch(t){
                        this.fwCtx.debug.println("bundle thread abort action stop failed: "+t.getMessage());
                        setPriority(this.vj$.Thread.MIN_PRIORITY);
                    }
                }else {
                    setPriority(this.vj$.Thread.MIN_PRIORITY);
                }
            }else if(org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(this.vj$.BundleThread.ABORT_ACTION_MINPRIO,s)){
                setPriority(this.vj$.Thread.MIN_PRIORITY);
            }
            this.res=new this.vj$.BundleException("Bundle#"+b.id+" "+opType+" failed",this.vj$.BundleException.STATECHANGE_ERROR,new this.vj$.Exception(reason));
            b.resetBundleThread();
            return this.res;
        }else {
            {
                this.fwCtx.bundleThreads.addFirst(this);
                if(op!==this.operation){
                }
                b.resetBundleThread();
                return this.res;
            }
        }
    },
    
    isExecutingBundleChanged:function(){
        return this.operation===this.vj$.BundleThread.OP_BUNDLE_EVENT;
    }
})
.inits(function(){
    this.vj$.BundleThread.stopMethod=this.initStopSupported();
})
.endType();