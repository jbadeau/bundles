/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.ListenerEntry') 

.protos({
    bc:null, 
    listener:null, 
    bRemoved:false, 
    
    constructs:function(bc,l){
        this.bc=bc;
        this.listener=l;
    },
    
    equals:function(o){
        if(bundles.framework.ListenerEntry.isInstance(o)){
            return this.bc===o.bc&&this.listener===o.listener;
        }
        return false;
    },
    
    hashCode:function(){
        return this.listener.hashCode();
    },
    
    setRemoved:function(b){
        this.bRemoved=b;
    },
    
    isRemoved:function(){
        return this.bRemoved;
    }
})
.endType();