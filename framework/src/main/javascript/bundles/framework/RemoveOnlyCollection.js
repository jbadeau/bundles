/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.RemoveOnlyCollection<E>') 

.inherits('org.eclipse.vjet.vjo.java.util.AbstractCollection<E>')
.protos({
    org:null, 
    
    constructs:function(values){
        this.base();
        this.org=values;
    },
    
    add:function(obj){
        throw new UnsupportedOperationException("objects can only be removed");
    },
    
    addAll:function(objs){
        throw new UnsupportedOperationException("objects can only be removed");
    },
    
    iterator:function(){
        return this.org.iterator();
    },
    
    size:function(){
        return this.org.size();
    },
    
    remove:function(o){
        if(arguments.length===1){
            if(arguments[0] instanceof Object){
                return this.remove_1_0_RemoveOnlyCollection_ovld(arguments[0]);
            }else if(this.base && this.base.remove){
                return this.base.remove.apply(this,arguments);
            }
        }else if(this.base && this.base.remove){
            return this.base.remove.apply(this,arguments);
        }
    },
    
    remove_1_0_RemoveOnlyCollection_ovld:function(o){
        return this.org.remove(o);
    }
})
.endType();