/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleHooks') 
.needs(['org.eclipse.vjet.vjo.java.util.ArrayList','org.eclipse.vjet.vjo.java.util.HashSet',
    'osgi.framework.hooks.bundle.FindHook','bundles.framework.RemoveOnlyCollection',
    'osgi.framework.BundleException','osgi.framework.Constants',
    'osgi.framework.hooks.bundle.EventHook','osgi.framework.hooks.bundle.CollisionHook'])

.protos({
    fwCtx:null, 
    
    constructs:function(fwCtx){
        this.fwCtx=fwCtx;
    },
    
    filterBundle:function(bc,bundle){
        if(bundle===null){
            return bundle;
        }
        var srl=this.fwCtx.services.get(this.vj$.FindHook.clazz.getName()); 
        if(srl===null||srl.isEmpty()){
            return bundle;
        }else {
            var bl=new this.vj$.ArrayList(); 
            bl.add(bundle);
            filterBundles(bc,bl);
            return bl.isEmpty()?null:bundle;
        }
    },
    
    filterBundles:function(bc,bundles){
        var srl=this.fwCtx.services.get(this.vj$.FindHook.clazz.getName()); 
        if(srl!==null){
            var filtered=new this.vj$.RemoveOnlyCollection(bundles); 
            for (var serviceRegistrationImpl,_$itr=srl.iterator();_$itr.hasNext();){
                serviceRegistrationImpl=_$itr.next();
                var sr=serviceRegistrationImpl.reference; 
                var fh=sr.getService(this.fwCtx.systemBundle); 
                if(fh!==null){
                    try {
                        fh.find(bc,filtered);
                    }
                    catch(e){
                        this.fwCtx.frameworkError(bc,new this.vj$.BundleException("Failed to call Bundle FindHook  #"+sr.getProperty(this.vj$.Constants.SERVICE_ID),e));
                    }
                }
            }
        }
    },
    
    filterBundleEventReceivers:function(evt,syncBundleListeners,bundleListeners){
        var eventHooks=this.fwCtx.services.get(this.vj$.EventHook.clazz.getName()); 
        {
            syncBundleListeners.addAll(this.fwCtx.listeners.syncBundleListeners);
        }
        {
            if(bundleListeners!==null){
                bundleListeners.addAll(this.fwCtx.listeners.bundleListeners);
            }
        }
        if(eventHooks!==null){
            var bundleContexts=new this.vj$.HashSet(); 
            for (var le,_$itr=syncBundleListeners.iterator();_$itr.hasNext();){
                le=_$itr.next();
                bundleContexts.add(le.bc);
            }
            if(bundleListeners!==null){
                for (var le,_$itr=bundleListeners.iterator();_$itr.hasNext();){
                    le=_$itr.next();
                    bundleContexts.add(le.bc);
                }
            }
            var unfilteredSize=bundleContexts.size(); 
            var filtered=new this.vj$.RemoveOnlyCollection(bundleContexts); 
            for (var serviceRegistrationImpl,_$itr=eventHooks.iterator();_$itr.hasNext();){
                serviceRegistrationImpl=_$itr.next();
                var sr=serviceRegistrationImpl.reference; 
                var eh=sr.getService(this.fwCtx.systemBundle); 
                if(eh!==null){
                    try {
                        eh.event(evt,filtered);
                    }
                    catch(e){
                        this.fwCtx.frameworkError(this.fwCtx.systemBundle,new this.vj$.BundleException("Failed to call Bundle EventHook #"+sr.getProperty(this.vj$.Constants.SERVICE_ID),e));
                    }
                }
            }
            if(unfilteredSize!==bundleContexts.size()){
                for (var le,_$itr=syncBundleListeners.iterator();_$itr.hasNext();){
                    le=_$itr.next();
                    if(!bundleContexts.contains(le.bc)){
                        syncBundleListeners.remove(le);
                    }
                }
                if(bundleListeners!==null){
                    for (var le,_$itr=bundleListeners.iterator();_$itr.hasNext();){
                        le=_$itr.next();
                        if(!bundleContexts.contains(le.bc)){
                            bundleListeners.remove(le);
                        }
                    }
                }
            }
        }
    },
    
    filterCollisions:function(mode,b,bundles){
        var srl=this.fwCtx.services.get(this.vj$.CollisionHook.clazz.getName()); 
        if(srl!==null){
            var filtered=new this.vj$.RemoveOnlyCollection(bundles); 
            for (var serviceRegistrationImpl,_$itr=srl.iterator();_$itr.hasNext();){
                serviceRegistrationImpl=_$itr.next();
                var sr=serviceRegistrationImpl.reference; 
                var ch=sr.getService(this.fwCtx.systemBundle); 
                if(ch!==null){
                    try {
                        ch.filterCollisions(mode,b,filtered);
                    }
                    catch(e){
                        this.fwCtx.frameworkError(b,new this.vj$.BundleException("Failed to call Bundle CollisionHook #"+sr.getProperty(this.vj$.Constants.SERVICE_ID),e));
                    }
                }
            }
        }
    }
})
.endType();