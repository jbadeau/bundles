/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.ImportPkg') 
.needs(['org.eclipse.vjet.vjo.java.util.Map','org.eclipse.vjet.vjo.java.lang.IllegalArgumentException',
    'org.eclipse.vjet.vjo.java.util.Collections','org.eclipse.vjet.vjo.java.util.HashMap',
    'org.eclipse.vjet.vjo.java.util.Collection','org.eclipse.vjet.vjo.java.lang.StringBuffer',
    'osgi.framework.Constants','bundles.framework.BundlePackages',
    'osgi.framework.VersionRange','bundles.framework.Pkg',
    'bundles.framework.ExportPkg','bundles.framework.Util',
    'osgi.framework.Filter','osgi.framework.Version',
    'osgi.framework.wiring.BundleRevision','osgi.framework.FrameworkUtil',
    'osgi.framework.InvalidSyntaxException','org.eclipse.vjet.vjo.java.lang.System',
    'osgi.framework.wiring.BundleCapability','org.eclipse.vjet.vjo.java.lang.StringUtil',
    'vjo.java.lang.ObjectUtil'])
.satisfies('osgi.framework.wiring.BundleRequirement')
.satisfies('Comparable<ImportPkg>')
.props({
    RESOLUTION_DYNAMIC:"dynamic", 
    PACKAGE_SPECIFICATION_VERSION:null, 
    importPkgCount:0 
})
.protos({
    orderal:0, 
    name:null, 
    bpkgs:null, 
    resolution:null, 
    bundleSymbolicName:null, 
    packageRange:null, 
    bundleRange:null, 
    attributes:null, 
    directives:null, 
    parent:null, 
    pkg:null, 
    provider:null, 
    internalOk:null, 
    dynId:0, 
    
    
    
    
    
    constructs:function(){
        this.orderal=++this.vj$.ImportPkg.importPkgCount;
        if(arguments.length===4){
            this.constructs_4_0_ImportPkg_ovld(arguments[0],arguments[1],arguments[2],arguments[3]);
        }else if(arguments.length===2){
            if(arguments[0] instanceof bundles.framework.ImportPkg && (arguments[1] instanceof String || typeof arguments[1]=="string")){
                this.constructs_2_0_ImportPkg_ovld(arguments[0],arguments[1]);
            }else if(arguments[0] instanceof bundles.framework.ImportPkg && arguments[1] instanceof bundles.framework.BundlePackages){
                this.constructs_2_1_ImportPkg_ovld(arguments[0],arguments[1]);
            }
        }else if(arguments.length===1){
            this.constructs_1_0_ImportPkg_ovld(arguments[0]);
        }
    },
    
    constructs_4_0_ImportPkg_ovld:function(name,he,b,dynamic){
        this.bpkgs=b;
        this.name=name;
        if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(name,"java.")){
            throw new this.vj$.IllegalArgumentException("You can not import a java.* package");
        }
        var dirs=he.getDirectives(); 
        var res=dirs.get(this.vj$.Constants.RESOLUTION_DIRECTIVE); 
        if(dynamic){
            if(res!==null){
                throw new this.vj$.IllegalArgumentException("Directives not supported for "+"Dynamic-Import, found "+this.vj$.Constants.RESOLUTION_DIRECTIVE+":="+res);
            }
            this.resolution=this.vj$.ImportPkg.RESOLUTION_DYNAMIC;
        }else {
            if(res!==null){
                if(vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.RESOLUTION_OPTIONAL,res)){
                    this.resolution=this.vj$.Constants.RESOLUTION_OPTIONAL;
                }else if(vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.RESOLUTION_MANDATORY,res)){
                    this.resolution=this.vj$.Constants.RESOLUTION_MANDATORY;
                }else {
                    throw new this.vj$.IllegalArgumentException("Directive "+this.vj$.Constants.RESOLUTION_DIRECTIVE+", unexpected value: "+res);
                }
            }else {
                this.resolution=this.vj$.Constants.RESOLUTION_MANDATORY;
            }
        }
        this.bundleSymbolicName=he.getAttributes().remove(this.vj$.Constants.BUNDLE_SYMBOLICNAME_ATTRIBUTE); 
        var versionStr=he.getAttributes().remove(this.vj$.Constants.VERSION_ATTRIBUTE); 
        var specVersionStr=he.getAttributes().remove(this.vj$.ImportPkg.PACKAGE_SPECIFICATION_VERSION); 
        if(specVersionStr!==null){
            this.packageRange=new this.vj$.VersionRange(specVersionStr);
            if(versionStr!==null&& !org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(this.packageRange,new this.vj$.VersionRange(versionStr))){
                throw new this.vj$.IllegalArgumentException("Both "+this.vj$.Constants.VERSION_ATTRIBUTE+" and "+this.vj$.ImportPkg.PACKAGE_SPECIFICATION_VERSION+" are specified, but differs");
            }
        }else if(versionStr!==null){
            this.packageRange=new this.vj$.VersionRange(versionStr);
        }else {
            this.packageRange=null;
        }
        var rangeStr=he.getAttributes().remove(this.vj$.Constants.BUNDLE_VERSION_ATTRIBUTE); 
        if(rangeStr!==null){
            this.bundleRange=new this.vj$.VersionRange(rangeStr);
        }else {
            this.bundleRange=null;
        }
        this.attributes=this.vj$.Collections.unmodifiableMap(he.getAttributes());
        var filter=this.toFilter(); 
        if(null!==filter){
            dirs.put(this.vj$.Constants.FILTER_DIRECTIVE,filter.toString());
        }
        this.directives=this.vj$.Collections.unmodifiableMap(dirs);
        this.parent=null;
    },
    
    constructs_2_0_ImportPkg_ovld:function(ip,name){
        this.name=name;
        this.bpkgs=ip.bpkgs;
        this.resolution=ip.resolution;
        this.bundleSymbolicName=ip.bundleSymbolicName;
        this.packageRange=ip.packageRange;
        this.bundleRange=ip.bundleRange;
        this.attributes=ip.attributes;
        this.directives=ip.directives;
        this.parent=ip;
    },
    
    constructs_2_1_ImportPkg_ovld:function(ip,bpkgs){
        this.name=ip.name;
        this.bpkgs=bpkgs;
        this.resolution=ip.resolution;
        this.bundleSymbolicName=ip.bundleSymbolicName;
        this.packageRange=ip.packageRange;
        this.bundleRange=ip.bundleRange;
        this.attributes=ip.attributes;
        this.directives=ip.directives;
        this.parent=ip.parent;
    },
    
    constructs_1_0_ImportPkg_ovld:function(p){
        this.name=p.name;
        this.bpkgs=p.bpkgs;
        this.resolution=this.vj$.Constants.RESOLUTION_MANDATORY;
        this.bundleSymbolicName=null;
        if(p.version===this.vj$.Version.emptyVersion){
            this.packageRange=null;
        }else {
            this.packageRange=new this.vj$.VersionRange(p.version.toString());
        }
        this.bundleRange=null;
        this.attributes=p.attributes;
        var dirs=new this.vj$.HashMap(); 
        var filter=this.toFilter(); 
        if(null!==filter){
            dirs.put(this.vj$.Constants.FILTER_DIRECTIVE,filter.toString());
        }
        this.directives=this.vj$.Collections.unmodifiableMap(dirs);
        this.parent=null;
    },
    
    attachPkg:function(p){
        this.vj$.Pkg=p;
    },
    
    detachPkg:function(){
        this.vj$.Pkg=null;
        this.provider=null;
    },
    
    okPackageVersion:function(ver){
        return this.packageRange===null||this.packageRange.includes(ver);
    },
    
    checkAttributes:function(ep){
        if(!checkMandatory(ep.mandatory)){
            return false;
        }
        if(!this.okPackageVersion(ep.version)||(this.bundleSymbolicName!==null&&!vjo.java.lang.ObjectUtil.equals(this.bundleSymbolicName,ep.bpkgs.bg.symbolicName))||(this.bundleRange!==null&&!this.bundleRange.includes(ep.bpkgs.bg.version))){
            return false;
        }
        for (var entry,_$itr=this.attributes.entrySet().iterator();_$itr.hasNext();){
            entry=_$itr.next();
            var a=ep.attributes.get(entry.getKey()); 
            if(a===null|| !vjo.java.lang.ObjectUtil.equals(a,entry.getValue())){
                return false;
            }
        }
        return true;
    },
    
    checkPermission:function(ep){
        return this.bpkgs.bg.bundle.fwCtx.perm.hasImportPackagePermission(this.bpkgs.bg.bundle,ep);
    },
    
    mustBeResolved:function(){
        return this.resolution===this.vj$.Constants.RESOLUTION_MANDATORY&&this.internalOk===null;
    },
    
    intersect:function(ip){
        if(ip.bundleSymbolicName!==null&&this.bundleSymbolicName!==null&& !vjo.java.lang.ObjectUtil.equals(ip.bundleSymbolicName,this.bundleSymbolicName)){
            return false;
        }
        for (var entry,_$itr=this.attributes.entrySet().iterator();_$itr.hasNext();){
            entry=_$itr.next();
            var a=ip.attributes.get(entry.getKey()); 
            if(a!==null&& !vjo.java.lang.ObjectUtil.equals(a,entry.getValue())){
                return false;
            }
        }
        if(this.packageRange!==null&&this.packageRange.intersection(ip.packageRange).isEmpty()){
            return false;
        }
        return this.bundleRange===null|| !this.bundleRange.intersection(ip.bundleRange).isEmpty();
    },
    
    pkgString:function(){
        if(this.packageRange!==null){
            return this.name+";"+this.vj$.Constants.VERSION_ATTRIBUTE+"="+this.packageRange;
        }else {
            return this.name;
        }
    },
    
    toString:function(){
        return this.pkgString()+"("+this.bpkgs.bg.bundle+")";
    },
    
    isDynamic:function(){
        return this.resolution===this.vj$.ImportPkg.RESOLUTION_DYNAMIC;
    },
    
    checkMandatory:function(mandatory){
        if(mandatory!==null){
            for (var a,_$itr=mandatory.iterator();_$itr.hasNext();){
                a=_$itr.next();
                if(vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.VERSION_ATTRIBUTE,a)){
                    if(this.packageRange===null){
                        return false;
                    }
                }else if(vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.BUNDLE_SYMBOLICNAME_ATTRIBUTE,a)){
                    if(this.bundleSymbolicName===null){
                        return false;
                    }
                }else if(vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.BUNDLE_VERSION_ATTRIBUTE,a)){
                    if(this.bundleRange===null){
                        return false;
                    }
                }else if(!this.attributes.containsKey(a)){
                    return false;
                }
            }
        }
        return true;
    },
    
    getNamespace:function(){
        return this.vj$.BundleRevision.PACKAGE_NAMESPACE;
    },
    
    getDirectives:function(){
        return this.directives;
    },
    
    toFilter:function(){
        var sb=new this.vj$.StringBuffer(80); 
        var multipleConditions=false; 
        sb.append('(');
        sb.append(this.vj$.BundleRevision.PACKAGE_NAMESPACE);
        sb.append('=');
        sb.append(this.name);
        if(this.name.length===0||org.eclipse.vjet.vjo.java.lang.StringUtil.endsWith(this.name,".")){
            sb.append('*');
        }
        sb.append(')');
        if(this.packageRange!==null){
            sb.append(this.packageRange.toFilterString(this.vj$.Constants.VERSION_ATTRIBUTE));
            multipleConditions=true;
        }
        if(this.bundleSymbolicName!==null){
            sb.append('(');
            sb.append(this.vj$.Constants.BUNDLE_SYMBOLICNAME_ATTRIBUTE);
            sb.append('=');
            sb.append(this.bundleSymbolicName);
            sb.append(')');
            multipleConditions|=true;
        }
        if(this.bundleRange!==null){
            sb.append(this.bundleRange.toFilterString(this.vj$.Constants.BUNDLE_VERSION_ATTRIBUTE));
            multipleConditions=true;
        }
        for (var entry,_$itr=this.attributes.entrySet().iterator();_$itr.hasNext();){
            entry=_$itr.next();
            sb.append('(');
            sb.append(entry.getKey());
            sb.append('=');
            sb.append(entry.getValue().toString());
            sb.append(')');
            multipleConditions|=true;
        }
        if(multipleConditions){
            sb.insert(0,"(&");
            sb.append(')');
        }
        try {
            return this.vj$.FrameworkUtil.createFilter(sb.toString());
        }
        catch(_ise){
            this.vj$.System.err.println("createFilter: '"+sb.toString()+"': "+_ise.getMessage());
            return null;
        }
    },
    
    getAttributes:function(){
        var res=this.vj$.Collections.EMPTY_MAP; 
        return res;
    },
    
    getRevision:function(){
        return this.bpkgs.bg.bundleRevision;
    },
    
    getResource:function(){
        return this.bpkgs.bg.bundleRevision;
    },
    
    matches:function(capability){
        if(vjo.java.lang.ObjectUtil.equals(this.vj$.BundleRevision.PACKAGE_NAMESPACE,capability.getNamespace())){
            return this.toFilter().matches(capability.getAttributes());
        }
        return false;
    },
    
    compareTo:function(o){
        return this.orderal-o.orderal;
    }
})
.inits(function(){
    this.vj$.ImportPkg.PACKAGE_SPECIFICATION_VERSION=this.vj$.Constants.PACKAGE_SPECIFICATION_VERSION;
})
.endType();