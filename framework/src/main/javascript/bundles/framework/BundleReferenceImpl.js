/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleReferenceImpl') 

.satisfies('osgi.framework.BundleReference')
.protos({
    bundle:null, 
    
    constructs:function(bundle){
        this.bundle=bundle;
    },
    
    getBundle:function(){
        return this.bundle;
    }
})
.endType();