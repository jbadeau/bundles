/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.itype('bundles.framework.BundleClassLoaderListener') 

.protos({
    
    bundleClassLoaderCreated:function(bcl){
    },
    
    bundleClassLoaderClosed:function(bcl){
    }
})
.endType();