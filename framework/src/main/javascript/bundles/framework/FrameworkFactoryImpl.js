define([ "vjo", "osgi/framework/launch/FrameworkFactory", "./FrameworkContext" ], function(vjo, FrameworkFactory, FrameworkContext) {

	return vjo.ctype("bundles.framework.FrameworkFactoryImpl")

	.satisfies(FrameworkFactory)

	.protos({

		constructs : function() {
		},

		newFramework : function(configuration) {
			var ctx = new FrameworkContext(configuration);
			return ctx.systemBundle;
		}

	})

	.endType();

})