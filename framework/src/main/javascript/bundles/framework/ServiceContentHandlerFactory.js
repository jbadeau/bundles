/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.ServiceContentHandlerFactory') 
.needs(['org.eclipse.vjet.vjo.java.util.HashMap','org.eclipse.vjet.vjo.java.lang.RuntimeException',
    'bundles.framework.Util','java.net.ContentHandler',
    'osgi.service.url.URLConstants','bundles.framework.ContentHandlerWrapper',
    'org.eclipse.vjet.vjo.java.lang.StringUtil'])

.satisfies('java.net.ContentHandlerFactory')
.props({
    
    convertMimetype:function(s){
        var bad=".,:;*-"; 
        for (var i=0;i<bad.length;i++){
            s=org.eclipse.vjet.vjo.java.lang.StringUtil.replace(s,bad.charAt(i),'_');
        }
        s=org.eclipse.vjet.vjo.java.lang.StringUtil.replace(s,'/','.');
        return s;
    }
})
.protos({
    framework:null, 
    jvmPkgs:null, 
    wrapMap:null, 
    
    constructs:function(fw){
        this.wrapMap=new this.vj$.HashMap();
        this.framework=fw;
        var s=this.framework.props.getProperty("java.content.handler.pkgs"); 
        if(s!==null&&s.length>0){
            this.jvmPkgs=this.vj$.Util.splitwords(s,"|");
            for (var i=0;i<this.jvmPkgs.length;i++){
                this.jvmPkgs[i]=org.eclipse.vjet.vjo.java.lang.StringUtil.trim(this.jvmPkgs[i]);
                if(this.framework.debug.url){
                    this.framework.debug.println("JVMClassPathCH - jvmPkgs["+i+"]="+this.jvmPkgs[i]);
                }
            }
        }
    },
    
    createContentHandler:function(mimetype){
        if(this.framework.debug.url){
            this.framework.debug.println("createContentHandler protocol="+mimetype);
        }
        var handler=this.getJVMClassPathHandler(mimetype); 
        if(handler!==null){
            if(this.framework.debug.url){
                this.framework.debug.println("using JVMClassPath handler for "+mimetype);
            }
            return handler;
        }
        handler=this.getServiceHandler(mimetype);
        if(handler!==null){
            if(this.framework.debug.url){
                this.framework.debug.println("Using service ContentHandler for "+mimetype+", handler="+handler);
            }
            return handler;
        }
        if(this.framework.debug.url){
            this.framework.debug.println("Using default ContentHandler for "+mimetype);
        }
        return null;
    },
    
    getServiceHandler:function(mimetype){
        try {
            var filter="("+this.vj$.URLConstants.URL_CONTENT_MIMETYPE+"="+mimetype+")"; 
            var srl=this.framework.services.get(this.vj$.ContentHandler.clazz.getName(),filter,null); 
            if(srl!==null&&srl.length>0){
                var wrapper=this.wrapMap.get(mimetype); 
                if(wrapper===null){
                    wrapper=new this.vj$.ContentHandlerWrapper(this.framework,mimetype);
                    this.wrapMap.put(mimetype,wrapper);
                }
                return wrapper;
            }
        }
        catch(e){
            throw new this.vj$.RuntimeException("Failed to get service: "+e);
        }
        return null;
    },
    
    getJVMClassPathHandler:function(mimetype){
        if(this.jvmPkgs!==null){
            for (var jvmPkg,_$i0=0;_$i0<this.jvmPkgs.length;_$i0++){
                jvmPkg=this.jvmPkgs[_$i0];
                var converted=this.vj$.ServiceContentHandlerFactory.convertMimetype(mimetype); 
                var className=jvmPkg+"."+converted+".Handler"; 
                try {
                    if(this.framework.debug.url){
                        this.framework.debug.println("JVMClassPathCH - trying ContentHandler class="+className);
                    }
                    var clazz=vjo.Class.forName(className); 
                    var handler=clazz.newInstance(); 
                    if(this.framework.debug.url){
                        this.framework.debug.println("JVMClassPathCH - created ContentHandler class="+className);
                    }
                    return handler;
                }
                catch(t){
                    if(this.framework.debug.url){
                        this.framework.debug.println("JVMClassPathCH - no ContentHandler class "+className);
                    }
                }
            }
        }
        if(this.framework.debug.url){
            this.framework.debug.println("JVMClassPath - no ContentHandler for "+mimetype);
        }
        return null;
    }
})
.endType();