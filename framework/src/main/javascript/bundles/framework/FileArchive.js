/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.itype('bundles.framework.FileArchive') 

.protos({
    
    getClassBytes:function(component){
    },
    
    getBundleResourceStream:function(component){
    },
    
    findResourcesPath:function(path){
    },
    
    checkNativeLibrary:function(path){
    },
    
    getNativeLibrary:function(libNameKey){
    },
    
    getBundleGeneration:function(){
    },
    
    getSubId:function(){
    },
    
    exists:function(path,dirs){
    },
    
    listDir:function(path){
    }
})
.endType();