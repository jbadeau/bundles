/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.RequiredBundleImpl') 

.satisfies('osgi.service.packageadmin.RequiredBundle')
.protos({
    bpkgs:null, 
    
    constructs:function(b){
        this.bpkgs=b;
    },
    
    getSymbolicName:function(){
        return this.bpkgs.bg.symbolicName;
    },
    
    getBundle:function(){
        if(this.bpkgs.isRegistered()){
            return this.bpkgs.bg.bundle;
        }else {
            return null;
        }
    },
    
    getRequiringBundles:function(){
        if(this.bpkgs.isRegistered()){
            var rl=this.bpkgs.bg.bundle.getRequiredBy(); 
            var res=vjo.createArray(null, rl.size()); 
            for (var i=rl.size()-1;i>=0;i--){
                res[i]=rl.get(i).bg.bundle;
            }
            return res;
        }
        return null;
    },
    
    getVersion:function(){
        return this.bpkgs.bg.version;
    },
    
    isRemovalPending:function(){
        return !this.bpkgs.bg.isCurrent();
    }
})
.endType();