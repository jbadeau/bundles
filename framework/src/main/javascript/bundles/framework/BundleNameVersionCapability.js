/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleNameVersionCapability') 
.needs(['org.eclipse.vjet.vjo.java.util.Collections','org.eclipse.vjet.vjo.java.util.HashMap',
    'osgi.framework.Constants','org.eclipse.vjet.vjo.java.lang.ObjectUtil'])

.satisfies('osgi.framework.wiring.BundleCapability')
.protos({
    gen:null, 
    namespace:null, 
    
    constructs:function(bundleGeneration,namespace){
        this.gen=bundleGeneration;
        this.namespace=namespace;
    },
    
    getNamespace:function(){
        return this.namespace;
    },
    
    getDirectives:function(){
        if(this.gen.symbolicNameParameters!==null){
            return this.vj$.Collections.unmodifiableMap(this.gen.symbolicNameParameters.getDirectives());
        }
        var res=this.vj$.Collections.EMPTY_MAP; 
        return res;
    },
    
    getAttributes:function(){
        var res=new this.vj$.HashMap(); 
        if(this.gen.symbolicNameParameters!==null){
            res.putAll(this.gen.symbolicNameParameters.getAttributes());
        }
        if(this.gen.symbolicName!==null){
            res.put(this.namespace,this.gen.symbolicName);
            res.put(this.vj$.Constants.BUNDLE_VERSION_ATTRIBUTE,this.gen.version);
        }
        return this.vj$.Collections.unmodifiableMap(res);
    },
    
    getRevision:function(){
        return this.gen.bundleRevision;
    },
    
    getResource:function(){
        return this.gen.bundleRevision;
    },
    
    hashCode:function(){
        var prime=31; 
        var result=1; 
        result=prime*result+((this.gen===null)?0:org.eclipse.vjet.vjo.java.lang.ObjectUtil.hashCode(this.gen));
        result=prime*result+((this.namespace===null)?0:vjo.java.lang.ObjectUtil.hashCode(this.namespace));
        return result;
    },
    
    equals:function(obj){
        if(this===obj){
            return true;
        }
        if(obj===null){
            return false;
        }
        if(this.getClass()!==obj.getClass()){
            return false;
        }
        var other=obj; 
        if(!org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(this.gen,other.gen)){
            return false;
        }
        if(!vjo.java.lang.ObjectUtil.equals(this.namespace,other.namespace)){
            return false;
        }
        return true;
    },
    
    toString:function(){
        return "BundleNameVersionCapability[nameSpace="+this.namespace+", attributes="+this.getAttributes()+", directives="+this.getDirectives()+", revision="+this.getRevision()+"]";
    }
})
.endType();