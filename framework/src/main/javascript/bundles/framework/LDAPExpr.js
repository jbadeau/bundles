/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.LDAPExpr') 
.needs(['org.eclipse.vjet.vjo.java.util.TreeSet','org.eclipse.vjet.vjo.java.util.ArrayList',
    'org.eclipse.vjet.vjo.java.lang.Character','org.eclipse.vjet.vjo.java.lang.Number',
    'org.eclipse.vjet.vjo.java.lang.Byte','org.eclipse.vjet.vjo.java.lang.Integer',
    'org.eclipse.vjet.vjo.java.lang.Short','org.eclipse.vjet.vjo.java.lang.Long',
    'org.eclipse.vjet.vjo.java.lang.Float','org.eclipse.vjet.vjo.java.lang.Double',
    'org.eclipse.vjet.vjo.java.util.Collection','vjo.java.lang.reflect.Array',
    'org.eclipse.vjet.vjo.java.util.HashMap','org.eclipse.vjet.vjo.java.lang.StringBuffer',
    'org.eclipse.vjet.vjo.java.util.Iterator','org.eclipse.vjet.vjo.java.util.NoSuchElementException',
    'osgi.framework.Constants','osgi.framework.InvalidSyntaxException',
    'java.util.AbstractSet','org.eclipse.vjet.vjo.java.lang.StringUtil',
    'vjo.java.lang.ObjectUtil','org.eclipse.vjet.vjo.java.lang.BooleanUtil'])
.needs('org.eclipse.vjet.vjo.java.lang.StringFactory','')

.props({
    AND:0, 
    OR:1, 
    NOT:2, 
    EQ:4, 
    LE:8, 
    GE:16, 
    APPROX:32, 
    COMPLEX:0, 
    SIMPLE:0, 
    WILDCARD:65535, 
    WILDCARD_STRING:null, 
    NULL:"Null query", 
    GARBAGE:"Trailing garbage", 
    EOS:"Unexpected end of query", 
    MALFORMED:"Malformed query", 
    OPERATOR:"Undefined operator", 
    classBigDecimal:null, 
    consBigDecimal:null, 
    compBigDecimal:null, 
    classBigInteger:null, 
    consBigInteger:null, 
    compBigInteger:null, 
    constructorMap:null, 
    ParseState:vjo.ctype() 
    .protos({
        pos:0, 
        str:null, 
        
        constructs:function(str){
            this.str=str;
            if(str.length===0){
                this.error(this.vj$.LDAPExpr.NULL);
            }
            this.pos=0;
        },
        
        prefix:function(pre){
            if(!org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(this.str,pre,this.pos)){
                return false;
            }
            this.pos+=pre.length;
            return true;
        },
        
        peek:function(){
            return this.str.charAt(this.pos);
        },
        
        skip:function(n){
            this.pos+=n;
        },
        
        rest:function(){
            return this.str.substring(this.pos);
        },
        
        skipWhite:function(){
            while(this.vj$.Character.isWhitespace(this.str.charAt(this.pos))){
                this.pos++;
            }
        },
        
        getAttributeName:function(){
            var start=this.pos; 
            var end=-1; 
            for (;;this.pos++){
                var c=this.str.charAt(this.pos); 
                if(c==='('||c===')'||c==='<'||c==='>'||c==='='||c==='~'){
                    break;
                }else if(!this.vj$.Character.isWhitespace(c)){
                    end=this.pos;
                }
            }
            if(end=== -1){
                return null;
            }
            return this.str.substring(start,end+1);
        },
        
        getAttributeValue:function(){
            var sb=new StringBuffer(); 
            label:
            for (;;this.pos++){
                var c=this.str.charAt(this.pos); 
                switch(c){
                    case '(':
                    case ')':
                        break label;
                    case '*':
                        sb.append(this.vj$.LDAPExpr.WILDCARD);
                        break;
                    case '\\':
                        sb.append(this.str.charAt(++this.pos));
                        break;
                    default: 
                        sb.append(c);
                        break;
                }
            }
            return sb.toString();
        },
        
        error:function(m){
            throw new this.vj$.InvalidSyntaxException(m,(this.str===null)?"":this.str.substring(this.pos));
        }
    })
    .endType(),
    OneSet:vjo.ctype() 
    .inherits('java.util.AbstractSet<E>')
    .protos({
        elem:null, 
        
        constructs:function(o){
            this.base();
            this.elem=o;
        },
        
        iterator:function(){
            return vjo.make(this,Iterator)
                .protos({
                    ielem:this.vj$.parent.elem,
                    hasNext:function(){
                        return this.ielem!==null;
                    },
                    next:function(){
                        if(this.ielem!==null){
                            var r=this.ielem; 
                            this.ielem=null;
                            return r;
                        }else {
                            throw new NoSuchElementException();
                        }
                    },
                    remove:function(){
                        throw new UnsupportedOperationException();
                    }
                })
                .endType();
        },
        
        size:function(){
            return this.elem===null?0:1;
        }
    })
    .endType(),
    
    query:function(filter,pd){
        return (new this(filter)).evaluate(pd,false);
    },
    
    getConstructor:function(clazz){
        {
            var cons=this.constructorMap.get(clazz); 
            if(!this.constructorMap.containsKey(clazz)){
                try {
                    cons=clazz.getConstructor([vjo.Object.clazz]);
                }
                catch(e){
                }
                this.constructorMap.put(clazz,cons);
            }
            return cons;
        }
    },
    
    compareString:function(s1,op,s2){
        switch(op){
            case this.LE:
                return org.eclipse.vjet.vjo.java.lang.StringUtil.compareTo(s1,s2)<=0;
            case this.GE:
                return org.eclipse.vjet.vjo.java.lang.StringUtil.compareTo(s1,s2)>=0;
            case this.EQ:
                return this.patSubstr(s1,s2);
            case this.APPROX:
                return vjo.java.lang.ObjectUtil.equals(this.fixupString(s2),this.fixupString(s1));
            default: 
                return false;
        }
    },
    
    fixupString:function(s){
        var sb=new this.vj$.StringBuffer(); 
        var len=s.length; 
        for (var i=0;i<len;i++){
            var c=s.charAt(i); 
            if(!this.vj$.Character.isWhitespace(c)){
                if(this.vj$.Character.isUpperCase(c)){
                    c=this.vj$.Character.toLowerCase(c);
                }
                sb.append(c);
            }
        }
        return sb.toString();
    },
    
    
    patSubstr:function(s,pat){
        if(arguments.length===2){
            return this.vj$.LDAPExpr.patSubstr_2_0_LDAPExpr_ovld(arguments[0],arguments[1]);
        }else if(arguments.length===4){
            return this.vj$.LDAPExpr.patSubstr_4_0_LDAPExpr_ovld(arguments[0],arguments[1],arguments[2],arguments[3]);
        }
    },
    
    patSubstr_2_0_LDAPExpr_ovld:function(s,pat){
        return s===null?false:patSubstr(org.eclipse.vjet.vjo.java.lang.StringUtil.toCharArray(s),0,org.eclipse.vjet.vjo.java.lang.StringUtil.toCharArray(pat),0);
    },
    
    patSubstr_4_0_LDAPExpr_ovld:function(s,si,pat,pi){
        if(pat.length-pi===0){
            return s.length-si===0;
        }
        if(pat[pi]===this.WILDCARD){
            pi++;
            for (;;){
                if(this.patSubstr(s,si,pat,pi)){
                    return true;
                }
                if(s.length-si===0){
                    return false;
                }
                si++;
            }
        }else {
            if(s.length-si===0){
                return false;
            }
            if(s[si]!==pat[pi]){
                return false;
            }
            return this.patSubstr(s,++si,pat,++pi);
        }
    },
    
    parseExpr:function(ps){
        ps.skipWhite();
        if(!ps.prefix("(")){
            ps.error(this.MALFORMED);
        }
        var operator; 
        ps.skipWhite();
        switch(ps.peek()){
            case '&':
                operator=this.AND;
                break;
            case '|':
                operator=this.OR;
                break;
            case '!':
                operator=this.NOT;
                break;
            default: 
                return this.parseSimple(ps);
        }
        ps.skip(1);
        var v=new this.vj$.ArrayList(); 
        do{
            v.add(this.parseExpr(ps));
            ps.skipWhite();
        }while(ps.peek()==='(');
        var n=v.size(); 
        if(!ps.prefix(")")||n===0||(operator===this.NOT&&n>1)){
            ps.error(this.MALFORMED);
        }
        var args=vjo.createArray(null, n); 
        v.toArray(args);
        return new this(operator,args);
    },
    
    parseSimple:function(ps){
        var attrName=ps.getAttributeName(); 
        if(attrName===null){
            ps.error(this.MALFORMED);
        }
        var operator=0; 
        if(ps.prefix("=")){
            operator=this.EQ;
        }else if(ps.prefix("<=")){
            operator=this.LE;
        }else if(ps.prefix(">=")){
            operator=this.GE;
        }else if(ps.prefix("~=")){
            operator=this.APPROX;
        }else {
            ps.error(this.OPERATOR);
        }
        var attrValue=ps.getAttributeValue(); 
        if(!ps.prefix(")")){
            ps.error(this.MALFORMED);
        }
        return new this(operator,attrName,attrValue);
    }
})
.protos({
    operator:0, 
    args:null, 
    attrName:null, 
    attrValue:null, 
    
    
    
    
    constructs:function(){
        if(arguments.length===1){
            this.constructs_1_0_LDAPExpr_ovld(arguments[0]);
        }else if(arguments.length===2){
            this.constructs_2_0_LDAPExpr_ovld(arguments[0],arguments[1]);
        }else if(arguments.length===3){
            this.constructs_3_0_LDAPExpr_ovld(arguments[0],arguments[1],arguments[2]);
        }
    },
    
    constructs_1_0_LDAPExpr_ovld:function(filter){
        var ps=new this.vj$.LDAPExpr.ParseState(filter); 
        var expr=null; 
        try {
            expr=this.vj$.LDAPExpr.parseExpr(ps);
        }
        catch(e){
            ps.error(this.vj$.LDAPExpr.EOS);
        }
        if(org.eclipse.vjet.vjo.java.lang.StringUtil.trim(ps.rest()).length()!==0){
            ps.error(this.vj$.LDAPExpr.GARBAGE+" '"+ps.rest()+"'");
        }
        this.operator=expr.operator;
        this.args=expr.args;
        this.attrName=expr.attrName;
        this.attrValue=expr.attrValue;
    },
    
    constructs_2_0_LDAPExpr_ovld:function(operator,args){
        this.operator=operator;
        this.args=args;
        this.attrName=null;
        this.attrValue=null;
    },
    
    constructs_3_0_LDAPExpr_ovld:function(operator,attrName,attrValue){
        this.operator=operator;
        this.args=null;
        this.attrName=attrName;
        this.attrValue=attrValue;
    },
    
    getMatchedObjectClasses:function(){
        var objClasses=null; 
        if(this.operator===this.vj$.LDAPExpr.EQ){
            if(org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(this.attrName,this.vj$.Constants.OBJECTCLASS)&&this.attrValue.indexOf(this.vj$.LDAPExpr.WILDCARD)<0){
                objClasses=new this.vj$.LDAPExpr.OneSet(this.attrValue);
            }
        }else if(this.operator===this.vj$.LDAPExpr.AND){
            for (var arg,_$i0=0;_$i0<this.args.length;_$i0++){
                arg=this.args[_$i0];
                var r=arg.getMatchedObjectClasses(); 
                if(r!==null){
                    if(objClasses===null){
                        objClasses=new this.vj$.TreeSet();
                    }
                    objClasses.addAll(r);
                }
            }
        }else if(this.operator===this.vj$.LDAPExpr.OR){
            for (var arg,_$i1=0;_$i1<this.args.length;_$i1++){
                arg=this.args[_$i1];
                var r=arg.getMatchedObjectClasses(); 
                if(r!==null){
                    if(objClasses===null){
                        objClasses=new this.vj$.TreeSet();
                    }
                    objClasses.addAll(r);
                }else {
                    objClasses=null;
                    break;
                }
            }
        }
        return objClasses;
    },
    
    isSimple:function(keywords,cache,matchCase){
        if(this.operator===this.vj$.LDAPExpr.EQ){
            var index; 
            if((index=keywords.indexOf(matchCase?this.attrName:org.eclipse.vjet.vjo.java.lang.StringUtil.toLowerCase(this.attrName)))>=0&&this.attrValue.indexOf(this.vj$.LDAPExpr.WILDCARD)<0){
                if(cache[index]===null){
                    cache[index]=new this.vj$.ArrayList();
                }
                cache[index].add(this.attrValue);
                return true;
            }
        }else if(this.operator===this.vj$.LDAPExpr.OR){
            for (var i=0;i<this.args.length;i++){
                if(!this.args[i].isSimple(keywords,cache,matchCase)){
                    return false;
                }
            }
            return true;
        }
        return false;
    },
    
    evaluate:function(p,matchCase){
        if((this.operator&this.vj$.LDAPExpr.SIMPLE)!==0){
            return compare(p.get(matchCase?this.attrName:org.eclipse.vjet.vjo.java.lang.StringUtil.toLowerCase(this.attrName)),this.operator,this.attrValue);
        }else {
            switch(this.operator){
                case this.vj$.LDAPExpr.AND:
                for (var i=0;i<this.args.length;i++){
                    if(!this.args[i].evaluate(p,matchCase)){
                        return false;
                    }
                }
                    return true;
                case this.vj$.LDAPExpr.OR:
                for (var arg,_$i2=0;_$i2<this.args.length;_$i2++){
                    arg=this.args[_$i2];
                    if(arg.evaluate(p,matchCase)){
                        return true;
                    }
                }
                    return false;
                case this.vj$.LDAPExpr.NOT:
                    return !this.args[0].evaluate(p,matchCase);
                default: 
                    return false;
            }
        }
    },
    
    compare:function(obj,op,s){
        if(obj===null){
            return false;
        }
        if(op===this.vj$.LDAPExpr.EQ&&vjo.java.lang.ObjectUtil.equals(s,this.vj$.LDAPExpr.WILDCARD_STRING)){
            return true;
        }
        try {
            if(obj instanceof String){
                return this.vj$.LDAPExpr.compareString(/*>>*/obj,op,s);
            }else if(vjo.getType('org.eclipse.vjet.vjo.java.lang.Character').isInstance(obj)){
                return this.vj$.LDAPExpr.compareString(obj.toString(),op,s);
            }else if(obj instanceof Boolean){
                if(op===this.vj$.LDAPExpr.LE||op===this.vj$.LDAPExpr.GE){
                    return false;
                }
                if(org.eclipse.vjet.vjo.java.lang.BooleanUtil.booleanValue(obj)){
                    return org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(s,"true");
                }else {
                    return org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(s,"false");
                }
            }else if(obj instanceof vjo.getType('org.eclipse.vjet.vjo.java.lang.Number')){
                if(vjo.getType('org.eclipse.vjet.vjo.java.lang.Byte').isInstance(obj)){
                    switch(op){
                        case this.vj$.LDAPExpr.LE:
                            return obj.byteValue()<=this.vj$.Byte.parseByte(s);
                        case this.vj$.LDAPExpr.GE:
                            return obj.byteValue()>=this.vj$.Byte.parseByte(s);
                        default: 
                            return (new this.vj$.Byte(s)).equals(obj);
                    }
                }else if(vjo.getType('org.eclipse.vjet.vjo.java.lang.Integer').isInstance(obj)){
                    switch(op){
                        case this.vj$.LDAPExpr.LE:
                            return obj.intValue()<=this.vj$.Integer.parseInt(s);
                        case this.vj$.LDAPExpr.GE:
                            return obj.intValue()>=this.vj$.Integer.parseInt(s);
                        default: 
                            return (new this.vj$.Integer(s)).equals(obj);
                    }
                }else if(vjo.getType('org.eclipse.vjet.vjo.java.lang.Short').isInstance(obj)){
                    switch(op){
                        case this.vj$.LDAPExpr.LE:
                            return obj.shortValue()<=this.vj$.Short.parseShort(s);
                        case this.vj$.LDAPExpr.GE:
                            return obj.shortValue()>=this.vj$.Short.parseShort(s);
                        default: 
                            return (new this.vj$.Short(s)).equals(obj);
                    }
                }else if(vjo.getType('org.eclipse.vjet.vjo.java.lang.Long').isInstance(obj)){
                    switch(op){
                        case this.vj$.LDAPExpr.LE:
                            return obj.longValue()<=this.vj$.Long.parseLong(s);
                        case this.vj$.LDAPExpr.GE:
                            return obj.longValue()>=this.vj$.Long.parseLong(s);
                        default: 
                            return (new this.vj$.Long(s)).equals(obj);
                    }
                }else if(vjo.getType('org.eclipse.vjet.vjo.java.lang.Float').isInstance(obj)){
                    switch(op){
                        case this.vj$.LDAPExpr.LE:
                            return obj.floatValue()<=(new this.vj$.Float(s)).floatValue();
                        case this.vj$.LDAPExpr.GE:
                            return obj.floatValue()>=(new this.vj$.Float(s)).floatValue();
                        default: 
                            return (new this.vj$.Float(s)).equals(obj);
                    }
                }else if(vjo.getType('org.eclipse.vjet.vjo.java.lang.Double').isInstance(obj)){
                    switch(op){
                        case this.vj$.LDAPExpr.LE:
                            return obj.doubleValue()<=(new this.vj$.Double(s)).doubleValue();
                        case this.vj$.LDAPExpr.GE:
                            return obj.doubleValue()>=(new this.vj$.Double(s)).doubleValue();
                        default: 
                            return (new this.vj$.Double(s)).equals(obj);
                    }
                }else if(this.vj$.LDAPExpr.classBigInteger!==null&&this.vj$.LDAPExpr.classBigInteger.isInstance(obj)){
                    var n=this.vj$.LDAPExpr.consBigInteger.newInstance([s]);
                    var c=(this.vj$.LDAPExpr.compBigInteger.invoke(obj,[n])).intValue(); 
                    switch(op){
                        case this.vj$.LDAPExpr.LE:
                            return c<=0;
                        case this.vj$.LDAPExpr.GE:
                            return c>=0;
                        default: 
                            return c===0;
                    }
                }else if(this.vj$.LDAPExpr.classBigDecimal!==null&&this.vj$.LDAPExpr.classBigDecimal.isInstance(obj)){
                    var n=this.vj$.LDAPExpr.consBigDecimal.newInstance([s]);
                    var c=(this.vj$.LDAPExpr.compBigDecimal.invoke(obj,[n])).intValue(); 
                    switch(op){
                        case this.vj$.LDAPExpr.LE:
                            return c<=0;
                        case this.vj$.LDAPExpr.GE:
                            return c>=0;
                        default: 
                            return c===0;
                    }
                }
            }else if(vjo.getType('org.eclipse.vjet.vjo.java.util.Collection').isInstance(obj)){
                for (var name,_$itr=obj.iterator();_$itr.hasNext();){
                    name=_$itr.next();
                    if(this.compare(name,op,s)){
                        return true;
                    }
                }
            }else if(obj.getClass().isArray()){
                var len=this.vj$.Array.getLength(obj); 
                for (var i=0;i<len;i++){
                    if(compare(this.vj$.Array.get(obj,i),op,s)){
                        return true;
                    }
                }
            }else {
                var clazz=obj.getClass(); 
                var cons=this.vj$.LDAPExpr.getConstructor(clazz); 
                if(cons!==null){
                    var other=cons.newInstance([s]);
                    if(Comparable.isInstance(obj)){
                        var c=obj.compareTo(other); 
                        switch(op){
                            case this.vj$.LDAPExpr.LE:
                                return c<=0;
                            case this.vj$.LDAPExpr.GE:
                                return c>=0;
                            default: 
                                return c===0;
                        }
                    }else {
                        var b=false; 
                        if(op===this.vj$.LDAPExpr.LE||op===this.vj$.LDAPExpr.GE||op===this.vj$.LDAPExpr.EQ||op===this.vj$.LDAPExpr.APPROX){
                            b=org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(obj,other);
                        }
                        return b;
                    }
                }
            }
        }
        catch(ignored_but_evals_to_false){
        }
        return false;
    },
    
    toString:function(){
        var res=new this.vj$.StringBuffer(); 
        res.append("(");
        if((this.operator&this.vj$.LDAPExpr.SIMPLE)!==0){
            res.append(this.attrName);
            switch(this.operator){
                case this.vj$.LDAPExpr.EQ:
                    res.append("=");
                    break;
                case this.vj$.LDAPExpr.LE:
                    res.append("<=");
                    break;
                case this.vj$.LDAPExpr.GE:
                    res.append(">=");
                    break;
                case this.vj$.LDAPExpr.APPROX:
                    res.append("~=");
                    break;
            }
            for (var i=0;i<this.attrValue.length;i++){
                var c=this.attrValue.charAt(i); 
                if(c==='('||c===')'||c==='*'||c==='\\'){
                    res.append('\\');
                }else if(c===this.vj$.LDAPExpr.WILDCARD){
                    c='*';
                }
                res.append(c);
            }
        }else {
            switch(this.operator){
                case this.vj$.LDAPExpr.AND:
                    res.append("&");
                    break;
                case this.vj$.LDAPExpr.OR:
                    res.append("|");
                    break;
                case this.vj$.LDAPExpr.NOT:
                    res.append("!");
                    break;
            }
            for (var arg,_$i3=0;_$i3<this.args.length;_$i3++){
                arg=this.args[_$i3];
                res.append(arg.toString());
            }
        }
        res.append(")");
        return res.toString();
    }
})
.inits(function(){
    this.vj$.LDAPExpr.COMPLEX=this.AND|this.OR|this.NOT;
    this.vj$.LDAPExpr.SIMPLE=this.EQ|this.LE|this.GE|this.APPROX;
    this.vj$.LDAPExpr.WILDCARD_STRING=org.eclipse.vjet.vjo.java.lang.StringFactory.build([this.WILDCARD]);
    this.vj$.LDAPExpr.constructorMap=new this.vj$.HashMap();
    {
        try {
            this.classBigDecimal=vjo.Class.forName("java.math.BigDecimal");
            this.consBigDecimal=this.getConstructor(this.classBigDecimal);
            this.compBigDecimal=this.classBigDecimal.getMethod("compareTo",[this.classBigDecimal]);
        }
        catch(ignore){
            this.classBigDecimal=null;
        }
        try {
            this.classBigInteger=vjo.Class.forName("java.math.BigInteger");
            this.consBigInteger=this.getConstructor(this.classBigInteger);
            this.compBigInteger=this.classBigInteger.getMethod("compareTo",[this.classBigInteger]);
        }
        catch(ignore){
            this.classBigInteger=null;
        }
    }
})
.endType();