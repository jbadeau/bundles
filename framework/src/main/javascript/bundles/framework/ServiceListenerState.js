/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.ServiceListenerState') 
.needs(['org.eclipse.vjet.vjo.java.util.ArrayList','org.eclipse.vjet.vjo.java.util.HashMap',
    'org.eclipse.vjet.vjo.java.util.HashSet','org.eclipse.vjet.vjo.java.util.Collections',
    'org.eclipse.vjet.vjo.java.util.Collection','osgi.framework.Constants',
    'bundles.framework.ServiceListenerEntry','org.eclipse.vjet.vjo.java.lang.StringUtil'])
.needs('bundles.framework.Listeners','')

.props({
    hashedKeys:null, 
    OBJECTCLASS_IX:0, 
    SERVICE_ID_IX:1, 
    SERVICE_PID_IX:2, 
    hashedKeysV:null 
})
.protos({
    complicatedListeners:null, 
    cache:null, 
    serviceSet:null, 
    listeners:null, 
    
    constructs:function(listeners){
        this.complicatedListeners=new this.vj$.ArrayList();
        this.cache=vjo.createArray(null, this.vj$.ServiceListenerState.hashedKeys.length);
        this.serviceSet=new this.vj$.HashSet();
        bundles.framework.Listeners=listeners;
        this.vj$.ServiceListenerState.hashedKeysV=new this.vj$.ArrayList();
        for (var i=0;i<this.vj$.ServiceListenerState.hashedKeys.length;i++){
            this.vj$.ServiceListenerState.hashedKeysV.add(this.vj$.ServiceListenerState.hashedKeys[i]);
            this.cache[i]=new this.vj$.HashMap();
        }
    },
    
    clear:function(){
        this.vj$.ServiceListenerState.hashedKeysV.clear();
        this.complicatedListeners.clear();
        for (var i=0;i<this.vj$.ServiceListenerState.hashedKeys.length;i++){
            this.cache[i].clear();
        }
        this.serviceSet.clear();
        bundles.framework.Listeners=null;
    },
    
    add:function(bc,listener,filter){
        var sle=new this.vj$.ServiceListenerEntry(bc,listener,filter); 
        if(this.serviceSet.contains(sle)){
            this.remove(bc,listener);
        }
        this.serviceSet.add(sle);
        this.fwCtx.serviceHooks.handleServiceListenerReg(sle);
        this.checkSimple(sle);
    },
    
    remove:function(bc,listener){
        for (var it=this.serviceSet.iterator();it.hasNext();){
            var sle=it.next(); 
            if(sle.bc===bc&&sle.listener===listener){
                sle.setRemoved(true);
                this.fwCtx.serviceHooks.handleServiceListenerUnreg(sle);
                this.removeFromCache(sle);
                it.remove();
                break;
            }
        }
    },
    
    removeFromCache:function(sle){
        if(sle.local_cache!==null){
            for (var i=0;i<this.vj$.ServiceListenerState.hashedKeys.length;i++){
                var keymap=this.cache[i]; 
                var l=sle.local_cache[i]; 
                if(l!==null){
                    for (var value,_$itr=l.iterator();_$itr.hasNext();){
                        value=_$itr.next();
                        var sles=keymap.get(value); 
                        if(sles!==null){
                            sles.remove(sles.indexOf(sle));
                            if(sles.isEmpty()){
                                keymap.remove(value);
                            }
                        }
                    }
                }
            }
        }else {
            this.complicatedListeners.remove(sle);
        }
    },
    
    removeAll:function(bc){
        for (var it=this.serviceSet.iterator();it.hasNext();){
            var sle=it.next(); 
            if(sle.bc===bc){
                this.removeFromCache(sle);
                it.remove();
            }
        }
    },
    
    hooksBundleStopped:function(bc){
        var entries=new this.vj$.ArrayList(); 
        for (var sle,_$itr=this.serviceSet.iterator();_$itr.hasNext();){
            sle=_$itr.next();
            if(sle.bc===bc){
                entries.add(sle);
            }
        }
        this.fwCtx.serviceHooks.handleServiceListenerUnreg(this.vj$.Collections.unmodifiableList(entries));
    },
    
    checkSimple:function(sle){
        if(sle.noFiltering||this.nocacheldap){
            this.complicatedListeners.add(sle);
        }else {
            var local_cache=vjo.createArray(null, this.vj$.ServiceListenerState.hashedKeys.length); 
            if(sle.ldap.isSimple(this.vj$.ServiceListenerState.hashedKeysV,local_cache,false)){
                sle.local_cache=local_cache;
                for (var i=0;i<this.vj$.ServiceListenerState.hashedKeys.length;i++){
                    if(local_cache[i]!==null){
                        for (var value,_$itr=local_cache[i].iterator();_$itr.hasNext();){
                            value=_$itr.next();
                            var sles=this.cache[i].get(value); 
                            if(sles===null){
                                this.cache[i].put(value,sles=new this.vj$.ArrayList());
                            }
                            sles.add(sle);
                        }
                    }
                }
            }else {
                if(this.fwCtx.debug.ldap){
                    this.fwCtx.debug.println("Too complicated filter: "+sle.ldap);
                }
                this.complicatedListeners.add(sle);
            }
        }
    },
    
    getMatchingListeners:function(sr){
        var set=new this.vj$.HashSet(); 
        var n=0; 
        for (var sle,_$itr=this.complicatedListeners.iterator();_$itr.hasNext();){
            sle=_$itr.next();
            if(sle.noFiltering||sle.ldap.evaluate(sr.getProperties(),false)){
                set.add(sle);
            }
            ++n;
        }
        if(this.fwCtx.debug.ldap){
            this.fwCtx.debug.println("Added "+set.size()+" out of "+n+" listeners with complicated filters");
        }
        var c=sr.getProperty(this.vj$.Constants.OBJECTCLASS); 
        for (var element,_$i0=0;_$i0<c.length;_$i0++){
            element=c[_$i0];
            this.addToSet(set,this.vj$.ServiceListenerState.OBJECTCLASS_IX,element);
        }
        var service_id=sr.getProperty(this.vj$.Constants.SERVICE_ID); 
        if(service_id!==null){
            this.addToSet(set,this.vj$.ServiceListenerState.SERVICE_ID_IX,service_id.toString());
        }
        var service_pid=sr.getProperty(this.vj$.Constants.SERVICE_PID);
        if(service_pid!==null){
            if(service_pid instanceof String){
                this.addToSet(set,this.vj$.ServiceListenerState.SERVICE_PID_IX,service_pid);
            }else if(isInstance(service_pid)){
                var sa=service_pid; 
                for (var element,_$i1=0;_$i1<sa.length;_$i1++){
                    element=sa[_$i1];
                    this.addToSet(set,this.vj$.ServiceListenerState.SERVICE_PID_IX,element);
                }
            }else if(vjo.getType('org.eclipse.vjet.vjo.java.util.Collection').isInstance(service_pid)){
                var pids=service_pid; 
                for (var pid,_$itr=pids.iterator();_$itr.hasNext();){
                    pid=_$itr.next();
                    this.addToSet(set,this.vj$.ServiceListenerState.SERVICE_PID_IX,pid);
                }
            }
        }
        return set;
    },
    
    addToSet:function(set,cache_ix,val){
        var l=this.cache[cache_ix].get(val); 
        if(l!==null){
            if(this.fwCtx.debug.ldap){
                this.fwCtx.debug.println(this.vj$.ServiceListenerState.hashedKeys[cache_ix]+" matches "+l.size());
            }
            set.addAll(l);
        }else {
            if(this.fwCtx.debug.ldap){
                this.fwCtx.debug.println(this.vj$.ServiceListenerState.hashedKeys[cache_ix]+" matches none");
            }
        }
    }
})
.inits(function(){
    this.vj$.ServiceListenerState.hashedKeys=[org.eclipse.vjet.vjo.java.lang.StringUtil.toLowerCase(this.vj$.Constants.OBJECTCLASS),org.eclipse.vjet.vjo.java.lang.StringUtil.toLowerCase(this.vj$.Constants.SERVICE_ID),org.eclipse.vjet.vjo.java.lang.StringUtil.toLowerCase(this.vj$.Constants.SERVICE_PID)];
})
.endType();