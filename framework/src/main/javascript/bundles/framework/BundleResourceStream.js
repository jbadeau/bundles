/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleResourceStream') 
.inherits('org.eclipse.vjet.vjo.java.io.InputStream')
.protos({
    wis:null, 
    contentLen:0, 
    
    constructs:function(is,length){
        this.base();
        this.wis=is;
        this.contentLen=length;
    },
    
    getContentLength:function(){
        return this.contentLen;
    },
    
    
    
    read:function(){
        if(arguments.length===0){
            if(arguments.length==0){
                return this.read_0_0_BundleResourceStream_ovld();
            }else if(this.base && this.base.read){
                return this.base.read.apply(this,arguments);
            }
        }else if(arguments.length===1){
            if(arguments[0] instanceof Array){
                return this.read_1_0_BundleResourceStream_ovld(arguments[0]);
            }else if(this.base && this.base.read){
                return this.base.read.apply(this,arguments);
            }
        }else if(arguments.length===3){
            if(arguments[0] instanceof Array && typeof arguments[1]=="number" && typeof arguments[2]=="number"){
                return this.read_3_0_BundleResourceStream_ovld(arguments[0],arguments[1],arguments[2]);
            }else if(this.base && this.base.read){
                return this.base.read.apply(this,arguments);
            }
        }else if(this.base && this.base.read){
            return this.base.read.apply(this,arguments);
        }
    },
    
    read_0_0_BundleResourceStream_ovld:function(){
        return this.wis.read();
    },
    
    read_1_0_BundleResourceStream_ovld:function(dest){
        return this.wis.read(dest);
    },
    
    read_3_0_BundleResourceStream_ovld:function(dest,off,len){
        return this.wis.read(dest,off,len);
    },
    
    skip:function(len){
        return this.wis.skip(len);
    },
    
    available:function(){
        return this.wis.available();
    },
    
    close:function(){
        this.wis.close();
    },
    
    mark:function(readlimit){
        this.wis.mark(readlimit);
    },
    
    reset:function(){
        this.wis.reset();
    },
    
    markSupported:function(){
        return this.wis.markSupported();
    }
})
.endType();