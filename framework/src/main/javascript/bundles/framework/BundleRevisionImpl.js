/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleRevisionImpl') 
.needs(['org.eclipse.vjet.vjo.java.util.ArrayList','bundles.framework.BundleWiringImpl',
    'osgi.framework.namespace.IdentityNamespace','vjo.java.lang.ObjectUtil'])

.inherits('bundles.framework.BundleReferenceImpl')
.satisfies('osgi.framework.wiring.BundleRevision')
.props({
    NS_BUNDLE:1, 
    NS_HOST:2, 
    NS_IDENTITY:4, 
    NS_PACKAGE:8, 
    NS_OTHER:16, 
    
    whichNameSpaces:function(namespace){
        var ns; 
        if(namespace===null){
            ns=this.NS_BUNDLE|this.NS_HOST|this.NS_IDENTITY|this.NS_PACKAGE|this.NS_OTHER;
        }else if(vjo.java.lang.ObjectUtil.equals(this.vj$.BundleRevision.BUNDLE_NAMESPACE,namespace)){
            ns=this.NS_BUNDLE;
        }else if(vjo.java.lang.ObjectUtil.equals(this.vj$.BundleRevision.HOST_NAMESPACE,namespace)){
            ns=this.NS_HOST;
        }else if(vjo.java.lang.ObjectUtil.equals(this.vj$.IdentityNamespace.IDENTITY_NAMESPACE,namespace)){
            ns=this.NS_IDENTITY;
        }else if(vjo.java.lang.ObjectUtil.equals(this.vj$.BundleRevision.PACKAGE_NAMESPACE,namespace)){
            ns=this.NS_PACKAGE;
        }else {
            ns=this.NS_OTHER;
        }
        return ns;
    }
})
.protos({
    gen:null, 
    bundleWiring:null, 
    
    constructs:function(gen){
        this.base(gen.bundle);
        this.gen=gen;
    },
    
    getSymbolicName:function(){
        return this.gen.symbolicName;
    },
    
    getVersion:function(){
        return this.gen.version;
    },
    
    getDeclaredCapabilities:function(namespace){
        var res=new this.vj$.ArrayList(); 
        var ns=this.vj$.BundleRevisionImpl.whichNameSpaces(namespace); 
        if((ns&this.vj$.BundleRevisionImpl.NS_BUNDLE)!==0){
            var bc=this.gen.getBundleCapability(); 
            if(bc!==null){
                res.add(bc);
            }
        }
        if((ns&this.vj$.BundleRevisionImpl.NS_HOST)!==0){
            var bc=this.gen.getHostCapability(); 
            if(bc!==null){
                res.add(bc);
            }
        }
        if((ns&this.vj$.BundleRevisionImpl.NS_IDENTITY)!==0){
            var bc=this.gen.getIdentityCapability(); 
            if(bc!==null){
                res.add(bc);
            }
        }
        if((ns&this.vj$.BundleRevisionImpl.NS_PACKAGE)!==0){
            res.addAll(this.gen.bpkgs.getDeclaredPackageCapabilities());
        }
        if((ns&this.vj$.BundleRevisionImpl.NS_OTHER)!==0){
            var caps=this.gen.getDeclaredCapabilities(); 
            if(null!==namespace){
                var lcap=caps.get(namespace); 
                if(lcap!==null){
                    res.addAll(lcap);
                }
            }else {
                for (var lcap,_$itr=caps.values().iterator();_$itr.hasNext();){
                    lcap=_$itr.next();
                    res.addAll(lcap);
                }
            }
        }
        return res;
    },
    
    getDeclaredRequirements:function(namespace){
        var res=new this.vj$.ArrayList(); 
        var ns=this.vj$.BundleRevisionImpl.whichNameSpaces(namespace); 
        if((ns&this.vj$.BundleRevisionImpl.NS_BUNDLE)!==0){
            res.addAll(this.gen.bpkgs.getDeclaredBundleRequirements());
        }
        if((ns&this.vj$.BundleRevisionImpl.NS_HOST)!==0){
            if(this.gen.isFragment()){
                res.add(this.gen.fragment);
            }
        }
        if((ns&this.vj$.BundleRevisionImpl.NS_PACKAGE)!==0){
            res.addAll(this.gen.bpkgs.getDeclaredPackageRequirements());
        }
        if((ns&(this.vj$.BundleRevisionImpl.NS_IDENTITY|this.vj$.BundleRevisionImpl.NS_OTHER))!==0){
            var reqs=this.gen.getDeclaredRequirements(); 
            if(null!==namespace){
                var lbr=reqs.get(namespace); 
                if(lbr!==null){
                    res.addAll(lbr);
                }
            }else {
                for (var lbr,_$itr=reqs.values().iterator();_$itr.hasNext();){
                    lbr=_$itr.next();
                    res.addAll(lbr);
                }
            }
        }
        return res;
    },
    
    getCapabilities:function(namespace){
        return this.getDeclaredCapabilities(namespace); 
    },
    
    getRequirements:function(namespace){
        return this.getDeclaredRequirements(namespace); 
    },
    
    getTypes:function(){
        return this.gen.isFragment()?this.vj$.BundleRevision.TYPE_FRAGMENT:0;
    },
    
    getWiring:function(){
        return this.bundleWiring;
    },
    
    toString:function(){
        return "BundleRevision["+this.getSymbolicName()+":"+this.getVersion()+"]";
    },
    
    getBundleGeneration:function(){
        return this.gen;
    },
    
    setWired:function(){
        this.bundleWiring=new this.vj$.BundleWiringImpl(this);
    },
    
    clearWiring:function(){
        this.bundleWiring=null;
    }
})
.endType();