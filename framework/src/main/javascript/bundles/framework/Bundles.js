/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.Bundles') 
.needs(['org.eclipse.vjet.vjo.java.util.Hashtable','org.eclipse.vjet.vjo.java.util.HashSet',
    'org.eclipse.vjet.vjo.java.io.InputStream','org.eclipse.vjet.vjo.java.lang.Exception',
    'org.eclipse.vjet.vjo.java.util.Enumeration','org.eclipse.vjet.vjo.java.util.Collection',
    'org.eclipse.vjet.vjo.java.util.ArrayList','org.eclipse.vjet.vjo.java.util.List',
    'org.eclipse.vjet.vjo.java.util.HashMap','org.eclipse.vjet.vjo.java.lang.IllegalStateException',
    'bundles.framework.BundleImpl','bundles.framework.FrameworkContext',
    'osgi.framework.Bundle','osgi.framework.BundleException',
    'bundles.framework.BundleArchive','java.net.URL',
    'java.net.URLConnection','bundles.framework.Util',
    'osgi.framework.BundleEvent','osgi.framework.Version',
    'osgi.framework.Constants','bundles.framework.BundleGeneration',
    'osgi.framework.VersionRange','java.io.IOException',
    'org.eclipse.vjet.vjo.java.lang.System','vjo.java.lang.ObjectUtil'])
.protos({
    bundles:null, 
    zombies:null, 
    fwCtx:null, 
    
    constructs:function(fw){
        this.bundles=new this.vj$.Hashtable();
        this.zombies=new this.vj$.HashSet();
        this.fwCtx=fw;
        this.vj$.Bundles.put(fw.systemBundle.location,fw.systemBundle);
    },
    
    clear:function(){
        this.vj$.Bundles.clear();
        this.fwCtx=null;
    },
    
    install:function(location,in_,caller){
        this.checkIllegalState();
        var b; 
        {
            b=this.vj$.Bundles.get(location);
            if(b!==null){
                b=b.fwCtx.bundleHooks.filterBundle(b.bundleContext,b); 
                if(b===null){
                    throw new this.vj$.BundleException("Rejected by a bundle hook",this.vj$.BundleException.REJECTED_BY_HOOK);
                }else {
                    return b;
                }
            }
            b=this.fwCtx.perm.callInstall0(this,location,in_,caller);
        }
        return b;
    },
    
    install0:function(location,in_,checkContext,caller){
        var bin; 
        var ba=null; 
        try {
            if(in_===null){
                var url=new this.vj$.URL(location); 
                var conn=url.openConnection(); 
                var auth=this.fwCtx.props.getProperty("http.proxyAuth"); 
                if(auth!==null&& !vjo.java.lang.ObjectUtil.equals("",auth)){
                    if(vjo.java.lang.ObjectUtil.equals("http",url.getProtocol())||vjo.java.lang.ObjectUtil.equals("https",url.getProtocol())){
                        var base64=this.vj$.Util.base64Encode(auth); 
                        conn.setRequestProperty("Proxy-Authorization","Basic "+base64);
                    }
                }
                var basicAuth=this.fwCtx.props.getProperty("http.basicAuth"); 
                if(basicAuth!==null&& !vjo.java.lang.ObjectUtil.equals("",basicAuth)){
                    if(vjo.java.lang.ObjectUtil.equals("http",url.getProtocol())||vjo.java.lang.ObjectUtil.equals("https",url.getProtocol())){
                        var base64=this.vj$.Util.base64Encode(basicAuth); 
                        conn.setRequestProperty("Authorization","Basic "+base64);
                    }
                }
                bin=conn.getInputStream();
            }else {
                bin=in_;
            }
            try {
                ba=this.fwCtx.storage.insertBundleJar(location,bin);
            }
            finally {
                bin.close();
            }
            var res=new this.vj$.BundleImpl(this.fwCtx,ba,checkContext,caller); 
            this.vj$.Bundles.put(location,res);
            this.fwCtx.listeners.bundleChanged(new this.vj$.BundleEvent(this.vj$.BundleEvent.INSTALLED,res,caller));
            return res;
        }
        catch(e){
            if(ba!==null){
                ba.purge();
            }
            if(SecurityException.isInstance(e)){
                throw e;
            }else if(osgi.framework.BundleException.isInstance(e)){
                throw e;
            }else {
                throw new this.vj$.BundleException("Failed to install bundle: "+e,this.vj$.BundleException.UNSPECIFIED,e);
            }
        }
    },
    
    remove:function(location){
        this.vj$.Bundles.remove(location);
    },
    
    addZombie:function(b){
        {
            this.zombies.add(b);
        }
    },
    
    removeZombie:function(b){
        {
            this.zombies.remove(b);
        }
    },
    
    
    getBundle:function(id){
        if(arguments.length===1){
            if(typeof arguments[0]=="number"){
                return this.getBundle_1_0_Bundles_ovld(arguments[0]);
            }else if(arguments[0] instanceof String || typeof arguments[0]=="string"){
                return this.getBundle_1_1_Bundles_ovld(arguments[0]);
            }
        }
    },
    
    getBundle_1_0_Bundles_ovld:function(id){
        this.checkIllegalState();
        {
            for (var e=this.vj$.Bundles.elements();e.hasMoreElements();){
                var b=e.nextElement(); 
                if(b.id===id){
                    return b;
                }
            }
        }
        return null;
    },
    
    getBundle_1_1_Bundles_ovld:function(location){
        this.checkIllegalState();
        return this.vj$.Bundles.get(location);
    },
    
    
    
    getBundles:function(name,version){
        if(arguments.length===2){
            if((arguments[0] instanceof String || typeof arguments[0]=="string") && arguments[1] instanceof osgi.framework.Version){
                return this.getBundles_2_0_Bundles_ovld(arguments[0],arguments[1]);
            }else if((arguments[0] instanceof String || typeof arguments[0]=="string") && arguments[1] instanceof osgi.framework.VersionRange){
                return this.getBundles_2_1_Bundles_ovld(arguments[0],arguments[1]);
            }
        }else if(arguments.length===0){
            return this.getBundles_0_0_Bundles_ovld();
        }
    },
    
    getBundles_2_0_Bundles_ovld:function(name,version){
        this.checkIllegalState();
        var res=new this.vj$.ArrayList(this.vj$.Bundles.size()); 
        if(vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.SYSTEM_BUNDLE_SYMBOLICNAME,name)&&org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(version,this.fwCtx.systemBundle.getVersion())){
            res.add(this.fwCtx.systemBundle);
        }
        {
            for (var e=this.vj$.Bundles.elements();e.hasMoreElements();){
                var b=e.nextElement(); 
                if(vjo.java.lang.ObjectUtil.equals(name,b.getSymbolicName())&&org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(version,b.getVersion())){
                    res.add(b);
                }
            }
        }
        return res;
    },
    
    getBundles_0_0_Bundles_ovld:function(){
        var res=new this.vj$.ArrayList(this.vj$.Bundles.size()); 
        {
            res.addAll(this.vj$.Bundles.values());
        }
        return res;
    },
    
    getBundleGenerations:function(name){
        var res=new this.vj$.ArrayList(); 
        if(vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.SYSTEM_BUNDLE_SYMBOLICNAME,name)){
            res.add(this.fwCtx.systemBundle.current());
        }
    },
    
    getBundles_2_1_Bundles_ovld:function(name,range){
    },
    
    getActiveBundles:function(){
    },
    
    getRemovalPendingBundles:function(res){
    },
    
    getUnattachedBundles:function(res){
    },
    
    checkExtensionBundleRestart:function(){
    },
    
    load:function(){
    },
    
    getFragmentBundles:function(target){
    },
    
    checkIllegalState:function(){
    }
})
.endType();