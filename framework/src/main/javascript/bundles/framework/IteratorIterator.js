/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.IteratorIterator<A>') 

.satisfies('org.eclipse.vjet.vjo.java.util.Iterator<A>')
.protos({
    iter:null, 
    current:null, 
    
    constructs:function(ilist){
        this.iter=ilist.iterator();
        if(this.iter.hasNext()){
            this.current=this.iter.next();
        }else {
            var empty=this.iter; 
            this.current=empty;
        }
    },
    
    hasNext:function(){
        return this.getIterator().hasNext();
    },
    
    remove:function(){
        this.current.remove();
    },
    
    next:function(){
        return this.getIterator().next();
    },
    
    getIterator:function(){
        while(!this.current.hasNext() && this.iter.hasNext()){
            this.current=this.iter.next();
        }
        return this.current;
    }
})
.endType();