/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.ServiceHooks') 
.needs(['org.eclipse.vjet.vjo.java.util.Collection','org.eclipse.vjet.vjo.java.lang.Exception',
    'org.eclipse.vjet.vjo.java.util.List','org.eclipse.vjet.vjo.java.util.HashSet',
    'org.eclipse.vjet.vjo.java.util.Iterator','org.eclipse.vjet.vjo.java.util.HashMap',
    'org.eclipse.vjet.vjo.java.util.ArrayList','org.eclipse.vjet.vjo.java.util.Collections',
    'org.eclipse.vjet.vjo.java.util.Set','org.eclipse.vjet.vjo.java.util.Map',
    'bundles.framework.FrameworkContext','osgi.util.tracker.ServiceTracker',
    'osgi.framework.hooks.service.ListenerHook','osgi.util.tracker.ServiceTrackerCustomizer',
    'osgi.framework.ServiceReference','bundles.framework.ServiceListenerEntry',
    'osgi.framework.Constants','bundles.framework.BundleContextImpl',
    'bundles.framework.ServiceRegistrationImpl','osgi.framework.hooks.service.FindHook',
    'bundles.framework.RemoveOnlyCollection','bundles.framework.ServiceReferenceImpl',
    'osgi.framework.BundleException','osgi.framework.ServiceEvent',
    'osgi.framework.BundleContext','osgi.framework.hooks.service.EventListenerHook'])
.needs('osgi.framework.hooks.service.EventHook','')
.props({
    RemoveOnlyMap:vjo.ctype() 
    .satisfies('org.eclipse.vjet.vjo.java.util.Map<K,V>')
    .protos({
        original:null, 
        
        constructs:function(original){
            this.original=original;
        },
        
        clear:function(){
            this.original.clear();
        },
        
        containsKey:function(k){
            return this.original.containsKey(k);
        },
        
        containsValue:function(v){
            return this.original.containsValue(v);
        },
        
        entrySet:function(){
            return this.original.entrySet();
        },
        
        get:function(k){
            return this.original.get(k);
        },
        
        isEmpty:function(){
            return this.original.isEmpty();
        },
        
        keySet:function(){
            return this.original.keySet();
        },
        
        put:function(k,v){
            throw new UnsupportedOperationException("objects can only be removed");
        },
        
        putAll:function(m){
            throw new UnsupportedOperationException("objects can only be removed");
        },
        
        remove:function(k){
            return this.original.remove(k);
        },
        
        size:function(){
            return this.original.size();
        },
        
        values:function(){
            return this.original.values();
        }
    })
    .endType(),
    
    toImmutableSet:function(obj){
        var set=new this.vj$.HashSet(); 
        set.add(obj);
        set=this.vj$.Collections.unmodifiableSet(set);
        return set;
    }
})
.protos({
    fwCtx:null, 
    listenerHookTracker:null, 
    bOpen:false, 
    
    constructs:function(fwCtx){
        this.fwCtx=fwCtx;
    },
    
    open:function(){
        if(this.fwCtx.debug.hooks){
            this.fwCtx.debug.println("opening hooks");
        }
        this.listenerHookTracker=new this.vj$.ServiceTracker(this.fwCtx.systemBundle.bundleContext,this.vj$.ListenerHook.clazz,
            vjo.make(this,this.vj$.ServiceTrackerCustomizer)
            .protos({
                addingService:function(reference){
                    var lh=this.vj$.parent.fwCtx.systemBundle.bundleContext.getService(reference); 
                    try {
                        var c=getServiceCollection(); 
                        var li=c; 
                        lh.added(li);
                    }
                    catch(e){
                        this.vj$.parent.fwCtx.debug.printStackTrace("Failed to call listener hook  #"+reference.getProperty(this.vj$.Constants.SERVICE_ID),e);
                    }
                    return lh;
                    var lh=this.vj$.parent.fwCtx.systemBundle.bundleContext.getService(reference); 
                    try {
                        var c=getServiceCollection(); 
                        var li=c; 
                        lh.added(li);
                    }
                    catch(e){
                        this.vj$.parent.fwCtx.debug.printStackTrace("Failed to call listener hook  #"+reference.getProperty(this.vj$.Constants.SERVICE_ID),e);
                    }
                    return lh;
                },
                modifiedService:function(reference,service){
                },
                removedService:function(reference,service){
                    this.vj$.parent.fwCtx.systemBundle.bundleContext.ungetService(reference);
                    this.vj$.parent.fwCtx.systemBundle.bundleContext.ungetService(reference);
                }
            })
            .endType());
        this.listenerHookTracker.open();
        this.bOpen=true;
    },
    
    close:function(){
        this.listenerHookTracker.close();
        this.listenerHookTracker=null;
        this.bOpen=false;
    },
    
    isOpen:function(){
        return this.bOpen;
    },
    
    filterServiceReferences:function(bc,service,filter,allServices,refs){
        var srl=this.fwCtx.services.get(this.vj$.FindHook.clazz.getName()); 
        if(srl!==null){
            var filtered=new this.vj$.RemoveOnlyCollection(refs); 
            for (var fhr,_$itr=srl.iterator();_$itr.hasNext();){
                fhr=_$itr.next();
                var sr=fhr.reference; 
                var fh=sr.getService(this.fwCtx.systemBundle); 
                if(fh!==null){
                    try {
                        fh.find(bc,service,filter,allServices,filtered);
                    }
                    catch(e){
                        this.fwCtx.frameworkError(bc,new this.vj$.BundleException("Failed to call find hook  #"+sr.getProperty(this.vj$.Constants.SERVICE_ID),e));
                    }
                }
            }
        }
    },
    
    filterServiceEventReceivers:function(evt,receivers){
        var eventHooks=this.fwCtx.services.get(osgi.framework.hooks.service.EventHook.clazz.getName()); 
        if(eventHooks!==null){
            var ctxs=new this.vj$.HashSet(); 
            for (var sle,_$itr=receivers.iterator();_$itr.hasNext();){
                sle=_$itr.next();
                ctxs.add(sle.getBundleContext());
            }
            var start_size=ctxs.size(); 
            var filtered=new this.vj$.RemoveOnlyCollection(ctxs); 
            for (var sregi,_$itr=eventHooks.iterator();_$itr.hasNext();){
                sregi=_$itr.next();
                var sr=sregi.reference; 
                var eh=sr.getService(this.fwCtx.systemBundle); 
                if(eh!==null){
                    try {
                        eh.event(evt,filtered);
                    }
                    catch(e){
                        this.fwCtx.debug.printStackTrace("Failed to call event hook  #"+sr.getProperty(this.vj$.Constants.SERVICE_ID),e);
                    }
                }
            }
            if(start_size!==ctxs.size()){
                for (var ir=receivers.iterator();ir.hasNext();){
                    if(!ctxs.contains(ir.next().getBundleContext())){
                        ir.remove();
                    }
                }
            }
        }
        var eventListenerHooks=this.fwCtx.services.get(this.vj$.EventListenerHook.clazz.getName()); 
        if(eventListenerHooks!==null){
            var listeners=new this.vj$.HashMap(); 
            for (var sle,_$itr=receivers.iterator();_$itr.hasNext();){
                sle=_$itr.next();
                if(!listeners.containsKey(sle.getBundleContext())){
                    listeners.put(sle.getBundleContext(),new this.vj$.ArrayList());
                }
                listeners.get(sle.getBundleContext()).add(sle);
            }
            for (var e,_$itr=listeners.entrySet().iterator();_$itr.hasNext();){
                e=_$itr.next();
                e.setValue(new this.vj$.RemoveOnlyCollection(e.getValue()));
            }
            var filtered=new this.vj$.ServiceHooks.RemoveOnlyMap(listeners); 
            for (var sri,_$itr=eventListenerHooks.iterator();_$itr.hasNext();){
                sri=_$itr.next();
                var elh=sri.reference.getService(this.fwCtx.systemBundle); 
                if(elh!==null){
                    try {
                        elh.event(evt,filtered);
                    }
                    catch(e){
                        this.fwCtx.debug.printStackTrace("Failed to call event hook  #"+sri.reference.getProperty(this.vj$.Constants.SERVICE_ID),e);
                    }
                }
            }
            receivers.clear();
            for (var li,_$itr=listeners.values().iterator();_$itr.hasNext();){
                li=_$itr.next();
                var sles=li; 
                receivers.addAll(sles);
            }
        }
    },
    
    getServiceCollection:function(){
        return this.vj$.Collections.unmodifiableSet(this.fwCtx.listeners.serviceListeners.serviceSet);
    },
    
    handleServiceListenerReg:function(sle){
        if(!this.isOpen()||this.listenerHookTracker.size()===0){
            return;
        }
        var srl=this.listenerHookTracker.getServiceReferences(); 
        var set=this.vj$.ServiceHooks.toImmutableSet(/*>>*/sle); 
        if(srl!==null){
            for (var sr,_$i0=0;_$i0<srl.length;_$i0++){
                sr=srl[_$i0];
                var lh=this.listenerHookTracker.getService(sr); 
                try {
                    lh.added(set);
                }
                catch(e){
                    this.fwCtx.debug.printStackTrace("Failed to call listener hook #"+sr.getProperty(this.vj$.Constants.SERVICE_ID),e);
                }
            }
        }
    },
    
    
    handleServiceListenerUnreg:function(sle){
        if(arguments.length===1){
            if(arguments[0] instanceof bundles.framework.ServiceListenerEntry){
                this.handleServiceListenerUnreg_1_0_ServiceHooks_ovld(arguments[0]);
            }else if(org.eclipse.vjet.vjo.java.util.Collection.clazz.isInstance(arguments[0])){
                this.handleServiceListenerUnreg_1_1_ServiceHooks_ovld(arguments[0]);
            }
        }
    },
    
    handleServiceListenerUnreg_1_0_ServiceHooks_ovld:function(sle){
        if(this.isOpen()){
            handleServiceListenerUnreg(this.vj$.ServiceHooks.toImmutableSet(sle));
        }
    },
    
    handleServiceListenerUnreg_1_1_ServiceHooks_ovld:function(set){
        if(!this.isOpen()||this.listenerHookTracker.size()===0){
            return;
        }
        var srl=this.listenerHookTracker.getServiceReferences(); 
        if(srl!==null){
            var lis=set; 
            for (var sr,_$i1=0;_$i1<srl.length;_$i1++){
                sr=srl[_$i1];
                var lh=this.listenerHookTracker.getService(sr); 
                try {
                    lh.removed(lis);
                }
                catch(e){
                    this.fwCtx.debug.printStackTrace("Failed to call listener hook #"+sr.getProperty(this.vj$.Constants.SERVICE_ID),e);
                }
            }
        }
    }
})
.endType();