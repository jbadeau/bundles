/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleRevisionsImpl') 
.needs('org.eclipse.vjet.vjo.java.util.ArrayList')

.inherits('bundles.framework.BundleReferenceImpl')
.satisfies('osgi.framework.wiring.BundleRevisions')
.protos({
    generations:null, 
    
    constructs:function(generations){
        this.base(generations.get(0).bundle);
        this.generations=generations;
    },
    
    getRevisions:function(){
        {
            var res=new this.vj$.ArrayList(this.generations.size()); 
            for (var bg,_$itr=this.generations.iterator();_$itr.hasNext();){
                bg=_$itr.next();
                if(!bg.isUninstalled()){
                    res.add(bg.bundleRevision);
                }
            }
            return res;
        }
    }
})
.endType();