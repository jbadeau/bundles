/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.RequireBundle') 
.needs(['org.eclipse.vjet.vjo.java.util.Map','org.eclipse.vjet.vjo.java.lang.IllegalArgumentException',
    'org.eclipse.vjet.vjo.java.util.Collections','org.eclipse.vjet.vjo.java.lang.StringBuffer',
    'org.eclipse.vjet.vjo.java.lang.RuntimeException','bundles.framework.BundlePackages',
    'osgi.framework.VersionRange','bundles.framework.Util',
    'osgi.framework.Constants','osgi.framework.Filter',
    'osgi.framework.wiring.BundleRevision','osgi.framework.FrameworkUtil',
    'osgi.framework.InvalidSyntaxException','osgi.framework.wiring.BundleCapability',
    'org.eclipse.vjet.vjo.java.lang.StringUtil','vjo.java.lang.ObjectUtil'])
.satisfies('osgi.framework.wiring.BundleRequirement')
.satisfies('Comparable<RequireBundle>')
.props({
    requireBundleCount:0 
})
.protos({
    orderal:0, 
    requestor:null, 
    name:null, 
    visibility:null, 
    resolution:null, 
    bundleRange:null, 
    bpkgs:null, 
    attributes:null, 
    directives:null, 
    
    
    
    constructs:function(){
        this.orderal=++this.vj$.RequireBundle.requireBundleCount;
        if(arguments.length===2){
            if(arguments[0] instanceof bundles.framework.RequireBundle && arguments[1] instanceof bundles.framework.BundlePackages){
                this.constructs_2_0_RequireBundle_ovld(arguments[0],arguments[1]);
            }else if(arguments[0] instanceof bundles.framework.BundlePackages && arguments[1] instanceof bundles.framework.Util.HeaderEntry){
                this.constructs_2_1_RequireBundle_ovld(arguments[0],arguments[1]);
            }
        }
    },
    
    constructs_2_0_RequireBundle_ovld:function(parent,requestor){
        this.requestor=requestor;
        this.name=parent.name;
        this.visibility=parent.visibility;
        this.resolution=parent.resolution;
        this.bundleRange=parent.bundleRange;
        this.attributes=parent.attributes;
        this.directives=parent.directives;
    },
    
    constructs_2_1_RequireBundle_ovld:function(requestor,he){
        this.requestor=requestor;
        this.name=he.getKey();
        var dirs=he.getDirectives(); 
        var visibility=dirs.get(this.vj$.Constants.VISIBILITY_DIRECTIVE); 
        if(visibility!==null){
            this.visibility=org.eclipse.vjet.vjo.java.lang.StringUtil.intern(visibility);
            if(this.visibility!==this.vj$.Constants.VISIBILITY_PRIVATE&&this.visibility!==this.vj$.Constants.VISIBILITY_REEXPORT){
                throw new this.vj$.IllegalArgumentException("Invalid directive : '"+this.vj$.Constants.VISIBILITY_DIRECTIVE+":="+this.visibility+"' in manifest header '"+this.vj$.Constants.REQUIRE_BUNDLE+": "+this.name+"' of bundle with id "+this.requestor.bg.bundle.getBundleId()+" ("+this.requestor.bg.symbolicName+")"+". The value must be either '"+this.vj$.Constants.VISIBILITY_PRIVATE+"' or '"+this.vj$.Constants.VISIBILITY_REEXPORT+"'.");
            }
        }else {
            this.visibility=this.vj$.Constants.VISIBILITY_PRIVATE;
        }
        var resolution=dirs.get(this.vj$.Constants.RESOLUTION_DIRECTIVE); 
        if(resolution!==null){
            this.resolution=org.eclipse.vjet.vjo.java.lang.StringUtil.intern(resolution);
            if(this.resolution!==this.vj$.Constants.RESOLUTION_MANDATORY&&this.resolution!==this.vj$.Constants.RESOLUTION_OPTIONAL){
                throw new this.vj$.IllegalArgumentException("Invalid directive : '"+this.vj$.Constants.RESOLUTION_DIRECTIVE+":="+this.resolution+"' in manifest header '"+this.vj$.Constants.REQUIRE_BUNDLE+": "+this.name+"' of bundle with id "+this.requestor.bg.bundle.getBundleId()+" ("+this.requestor.bg.symbolicName+")"+". The value must be either '"+this.vj$.Constants.RESOLUTION_MANDATORY+"' or '"+this.vj$.Constants.RESOLUTION_OPTIONAL+"'.");
            }
        }else {
            this.resolution=this.vj$.Constants.RESOLUTION_MANDATORY;
        }
        this.attributes=he.getAttributes();
        var range=this.attributes.remove(this.vj$.Constants.BUNDLE_VERSION_ATTRIBUTE); 
        if(range!==null){
            this.bundleRange=new this.vj$.VersionRange(range);
        }else {
            this.bundleRange=null;
        }
        var filter=this.toFilter(); 
        if(null!==filter){
            dirs.put(this.vj$.Constants.FILTER_DIRECTIVE,filter.toString());
        }
        this.directives=this.vj$.Collections.unmodifiableMap(dirs);
    },
    
    overlap:function(rb){
        if(vjo.java.lang.ObjectUtil.equals(this.visibility,this.vj$.Constants.VISIBILITY_REEXPORT)&& !vjo.java.lang.ObjectUtil.equals(rb.visibility,this.vj$.Constants.VISIBILITY_REEXPORT)){
            return false;
        }
        if(vjo.java.lang.ObjectUtil.equals(this.resolution,this.vj$.Constants.RESOLUTION_MANDATORY)&& !vjo.java.lang.ObjectUtil.equals(rb.resolution,this.vj$.Constants.RESOLUTION_MANDATORY)){
            return false;
        }
        return this.bundleRange===null|| !this.bundleRange.intersection(rb.bundleRange).isEmpty();
    },
    
    getNamespace:function(){
        return this.vj$.BundleRevision.BUNDLE_NAMESPACE;
    },
    
    getDirectives:function(){
        return this.directives;
    },
    
    toFilter:function(){
        var sb=new this.vj$.StringBuffer(80); 
        var multipleConditions=false; 
        sb.append('(');
        sb.append(this.vj$.BundleRevision.BUNDLE_NAMESPACE);
        sb.append('=');
        sb.append(this.name);
        sb.append(')');
        if(this.bundleRange!==null){
            sb.append(this.bundleRange.toFilterString(this.vj$.Constants.BUNDLE_VERSION_ATTRIBUTE));
            multipleConditions=true;
        }
        for (var entry,_$itr=this.attributes.entrySet().iterator();_$itr.hasNext();){
            entry=_$itr.next();
            sb.append('(');
            sb.append(entry.getKey());
            sb.append('=');
            sb.append(entry.getValue().toString());
            sb.append(')');
            multipleConditions=true;
        }
        if(multipleConditions){
            sb.insert(0,"(&");
            sb.append(')');
        }
        try {
            return this.vj$.FrameworkUtil.createFilter(sb.toString());
        }
        catch(_ise){
            throw new this.vj$.RuntimeException("Internal error, createFilter: '"+sb.toString()+"': "+_ise.getMessage());
        }
    },
    
    getAttributes:function(){
        return this.vj$.Collections.EMPTY_MAP;
    },
    
    getRevision:function(){
        return this.requestor.bg.bundleRevision;
    },
    
    getResource:function(){
        return this.requestor.bg.bundleRevision;
    },
    
    matches:function(capability){
        if(vjo.java.lang.ObjectUtil.equals(this.vj$.BundleRevision.BUNDLE_NAMESPACE,capability.getNamespace())){
            return this.toFilter().matches(capability.getAttributes());
        }
        return false;
    },
    
    compareTo:function(o){
        return this.orderal-o.orderal;
    }
})
.endType();