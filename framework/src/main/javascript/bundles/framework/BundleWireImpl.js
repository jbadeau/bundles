/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleWireImpl') 
.needs('org.eclipse.vjet.vjo.java.lang.ObjectUtil')

.satisfies('osgi.framework.wiring.BundleWire')
.protos({
    capability:null, 
    requirement:null, 
    providerGen:null, 
    requirerGen:null, 
    
    constructs:function(capability,provider,requirement,requirer){
        this.capability=capability;
        this.providerGen=provider;
        this.requirement=requirement;
        this.requirerGen=requirer;
    },
    
    getCapability:function(){
        return this.capability;
    },
    
    getRequirement:function(){
        return this.requirement;
    },
    
    getProviderWiring:function(){
        return this.providerGen.bundleRevision.getWiring();
    },
    
    getRequirerWiring:function(){
        return this.requirerGen.bundleRevision.getWiring();
    },
    
    getProvider:function(){
        return this.providerGen.bundleRevision;
    },
    
    getRequirer:function(){
        return this.requirerGen.bundleRevision;
    },
    
    hashCode:function(){
        var prime=31; 
        var result=1; 
        result=prime*result+((this.capability===null)?0:this.capability.hashCode());
        result=prime*result+((this.providerGen===null)?0:org.eclipse.vjet.vjo.java.lang.ObjectUtil.hashCode(this.providerGen));
        result=prime*result+((this.requirement===null)?0:this.requirement.hashCode());
        result=prime*result+((this.requirerGen===null)?0:org.eclipse.vjet.vjo.java.lang.ObjectUtil.hashCode(this.requirerGen));
        return result;
    },
    
    equals:function(obj){
        if(this===obj){
            return true;
        }
        if(obj===null){
            return false;
        }
        if(!(bundles.framework.BundleWireImpl.isInstance(obj))){
            return false;
        }
        var other=obj; 
        if(this.capability===null){
            if(other.capability!==null){
                return false;
            }
        }else if(!this.capability.equals(other.capability)){
            return false;
        }
        if(this.providerGen===null){
            if(other.providerGen!==null){
                return false;
            }
        }else if(!org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(this.providerGen,other.providerGen)){
            return false;
        }
        if(this.requirement===null){
            if(other.requirement!==null){
                return false;
            }
        }else if(!this.requirement.equals(other.requirement)){
            return false;
        }
        if(this.requirerGen===null){
            if(other.requirerGen!==null){
                return false;
            }
        }else if(!org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(this.requirerGen,other.requirerGen)){
            return false;
        }
        return true;
    },
    
    getProviderGeneration:function(){
        return this.providerGen;
    },
    
    getRequirerGeneration:function(){
        return this.requirerGen;
    }
})
.endType();