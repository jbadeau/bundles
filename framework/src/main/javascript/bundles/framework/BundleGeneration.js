/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleGeneration') 
.needs(['org.eclipse.vjet.vjo.java.util.Map','org.eclipse.vjet.vjo.java.util.List',
    'org.eclipse.vjet.vjo.java.util.HashMap','org.eclipse.vjet.vjo.java.util.Set',
    'org.eclipse.vjet.vjo.java.util.Vector','org.eclipse.vjet.vjo.java.lang.System',
    'org.eclipse.vjet.vjo.java.lang.Exception','org.eclipse.vjet.vjo.java.lang.IllegalArgumentException',
    'org.eclipse.vjet.vjo.java.util.ArrayList','org.eclipse.vjet.vjo.java.util.Collection',
    'org.eclipse.vjet.vjo.java.lang.RuntimeException','org.eclipse.vjet.vjo.java.lang.IllegalStateException',
    'org.eclipse.vjet.vjo.java.util.Hashtable','org.eclipse.vjet.vjo.java.util.ConcurrentModificationException',
    'org.eclipse.vjet.vjo.java.util.Enumeration','org.eclipse.vjet.vjo.java.lang.StringBuffer',
    'org.eclipse.vjet.vjo.java.util.Iterator','org.eclipse.vjet.vjo.java.util.HashSet',
    'bundles.framework.BundleImpl','bundles.framework.BundlePackages',
    'bundles.framework.BundleArchive','bundles.framework.Util',
    'osgi.framework.Version','bundles.framework.HeaderDictionary',
    'java.security.ProtectionDomain','bundles.framework.BundleClassPath',
    'bundles.framework.BundleRevisionImpl','osgi.framework.wiring.BundleCapability',
    'osgi.framework.Constants','bundles.framework.BundleCapabilityImpl',
    'osgi.framework.Bundle','bundles.framework.BundleRequirementImpl',
    'osgi.framework.namespace.ExecutionEnvironmentNamespace','osgi.framework.hooks.bundle.CollisionHook',
    'osgi.framework.BundleException','java.io.IOException',
    'bundles.framework.BundleNameVersionCapability','osgi.framework.wiring.BundleRevision',
    'bundles.framework.BundleClassLoader','bundles.framework.BundleGeneration',
    'java.util.Dictionary','java.util.Locale',
    'java.net.URL','bundles.framework.BundleURLStreamHandler',
    'java.net.MalformedURLException','java.security.cert.X509Certificate',
    'bundles.framework.Validator','bundles.framework.FrameworkProperties',
    'bundles.framework.FileArchive','bundles.framework.BundleWireImpl',
    'org.eclipse.vjet.vjo.java.lang.StringUtil','vjo.java.lang.ObjectUtil'])
.needs('bundles.framework.Fragment','')
.satisfies('Comparable<BundleGeneration>')
.props({
    BUNDLES_SYMBOLICNAME:"org.bundles.framework" 
})
.protos({
    bundle:null, 
    bpkgs:null, 
    archive:null, 
    generation:0, 
    v2Manifest:false, 
    symbolicName:null, 
    symbolicNameParameters:null, 
    singleton:false, 
    version:null, 
    attachPolicy:null, 
    fragment:null, 
    requirements:null, 
    capabilities:null, 
    lazyActivation:false, 
    lazyIncludes:null, 
    lazyExcludes:null, 
    timeStamp:0, 
    fragments:null, 
    cachedRawHeaders:null, 
    classLoader:null, 
    protectionDomain:null, 
    unresolvedBundleClassPath:null, 
    bundleRevision:null, 
    identity:null, 
    
    
    
    
    constructs:function(){
        this.requirements=new this.vj$.HashMap();
        this.capabilities=new this.vj$.HashMap();
        if(arguments.length===3){
            this.constructs_3_0_BundleGeneration_ovld(arguments[0],arguments[1],arguments[2]);
        }else if(arguments.length===4){
            this.constructs_4_0_BundleGeneration_ovld(arguments[0],arguments[1],arguments[2],arguments[3]);
        }else if(arguments.length===1){
            this.constructs_1_0_BundleGeneration_ovld(arguments[0]);
        }
    },
    
    constructs_3_0_BundleGeneration_ovld:function(b,exportStr,capabilityStr){
        this.bundle=b;
        this.archive=null;
        this.generation=0;
        this.v2Manifest=true;
        this.symbolicName=this.vj$.BundleGeneration.BUNDLES_SYMBOLICNAME;
        this.symbolicNameParameters=null;
        this.singleton=false;
        this.version=new this.vj$.Version(this.vj$.Util.readFrameworkVersion());
        this.attachPolicy=this.vj$.Constants.FRAGMENT_ATTACHMENT_ALWAYS;
        bundles.framework.Fragment=null;
        this.protectionDomain=null;
        this.lazyActivation=false;
        this.lazyIncludes=null;
        this.lazyExcludes=null;
        this.timeStamp=this.vj$.System.currentTimeMillis();
        this.bpkgs=new this.vj$.BundlePackages(this,exportStr);
        this.bundleRevision=new this.vj$.BundleRevisionImpl(this);
        this.classLoader=b.getClassLoader();
        this.processCapabilities(capabilityStr);
        this.identity=new this.vj$.BundleCapabilityImpl(this);
    },
    
    constructs_4_0_BundleGeneration_ovld:function(b,ba,prev,caller){
        this.bundle=b;
        this.generation=(prev!==null?prev.generation:-1)+1;
        this.archive=ba;
        this.checkCertificates();
        var mv=this.archive.getAttribute(this.vj$.Constants.BUNDLE_MANIFESTVERSION); 
        this.v2Manifest=mv!==null&&org.eclipse.vjet.vjo.java.lang.StringUtil.trim(mv).equals("2");
        var mbv=this.archive.getAttribute(this.vj$.Constants.BUNDLE_VERSION); 
        var newVer=this.vj$.Version.emptyVersion; 
        if(mbv!==null){
            try {
                newVer=new this.vj$.Version(mbv);
            }
            catch(ee){
                if(this.v2Manifest){
                    throw new this.vj$.IllegalArgumentException("Bundle does not specify a valid "+this.vj$.Constants.BUNDLE_VERSION+" header. Got exception: "+ee.getMessage());
                }
            }
        }
        this.version=newVer;
        var hes=this.vj$.Util.parseManifestHeader(this.vj$.Constants.FRAGMENT_HOST,this.archive.getAttribute(this.vj$.Constants.FRAGMENT_HOST),true,true,true); 
        if(!hes.isEmpty()){
            bundles.framework.Fragment=new bundles.framework.Fragment(this,hes.get(0));
        }else {
            bundles.framework.Fragment=null;
        }
        hes=this.vj$.Util.parseManifestHeader(this.vj$.Constants.BUNDLE_ACTIVATIONPOLICY,this.archive.getAttribute(this.vj$.Constants.BUNDLE_ACTIVATIONPOLICY),true,true,true);
        if(!hes.isEmpty()){
            var he=hes.get(0); 
            this.lazyActivation=vjo.java.lang.ObjectUtil.equals(this.vj$.Constants.ACTIVATION_LAZY,he.getKey());
            if(this.lazyActivation){
                if(he.getDirectives().containsKey(this.vj$.Constants.INCLUDE_DIRECTIVE)){
                    this.lazyIncludes=this.vj$.Util.parseEnumeration(this.vj$.Constants.INCLUDE_DIRECTIVE,he.getDirectives().get(this.vj$.Constants.INCLUDE_DIRECTIVE));
                }else {
                    this.lazyIncludes=null;
                }
                if(he.getDirectives().containsKey(this.vj$.Constants.EXCLUDE_DIRECTIVE)){
                    this.lazyExcludes=this.vj$.Util.parseEnumeration(this.vj$.Constants.EXCLUDE_DIRECTIVE,he.getDirectives().get(this.vj$.Constants.EXCLUDE_DIRECTIVE));
                    if(this.lazyIncludes!==null){
                        for (var entry,_$itr=this.lazyExcludes.iterator();_$itr.hasNext();){
                            entry=_$itr.next();
                            if(this.lazyIncludes.contains(entry)){
                                throw new this.vj$.IllegalArgumentException("Conflicting "+this.vj$.Constants.INCLUDE_DIRECTIVE+"/"+this.vj$.Constants.EXCLUDE_DIRECTIVE+" entries in "+this.vj$.Constants.BUNDLE_ACTIVATIONPOLICY+": '"+entry+"' both included and excluded");
                            }
                        }
                    }
                }else {
                    this.lazyExcludes=null;
                }
            }else {
                this.lazyIncludes=null;
                this.lazyExcludes=null;
            }
        }else {
            this.lazyActivation=false;
            this.lazyIncludes=null;
            this.lazyExcludes=null;
        }
        hes=this.vj$.Util.parseManifestHeader(this.vj$.Constants.REQUIRE_CAPABILITY,this.archive.getAttribute(this.vj$.Constants.REQUIRE_CAPABILITY),true,true,false);
        for (var e,_$itr=hes.iterator();_$itr.hasNext();){
            e=_$itr.next();
            var bri=new this.vj$.BundleRequirementImpl(this,e); 
            var ns=bri.getNamespace(); 
            var nsReqs=this.requirements.get(ns); 
            if(null===nsReqs){
                if(this.isExtension()&& !vjo.java.lang.ObjectUtil.equals(ns,this.vj$.ExecutionEnvironmentNamespace.EXECUTION_ENVIRONMENT_NAMESPACE)){
                    throw new this.vj$.IllegalArgumentException("An extension bundle can only specify capability requirements in the "+this.vj$.ExecutionEnvironmentNamespace.EXECUTION_ENVIRONMENT_NAMESPACE+" namespace. Not in "+ns);
                }
                nsReqs=new this.vj$.ArrayList();
                this.requirements.put(ns,nsReqs);
            }
            nsReqs.add(bri);
        }
        var ee=this.archive.getAttribute(this.vj$.Constants.BUNDLE_REQUIREDEXECUTIONENVIRONMENT); 
        if(ee!==null){
            var bri=new this.vj$.BundleRequirementImpl(this,ee); 
            var nsReqs=this.requirements.get(bri.getNamespace()); 
            if(null===nsReqs){
                nsReqs=new this.vj$.ArrayList();
                this.requirements.put(bri.getNamespace(),nsReqs);
            }
            nsReqs.add(bri);
        }
        this.processCapabilities(this.archive.getAttribute(this.vj$.Constants.PROVIDE_CAPABILITY));
        this.bpkgs=new this.vj$.BundlePackages(this);
        try {
            if(b.fwCtx.startLevelController===null){
                this.archive.setStartLevel(0);
            }else {
                if(this.archive.getStartLevel()=== -1){
                    this.archive.setStartLevel(b.fwCtx.startLevelController.getInitialBundleStartLevel());
                }
            }
        }
        catch(exc){
            b.fwCtx.frameworkError(b,exc);
        }
        this.archive.setBundleGeneration(this);
        var lastModified=prev!==null?0:this.archive.getLastModified(); 
        if(lastModified===0){
            lastModified=this.vj$.System.currentTimeMillis();
        }
        this.bundleRevision=new this.vj$.BundleRevisionImpl(this);
        this.timeStamp=lastModified;
        hes=this.vj$.Util.parseManifestHeader(this.vj$.Constants.BUNDLE_SYMBOLICNAME,this.archive.getAttribute(this.vj$.Constants.BUNDLE_SYMBOLICNAME),true,true,true);
        var he=null; 
        if(!hes.isEmpty()){
            he=hes.get(0);
            this.symbolicName=he.getKey();
        }else {
            if(this.v2Manifest){
                throw new this.vj$.IllegalArgumentException("Bundle has no symbolic name, location="+this.bundle.location);
            }else {
                this.symbolicName=null;
            }
        }
        if(he!==null){
            this.singleton=vjo.java.lang.ObjectUtil.equals("true",he.getDirectives().get(this.vj$.Constants.SINGLETON_DIRECTIVE));
            var tmp=he.getDirectives().get(this.vj$.Constants.FRAGMENT_ATTACHMENT_DIRECTIVE); 
            this.attachPolicy=tmp===null?this.vj$.Constants.FRAGMENT_ATTACHMENT_ALWAYS:tmp;
            this.symbolicNameParameters=he;
            if(!vjo.java.lang.ObjectUtil.equals(this.bundle.fwCtx.bsnversionMode,this.vj$.Constants.FRAMEWORK_BSNVERSION_MULTIPLE)){
                var snbl=b.fwCtx.bundles.getBundles(this.symbolicName,this.version); 
                if(!snbl.isEmpty()&&(snbl.size()>1||!snbl.contains(this.bundle))){
                    var fail; 
                    if(vjo.java.lang.ObjectUtil.equals(this.bundle.fwCtx.bsnversionMode,this.vj$.Constants.FRAMEWORK_BSNVERSION_MANAGED)){
                        this.bundle.fwCtx.bundleHooks.filterCollisions(prev!==null?this.vj$.CollisionHook.UPDATING:this.vj$.CollisionHook.INSTALLING,caller,snbl);
                        fail=!snbl.isEmpty()&&(snbl.size()>1||!snbl.contains(this.bundle));
                    }else {
                        fail=true;
                    }
                    if(fail){
                        throw new this.vj$.BundleException("Bundle#"+b.id+", a bundle with same symbolic name and version "+"is already installed ("+this.symbolicName+", "+this.version,this.vj$.BundleException.DUPLICATE_BUNDLE_ERROR);
                    }
                }
            }
        }else {
            this.attachPolicy=this.vj$.Constants.FRAGMENT_ATTACHMENT_ALWAYS;
            this.singleton=false;
            this.symbolicNameParameters=null;
        }
        this.identity=this.symbolicName!==null?new this.vj$.BundleCapabilityImpl(this):null;
    },
    
    constructs_1_0_BundleGeneration_ovld:function(prev){
        this.bundle=prev.bundle;
        this.archive=null;
        this.generation=prev.generation+1;
        this.v2Manifest=prev.v2Manifest;
        this.symbolicName=prev.symbolicName;
        this.symbolicNameParameters=prev.symbolicNameParameters;
        this.singleton=prev.singleton;
        this.version=prev.version;
        this.attachPolicy=this.vj$.Constants.FRAGMENT_ATTACHMENT_NEVER;
        bundles.framework.Fragment=prev.fragment;
        this.protectionDomain=null;
        this.lazyActivation=false;
        this.lazyIncludes=null;
        this.lazyExcludes=null;
        this.timeStamp=this.vj$.System.currentTimeMillis();
        this.bpkgs=null;
        this.cachedRawHeaders=prev.cachedRawHeaders;
        this.classLoader=null;
        this.bundleRevision=null;
        this.identity=null;
    },
    
    processCapabilities:function(capabilityStr){
        if(capabilityStr!==null&&capabilityStr.length>0){
            for (var he,_$itr=this.vj$.Util.parseManifestHeader(this.vj$.Constants.PROVIDE_CAPABILITY,capabilityStr,true,true,false).iterator();_$itr.hasNext();){
                he=_$itr.next();
                var bci=new this.vj$.BundleCapabilityImpl(this,he); 
                var nsCaps=this.capabilities.get(bci.getNamespace()); 
                if(null===nsCaps){
                    nsCaps=new this.vj$.ArrayList();
                    this.capabilities.put(bci.getNamespace(),nsCaps);
                }
                nsCaps.add(bci);
            }
        }
    },
    
    compareTo:function(bg){
        var diff=this.bundle.id-bg.bundle.id; 
        if(diff<0){
            return -1;
        }else if(diff===0){
            return 0;
        }else {
            return 1;
        }
    },
    
    toString:function(){
        return "BundleGeneration[bid="+this.bundle.id+", gen="+this.generation+"]";
    },
    
    checkPermissions:function(checkContext){
        this.protectionDomain=this.bundle.secure.getProtectionDomain(this);
        try {
            this.bundle.secure.checkExtensionLifecycleAdminPerm(this.bundle,checkContext);
            if(this.isExtension()&& !this.bundle.secure.okAllPerm(this.bundle)){
                throw new this.vj$.IllegalArgumentException("An extension bundle must have AllPermission");
            }
        }
        catch(re){
            this.purgeProtectionDomain();
            throw re;
        }
        if(this.archive.getLastModified()!==this.timeStamp){
            try {
                this.archive.setLastModified(this.timeStamp);
            }
            catch(ioe){
                this.bundle.fwCtx.frameworkError(this.bundle,ioe);
            }
        }
    },
    
    getDeclaredCapabilities:function(){
        return this.capabilities;
    },
    
    getDeclaredRequirements:function(){
        return this.requirements;
    },
    
    resolvePackages:function(triggers){
        var detached=null; 
        this.attachFragments();
        while(true){
            if(this.bpkgs.resolvePackages(triggers)){
                if(detached!==null){
                    for (var i=detached.size()-2;i>=0;i--){
                        var bg=detached.get(i); 
                        if(bg.bundle.attachToFragmentHost(this)){
                            this.fragments.add(bg);
                        }
                    }
                }
                this.classLoader=this.bundle.secure.newBundleClassLoader(this);
                return true;
            }
            if(this.isFragmentHost()){
                if(this.bundle.fwCtx.debug.resolver){
                    this.bundle.fwCtx.debug.println("Resolve failed, remove last fragment and retry");
                }
                if(detached===null){
                    detached=new this.vj$.ArrayList();
                }
                detached.add(this.detachLastFragment(true));
            }else {
                break;
            }
        }
        return false;
    },
    
    isPkgActivationTrigger:function(packageName){
        return this.lazyActivation&&((this.lazyIncludes===null&&this.lazyExcludes===null)||(this.lazyIncludes!==null&&this.lazyIncludes.contains(packageName))||(this.lazyExcludes!==null&&!this.lazyExcludes.contains(packageName)));
    },
    
    getClassLoader:function(){
        return this.classLoader;
    },
    
    getProtectionDomain:function(){
        return this.protectionDomain;
    },
    
    attachFragments:function(){
        if(!vjo.java.lang.ObjectUtil.equals(this.attachPolicy,this.vj$.Constants.FRAGMENT_ATTACHMENT_NEVER)){
            var hosting=this.bundle.fwCtx.bundles.getFragmentBundles(this); 
            if(hosting.size()>0&&this.bundle.secure.okHostBundlePerm(this.bundle)){
                for (var fbg,_$itr=hosting.iterator();_$itr.hasNext();){
                    fbg=_$itr.next();
                    fbg.bundle.attachToFragmentHost(this);
                }
            }
        }
    },
    
    attachFragment:function(fragmentBundle){
        if(vjo.java.lang.ObjectUtil.equals(this.attachPolicy,this.vj$.Constants.FRAGMENT_ATTACHMENT_NEVER)){
            throw new this.vj$.IllegalStateException("Bundle does not allow fragments to attach");
        }
        if(vjo.java.lang.ObjectUtil.equals(this.attachPolicy,this.vj$.Constants.FRAGMENT_ATTACHMENT_RESOLVETIME)&&this.bundle.isResolved()){
            throw new this.vj$.IllegalStateException("Bundle does not allow fragments to attach dynamically");
        }
        if(!this.bundle.fwCtx.resolverHooks.filterMatches(fragmentBundle.fragment,new this.vj$.BundleNameVersionCapability(this,this.vj$.BundleRevision.HOST_NAMESPACE))){
            throw new this.vj$.IllegalStateException("Resolver hooks vetoed attach to: "+this);
        }
        var failReason=this.bpkgs.attachFragment(fragmentBundle.bpkgs); 
        if(failReason!==null){
            throw new this.vj$.IllegalStateException("Failed to attach: "+failReason);
        }
        if(this.classLoader!==null&&bundles.framework.BundleClassLoader.isInstance(this.classLoader)){
            try {
                this.classLoader.attachFragment(fragmentBundle);
            }
            catch(be){
                throw new this.vj$.IllegalStateException(be.getMessage());
            }
        }
        if(this.bundle.fwCtx.debug.resolver){
            this.bundle.fwCtx.debug.println("Fragment("+fragmentBundle.bundle+") attached to host(id="+this.bundle.id+",gen="+this.generation+")");
        }
        if(this.fragments===null){
            this.fragments=new this.vj$.Vector();
        }
        var i=0; 
        for (;i<this.fragments.size();i++){
            var b=this.fragments.get(i); 
            if(b.bundle.id>fragmentBundle.bundle.id){
                break;
            }
        }
        this.fragments.add(i,fragmentBundle);
    },
    
    detachLastFragment:function(unregister){
        var last=this.fragments.size()-1; 
        if(last>=0){
            var fbg=this.fragments.remove(last); 
            this.bpkgs.detachFragmentSynchronized(fbg,unregister);
            if(this.bundle.fwCtx.debug.resolver){
                this.bundle.fwCtx.debug.println("Fragment(id="+fbg.bundle.id+") detached from host(id="+this.bundle.id+",gen="+this.generation+")");
            }
            if(fbg.bundle.state!==this.vj$.Bundle.UNINSTALLED){
                fbg.fragment.removeHost(this);
                if(!fbg.fragment.hasHosts()){
                    if(fbg.isCurrent()){
                        fbg.bundle.setStateInstalled(true);
                    }
                }
            }
            return fbg;
        }
        return null;
    },
    
    updateStateFragments:function(){
        if(this.fragments!==null){
            var fix=this.fragments.clone(); 
            for (var bundleGeneration,_$itr=fix.iterator();_$itr.hasNext();){
                bundleGeneration=_$itr.next();
                var fb=bundleGeneration.bundle; 
                fb.getUpdatedState(null);
            }
        }
    },
    
    getHosts:function(){
        return bundles.framework.Fragment!==null?bundles.framework.Fragment.getHosts():null;
    },
    
    isFragment:function(){
        return bundles.framework.Fragment!==null;
    },
    
    isFragmentHost:function(){
        return this.fragments!==null&& !this.fragments.isEmpty();
    },
    
    isBootClassPathExtension:function(){
        return bundles.framework.Fragment!==null&&this.extension!==null&&vjo.java.lang.ObjectUtil.equals(this.extension,this.vj$.Constants.EXTENSION_BOOTCLASSPATH);
    },
    
    isExtension:function(){
        return bundles.framework.Fragment!==null&&this.extension!==null;
    },
    
    getLocaleDictionary:function(locale,baseName){
        var defaultLocale=this.vj$.Locale.getDefault().toString(); 
        if(locale===null){
            locale=defaultLocale;
        }else if(vjo.java.lang.ObjectUtil.equals(locale,"")){
            return null;
        }
        var localization_entries=new this.vj$.Hashtable(); 
        if(baseName===null){
            baseName=this.vj$.Constants.BUNDLE_LOCALIZATION_DEFAULT_BASENAME;
        }
        var h=this.getHosts(); 
        if(h!==null){
            var best; 
            while(true){
                try {
                    best=null;
                    for (var bundleGeneration,_$itr=h.iterator();_$itr.hasNext();){
                        bundleGeneration=_$itr.next();
                        var bg=bundleGeneration; 
                        if(best===null||bg.version.compareTo(best.version)>0){
                            best=bg;
                        }
                    }
                    break;
                }
                catch(ignore){
                }
            }
            if(best===this.bundle.fwCtx.systemBundle.current()){
                this.bundle.fwCtx.systemBundle.readLocalization("",localization_entries,baseName);
                this.bundle.fwCtx.systemBundle.readLocalization(defaultLocale,localization_entries,baseName);
                if(!vjo.java.lang.ObjectUtil.equals(locale,defaultLocale)){
                    this.bundle.fwCtx.systemBundle.readLocalization(locale,localization_entries,baseName);
                }
                return localization_entries;
            }else if(best!==null){
                return best.getLocaleDictionary(locale,baseName);
            }
        }
        this.readLocalization("",localization_entries,baseName);
        this.readLocalization(defaultLocale,localization_entries,baseName);
        if(!vjo.java.lang.ObjectUtil.equals(locale,defaultLocale)){
            this.readLocalization(locale,localization_entries,baseName);
        }
        return localization_entries;
    },
    
    getHeaders0:function(locale){
        if(this.cachedRawHeaders===null){
            this.cachedRawHeaders=this.archive.getUnlocalizedAttributes();
        }
        if(vjo.java.lang.ObjectUtil.equals("",locale)){
            return this.cachedRawHeaders.clone(); 
        }
        if(this.bundle.state!==this.vj$.Bundle.UNINSTALLED){
            var base=this.cachedRawHeaders.get(this.vj$.Constants.BUNDLE_LOCALIZATION); 
            try {
                return this.localize(this.getLocaleDictionary(locale,base));
            }
            catch(e){
                if(true){
                    throw e;
                }
            }
        }
        return null;
    },
    
    addResourceEntries:function(res,path,pattern,recurse){
        if(this.archive===null){
            throw new this.vj$.IllegalStateException("Bundle is in UNINSTALLED state");
        }
        var e=this.archive.findResourcesPath(path); 
        if(e!==null){
            while(e.hasMoreElements()){
                var fp=e.nextElement(); 
                var isDirectory=org.eclipse.vjet.vjo.java.lang.StringUtil.endsWith(fp,"/"); 
                var searchBackwardFrom=fp.length-1; 
                if(isDirectory){
                    searchBackwardFrom=searchBackwardFrom-1;
                }
                var l=fp.lastIndexOf('/',searchBackwardFrom); 
                var lastComponentOfPath=fp.substring(l+1,searchBackwardFrom+1); 
                if(pattern===null||this.vj$.Util.filterMatch(pattern,lastComponentOfPath)){
                    var url=this.getURL(0,fp); 
                    if(url!==null){
                        res.add(url);
                    }
                }
                if(isDirectory&&recurse){
                    this.addResourceEntries(res,fp,pattern,recurse);
                }
            }
        }
    },
    
    findEntries:function(path,filePattern,recurse){
        var res=new this.vj$.Vector(); 
        this.addResourceEntries(res,path,filePattern,recurse);
        if(this.isFragmentHost()){
            var fix=this.fragments.clone(); 
            for (var fbg,_$itr=fix.iterator();_$itr.hasNext();){
                fbg=_$itr.next();
                fbg.addResourceEntries(res,path,filePattern,recurse);
            }
        }
        return res;
    },
    
    getURL:function(subId,path){
        try {
            var u=new this.vj$.StringBuffer(this.vj$.BundleURLStreamHandler.PROTOCOL); 
            u.append("://");
            u.append(this.bundle.id);
            if(this.generation>0){
                u.append('.').append(this.generation);
            }
            if(this.bundle.fwCtx.id>0){
                u.append('!').append(this.bundle.fwCtx.id);
            }
            if(subId>0){
                u.append(':').append(subId);
            }
            if(!org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(path,"/")){
                u.append('/');
            }
            u.append(path);
            return this.bundle.secure.getBundleURL(this.bundle.fwCtx,u.toString());
        }
        catch(e){
            return null;
        }
    },
    
    closeClassLoader:function(){
        if(this.bundle.fwCtx.debug.classLoader){
            this.bundle.fwCtx.debug.println("closeClassLoader: "+this.bundle+" gen = "+this.generation);
        }
        var tmp=this.classLoader; 
        if(tmp!==null){
            this.classLoader=null;
            tmp.close();
        }
    },
    
    purge:function(unregister){
        if(this.bundle.fwCtx.debug.classLoader){
            this.bundle.fwCtx.debug.println("BundleGeneration.purge: "+this.bundle+" gen = "+this.generation);
        }
        if(unregister){
            this.unregisterPackages(true);
        }
        this.closeClassLoader();
        this.purgeProtectionDomain();
        if(this.archive!==null){
            this.archive.purge();
        }
        this.clearWiring();
    },
    
    unregisterPackages:function(force){
        var res=this.bpkgs.unregisterPackages(force); 
        if(res&&this.isFragmentHost()){
            while(this.detachLastFragment(false)!==null){
            }
        }
        return res;
    },
    
    purgeProtectionDomain:function(){
        this.bundle.secure.purge(this.bundle,this.protectionDomain);
    },
    
    checkCertificates:function(){
        var cs=this.archive.getCertificateChains(false); 
        if(cs!==null){
            if(this.bundle.fwCtx.validator!==null){
                if(this.bundle.fwCtx.debug.certificates){
                    this.bundle.fwCtx.debug.println("Validate certs for bundle #"+this.archive.getBundleId());
                }
                cs=new this.vj$.ArrayList(cs);
                for (var vi=this.bundle.fwCtx.validator.iterator();!cs.isEmpty() && vi.hasNext();){
                    var v=vi.next(); 
                    for (var ci=cs.iterator();ci.hasNext();){
                        var c=ci.next(); 
                        if(v.validateCertificateChain(c)){
                            this.archive.trustCertificateChain(c);
                            ci.remove();
                            if(this.bundle.fwCtx.debug.certificates){
                                this.bundle.fwCtx.debug.println("Validated cert: "+c.get(0));
                            }
                        }else {
                            if(this.bundle.fwCtx.debug.certificates){
                                this.bundle.fwCtx.debug.println("Failed to validate cert: "+c.get(0));
                            }
                        }
                    }
                }
                if(cs.isEmpty()){
                    return;
                }
            }
        }
        if(this.bundle.fwCtx.props.getBooleanProperty(this.vj$.FrameworkProperties.ALL_SIGNED_PROP)){
            throw new this.vj$.IllegalArgumentException("All installed bundles must be signed!");
        }
    },
    
    localize:function(localization_entries){
        var localized=this.cachedRawHeaders.clone(); 
        if(localization_entries!==null){
            for (var e=localized.keys();e.hasMoreElements();){
                var key=e.nextElement(); 
                var unlocalizedEntry=localized.get(key); 
                if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(unlocalizedEntry,"%")){
                    var k=unlocalizedEntry.substring(1); 
                    var val=localization_entries.get(k); 
                    if(val===null){
                        localized.put(key,k);
                    }else {
                        localized.put(key,val);
                    }
                }
            }
        }
        return localized;
    },
    
    readLocalization:function(locale,localization_entries,baseName){
        if(!vjo.java.lang.ObjectUtil.equals(locale,"")){
            locale="_"+locale;
        }
        while(true){
            var l=baseName+locale+".properties"; 
            var res=this.getLocalizationEntries(l); 
            if(res!==null){
                localization_entries.putAll(res);
                break;
            }
            var pos=locale.lastIndexOf('_'); 
            if(pos=== -1){
                break;
            }
            locale=locale.substring(0,pos);
        }
    },
    
    getLocalizationEntries:function(name){
        var res=this.archive.getLocalizationEntries(name); 
        if(res===null&&this.isFragmentHost()){
            var fix=this.fragments.clone(); 
            for (var bg,_$itr=fix.iterator();_$itr.hasNext();){
                bg=_$itr.next();
                if(bg.archive!==null){
                    res=bg.archive.getLocalizationEntries(name);
                    if(res!==null){
                        break;
                    }
                }
            }
        }
        return res;
    },
    
    isUninstalled:function(){
        return this.bpkgs===null;
    },
    
    getHostCapability:function(){
        if(this.v2Manifest&& !vjo.java.lang.ObjectUtil.equals(this.attachPolicy,this.vj$.Constants.FRAGMENT_ATTACHMENT_NEVER)&&bundles.framework.Fragment===null){
            return new this.vj$.BundleNameVersionCapability(this,this.vj$.BundleRevision.HOST_NAMESPACE);
        }
        return null;
    },
    
    getBundleCapability:function(){
        if(this.v2Manifest&&bundles.framework.Fragment===null){
            return new this.vj$.BundleNameVersionCapability(this,this.vj$.BundleRevision.BUNDLE_NAMESPACE);
        }
        return null;
    },
    
    getBundleClassPathEntries:function(name,onlyFirst){
        var bcp=this.unresolvedBundleClassPath; 
        if(bcp===null){
            bcp=new this.vj$.BundleClassPath(this.archive,this.bundle.fwCtx);
            this.unresolvedBundleClassPath=bcp;
        }
        var fas=bcp.componentExists(name,onlyFirst,false); 
        if(fas!==null){
            var res=new this.vj$.Vector(); 
            for (var fa,_$itr=fas.iterator();_$itr.hasNext();){
                fa=_$itr.next();
                var url=fa.getBundleGeneration().getURL(fa.getSubId(),name); 
                if(url!==null){
                    res.addElement(url);
                }else {
                    return null;
                }
            }
            return res;
        }
        return null;
    },
    
    getCapabilityWires:function(){
        if(this.bpkgs!==null){
            var res=new this.vj$.ArrayList(); 
            for (var lbc,_$itr=this.bpkgs.getOtherCapabilities().values().iterator();_$itr.hasNext();){
                lbc=_$itr.next();
                for (var bc,_$itr=lbc.iterator();_$itr.hasNext();){
                    bc=_$itr.next();
                    bc.getWires(res);
                }
            }
            return res;
        }else {
            return null;
        }
    },
    
    getRequirementWires:function(){
        var res=new this.vj$.ArrayList(); 
        for (var lbr,_$itr=this.getOtherRequirements().values().iterator();_$itr.hasNext();){
            lbr=_$itr.next();
            for (var br,_$itr=lbr.iterator();_$itr.hasNext();){
                br=_$itr.next();
                var wire=br.getWire(); 
                if(wire!==null){
                    res.add(wire);
                }
            }
        }
        return res;
    },
    
    getOtherRequirements:function(){
        var res=this.getDeclaredRequirements(); 
        if(this.isFragmentHost()){
            var copied=false; 
            var fix=this.fragments.clone(); 
            for (var fbg,_$itr=fix.iterator();_$itr.hasNext();){
                fbg=_$itr.next();
                var frm=fbg.getDeclaredRequirements(); 
                if(!frm.isEmpty()){
                    if(!copied){
                        res=new this.vj$.HashMap(res);
                        copied=true;
                    }
                    for (var e,_$itr=frm.entrySet().iterator();_$itr.hasNext();){
                        e=_$itr.next();
                        var ns=e.getKey(); 
                        var p=res.get(ns); 
                        if(p!==null){
                            p=new this.vj$.ArrayList(p);
                            p.addAll(e.getValue());
                        }else {
                            p=e.getValue();
                        }
                        res.put(ns,p);
                    }
                }
            }
        }
        return res;
    },
    
    isCurrent:function(){
        return this===this.bundle.current();
    },
    
    bsnAttrMatch:function(attributes){
        if(this.symbolicNameParameters!==null){
            var attr=this.symbolicNameParameters.getAttributes(); 
            var mandatory=this.symbolicNameParameters.getDirectives().get(this.vj$.Constants.MANDATORY_DIRECTIVE); 
            var mAttr=new this.vj$.HashSet(); 
            if(mandatory!==null){
                var split=this.vj$.Util.splitwords(mandatory,","); 
                for (var e,_$i0=0;_$i0<split.length;_$i0++){
                    e=split[_$i0];
                    mAttr.add(org.eclipse.vjet.vjo.java.lang.StringUtil.trim(e));
                }
            }
            for (var e,_$itr=attributes.entrySet().iterator();_$itr.hasNext();){
                e=_$itr.next();
                var key=e.getKey(); 
                if(e.getValue().equals(attr.get(key))){
                    mAttr.remove(key);
                }else {
                    return false;
                }
            }
            return mAttr.isEmpty();
        }
        return true;
    },
    
    setWired:function(){
        this.bundleRevision.setWired();
    },
    
    clearWiring:function(){
        this.bundleRevision.clearWiring();
    },
    
    getResolvedHosts:function(){
        var res=new this.vj$.HashSet(); 
        var tgts=bundles.framework.Fragment.targets(); 
        for (var t,_$itr=tgts.iterator();_$itr.hasNext();){
            t=_$itr.next();
            if(t.bundle.isResolved()){
                res.add(t.bundle);
            }
        }
        return res;
    },
    
    getIdentityCapability:function(){
        return this.identity;
    }
})
.endType();