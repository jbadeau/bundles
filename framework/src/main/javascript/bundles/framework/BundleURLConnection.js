/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleURLConnection') 
.needs(['org.eclipse.vjet.vjo.java.lang.Long','bundles.framework.BundleClassLoader',
    'java.io.IOException','org.eclipse.vjet.vjo.java.lang.Util'])

.inherits('java.net.URLConnection')
.protos({
    is:null, 
    fwCtx:null, 
    bundle:null, 
    contentLength:0, 
    contentType:null, 
    lastModified:0, 
    
    constructs:function(u,fwCtx){
        this.base(u);
        this.fwCtx=fwCtx;
    },
    
    getBundleArchive:function(){
        this.bundle=null;
        var gen=0; 
        try {
            var s=this.vj$.url.getHost(); 
            var i=s.indexOf('!'); 
            if(i>=0){
                s=s.substring(0,i);
            }
            i=s.indexOf('.');
            if(i>=0){
                gen=this.vj$.Long.parseLong(s.substring(i+1));
                s=s.substring(0,i);
            }
            this.bundle=this.fwCtx.bundles.getBundle(this.vj$.Long.parseLong(s)); 
        }
        catch(_ignore){
        }
        if(this.bundle!==null){
            return this.bundle.getBundleArchive(gen);
        }
        return null;
    },
    
    connect:function(){
        if(!connected){
            var a=this.getBundleArchive(); 
            if(a!==null){
                var port=this.vj$.url.getPort(); 
                this.is=this.bundle.secure.callGetBundleResourceStream(a,this.vj$.url.getFile(),port!==-1?port:0);
            }
            if(this.is!==null){
                connected=true;
                if(this.vj$.BundleClassLoader.bDalvik){
                    this.contentLength=-1;
                }else {
                    this.contentLength=this.vj$.Util.cast(this.is.getContentLength(),'int');
                }
                this.contentType=this.vj$.URLConnection.guessContentTypeFromName(this.vj$.url.getFile());
                this.lastModified=a.getLastModified();
            }else {
                throw new this.vj$.IOException("URL not found");
            }
        }
    },
    
    getInputStream:function(){
        this.connect();
        return this.is;
    },
    
    getContentType:function(){
        try {
            this.connect();
            return this.contentType;
        }
        catch(e){
            return null;
        }
    },
    
    getContentLength:function(){
        try {
            this.connect();
            return this.contentLength;
        }
        catch(e){
            return -1;
        }
    },
    
    getLastModified:function(){
        try {
            this.connect();
            return this.lastModified;
        }
        catch(e){
            return 0;
        }
    }
})
.endType();