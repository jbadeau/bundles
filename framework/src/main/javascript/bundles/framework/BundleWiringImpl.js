/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleWiringImpl') 
.needs(['org.eclipse.vjet.vjo.java.util.ArrayList','org.eclipse.vjet.vjo.java.util.Collections',
    'org.eclipse.vjet.vjo.java.util.TreeSet','org.eclipse.vjet.vjo.java.util.Comparator',
    'bundles.framework.BundleRevisionImpl','bundles.framework.BundleWireImpl',
    'bundles.framework.BundleClassLoader','vjo.java.lang.ObjectUtil'])

.satisfies('osgi.framework.wiring.BundleWiring')
.protos({
    bundleRevision:null, 
    
    constructs:function(br){
        this.bundleRevision=br;
    },
    
    getBundle:function(){
        return this.bundleRevision.getBundle();
    },
    
    isCurrent:function(){
        return this===this.bundleRevision.getWiring()&&this.bundleRevision.bundle.current()===this.bundleRevision.gen;
    },
    
    isInUse:function(){
        return this===this.bundleRevision.getWiring();
    },
    
    getCapabilities:function(namespace){
        if(!this.isInUse()){
            return null;
        }
        var gen=this.bundleRevision.getBundleGeneration(); 
        var ns=this.vj$.BundleRevisionImpl.whichNameSpaces(namespace); 
        var res=new this.vj$.ArrayList(); 
        if((ns&this.vj$.BundleRevisionImpl.NS_IDENTITY)!==0){
            var bc=gen.getIdentityCapability(); 
            if(bc!==null){
                res.add(bc);
            }
        }
        if(!gen.isFragment()){
            if((ns&this.vj$.BundleRevisionImpl.NS_BUNDLE)!==0){
                var bc=gen.getBundleCapability(); 
                if(bc!==null){
                    res.add(bc);
                }
            }
            if((ns&this.vj$.BundleRevisionImpl.NS_HOST)!==0){
                var bc=gen.getHostCapability(); 
                if(bc!==null){
                    res.add(bc);
                }
            }
            if((ns&this.vj$.BundleRevisionImpl.NS_PACKAGE)!==0){
                for (var ep,_$itr=gen.bpkgs.getPackageCapabilities().iterator();_$itr.hasNext();){
                    ep=_$itr.next();
                    if(ep.checkPermission()){
                        res.add(ep);
                    }
                }
            }
            if((ns&this.vj$.BundleRevisionImpl.NS_OTHER)!==0){
                var caps=gen.bpkgs.getOtherCapabilities(); 
                var clbc=null; 
                if(null!==namespace){
                    var lbc=caps.get(namespace); 
                    if(lbc!==null){
                        clbc=this.vj$.Collections.singletonSet(lbc);
                    }
                }else {
                    clbc=caps.values();
                }
                if(null!==clbc){
                    for (var lbc,_$itr=clbc.iterator();_$itr.hasNext();){
                        lbc=_$itr.next();
                        for (var bc,_$itr=lbc.iterator();_$itr.hasNext();){
                            bc=_$itr.next();
                            if(bc.isEffectiveResolve()&&bc.checkPermission()){
                                res.add(bc);
                            }
                        }
                    }
                }
            }
        }
        return res;
    },
    
    getRequirements:function(namespace){
        if(!this.isInUse()){
            return null;
        }
        var gen=this.bundleRevision.getBundleGeneration(); 
        var ns=this.vj$.BundleRevisionImpl.whichNameSpaces(namespace); 
        var res=new this.vj$.ArrayList(); 
        if(gen.isFragment()){
            if((ns&this.vj$.BundleRevisionImpl.NS_HOST)!==0){
                res.add(gen.fragment);
            }
        }else {
            if((ns&this.vj$.BundleRevisionImpl.NS_BUNDLE)!==0){
                for (var irb=gen.bpkgs.getRequire();irb.hasNext();){
                    var rb=irb.next(); 
                    if(null!==rb.bpkgs&&rb.bpkgs.isRequiredBy(gen.bpkgs)){
                        res.add(rb);
                    }
                }
            }
            if((ns&this.vj$.BundleRevisionImpl.NS_PACKAGE)!==0){
                res.addAll(gen.bpkgs.getPackageRequirements());
            }
            if((ns&(this.vj$.BundleRevisionImpl.NS_IDENTITY|this.vj$.BundleRevisionImpl.NS_OTHER))!==0){
                var reqs=gen.getOtherRequirements(); 
                var clbr=null; 
                if(null!==namespace){
                    var lbr=reqs.get(namespace); 
                    if(lbr!==null){
                        clbr=this.vj$.Collections.singletonSet(lbr);
                    }
                }else {
                    clbr=reqs.values();
                }
                if(null!==clbr){
                    for (var lbr,_$itr=clbr.iterator();_$itr.hasNext();){
                        lbr=_$itr.next();
                        for (var br,_$itr=lbr.iterator();_$itr.hasNext();){
                            br=_$itr.next();
                            if(br.isWired()){
                                res.add(br);
                            }
                        }
                    }
                }
            }
        }
        return res;
    },
    
    getProvidedWires:function(namespace){
        if(!this.isInUse()){
            return null;
        }
        var gen=this.bundleRevision.getBundleGeneration(); 
        var ns=this.vj$.BundleRevisionImpl.whichNameSpaces(namespace); 
        var res=new this.vj$.ArrayList(); 
        if((ns&this.vj$.BundleRevisionImpl.NS_BUNDLE)!==0){
            var reqBys=gen.bpkgs.getRequiredBy(); 
            for (var bp,_$itr=reqBys.iterator();_$itr.hasNext();){
                bp=_$itr.next();
                for (var irb=bp.getRequire();irb.hasNext();){
                    var rb=irb.next(); 
                    if(rb.bpkgs===gen.bpkgs){
                        res.add(new this.vj$.BundleWireImpl(gen.getBundleCapability(),gen,rb,bp.bg));
                    }
                }
            }
        }
        if((ns&this.vj$.BundleRevisionImpl.NS_HOST)!==0){
            if(gen.isFragmentHost()){
                var fix=gen.fragments.clone(); 
                for (var fbg,_$itr=fix.iterator();_$itr.hasNext();){
                    fbg=_$itr.next();
                    res.add(new this.vj$.BundleWireImpl(gen.getHostCapability(),gen,fbg.fragment,fbg));
                }
            }
        }
        if((ns&this.vj$.BundleRevisionImpl.NS_PACKAGE)!==0){
            for (var bc,_$itr=gen.bpkgs.getPackageCapabilities().iterator();_$itr.hasNext();){
                bc=_$itr.next();
                var ep=bc; 
                var ips=ep.getPackageImporters(); 
                if(ips!==null){
                    for (var ip,_$itr=ips.iterator();_$itr.hasNext();){
                        ip=_$itr.next();
                        var oip; 
                        if(ip.parent!==null&&ip.parent.bpkgs!==ip.bpkgs){
                            oip=ip.parent;
                        }else {
                            oip=ip;
                        }
                        res.add(new this.vj$.BundleWireImpl(ep,gen,oip,ip.bpkgs.bg));
                    }
                }
            }
        }
        if((ns&(this.vj$.BundleRevisionImpl.NS_IDENTITY|this.vj$.BundleRevisionImpl.NS_OTHER))!==0){
            var other=gen.getCapabilityWires(); 
            if(other!==null){
                for (var bw,_$itr=other.iterator();_$itr.hasNext();){
                    bw=_$itr.next();
                    if(namespace===null||vjo.java.lang.ObjectUtil.equals(namespace,bw.getCapability().getNamespace())){
                        res.add(bw);
                    }
                }
            }
        }
        return res;
    },
    
    getRequiredWires:function(namespace){
        if(!this.isInUse()){
            return null;
        }
        var gen=this.bundleRevision.getBundleGeneration(); 
        var ns=this.vj$.BundleRevisionImpl.whichNameSpaces(namespace); 
        var res=new this.vj$.ArrayList(); 
        if((ns&this.vj$.BundleRevisionImpl.NS_BUNDLE)!==0){
            for (var irb=gen.bpkgs.getRequire();irb.hasNext();){
                var rb=irb.next(); 
                if(null!==rb.bpkgs&&rb.bpkgs.isRequiredBy(gen.bpkgs)){
                    res.add(new this.vj$.BundleWireImpl(rb.bpkgs.bg.getBundleCapability(),rb.bpkgs.bg,rb,gen));
                }
            }
        }
        if((ns&this.vj$.BundleRevisionImpl.NS_HOST)!==0){
            if(gen.isFragment()){
                for (var hbg,_$itr=gen.getHosts().iterator();_$itr.hasNext();){
                    hbg=_$itr.next();
                    res.add(new this.vj$.BundleWireImpl(hbg.getHostCapability(),hbg,gen.fragment,gen));
                }
            }
        }
        if((ns&this.vj$.BundleRevisionImpl.NS_PACKAGE)!==0){
            var dynamic=new this.vj$.TreeSet(
                vjo.make(this,this.vj$.Comparator)
                .protos({
                    compare:function(o1,o2){
                        return o1.dynId-o2.dynId;
                        return o1.dynId-o2.dynId;
                    }
                })
                .endType()); 
            for (var ip,_$itr=gen.bpkgs.getPackageRequirements().iterator();_$itr.hasNext();){
                ip=_$itr.next();
                var ep=ip.provider; 
                if(ep!==null){
                    res.add(new this.vj$.BundleWireImpl(ep,ep.bpkgs.bg,ip,gen));
                }else {
                    for (var cip,_$itr=gen.bpkgs.getActiveChildImports(ip).iterator();_$itr.hasNext();){
                        cip=_$itr.next();
                        if(ip.isDynamic()){
                            dynamic.add(cip);
                        }else {
                            res.add(new this.vj$.BundleWireImpl(cip.provider,cip.provider.bpkgs.bg,ip,gen));
                        }
                    }
                }
            }
            for (var cip,_$itr=dynamic.iterator();_$itr.hasNext();){
                cip=_$itr.next();
                res.add(new this.vj$.BundleWireImpl(cip.provider,cip.provider.bpkgs.bg,cip.parent,gen));
            }
        }
        if((ns&(this.vj$.BundleRevisionImpl.NS_IDENTITY|this.vj$.BundleRevisionImpl.NS_OTHER))!==0){
            var other=gen.getRequirementWires(); 
            if(other!==null){
                for (var bw,_$itr=other.iterator();_$itr.hasNext();){
                    bw=_$itr.next();
                    if(namespace===null||vjo.java.lang.ObjectUtil.equals(namespace,bw.getRequirement().getNamespace())){
                        res.add(bw);
                    }
                }
            }
        }
        return res;
    },
    
    getRevision:function(){
        return this.bundleRevision;
    },
    
    getClassLoader:function(){
        if(this.isInUse()){
            return this.bundleRevision.getBundleGeneration().getClassLoader();
        }
        return null;
    },
    
    findEntries:function(path,filePattern,options){
        var gen=this.bundleRevision.getBundleGeneration(); 
        if(this.isInUse()){
            return this.vj$.Collections.unmodifiableList(gen.bundle.secure.callFindEntries(gen,path,filePattern,(options&this.vj$.BundleWiring.FINDENTRIES_RECURSE)!==0));
        }
        return null;
    },
    
    listResources:function(path,filePattern,options){
        var gen=this.bundleRevision.getBundleGeneration(); 
        if(!this.isInUse()){
            return null;
        }
        var res=this.vj$.Collections.EMPTY_SET; 
        var cl=gen.getClassLoader(); 
        if(cl!==null&&bundles.framework.BundleClassLoader.isInstance(cl)){
            var bcl=gen.getClassLoader(); 
            res=bcl.listResources(path,filePattern,options);
        }
        return res;
    },
    
    getResourceCapabilities:function(namespace){
        return this.getCapabilities(namespace); 
    },
    
    getResourceRequirements:function(namespace){
        return this.getRequirements(namespace); 
    },
    
    getProvidedResourceWires:function(namespace){
        return this.getProvidedWires(namespace); 
    },
    
    getRequiredResourceWires:function(namespace){
        return this.getRequiredWires(namespace); 
    },
    
    getResource:function(){
        return this.bundleRevision;
    }
})
.endType();