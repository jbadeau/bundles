/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.PropertiesDictionary') 
.needs(['org.eclipse.vjet.vjo.java.util.Enumeration','org.eclipse.vjet.vjo.java.lang.IllegalArgumentException',
    'org.eclipse.vjet.vjo.java.lang.ClassCastException','org.eclipse.vjet.vjo.java.lang.Long',
    'org.eclipse.vjet.vjo.java.lang.System','osgi.framework.Constants',
    'org.eclipse.vjet.vjo.java.lang.StringUtil'])
.inherits('java.util.Dictionary<String,Object>')
.props({
    nextServiceID:1 
})
.protos({
    keys:null, 
    values:null, 
    size:0, 
    ocIndex:-1, 
    sidIndex:-1, 
    
    
    
    constructs:function(){
        if(arguments.length===1){
            this.constructs_1_0_PropertiesDictionary_ovld(arguments[0]);
        }else if(arguments.length===3){
            this.constructs_3_0_PropertiesDictionary_ovld(arguments[0],arguments[1],arguments[2]);
        }
    },
    
    constructs_1_0_PropertiesDictionary_ovld:function(in_){
        this.base();
        var max_size=in_!==null?in_.size()+2:2; 
        this.keys=vjo.createArray(null, max_size);
        this.values=vjo.createArray(null, max_size);
        this.size=0;
        if(in_!==null){
            try {
                for (var e=in_.keys();e.hasMoreElements();){
                    var key=e.nextElement(); 
                    if(this.ocIndex=== -1&&org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(key,this.vj$.Constants.OBJECTCLASS)){
                        this.ocIndex=this.size;
                    }else if(this.sidIndex=== -1&&org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(key,this.vj$.Constants.SERVICE_ID)){
                        this.sidIndex=this.size;
                    }else {
                        for (var i=this.size-1;i>=0;i--){
                            if(org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(key,this.keys[i])){
                                throw new this.vj$.IllegalArgumentException("Several entries for property: "+key);
                            }
                        }
                    }
                    this.keys[this.size]=key;
                    this.values[this.size++]=in_.get(key);
                }
            }
            catch(ignore){
                throw new this.vj$.IllegalArgumentException("Properties contains key that is not of type java.lang.String");
            }
        }
    },
    
    constructs_3_0_PropertiesDictionary_ovld:function(in_,classes,sid){
        this.constructs_1_0_PropertiesDictionary_ovld(in_);
        if(this.ocIndex=== -1){
            this.keys[this.size]=this.vj$.Constants.OBJECTCLASS;
            this.ocIndex=this.size++;
        }
        this.values[this.ocIndex]=classes;
        if(this.sidIndex=== -1){
            this.keys[this.size]=this.vj$.Constants.SERVICE_ID;
            this.sidIndex=this.size++;
        }
        this.values[this.sidIndex]=sid!==null?sid:new this.vj$.Long(this.vj$.PropertiesDictionary.nextServiceID++);
    },
    
    get:function(key){
        if(key===this.vj$.Constants.OBJECTCLASS){
            return (this.ocIndex>=0)?this.values[this.ocIndex]:null;
        }else if(key===this.vj$.Constants.SERVICE_ID){
            return (this.sidIndex>=0)?this.values[this.sidIndex]:null;
        }
        for (var i=this.size-1;i>=0;i--){
            if(org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(key,this.keys[i])){
                return this.values[i];
            }
        }
        return null;
    },
    
    keyArray:function(){
        var nkeys=vjo.createArray(null, this.size); 
        this.vj$.System.arraycopy(this.keys,0,nkeys,0,this.size);
        return nkeys;
    },
    
    size:function(){
        return this.size;
    },
    
    elements:function(){
        throw new UnsupportedOperationException("Not implemented");
    },
    
    isEmpty:function(){
        throw new UnsupportedOperationException("Not implemented");
    },
    
    keys:function(){
        throw new UnsupportedOperationException("Not implemented");
    },
    
    put:function(k,v){
        throw new UnsupportedOperationException("Not implemented");
    },
    
    remove:function(k){
        throw new UnsupportedOperationException("Not implemented");
    }
})
.endType();