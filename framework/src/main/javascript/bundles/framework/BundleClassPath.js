/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundleClassPath') 
.needs(['org.eclipse.vjet.vjo.java.util.ArrayList','org.eclipse.vjet.vjo.java.util.Map',
    'org.eclipse.vjet.vjo.java.util.List','org.eclipse.vjet.vjo.java.util.Vector',
    'org.eclipse.vjet.vjo.java.io.InputStream','org.eclipse.vjet.vjo.java.lang.System',
    'org.eclipse.vjet.vjo.java.util.StringTokenizer','org.eclipse.vjet.vjo.java.lang.IllegalArgumentException',
    'org.eclipse.vjet.vjo.java.util.Iterator','org.eclipse.vjet.vjo.java.util.HashMap',
    'bundles.framework.FrameworkContext','bundles.framework.FileArchive',
    'bundles.framework.BundleArchive','bundles.framework.BundleGeneration',
    'osgi.framework.Constants','bundles.framework.Util',
    'bundles.framework.Alias','osgi.framework.Version',
    'osgi.framework.VersionRange','osgi.framework.FrameworkUtil',
    'osgi.framework.InvalidSyntaxException','osgi.framework.BundleException',
    'org.eclipse.vjet.vjo.java.lang.StringUtil','vjo.java.lang.ObjectUtil'])
.needs('bundles.framework.Debug','')
.protos({
    fwCtx:null, 
    archives:null, 
    nativeLibs:null, 
    debug:null, 
    bid:0, 
    
    
    
    constructs:function(){
        this.archives=new this.vj$.ArrayList(4);
        if(arguments.length===3){
            this.constructs_3_0_BundleClassPath_ovld(arguments[0],arguments[1],arguments[2]);
        }else if(arguments.length===2){
            this.constructs_2_0_BundleClassPath_ovld(arguments[0],arguments[1]);
        }
    },
    
    constructs_3_0_BundleClassPath_ovld:function(ba,frags,fwCtx){
        this.fwCtx=fwCtx;
        bundles.framework.Debug=fwCtx.debug;
        this.bid=ba.getBundleId();
        this.checkBundleArchive(ba,frags);
        if(frags!==null){
            for (var bundleGeneration,_$itr=frags.iterator();_$itr.hasNext();){
                bundleGeneration=_$itr.next();
                this.checkBundleArchive(bundleGeneration.archive,null);
            }
        }
        this.resolveNativeCode(ba,false);
        if(frags!==null){
            for (var bundleGeneration,_$itr=frags.iterator();_$itr.hasNext();){
                bundleGeneration=_$itr.next();
                this.resolveNativeCode(bundleGeneration.archive,true);
            }
        }
    },
    
    constructs_2_0_BundleClassPath_ovld:function(ba,fwCtx){
        this.fwCtx=fwCtx;
        bundles.framework.Debug=fwCtx.debug;
        this.bid=ba.getBundleId();
        this.checkBundleArchive(ba,null);
    },
    
    attachFragment:function(gen){
        this.checkBundleArchive(gen.archive,null);
        this.resolveNativeCode(gen.archive,true);
    },
    
    componentExists:function(component,onlyFirst,dirs){
        var v=null; 
        if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(component,"/")){
            component=component.substring(1);
        }
        if(this.classLoader){
            bundles.framework.Debug.println(this+"compentExists: "+component);
        }
        if(0===component.length){
            if(onlyFirst){
                v=new this.vj$.Vector(1);
                v.addElement(this.archives.get(0));
                if(this.classLoader){
                    bundles.framework.Debug.println(this+"compentExists added first top in classpath.");
                }
            }else {
                v=new this.vj$.Vector(this.archives);
                if(this.classLoader){
                    bundles.framework.Debug.println(this+"compentExists added all tops in classpath.");
                }
            }
        }else {
            for (var fa,_$itr=this.archives.iterator();_$itr.hasNext();){
                fa=_$itr.next();
                if(fa.exists(component,dirs)){
                    if(v===null){
                        v=new this.vj$.Vector();
                    }
                    v.addElement(fa);
                    if(this.classLoader){
                        bundles.framework.Debug.println(this+"compentExists added: "+fa);
                    }
                    if(onlyFirst){
                        break;
                    }
                }
            }
        }
        return v;
    },
    
    getInputStream:function(component,ix){
        if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(component,"/")){
            component=component.substring(1);
        }
        return this.archives.get(ix).getBundleResourceStream(component);
    },
    
    getNativeLibrary:function(libName){
        if(this.classLoader){
            bundles.framework.Debug.println(this+"getNativeLibrary: lib="+libName);
        }
        if(this.nativeLibs!==null){
            var keys=[this.vj$.System.mapLibraryName(libName),libName]; 
            var fa=null; 
            var key=null; 
            for (var k,_$i0=0;_$i0<keys.length;_$i0++){
                k=keys[_$i0];
                key=k;
                if(this.classLoader){
                    bundles.framework.Debug.println(this+"getNativeLibrary: try, "+key);
                }
                fa=this.nativeLibs.get(key);
                if(fa===null){
                    var libExtensions=this.fwCtx.props.getProperty(this.vj$.Constants.FRAMEWORK_LIBRARY_EXTENSIONS); 
                    var pos=key.lastIndexOf("."); 
                    if(libExtensions.length>0&&pos> -1){
                        var baseKey=key.substring(0,pos+1); 
                        var exts=this.vj$.Util.splitwords(libExtensions,", \t"); 
                        for (var ext,_$i1=0;_$i1<exts.length;_$i1++){
                            ext=exts[_$i1];
                            key=baseKey+ext;
                            if(this.classLoader){
                                bundles.framework.Debug.println(this+"getNativeLibrary: try, "+key);
                            }
                            fa=this.nativeLibs.get(key);
                            if(fa!==null){
                                break;
                            }
                        }
                    }
                }
                if(fa!==null){
                    break;
                }
            }
            if(fa===null){
                return null;
            }
            if(this.classLoader){
                bundles.framework.Debug.println(this+"getNativeLibrary: got, "+fa);
            }
            return fa.getNativeLibrary(key);
        }
        return null;
    },
    
    toString:function(){
        return "BundleClassPath(#"+this.bid+").";
    },
    
    checkBundleArchive:function(ba,frags){
        var bcp=ba.getAttribute(this.vj$.Constants.BUNDLE_CLASSPATH); 
        if(bcp!==null){
            var st=new this.vj$.StringTokenizer(bcp,","); 
            while(st.hasMoreTokens()){
                var path=st.nextToken().trim(); 
                var a=ba.getFileArchive(path); 
                if(a===null&&frags!==null){
                    for (var bundleGeneration,_$itr=frags.iterator();_$itr.hasNext();){
                        bundleGeneration=_$itr.next();
                        a=bundleGeneration.archive.getFileArchive(path);
                        if(a!==null){
                            break;
                        }
                    }
                }
                if(a!==null){
                    this.archives.add(a);
                    if(this.classLoader){
                        bundles.framework.Debug.println(this+"- Added path entry: "+a);
                    }
                }else {
                    this.fwCtx.frameworkWarning(ba.getBundleGeneration().bundle,new this.vj$.IllegalArgumentException(this.vj$.Constants.BUNDLE_CLASSPATH+" entry "+path+" not found in bundle"));
                    if(this.classLoader){
                        bundles.framework.Debug.println(this+"- Failed to find class path entry: "+path);
                    }
                }
            }
        }else {
            this.archives.add(ba.getFileArchive("."));
        }
    },
    
    resolveNativeCode:function(ba,isFrag){
        var bnc=ba.getAttribute(this.vj$.Constants.BUNDLE_NATIVECODE); 
        if(bnc!==null){
            var proc=new this.vj$.ArrayList(3); 
            var procP=org.eclipse.vjet.vjo.java.lang.StringUtil.toLowerCase(this.fwCtx.props.getProperty(this.vj$.Constants.FRAMEWORK_PROCESSOR)); 
            proc.add(procP);
            var procS=this.vj$.System.getProperty("os.arch").toLowerCase(); 
            if(!vjo.java.lang.ObjectUtil.equals(procP,procS)){
                proc.add(procS);
            }
            if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(procP,"arm_")){
                proc.add("arm");
            }
            for (var i=0;i<this.vj$.Alias.processorAliases.length;i++){
                if(org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(procP,this.vj$.Alias.processorAliases[i][0])){
                    for (var j=1;j<this.vj$.Alias.processorAliases[i].length;j++){
                        if(!vjo.java.lang.ObjectUtil.equals(procS,this.vj$.Alias.processorAliases[i][j])){
                            proc.add(this.vj$.Alias.processorAliases[i][j]);
                        }
                    }
                    break;
                }
            }
            var os=new this.vj$.ArrayList(); 
            var osP=org.eclipse.vjet.vjo.java.lang.StringUtil.toLowerCase(this.fwCtx.props.getProperty(this.vj$.Constants.FRAMEWORK_OS_NAME)); 
            os.add(osP);
            var osS=this.vj$.System.getProperty("os.name").toLowerCase(); 
            if(!vjo.java.lang.ObjectUtil.equals(osS,osP)){
                os.add(osS);
            }
            for (var i=0;i<this.vj$.Alias.osNameAliases.length;i++){
                if(org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(osP,this.vj$.Alias.osNameAliases[i][0])){
                    for (var j=1;j<this.vj$.Alias.osNameAliases[i].length;j++){
                        if(!vjo.java.lang.ObjectUtil.equals(osS,this.vj$.Alias.osNameAliases[i][j])){
                            os.add(this.vj$.Alias.osNameAliases[i][j]);
                        }
                    }
                    break;
                }
            }
            var osVer=new this.vj$.Version(this.fwCtx.props.getProperty(this.vj$.Constants.FRAMEWORK_OS_VERSION)); 
            var osLang=this.fwCtx.props.getProperty(this.vj$.Constants.FRAMEWORK_LANGUAGE); 
            var optional=false; 
            var best=null; 
            var bestVer=null; 
            var bestLang=false; 
            var hes=this.vj$.Util.parseManifestHeader(this.vj$.Constants.BUNDLE_NATIVECODE,bnc,false,false,false); 
            for (var heIt=hes.iterator();heIt.hasNext();){
                var he=heIt.next(); 
                var matchVer=null; 
                var matchLang=false; 
                var keys=he.getKeys(); 
                if(keys.size()===1&&vjo.java.lang.ObjectUtil.equals("*",keys.get(0))&& !heIt.hasNext()){
                    optional=true;
                    break;
                }
                var pl=he.getAttributes().get(this.vj$.Constants.BUNDLE_NATIVECODE_PROCESSOR); 
                if(pl!==null){
                    if(!containsIgnoreCase(proc,pl)){
                        continue;
                    }
                }else {
                    continue;
                }
                var ol=he.getAttributes().get(this.vj$.Constants.BUNDLE_NATIVECODE_OSNAME); 
                if(ol!==null){
                    if(!containsIgnoreCase(os,ol)){
                        continue;
                    }
                }else {
                    continue;
                }
                var ver=he.getAttributes().get(this.vj$.Constants.BUNDLE_NATIVECODE_OSVERSION); 
                if(ver!==null){
                    var okVer=false; 
                    for (var string,_$itr=ver.iterator();_$itr.hasNext();){
                        string=_$itr.next();
                        matchVer=new this.vj$.VersionRange(string);
                        if(matchVer.includes(osVer)){
                            okVer=true;
                            break;
                        }
                    }
                    if(!okVer){
                        continue;
                    }
                }
                var lang=he.getAttributes().get(this.vj$.Constants.BUNDLE_NATIVECODE_LANGUAGE); 
                if(lang!==null){
                    for (var string,_$itr=lang.iterator();_$itr.hasNext();){
                        string=_$itr.next();
                        if(org.eclipse.vjet.vjo.java.lang.StringUtil.equalsIgnoreCase(osLang,string)){
                            matchLang=true;
                            break;
                        }
                    }
                    if(!matchLang){
                        continue;
                    }
                }
                var sf=he.getAttributes().get(this.vj$.Constants.SELECTION_FILTER_ATTRIBUTE); 
                if(sf!==null){
                    var sfs=sf.get(0); 
                    if(sf.size()===1){
                        try {
                            if(!(this.vj$.FrameworkUtil.createFilter(sfs)).match(this.fwCtx.props.getProperties())){
                                continue;
                            }
                        }
                        catch(ise){
                            throw new this.vj$.BundleException("Bundle#"+this.bid+", Invalid syntax for native code selection filter: "+sfs,this.vj$.BundleException.NATIVECODE_ERROR,ise);
                        }
                    }else {
                        throw new this.vj$.BundleException("Bundle#"+this.bid+", Invalid character after native code selection filter: "+sfs,this.vj$.BundleException.NATIVECODE_ERROR);
                    }
                }
                if(best!==null){
                    var verEqual=false; 
                    if(bestVer!==null){
                        if(matchVer===null){
                            continue;
                        }
                        var d=bestVer.getLeft().compareTo(matchVer.getLeft()); 
                        if(d===0){
                            verEqual=true;
                        }else if(d>0){
                            continue;
                        }
                    }else if(matchVer===null){
                        verEqual=true;
                    }
                    if(verEqual&&(!matchLang||bestLang)){
                        continue;
                    }
                }
                best=keys;
                bestVer=matchVer;
                bestLang=matchLang;
            }
            if(best===null){
                if(optional){
                    return;
                }else {
                    throw new this.vj$.BundleException("Bundle#"+this.bid+", no matching native code libraries found for os="+os+" version="+osVer+", processor="+proc+" and language="+osLang+".",this.vj$.BundleException.NATIVECODE_ERROR);
                }
            }
            this.nativeLibs=new this.vj$.HashMap();
            bloop:
            for (var name,_$itr=best.iterator();_$itr.hasNext();){
                name=_$itr.next();
                for (var fa,_$itr=this.archives.iterator();_$itr.hasNext();){
                    fa=_$itr.next();
                    if(!isFrag||fa.getBundleGeneration().archive===ba){
                        var key=fa.checkNativeLibrary(name); 
                        if(key!==null){
                            this.nativeLibs.put(key,fa);
                            if(this.classLoader){
                                bundles.framework.Debug.println(this+"- Registered native library: "+key+" -> "+fa);
                            }
                            continue bloop;
                        }
                    }
                }
                throw new this.vj$.BundleException("Bundle#"+this.bid+", failed to resolve native code: "+name,this.vj$.BundleException.NATIVECODE_ERROR);
            }
        }else {
            this.nativeLibs=null;
        }
    },
    
    containsIgnoreCase:function(fl,l){
        for (var string,_$itr=l.iterator();_$itr.hasNext();){
            string=_$itr.next();
            var s=org.eclipse.vjet.vjo.java.lang.StringUtil.toLowerCase(string); 
            for (var string2,_$itr=fl.iterator();_$itr.hasNext();){
                string2=_$itr.next();
                if(this.vj$.Util.filterMatch(string2,s)){
                    return true;
                }
            }
        }
        return false;
    }
})
.endType();