/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.ExportedPackageImpl') 
.needs('org.eclipse.vjet.vjo.java.util.HashSet')
.needs('bundles.framework.Pkg','')

.satisfies('osgi.service.packageadmin.ExportedPackage')
.protos({
    pkg:null, 
    
    constructs:function(pkg){
        bundles.framework.Pkg=pkg;
    },
    
    getName:function(){
        return bundles.framework.Pkg.name;
    },
    
    getExportingBundle:function(){
        if(this.pkg!==null){
            return bundles.framework.Pkg.bpkgs.bg.bundle;
        }else {
            return null;
        }
    },
    
    getImportingBundles:function(){
        var imps=bundles.framework.Pkg.getPackageImporters(); 
        if(imps!==null){
            var bs=new this.vj$.HashSet(); 
            for (var ip,_$itr=imps.iterator();_$itr.hasNext();){
                ip=_$itr.next();
                bs.add(ip.bpkgs.bg.bundle);
            }
            var rl=bundles.framework.Pkg.bpkgs.getRequiredBy(); 
            for (var bp,_$itr=rl.iterator();_$itr.hasNext();){
                bp=_$itr.next();
                bs.add(bp.bg.bundle);
            }
            return bs.toArray(vjo.createArray(null, bs.size()));
        }else {
            return null;
        }
    },
    
    getSpecificationVersion:function(){
        return bundles.framework.Pkg.version.toString();
    },
    
    isRemovalPending:function(){
        if(bundles.framework.Pkg.isProvider()){
            return bundles.framework.Pkg.zombie;
        }else {
            return false;
        }
    },
    
    getVersion:function(){
        return bundles.framework.Pkg.version;
    },
    
    toString:function(){
        return bundles.framework.Pkg.name+"("+bundles.framework.Pkg.version+")";
    }
})
.endType();