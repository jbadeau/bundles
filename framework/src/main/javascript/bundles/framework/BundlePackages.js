/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.BundlePackages') 
.needs(['org.eclipse.vjet.vjo.java.util.ArrayList','org.eclipse.vjet.vjo.java.util.Map',
    'org.eclipse.vjet.vjo.java.util.List','org.eclipse.vjet.vjo.java.util.TreeMap',
    'org.eclipse.vjet.vjo.java.util.Iterator','org.eclipse.vjet.vjo.java.lang.IllegalArgumentException',
    'org.eclipse.vjet.vjo.java.lang.Math','org.eclipse.vjet.vjo.java.lang.IllegalStateException',
    'org.eclipse.vjet.vjo.java.util.HashMap','org.eclipse.vjet.vjo.java.util.Set',
    'org.eclipse.vjet.vjo.java.util.HashSet','org.eclipse.vjet.vjo.java.util.SortedSet',
    'org.eclipse.vjet.vjo.java.util.TreeSet','bundles.framework.BundleGeneration',
    'bundles.framework.ExportPkg','bundles.framework.ImportPkg',
    'bundles.framework.RequireBundle','bundles.framework.BundleArchive',
    'bundles.framework.Util','osgi.framework.Constants',
    'osgi.framework.Bundle','bundles.framework.BundleCapabilityImpl',
    'bundles.framework.BundleRequirementImpl','bundles.framework.BundleImpl',
    'bundles.framework.SystemBundle','bundles.framework.FrameworkContext',
    'osgi.framework.BundleException','bundles.framework.IteratorIterator',
    'org.eclipse.vjet.vjo.java.util.Collections','bundles.framework.IteratorIteratorSorted',
    'osgi.framework.wiring.BundleRequirement','vjo.java.lang.ObjectUtil',
    'org.eclipse.vjet.vjo.java.lang.StringUtil'])
.props({
    EMPTY_STRING:"", 
    epComp:null, 
    epFind:null, 
    ipComp:null, 
    ipFind:null 
})
.protos({
    bg:null, 
    exports:null, 
    imports:null, 
    dImportPatterns:null, 
    capabilities:null, 
    fragments:null, 
    require:null, 
    requiredBy:null, 
    okImports:null, 
    registered:false, 
    failReason:null, 
    nextDynId:0, 
    
    
    
    
    constructs:function(){
        this.exports=new this.vj$.ArrayList(1);
        this.imports=new this.vj$.ArrayList(1);
        this.dImportPatterns=new this.vj$.ArrayList(1);
        if(arguments.length===1){
            this.constructs_1_0_BundlePackages_ovld(arguments[0]);
        }else if(arguments.length===2){
            this.constructs_2_0_BundlePackages_ovld(arguments[0],arguments[1]);
        }else if(arguments.length===3){
            this.constructs_3_0_BundlePackages_ovld(arguments[0],arguments[1],arguments[2]);
        }
    },
    
    constructs_1_0_BundlePackages_ovld:function(bg){
        this.bg=bg;
        var ba=bg.archive; 
        for (var he,_$itr=this.vj$.Util.parseManifestHeader(this.vj$.Constants.IMPORT_PACKAGE,ba.getAttribute(this.vj$.Constants.IMPORT_PACKAGE),false,true,false).iterator();_$itr.hasNext();){
            he=_$itr.next();
            var pi=he.getKeys().iterator(); 
            var ip=new this.vj$.ImportPkg(pi.next(),he,this,false); 
            for (;;){
                var ii=this.vj$.Util.binarySearch(this.imports,this.vj$.BundlePackages.ipComp,ip); 
                if(ii<0){
                    this.imports.add(-ii-1,ip);
                }else {
                    throw new this.vj$.IllegalArgumentException("Duplicate import definitions for - "+ip.name);
                }
                if(pi.hasNext()){
                    ip=new this.vj$.ImportPkg(ip,pi.next());
                }else {
                    break;
                }
            }
        }
        for (var he,_$itr=this.vj$.Util.parseManifestHeader(this.vj$.Constants.EXPORT_PACKAGE,ba.getAttribute(this.vj$.Constants.EXPORT_PACKAGE),false,true,false).iterator();_$itr.hasNext();){
            he=_$itr.next();
            var keys=he.getKeys(); 
            var pi=keys.iterator(); 
            var ep=new this.vj$.ExportPkg(pi.next(),he,this); 
            for (;;){
                var ei=this.vj$.Math.abs(this.vj$.Util.binarySearch(this.exports,this.vj$.BundlePackages.epComp,ep)+1); 
                this.exports.add(ei,ep);
                if(!bg.v2Manifest){
                    var ip=new this.vj$.ImportPkg(ep); 
                    var ii=this.vj$.Util.binarySearch(this.imports,this.vj$.BundlePackages.ipComp,ip); 
                    if(ii<0){
                        this.imports.add(-ii-1,ip);
                    }
                }
                if(pi.hasNext()){
                    ep=new this.vj$.ExportPkg(ep,pi.next());
                }else {
                    break;
                }
            }
        }
        this.parseDynamicImports(ba.getAttribute(this.vj$.Constants.DYNAMICIMPORT_PACKAGE));
        var hes=this.vj$.Util.parseManifestHeader(this.vj$.Constants.REQUIRE_BUNDLE,ba.getAttribute(this.vj$.Constants.REQUIRE_BUNDLE),true,true,false); 
        if(!hes.isEmpty()){
            this.require=new this.vj$.ArrayList();
            for (var he,_$itr=hes.iterator();_$itr.hasNext();){
                he=_$itr.next();
                this.require.add(new this.vj$.RequireBundle(this,he));
            }
        }else {
            this.require=null;
        }
        this.capabilities=bg.getDeclaredCapabilities();
    },
    
    constructs_2_0_BundlePackages_ovld:function(bg,exportString){
        this.bg=bg;
        for (var he,_$itr=this.vj$.Util.parseManifestHeader(this.vj$.Constants.EXPORT_PACKAGE,exportString,false,true,false).iterator();_$itr.hasNext();){
            he=_$itr.next();
            var keys=he.getKeys(); 
            var pi=keys.iterator(); 
            var ep=new this.vj$.ExportPkg(pi.next(),he,this); 
            for (;;){
                var ei=this.vj$.Util.binarySearch(this.exports,this.vj$.BundlePackages.epComp,ep); 
                if(ei>=0){
                    this.exports.set(ei,ep);
                }else {
                    this.exports.add(-ei-1,ep);
                }
                if(pi.hasNext()){
                    ep=new this.vj$.ExportPkg(ep,pi.next());
                }else {
                    break;
                }
            }
        }
        this.require=null;
        this.capabilities=bg.getDeclaredCapabilities();
    },
    
    constructs_3_0_BundlePackages_ovld:function(host,frag,noNew){
        this.bg=host.bg;
        for (var iiter=frag.getImports();iiter.hasNext();){
            var fip=iiter.next(); 
            var ip=host.getImport(fip.name); 
            if(ip!==null){
                if(!fip.intersect(ip)){
                    throw new this.vj$.IllegalStateException("Host bundle import package and fragment bundle "+"import package doesn't intersect, resolve isn't possible.");
                }
            }else if(noNew){
                throw new this.vj$.IllegalStateException("Resolve host bundle package would "+"be shadow by new fragment import.");
            }
            this.imports.add(new this.vj$.ImportPkg(fip,this));
        }
        if(frag.require!==null){
            this.require=new this.vj$.ArrayList();
            for (var fragReq,_$itr=frag.require.iterator();_$itr.hasNext();){
                fragReq=_$itr.next();
                var match=false; 
                if(host.require!==null){
                    for (var req,_$itr=host.require.iterator();_$itr.hasNext();){
                        req=_$itr.next();
                        if(vjo.java.lang.ObjectUtil.equals(fragReq.name,req.name)){
                            if(fragReq.overlap(req)){
                                match=true;
                            }else {
                                throw new this.vj$.IllegalStateException("Fragment bundle required bundle doesn't completely "+"overlap required bundle in host bundle.");
                            }
                        }
                    }
                }
                if(!match){
                    if(this.bg.bundle.state!==this.vj$.Bundle.INSTALLED){
                        throw new this.vj$.IllegalStateException("Can not attach a fragment with new required "+"bundles to a resolved host");
                    }
                    this.require.add(new this.vj$.RequireBundle(fragReq,this));
                }
            }
        }else {
            this.require=null;
        }
        for (var eiter=frag.getExports();eiter.hasNext();){
            var fep=eiter.next(); 
            var hep=this.getExport(fep.name); 
            if(fep.pkgEquals(hep)){
                continue;
            }
            this.exports.add(new this.vj$.ExportPkg(fep,this));
        }
        this.capabilities=new this.vj$.HashMap();
        for (var e,_$itr=frag.bg.getDeclaredCapabilities().entrySet().iterator();_$itr.hasNext();){
            e=_$itr.next();
            var l=new this.vj$.ArrayList(); 
            for (var bc,_$itr=e.getValue().iterator();_$itr.hasNext();){
                bc=_$itr.next();
                l.add(new this.vj$.BundleCapabilityImpl(bc,this.bg));
            }
            this.capabilities.put(e.getKey(),l);
        }
    },
    
    registerPackages:function(){
        this.bg.bundle.fwCtx.resolver.registerCapabilities(this.capabilities,this.exports.iterator(),this.imports.iterator());
        this.registered=true;
    },
    
    unregisterPackages:function(force){
        if(this.registered){
            if(this.bg.bundle.fwCtx.resolver.unregisterCapabilities(this.capabilities,this.getExports(),this.getImports(),force)){
                this.registered=false;
                if(this.okImports!==null){
                    this.okImports=null;
                    for (var lbr,_$itr=this.bg.getOtherRequirements().values().iterator();_$itr.hasNext();){
                        lbr=_$itr.next();
                        for (var br,_$itr=lbr.iterator();_$itr.hasNext();){
                            br=_$itr.next();
                            br.resetWire();
                        }
                    }
                    this.unRequireBundles();
                    this.detachFragments();
                }
            }else {
                return false;
            }
        }
        return true;
    },
    
    resolvePackages:function(triggers){
        this.failReason=this.bg.bundle.fwCtx.resolver.resolve(this.bg,this,triggers);
        if(this.failReason===null){
            this.okImports=new this.vj$.ArrayList(this.imports.size());
            for (var i=this.getImports();i.hasNext();){
                var ip=i.next(); 
                if(ip.provider!==null){
                    this.okImports.add(ip);
                }
            }
            return true;
        }else {
            return false;
        }
    },
    
    getResolveFailReason:function(){
        return this.failReason;
    },
    
    getProviderBundlePackages:function(pkg){
        if(bundles.framework.SystemBundle.isInstance(this.bg.bundle)){
            return this.isExported(pkg)?this:null;
        }
        if(this.okImports===null){
            return null;
        }
        var ii=this.vj$.Util.binarySearch(this.okImports,this.vj$.BundlePackages.ipFind,pkg); 
        if(ii>=0){
            return this.okImports.get(ii).provider.bpkgs;
        }
        return null;
    },
    
    getSubProvider:function(pkg){
        var res=new this.vj$.HashSet(); 
        if(pkg.length>0){
            pkg=pkg+".";
        }
        if(this.okImports!==null){
            for (var ip,_$itr=this.okImports.iterator();_$itr.hasNext();){
                ip=_$itr.next();
                if(ip.provider!==null&&org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(ip.name,pkg)){
                    var n=ip.name.substring(pkg.length); 
                    if(n.indexOf('.')=== -1){
                        res.add(n);
                    }
                }
            }
            for (var irb=this.getRequire();irb.hasNext();){
                var rb=irb.next(); 
                if(rb.bpkgs!==null){
                    for (var iep=rb.bpkgs.getExports();iep.hasNext();){
                        var ep=iep.next(); 
                        if(org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(ep.name,pkg)){
                            var n=ep.name.substring(pkg.length); 
                            if(n.indexOf('.')=== -1){
                                res.add(n);
                            }
                        }
                    }
                }
            }
        }
        return res;
    },
    
    getDynamicProviderBundlePackages:function(pkg){
        if(this.okImports===null){
            return null;
        }
        var ii=this.vj$.Util.binarySearch(this.okImports,this.vj$.BundlePackages.ipFind,pkg); 
        if(ii>=0){
            return this.okImports.get(ii).provider.bpkgs;
        }
        var res=null; 
        var trigger=null; 
        var fwCtx=this.bg.bundle.fwCtx; 
        try {
            for (var ip,_$itr=this.dImportPatterns.iterator();_$itr.hasNext();){
                ip=_$itr.next();
                if(ip.name===this.vj$.BundlePackages.EMPTY_STRING||(org.eclipse.vjet.vjo.java.lang.StringUtil.endsWith(ip.name,".")&&org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(pkg,ip.name))||vjo.java.lang.ObjectUtil.equals(pkg,ip.name)){
                    if(trigger===null){
                        trigger=[this.bg.bundle];
                        fwCtx.resolverHooks.beginResolve(trigger);
                    }
                    var nip=new this.vj$.ImportPkg(ip,pkg); 
                    var ep=fwCtx.resolver.registerDynamicImport(nip); 
                    if(ep!==null){
                        nip.provider=ep;
                        nip.dynId=++this.nextDynId;
                        this.okImports.add(-ii-1,nip);
                        res=ep.bpkgs;
                        break;
                    }
                }
            }
        }
        catch(be){
            fwCtx.frameworkError(this.bg.bundle,be);
        }
        if(trigger!==null){
            try {
                fwCtx.resolverHooks.endResolve(trigger);
            }
            catch(be){
                fwCtx.frameworkError(this.bg.bundle,be);
            }
        }
        return res;
    },
    
    getRequire:function(){
        if(this.fragments!==null){
            {
                var iters=new this.vj$.ArrayList(this.fragments.size()+1); 
                if(this.require!==null){
                    iters.add(this.require.iterator());
                }
                for (var bundlePackages,_$itr=this.fragments.values().iterator();_$itr.hasNext();){
                    bundlePackages=_$itr.next();
                    iters.add(bundlePackages.getRequire());
                }
                return new this.vj$.IteratorIterator(iters);
            }
        }else if(this.require!==null){
            return this.require.iterator();
        }else {
            var res=this.vj$.Collections.EMPTY_LIST.iterator(); 
            return res;
        }
    },
    
    getRequiredBundleGenerations:function(pkg){
        var res=null; 
        for (var i=this.getRequire();i.hasNext();){
            var rb=i.next(); 
            if(rb.bpkgs!==null&&rb.bpkgs.isExported(pkg)){
                if(res===null){
                    res=new this.vj$.ArrayList(2);
                }
                res.add(rb.bpkgs.bg);
            }
        }
        return res;
    },
    
    addRequiredBy:function(r){
        if(this.requiredBy===null){
            this.requiredBy=new this.vj$.ArrayList();
        }
        this.requiredBy.add(r);
    },
    
    isRequired:function(){
        return this.requiredBy!==null&& !this.requiredBy.isEmpty();
    },
    
    isRequiredBy:function(cbp){
        return this.requiredBy!==null&&this.requiredBy.contains(cbp);
    },
    
    getRequiredBy:function(){
        var res=new this.vj$.ArrayList(); 
        if(this.requiredBy!==null){
            {
                res.addAll(this.requiredBy);
            }
            if(this.fragments!==null){
                {
                    for (var bundlePackages,_$itr=this.fragments.values().iterator();_$itr.hasNext();){
                        bundlePackages=_$itr.next();
                        var fl=bundlePackages.getRequiredBy(); 
                        if(fl!==null){
                            res.addAll(fl);
                        }
                    }
                }
            }
        }
        return res;
    },
    
    checkReExport:function(ep){
        var i=this.vj$.Util.binarySearch(this.exports,this.vj$.BundlePackages.epFind,ep.name); 
        if(i<0){
            var nep=new this.vj$.ExportPkg(ep,this); 
            this.exports.add(-i-1,nep);
            ep.pkg.addExporter(nep);
        }
    },
    
    getExport:function(pkg){
        var i=this.vj$.Util.binarySearch(this.exports,this.vj$.BundlePackages.epFind,pkg); 
        if(i>=0){
            return this.exports.get(i);
        }else {
            return null;
        }
    },
    
    
    getExports:function(){
        if(arguments.length===0){
            return this.getExports_0_0_BundlePackages_ovld();
        }else if(arguments.length===1){
            return this.getExports_1_0_BundlePackages_ovld(arguments[0]);
        }
    },
    
    getExports_0_0_BundlePackages_ovld:function(){
        if(this.fragments!==null){
            {
                var iters=new this.vj$.ArrayList(this.fragments.size()+1); 
                iters.add(this.exports.iterator());
                for (var bundlePackages,_$itr=this.fragments.values().iterator();_$itr.hasNext();){
                    bundlePackages=_$itr.next();
                    iters.add(bundlePackages.getExports());
                }
                return new this.vj$.IteratorIteratorSorted(iters,this.vj$.BundlePackages.epComp);
            }
        }else {
            return this.exports.iterator();
        }
    },
    
    getExports_1_0_BundlePackages_ovld:function(pkg){
        var res=new this.vj$.ArrayList(2); 
        var ep=this.getExport(pkg); 
        if(ep!==null){
            res.add(ep);
        }
        if(this.fragments!==null){
            {
                for (var bundlePackages,_$itr=this.fragments.values().iterator();_$itr.hasNext();){
                    bundlePackages=_$itr.next();
                    ep=bundlePackages.getExport(pkg);
                    if(ep!==null){
                        res.add(ep);
                    }
                }
            }
        }
        return res.isEmpty()?null:res.iterator();
    },
    
    isExported:function(pkg){
        if(this.getExport(pkg)!==null){
            return true;
        }
        if(this.fragments!==null){
            {
                for (var bundlePackages,_$itr=this.fragments.values().iterator();_$itr.hasNext();){
                    bundlePackages=_$itr.next();
                    if(bundlePackages.getExport(pkg)!==null){
                        return true;
                    }
                }
            }
        }
        return false;
    },
    
    getImports:function(){
        if(this.fragments!==null){
            {
                var iters=new this.vj$.ArrayList(this.fragments.size()+1); 
                iters.add(this.imports.iterator());
                for (var bundlePackages,_$itr=this.fragments.values().iterator();_$itr.hasNext();){
                    bundlePackages=_$itr.next();
                    iters.add(bundlePackages.getImports());
                }
                return new this.vj$.IteratorIteratorSorted(iters,this.vj$.BundlePackages.ipComp);
            }
        }else {
            return this.imports.iterator();
        }
    },
    
    getActiveImports:function(){
        if(this.okImports!==null){
            return this.okImports.iterator();
        }else if(this.bg.isFragment()){
            return this.bg.bpkgs.getActiveImports();
        }else {
            return null;
        }
    },
    
    getDeclaredPackageCapabilities:function(){
        var epCreationOrder=new this.vj$.TreeSet(this.exports); 
        return epCreationOrder;
    },
    
    getDeclaredPackageRequirements:function(){
        var ipCreationOrder=new this.vj$.TreeSet(this.imports); 
        ipCreationOrder.addAll(this.dImportPatterns);
        return ipCreationOrder;
    },
    
    getPackageCapabilities:function(){
        var res=new this.vj$.ArrayList(this.getDeclaredPackageCapabilities()); 
        if(this.fragments!==null){
            {
                for (var bpkgs,_$itr=this.fragments.values().iterator();_$itr.hasNext();){
                    bpkgs=_$itr.next();
                    res.addAll(bpkgs.getDeclaredPackageCapabilities());
                }
            }
        }
        return res;
    },
    
    getPackageRequirements:function(){
        var res=new this.vj$.ArrayList(); 
        for (var ip,_$itr=this.getDeclaredPackageRequirements().iterator();_$itr.hasNext();){
            ip=_$itr.next();
            if(ip.provider!==null||ip.isDynamic()){
                res.add(ip);
            }
        }
        if(this.fragments!==null){
            var parents=new this.vj$.HashSet(); 
            {
                for (var oip,_$itr=this.okImports.iterator();_$itr.hasNext();){
                    oip=_$itr.next();
                    if(oip.parent!==null&&oip.parent.bpkgs!==oip.bpkgs){
                        parents.add(oip.parent);
                    }
                }
            }
            {
                for (var bpkgs,_$itr=this.fragments.values().iterator();_$itr.hasNext();){
                    bpkgs=_$itr.next();
                    for (var ip,_$itr=bpkgs.getDeclaredPackageRequirements().iterator();_$itr.hasNext();){
                        ip=_$itr.next();
                        if(ip.isDynamic()||parents.contains(ip)){
                            res.add(ip);
                        }
                    }
                }
            }
        }
        return res;
    },
    
    getActiveChildImports:function(ip){
        var res=new this.vj$.ArrayList(); 
        for (var oip,_$itr=this.okImports.iterator();_$itr.hasNext();){
            oip=_$itr.next();
            if(oip.parent===ip){
                res.add(oip);
            }
        }
        return res;
    },
    
    getDeclaredBundleRequirements:function(){
        var res=new this.vj$.ArrayList(); 
        if(this.require!==null){
            var rbCreationOrder=new this.vj$.TreeSet(this.require); 
            res.addAll(rbCreationOrder);
        }
        return res;
    },
    
    getOtherCapabilities:function(){
        var res=this.capabilities; 
        if(this.fragments!==null){
            {
                var copied=false; 
                for (var bpkgs,_$itr=this.fragments.values().iterator();_$itr.hasNext();){
                    bpkgs=_$itr.next();
                    var frm=bpkgs.getOtherCapabilities(); 
                    if(!frm.isEmpty()){
                        if(!copied){
                            res=new this.vj$.HashMap(res);
                            copied=true;
                        }
                        for (var e,_$itr=frm.entrySet().iterator();_$itr.hasNext();){
                            e=_$itr.next();
                            var ns=e.getKey(); 
                            var p=res.get(ns); 
                            if(p!==null){
                                p=new this.vj$.ArrayList(p);
                                p.addAll(e.getValue());
                            }else {
                                p=e.getValue();
                            }
                            res.put(ns,p);
                        }
                    }
                }
            }
        }
        return res;
    },
    
    getClassLoader:function(){
        return this.bg.getClassLoader();
    },
    
    isRegistered:function(){
        return this.registered;
    },
    
    attachFragment:function(fbpkgs){
        var resolvedHost=this.okImports!==null; 
        var nfbpkgs=new this.vj$.BundlePackages(this,fbpkgs,resolvedHost); 
        nfbpkgs.registerPackages();
        if(resolvedHost){
            try {
                this.failReason=this.bg.bundle.fwCtx.resolver.resolve(this.bg,nfbpkgs,null);
            }
            catch(be){
                nfbpkgs.unregisterPackages(true);
                throw be;
            }
            if(this.failReason===null){
                for (var i=nfbpkgs.getImports();i.hasNext();){
                    var ip=i.next(); 
                    if(ip.provider!==null){
                        var ii=this.vj$.Util.binarySearch(this.okImports,this.vj$.BundlePackages.ipComp,ip); 
                        if(ii<0){
                            this.okImports.add(-ii-1,ip);
                        }
                    }
                }
            }else {
                nfbpkgs.unregisterPackages(true);
                return this.failReason;
            }
        }
        if(this.fragments===null){
            this.fragments=new this.vj$.TreeMap();
        }
        this.fragments.put(fbpkgs.bg,nfbpkgs);
        return null;
    },
    
    fragmentIsZombie:function(fb){
        if(null!==this.exports){
            if(this.bg.bundle.fwCtx.debug.resolver){
                this.bg.bundle.fwCtx.debug.println("Marking all packages exported by host bundle(id="+this.bg.bundle.id+",gen="+this.bg.generation+") as zombies since the attached fragment (id="+fb.getBundleId()+") was updated/uninstalled.");
            }
            for (var exportPkg,_$itr=this.exports.iterator();_$itr.hasNext();){
                exportPkg=_$itr.next();
                exportPkg.zombie=true;
            }
        }
    },
    
    detachFragmentSynchronized:function(fbg,unregister){
        if(this.fragments!==null){
            {
                this.detachFragment(fbg,unregister);
            }
        }
    },
    
    unregister:function(){
        this.registered=false;
        this.unRequireBundles();
    },
    
    toString:function(){
        return "BundlePackages"+this.bundleGenInfo();
    },
    
    bundleGenInfo:function(){
        return "[id="+this.bg.bundle.id+",gen="+this.bg.generation+"]";
    },
    
    isActive:function(){
        return this.okImports!==null;
    },
    
    getImport:function(pkg){
        var i=this.vj$.Util.binarySearch(this.imports,this.vj$.BundlePackages.ipFind,pkg); 
        if(i>=0){
            return this.imports.get(i);
        }else {
            return null;
        }
    },
    
    unRequireBundles:function(){
        if(this.require!==null){
            for (var req,_$itr=this.require.iterator();_$itr.hasNext();){
                req=_$itr.next();
                if(null!==req.bpkgs&&null!==req.bpkgs.requiredBy){
                    req.bpkgs.requiredBy.remove(this);
                }
            }
        }
    },
    
    detachFragments:function(){
        if(this.fragments!==null){
            {
                while(!this.fragments.isEmpty()){
                    this.detachFragment(this.fragments.lastKey(),false);
                }
                this.fragments=null;
            }
        }
    },
    
    detachFragment:function(fbg,unregisterPkg){
        if(null===this.okImports){
            var fbpkgs=this.fragments.remove(fbg); 
            if(fbpkgs!==null){
                if(unregisterPkg){
                    fbpkgs.unregisterPackages(true);
                }else {
                    fbpkgs.unregister();
                }
            }
        }
    },
    
    parseDynamicImports:function(s){
        for (var he,_$itr=this.vj$.Util.parseManifestHeader(this.vj$.Constants.DYNAMICIMPORT_PACKAGE,s,false,true,false).iterator();_$itr.hasNext();){
            he=_$itr.next();
            if(he.getDirectives().containsKey(this.vj$.Constants.RESOLUTION_DIRECTIVE)){
                throw new this.vj$.IllegalArgumentException(this.vj$.Constants.DYNAMICIMPORT_PACKAGE+" entry illegal contains a "+this.vj$.Constants.RESOLUTION_DIRECTIVE+" directive.");
            }
            var tmpl=null; 
            for (var key,_$itr=he.getKeys().iterator();_$itr.hasNext();){
                key=_$itr.next();
                if(vjo.java.lang.ObjectUtil.equals(key,"*")){
                    key=this.vj$.BundlePackages.EMPTY_STRING;
                }else if(org.eclipse.vjet.vjo.java.lang.StringUtil.endsWith(key,".*")){
                    key=key.substring(0,key.length-1);
                }else if(org.eclipse.vjet.vjo.java.lang.StringUtil.endsWith(key,".")){
                    throw new this.vj$.IllegalArgumentException(this.vj$.Constants.DYNAMICIMPORT_PACKAGE+" entry ends with '.': "+key);
                }else if(key.indexOf("*")!== -1){
                    throw new this.vj$.IllegalArgumentException(this.vj$.Constants.DYNAMICIMPORT_PACKAGE+" entry contains a '*': "+key);
                }
                if(tmpl!==null){
                    this.dImportPatterns.add(new this.vj$.ImportPkg(tmpl,key));
                }else {
                    tmpl=new this.vj$.ImportPkg(key,he,this,true);
                    this.dImportPatterns.add(tmpl);
                }
            }
        }
    }
})
.inits(function(){
    this.vj$.BundlePackages.epComp=
        vjo.make(this,this.vj$.Util.Comparator)
        .protos({
            compare:function(a,b){
                return org.eclipse.vjet.vjo.java.lang.StringUtil.compareTo(a.name,b.name);
            }
        })
        .endType();
    this.vj$.BundlePackages.epFind=
        vjo.make(this,this.vj$.Util.Comparator)
        .protos({
            compare:function(a,b){
                return org.eclipse.vjet.vjo.java.lang.StringUtil.compareTo(a.name,b);
            }
        })
        .endType();
    this.vj$.BundlePackages.ipComp=
        vjo.make(this,this.vj$.Util.Comparator)
        .protos({
            compare:function(a,b){
                return org.eclipse.vjet.vjo.java.lang.StringUtil.compareTo(a.name,b.name);
            }
        })
        .endType();
    this.vj$.BundlePackages.ipFind=
        vjo.make(this,this.vj$.Util.Comparator)
        .protos({
            compare:function(a,b){
                return org.eclipse.vjet.vjo.java.lang.StringUtil.compareTo(a.name,b);
            }
        })
        .endType();
})
.endType();