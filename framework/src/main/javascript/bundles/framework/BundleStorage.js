/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.itype('bundles.framework.BundleStorage') 

.protos({
    
    insertBundleJar:function(location,is){
    },
    
    updateBundleArchive:function(old,is){
    },
    
    replaceBundleArchive:function(oldBA,newBA){
    },
    
    getAllBundleArchives:function(){
    },
    
    getStartOnLaunchBundles:function(){
    },
    
    close:function(){
    }
})
.endType();