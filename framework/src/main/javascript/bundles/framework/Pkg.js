/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.Pkg') 
.needs(['org.eclipse.vjet.vjo.java.util.ArrayList','org.eclipse.vjet.vjo.java.lang.Math',
    'org.eclipse.vjet.vjo.java.lang.StringBuffer','bundles.framework.Util',
    'osgi.framework.Version'])

.props({
    epComp:null, 
    ipComp:null 
})
.protos({
    pkg:null, 
    exporters:null, 
    importers:null, 
    providers:null, 
    
    constructs:function(pkg){
        this.exporters=new this.vj$.ArrayList(1);
        this.importers=new this.vj$.ArrayList();
        this.providers=new this.vj$.ArrayList(1);
        this.vj$.Pkg=pkg;
    },
    
    addExporter:function(ep){
        var i=this.vj$.Math.abs(this.vj$.Util.binarySearch(this.exporters,this.vj$.Pkg.epComp,ep)+1); 
        this.exporters.add(i,ep);
        ep.attachPkg(this);
    },
    
    removeExporter:function(p){
        this.providers.remove(p);
        this.exporters.remove(p);
        p.detachPkg();
        return true;
    },
    
    addImporter:function(ip){
        var i=this.vj$.Math.abs(this.vj$.Util.binarySearch(this.importers,this.vj$.Pkg.ipComp,ip)+1); 
        this.importers.add(i,ip);
        ip.attachPkg(this);
    },
    
    removeImporter:function(ip){
        this.importers.remove(ip);
        ip.detachPkg();
    },
    
    addProvider:function(ep){
        var i=this.vj$.Util.binarySearch(this.providers,this.vj$.Pkg.epComp,ep); 
        if(i<0){
            this.providers.add(-i-1,ep);
        }
    },
    
    getBestProvider:function(){
        if(!this.providers.isEmpty()){
            return this.providers.get(0);
        }else {
            var best=null; 
            for (var exportPkg,_$itr=this.exporters.iterator();_$itr.hasNext();){
                exportPkg=_$itr.next();
                var ep=exportPkg; 
                if(ep.bpkgs.bg.bundle.isResolved()){
                    if(best===null||best.version.compareTo(ep.version)<0){
                        best=ep;
                    }
                }
            }
            return best;
        }
    },
    
    isEmpty:function(){
        return this.exporters.size()===0&&this.importers.size()===0;
    },
    
    
    toString:function(){
        if(arguments.length===0){
            return this.toString_0_0_Pkg_ovld();
        }else if(arguments.length===1){
            return this.toString_1_0_Pkg_ovld(arguments[0]);
        }
    },
    
    toString_0_0_Pkg_ovld:function(){
        return this.toString(2);
    },
    
    toString_1_0_Pkg_ovld:function(level){
        var sb=new this.vj$.StringBuffer(); 
        sb.append("Pkg[");
        if(level>0){
            sb.append("pkg="+this.vj$.Pkg);
        }
        if(level>1){
            sb.append(", providers="+this.providers);
        }
        if(level>2){
            sb.append(", exporters="+this.exporters);
        }
        sb.append("]");
        return sb.toString();
    }
})
.inits(function(){
    this.vj$.Pkg.epComp=
        vjo.make(this,this.vj$.Util.Comparator)
        .protos({
            compare:function(a,b){
                var d=a.version.compareTo(b.version); 
                if(d===0){
                    var ld=b.bpkgs.bg.bundle.id-a.bpkgs.bg.bundle.id; 
                    if(ld<0){
                        d=-1;
                    }else if(ld>0){
                        d=1;
                    }
                }
                return d;
            }
        })
        .endType();
    this.vj$.Pkg.ipComp=
        vjo.make(this,this.vj$.Util.Comparator)
        .protos({
            compare:function(a,b){
                var d; 
                if(a.packageRange===null){
                    d=b.packageRange===null?0:this.vj$.Version.emptyVersion.compareTo(b.packageRange.getLeft());
                }else if(b.packageRange===null){
                    d=a.packageRange.getLeft().compareTo(this.vj$.Version.emptyVersion);
                }else {
                    d=a.packageRange.getLeft().compareTo(b.packageRange.getLeft());
                }
                if(d===0){
                    var ld=b.bpkgs.bg.bundle.id-a.bpkgs.bg.bundle.id; 
                    if(ld<0){
                        d=-1;
                    }else if(ld>0){
                        d=1;
                    }
                }
                return d;
            }
        })
        .endType();
})
.endType();