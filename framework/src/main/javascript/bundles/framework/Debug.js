define([ "vjo", "osgi/framework/launch/FrameworkFactory", "./FrameworkContext" ], function(vjo, FrameworkFactory, FrameworkContext) {

	return vjo.ctype('bundles.framework.Debug')

	.props({

		AUTOMANIFEST_PROP : "org.bundles.framework.debug.automanifest",

		BUNDLE_RESOURCE_PROP : "org.bundles.framework.debug.bundle_resource",

		CERTIFICATES_PROP : "org.bundles.framework.debug.certificates",

		CLASSLOADER_PROP : "org.bundles.framework.debug.classloader",

		ERRORS_PROP : "org.bundles.framework.debug.errors",

		FRAMEWORK_PROP : "org.bundles.framework.debug.framework",

		HOOKS_PROP : "org.bundles.framework.debug.hooks",

		LAZY_ACTIVATION_PROP : "org.bundles.framework.debug.lazy_activation",

		LDAP_PROP : "org.bundles.framework.debug.ldap",

		RESOLVER_PROP : "org.bundles.framework.debug.resolver",

		PATCH_PROP : "org.bundles.framework.debug.patch",

		PERMISSIONS_PROP : "org.bundles.framework.debug.permissions",

		SERVICE_REFERENCE_PROP : "org.bundles.framework.debug.service_reference",

		STARTLEVEL_PROP : "org.bundles.framework.debug.startlevel",

		URL_PROP : "org.bundles.framework.debug.url",

		WARNINGS_PROP : "org.bundles.framework.debug.warnings"

	})

	.protos({

		insideDebug : null,

		automanifest : false,

		bundle_resource : false,

		certificates : false,

		classLoader : false,

		errors : false,

		framework : false,

		hooks : false,

		lazy_activation : false,

		ldap : false,

		resolver : false,

		patch : false,

		permissions : false,

		service_reference : false,

		startlevel : false,

		url : false,

		warnings : false,

		constructs : function(props) {
			props.setPropertyDefault(this.vj$.Debug.AUTOMANIFEST_PROP, props.vj$.FrameworkProperties.FALSE);
			props.setPropertyDefault(this.vj$.Debug.BUNDLE_RESOURCE_PROP, props.vj$.FrameworkProperties.FALSE);
			props.setPropertyDefault(this.vj$.Debug.CERTIFICATES_PROP, props.vj$.FrameworkProperties.FALSE);
			props.setPropertyDefault(this.vj$.Debug.CLASSLOADER_PROP, props.vj$.FrameworkProperties.FALSE);
			props.setPropertyDefault(this.vj$.Debug.ERRORS_PROP, props.vj$.FrameworkProperties.FALSE);
			props.setPropertyDefault(this.vj$.Debug.FRAMEWORK_PROP, props.vj$.FrameworkProperties.FALSE);
			props.setPropertyDefault(this.vj$.Debug.HOOKS_PROP, props.vj$.FrameworkProperties.FALSE);
			props.setPropertyDefault(this.vj$.Debug.LAZY_ACTIVATION_PROP, props.vj$.FrameworkProperties.FALSE);
			props.setPropertyDefault(this.vj$.Debug.RESOLVER_PROP, props.vj$.FrameworkProperties.FALSE);
			props.setPropertyDefault(this.vj$.Debug.PATCH_PROP, props.vj$.FrameworkProperties.FALSE);
			props.setPropertyDefault(this.vj$.Debug.PERMISSIONS_PROP, props.vj$.FrameworkProperties.FALSE);
			props.setPropertyDefault(this.vj$.Debug.SERVICE_REFERENCE_PROP, props.vj$.FrameworkProperties.FALSE);
			props.setPropertyDefault(this.vj$.Debug.STARTLEVEL_PROP, props.vj$.FrameworkProperties.FALSE);
			props.setPropertyDefault(this.vj$.Debug.URL_PROP, props.vj$.FrameworkProperties.FALSE);
			this.automanifest = props.getBooleanProperty(this.vj$.Debug.AUTOMANIFEST_PROP);
			this.bundle_resource = props.getBooleanProperty(this.vj$.Debug.BUNDLE_RESOURCE_PROP);
			this.certificates = props.getBooleanProperty(this.vj$.Debug.CERTIFICATES_PROP);
			this.classLoader = props.getBooleanProperty(this.vj$.Debug.CLASSLOADER_PROP);
			this.errors = props.getBooleanProperty(this.vj$.Debug.ERRORS_PROP);
			this.framework = props.getBooleanProperty(this.vj$.Debug.FRAMEWORK_PROP);
			this.hooks = props.getBooleanProperty(this.vj$.Debug.HOOKS_PROP);
			this.lazy_activation = props.getBooleanProperty(this.vj$.Debug.LAZY_ACTIVATION_PROP);
			this.resolver = props.getBooleanProperty(this.vj$.Debug.RESOLVER_PROP);
			this.patch = props.getBooleanProperty(this.vj$.Debug.PATCH_PROP);
			this.permissions = props.getBooleanProperty(this.vj$.Debug.PERMISSIONS_PROP);
			this.service_reference = props.getBooleanProperty(this.vj$.Debug.SERVICE_REFERENCE_PROP);
			this.startlevel = props.getBooleanProperty(this.vj$.Debug.STARTLEVEL_PROP);
			this.url = props.getBooleanProperty(this.vj$.Debug.URL_PROP);
			this.warnings = props.getBooleanProperty(this.vj$.Debug.WARNINGS_PROP);
		},

		useDoPrivileged : function() {
			if (this.vj$.System.getSecurityManager() !== null) {
				if (this.insideDebug === null) {
					this.insideDebug = vjo.make(this, this.vj$.ThreadLocal).protos({
						initialValue : function() {
							return org.eclipse.vjet.vjo.java.lang.BooleanUtil.valueOf_(false);
						}
					}).endType();
				}
				return true;
			}
			return false;
		},

		inside : function(b) {
			this.insideDebug.set(org.eclipse.vjet.vjo.java.lang.BooleanUtil.valueOf_(b));
		},

		isInside : function() {
			return (this.insideDebug.get()).booleanValue();
		},

		println0 : function(str) {
			this.vj$.System.err.println("## DEBUG: " + str);
		},

		println : function(str) {
			if (this.useDoPrivileged()) {
				if (!this.isInside()) {
					this.vj$.AccessController.doPrivileged(vjo.make(this, this.vj$.PrivilegedAction).protos({
						run : function() {
							inside(true);
							println0(str);
							inside(false);
							return null;
						}
					}).endType());
				}
			}
			else {
				this.println0(str);
			}
		},

		printStackTrace0 : function(str, t) {
			this.vj$.System.err.println("## DEBUG: " + str);
			t.printStackTrace();
			if (osgi.framework.BundleException.isInstance(t)) {
				var n = t.getNestedException();
				if (n !== null) {
					this.vj$.System.err.println("Nested bundle exception:");
					n.printStackTrace();
				}
			}
		},

		printStackTrace : function(str, t) {
			if (this.useDoPrivileged()) {
				if (!this.isInside()) {
					this.vj$.AccessController.doPrivileged(vjo.make(this, this.vj$.PrivilegedAction).protos({
						run : function() {
							inside(true);
							printStackTrace0(str, t);
							inside(false);
							return null;
						}
					}).endType());
				}
			}
			else {
				this.printStackTrace0(str, t);
			}
		}
	})

	.endType();

})