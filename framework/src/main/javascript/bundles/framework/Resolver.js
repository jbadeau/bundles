/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.Resolver') 
.needs(['org.eclipse.vjet.vjo.java.util.Hashtable','org.eclipse.vjet.vjo.java.util.HashSet',
    'org.eclipse.vjet.vjo.java.util.HashMap','org.eclipse.vjet.vjo.java.util.ArrayList',
    'org.eclipse.vjet.vjo.java.util.Collections','org.eclipse.vjet.vjo.java.lang.StringBuffer',
    'org.eclipse.vjet.vjo.java.util.TreeSet','org.eclipse.vjet.vjo.java.util.Comparator',
    'org.eclipse.vjet.vjo.java.lang.Exception','org.eclipse.vjet.vjo.java.util.LinkedList',
    'bundles.framework.Capabilities','bundles.framework.Pkg',
    'osgi.framework.Bundle','bundles.framework.FrameworkProperties'])

.props({
    RESOLVER_HOOK_VETO:"ResolverHook Veto" 
})
.protos({
    framework:null, 
    packages:null, 
    capabilities:null, 
    tempResolved:null, 
    tempProvider:null, 
    tempRequired:null, 
    tempBlackList:null, 
    tempBackTracked:null, 
    tempWires:null, 
    tempCollision:null, 
    tempBlackListChecks:0, 
    tempBlackListHits:0, 
    
    constructs:function(fw){
        this.packages=new this.vj$.Hashtable();
        this.capabilities=new this.vj$.Capabilities();
        this.framework=fw;
    },
    
    clear:function(){
        this.packages.clear();
        if(null!==this.tempResolved){
            this.tempResolved.clear();
        }
        if(null!==this.tempProvider){
            this.tempProvider.clear();
        }
        if(null!==this.tempRequired){
            this.tempRequired.clear();
        }
        if(null!==this.tempBlackList){
            this.tempBlackList.clear();
        }
        if(null!==this.tempBackTracked){
            this.tempBackTracked.clear();
        }
        if(null!==this.tempWires){
            this.tempWires.clear();
        }
    },
    
    registerCapabilities:function(capabilities,exports,imports){
        this.capabilities.addCapabilities(capabilities);
        while(exports.hasNext()){
            var pe=exports.next(); 
            var p=this.packages.get(pe.name); 
            if(p===null){
                p=new this.vj$.Pkg(pe.name);
                this.packages.put(pe.name,p);
            }
            p.addExporter(pe);
            if(this.framework.debug.resolver){
                this.framework.debug.println("registerPackages: export, "+pe);
            }
        }
        while(imports.hasNext()){
            var pe=imports.next(); 
            var p=this.packages.get(pe.name); 
            if(p===null){
                p=new this.vj$.Pkg(pe.name);
                this.packages.put(pe.name,p);
            }
            p.addImporter(pe);
            if(this.framework.debug.resolver){
                this.framework.debug.println("registerPackages: import, "+pe);
            }
        }
    },
    
    registerDynamicImport:function(ip){
        if(this.framework.debug.resolver){
            this.framework.debug.println("registerDynamicImport: try "+ip);
        }
        var res=null; 
        var p=this.packages.get(ip.name); 
        if(p!==null){
            while(this.tempResolved!==null){
                this.checkThread();
                try {
                    wait();
                }
                catch(_ignore){
                }
            }
            this.tempResolved=new this.vj$.HashSet();
            this.tempProvider=new this.vj$.HashMap();
            this.tempRequired=new this.vj$.HashMap();
            this.tempBlackList=new this.vj$.HashSet();
            this.tempBackTracked=new this.vj$.HashSet();
            this.backTrackUses(ip);
            this.tempBackTracked=null;
            var pkgs=this.vj$.Collections.singletonList(ip); 
            p.addImporter(ip);
            try {
                if(resolvePackages(pkgs.iterator(),null)){
                    this.registerNewProviders(ip.bpkgs.bg.bundle);
                    res=this.tempProvider.get(ip.name);
                    ip.provider=res;
                }else {
                    p.removeImporter(ip);
                }
            }
            catch(be){
                p.removeImporter(ip);
                throw be;
            }
            finally {
                this.tempBlackList=null;
                this.tempProvider=null;
                this.tempRequired=null;
                this.tempResolved=null;
                notifyAll();
            }
        }
        if(this.framework.debug.resolver){
            this.framework.debug.println("registerDynamicImport: Done for "+ip.name+", res = "+res);
        }
        return res;
    },
    
    unregisterCapabilities:function(capabilities,exports,imports,force){
        if(!force){
            var saved=new this.vj$.ArrayList(); 
            for (var i=exports;i.hasNext();){
                var ep=i.next(); 
                saved.add(ep);
                if(ep.bpkgs.isRequired()){
                    if(this.framework.debug.resolver){
                        this.framework.debug.println("unregisterPackages: Failed to unregister, "+ep+" is still in use via Require-Bundle.");
                    }
                    markAsZombies(saved,exports);
                    return false;
                }
                var p=ep.pkg; 
                if(p.providers.contains(ep)){
                    for (var element,_$itr=p.importers.iterator();_$itr.hasNext();){
                        element=_$itr.next();
                        var ip=element; 
                        if(ep===ip.provider&&ep.bpkgs!==ip.bpkgs){
                            if(this.framework.debug.resolver){
                                this.framework.debug.println("unregisterPackages: Failed to unregister, "+ep+" is still in use via import-package.");
                            }
                            markAsZombies(saved,exports);
                            return false;
                        }
                    }
                }
            }
            exports=saved.iterator();
            for (var lbc,_$itr=capabilities.values().iterator();_$itr.hasNext();){
                lbc=_$itr.next();
                for (var bc,_$itr=lbc.iterator();_$itr.hasNext();){
                    bc=_$itr.next();
                    if(bc.isWired()){
                        return false;
                    }
                }
            }
        }
        while(exports.hasNext()){
            var ep=exports.next(); 
            var p=ep.pkg; 
            if(this.framework.debug.resolver){
                this.framework.debug.println("unregisterPackages: unregister export - "+ep);
            }
            p.removeExporter(ep);
            if(p.isEmpty()){
                this.packages.remove(ep.name);
            }
        }
        while(imports.hasNext()){
            var ip=imports.next(); 
            var p=ip.pkg; 
            if(this.framework.debug.resolver){
                this.framework.debug.println("unregisterPackages: unregister import - "+ip.pkgString());
            }
            p.removeImporter(ip);
            if(p.isEmpty()){
                this.packages.remove(ip.name);
            }
        }
        this.capabilities.removeCapabilities(capabilities);
        for (var lbc,_$itr=capabilities.values().iterator();_$itr.hasNext();){
            lbc=_$itr.next();
            for (var bc,_$itr=lbc.iterator();_$itr.hasNext();){
                bc=_$itr.next();
                bc.removeWires();
            }
        }
        return true;
    },
    
    resolve:function(bg,importBpkgs,triggers){
        var res=null; 
        if(this.framework.debug.resolver){
            this.framework.debug.println("resolve: "+bg);
        }
        if(this.tempResolved!==null){
            if(this.tempResolved.remove(bg)){
                return null;
            }
            this.checkThread();
            do{
                try {
                    wait();
                }
                catch(_ignore){
                }
            }while(this.tempResolved!==null);
        }
        this.tempResolved=new this.vj$.HashSet();
        try {
            if(!this.addTempResolved(bg)){
                res=this.vj$.Resolver.RESOLVER_HOOK_VETO;
            }
        }
        catch(be){
            this.tempResolved=null;
            notifyAll();
            throw be;
        }
        if(res===null){
            var sbg=this.checkBundleSingleton(bg); 
            if(sbg!==null){
                res="Singleton bundle failed to resolve because "+sbg.bundle+" is already resolved";
            }
        }
        if(res!==null){
            this.tempResolved=null;
            notifyAll();
            return res;
        }
        var baseTempBlackList=new this.vj$.HashSet(); 
        this.tempBlackList=new this.vj$.HashSet();
        this.tempProvider=new this.vj$.HashMap();
        this.tempRequired=new this.vj$.HashMap();
        this.tempWires=new this.vj$.ArrayList();
        try {
            while(true){
                this.tempCollision=null;
                res=this.checkBundleRequirements(bg);
                if(res===null){
                    res=this.checkRequireBundle(bg);
                    if(res===null){
                        var failReason=new this.vj$.StringBuffer("Missing package(s) or can not resolve all of the them:"); 
                        if(this.resolvePackages(importBpkgs.getImports(),failReason)){
                            if(triggers!==null&&triggers.length===1){
                                this.framework.resolverHooks.endResolve(triggers);
                            }
                            this.registerNewWires();
                            this.registerNewProviders(bg.bundle);
                            res=null;
                        }else {
                            res=failReason.toString();
                        }
                    }
                }
                if(res!==null&&this.tempCollision!==null){
                    baseTempBlackList.add(this.tempCollision);
                    this.tempResolved.clear();
                    this.tempResolved.add(bg);
                    this.tempBlackList.clear();
                    this.tempBlackList.addAll(baseTempBlackList);
                    this.tempProvider.clear();
                    this.tempRequired.clear();
                    this.tempWires.clear();
                }else {
                    break;
                }
            }
        }
        finally {
            this.tempResolved=null;
            this.tempProvider=null;
            this.tempRequired=null;
            this.tempBlackList=null;
            this.tempWires=null;
            notifyAll();
        }
        if(this.framework.debug.resolver){
            this.framework.debug.println("resolve: Done for "+bg);
        }
        return res;
    },
    
    addTempResolved:function(bg){
        if(this.framework.resolverHooks.filterResolvable(bg)){
            this.tempResolved.add(bg);
            return true;
        }
        return false;
    },
    
    getPkg:function(pkg){
        return this.packages.get(pkg);
    },
    
    getZombieAffected:function(bundles){
        var affected=new this.vj$.TreeSet(
            vjo.make(this,this.vj$.Comparator)
            .protos({
                compare:function(b1,b2){
                    var dif=b1.getStartLevel()-b2.getStartLevel(); 
                    if(dif===0){
                        dif=this.vj$.Util.cast((b1.getBundleId()-b2.getBundleId()),'int');
                    }
                    return dif;
                    var dif=b1.getStartLevel()-b2.getStartLevel(); 
                    if(dif===0){
                        dif=this.vj$.Util.cast((b1.getBundleId()-b2.getBundleId()),'int');
                    }
                    return dif;
                },
                equals:function(o){
                    return ((o!==null)&&getClass().equals(o.getClass()));
                    return ((o!==null)&&getClass().equals(o.getClass()));
                }
            })
            .endType()); 
        if(bundles===null){
            if(this.framework.debug.resolver){
                this.framework.debug.println("getZombieAffected: check - null");
            }
            this.framework.bundles.getRemovalPendingBundles(affected);
            this.framework.bundles.getUnattachedBundles(affected);
        }else {
            for (var bundle,_$i0=0;_$i0<bundles.length;_$i0++){
                bundle=bundles[_$i0];
                var tmp=bundle; 
                if(tmp!==null){
                    if(this.framework.debug.resolver){
                        this.framework.debug.println("getZombieAffected: check - "+bundle);
                    }
                    affected.add(tmp);
                }
            }
        }
        closure(affected);
        return affected;
    },
    
    closure:function(bundles){
        var moreBundles=new this.vj$.ArrayList(bundles); 
        for (var i=0;i<moreBundles.size();i++){
            var b=moreBundles.get(i); 
            for (var j=b.getExports();j.hasNext();){
                var ep=j.next(); 
                if(ep.pkg!==null&&ep.pkg.providers.contains(ep)){
                    for (var ip,_$itr=ep.getPackageImporters().iterator();_$itr.hasNext();){
                        ip=_$itr.next();
                        var ib=ip.bpkgs.bg.bundle; 
                        if(!bundles.contains(ib)){
                            moreBundles.add(ib);
                            if(this.framework.debug.resolver){
                                this.framework.debug.println("closure: added importing bundle - "+ib);
                            }
                            bundles.add(ib);
                        }
                    }
                }
                for (var element,_$itr=ep.bpkgs.getRequiredBy().iterator();_$itr.hasNext();){
                    element=_$itr.next();
                    var rbpkgs=element; 
                    var rb=rbpkgs.bg.bundle; 
                    if(!bundles.contains(rb)){
                        moreBundles.add(rb);
                        if(this.framework.debug.resolver){
                            this.framework.debug.println("closure: added requiring bundle - "+rb);
                        }
                        bundles.add(rb);
                    }
                }
            }
            for (var bbg,_$itr=b.generations.iterator();_$itr.hasNext();){
                bbg=_$itr.next();
                var bwl=bbg.getCapabilityWires(); 
                if(bwl!==null){
                    for (var bcw,_$itr=bwl.iterator();_$itr.hasNext();){
                        bcw=_$itr.next();
                        var bbr=bcw.getRequirerGeneration().bundle; 
                        if(!bundles.contains(bbr)){
                            moreBundles.add(bbr);
                            if(this.framework.debug.resolver){
                                this.framework.debug.println("closure: added wired bundle - "+bbr);
                            }
                            bundles.add(bbr);
                        }
                    }
                }
                if(bbg.isFragmentHost()){
                    var fix=bbg.fragments.clone(); 
                    for (var fbg,_$itr=fix.iterator();_$itr.hasNext();){
                        fbg=_$itr.next();
                        if(!bundles.contains(fbg.bundle)){
                            moreBundles.add(fbg.bundle);
                            if(this.framework.debug.resolver){
                                this.framework.debug.println("closure: added fragment bundle - "+fbg.bundle);
                            }
                            bundles.add(fbg.bundle);
                        }
                    }
                }
                if(bbg.isFragment()){
                    var hosts=bbg.getResolvedHosts(); 
                    for (var hb,_$itr=hosts.iterator();_$itr.hasNext();){
                        hb=_$itr.next();
                        if(!bundles.contains(hb)){
                            moreBundles.add(hb);
                            if(this.framework.debug.resolver){
                                this.framework.debug.println("closure: added fragment host bundle - "+hb);
                            }
                            bundles.add(hb);
                        }
                    }
                }
            }
        }
    },
    
    backTrackUses:function(ip){
        if(this.framework.debug.resolver){
            this.framework.debug.println("backTrackUses: check - "+ip.pkgString());
        }
        if(this.tempBackTracked.contains(ip.bpkgs)){
            return false;
        }
        this.tempBackTracked.add(ip.bpkgs);
        var i=this.getPackagesProvidedBy(ip.bpkgs).iterator(); 
        if(i.hasNext()){
            do{
                var ep=i.next(); 
                var foundUses=false; 
                for (var element,_$itr=ep.pkg.importers.iterator();_$itr.hasNext();){
                    element=_$itr.next();
                    var iip=element; 
                    if(iip.provider===ep){
                        if(this.backTrackUses(iip)){
                            foundUses=true;
                        }
                    }
                }
                if(!foundUses){
                    this.checkUses(ep.uses,ep,ep.bpkgs);
                }
            }while(i.hasNext());
            return true;
        }else {
            return false;
        }
    },
    
    markAsZombies:function(e1,e2){
        for (var exportPkg,_$itr=e1.iterator();_$itr.hasNext();){
            exportPkg=_$itr.next();
            exportPkg.zombie=true;
        }
        while(e2.hasNext()){
            e2.next().zombie=true;
        }
    },
    
    getPackagesProvidedBy:function(bpkgs){
        var res=new this.vj$.ArrayList(); 
        for (var i=bpkgs.getExports();i.hasNext();){
            var ep=i.next(); 
            if(ep.pkg.providers.contains(ep)){
                res.add(ep);
            }
        }
        return res;
    },
    
    resolvePackages:function(pkgs,failReason){
        var pkgFail=failReason!==null?new this.vj$.StringBuffer():null; 
        var res=true; 
        this.tempCollision=null;
        while(pkgs.hasNext()){
            var provider=null; 
            var ip=pkgs.next(); 
            if(ip.provider!==null){
                this.framework.frameworkError(ip.bpkgs.bg.bundle,new this.vj$.Exception("resolvePackages: InternalError1!"));
            }
            if(this.framework.debug.resolver){
                this.framework.debug.println("resolvePackages: check - "+ip.pkgString());
            }
            var possibleProvider=new this.vj$.LinkedList(); 
            for (var ep,_$itr=ip.pkg.exporters.iterator();_$itr.hasNext();){
                ep=_$itr.next();
                if(ip.checkAttributes(ep)){
                    if(ip.bpkgs===ep.bpkgs||ip.checkPermission(ep)){
                        possibleProvider.add(ep);
                    }else if(pkgFail!==null){
                        this.newFailReason(pkgFail,"No import permission",ep);
                    }
                }else if(pkgFail!==null){
                    this.newFailReason(pkgFail,"Attributes don't match",ep);
                }
            }
            if(pkgFail!==null){
                if(possibleProvider.isEmpty()&&pkgFail.length()===0){
                    pkgFail.append("No providers found.");
                }
            }
            this.framework.resolverHooks.filterMatches(/*>>*/ip,/*>>*/possibleProvider);
            if(pkgFail!==null&&pkgFail.length()===0&&possibleProvider.isEmpty()){
                pkgFail.append("Resolver hooks filtered all possible providers");
            }
            provider=this.tempProvider.get(ip.name);
            if(provider!==null){
                if(this.framework.debug.resolver){
                    this.framework.debug.println("resolvePackages: "+ip.name+" - has temporary provider - "+provider);
                }
                if(!possibleProvider.contains(provider)){
                    var r="provider not used, rejected by constraints or resolver hooks - "+provider; 
                    if(this.framework.debug.resolver){
                        this.framework.debug.println("resolvePackages: "+ip.name+" - "+r);
                    }
                    if(possibleProvider.isEmpty()){
                        provider=null;
                    }else {
                        this.tempCollision=provider;
                        return false;
                    }
                }
            }else {
                for (var ep,_$itr=ip.pkg.providers.iterator();_$itr.hasNext();){
                    ep=_$itr.next();
                    if(!possibleProvider.contains(ep)){
                        continue;
                    }
                    if(this.tempBlackList.contains(ep)){
                        possibleProvider.remove(ep);
                        this.tempBlackListHits++;
                        if(pkgFail!==null){
                            this.newFailReason(pkgFail,"Collied with previous selection",ep);
                        }
                        continue;
                    }
                    if(ep.zombie){
                        continue;
                    }
                    var oldTempProvider=this.tempProviderClone(); 
                    if(this.checkUses(ep.uses,ep,ep.bpkgs)){
                        provider=ep;
                        break;
                    }else {
                        this.tempProvider=oldTempProvider;
                        this.tempBlackList.add(ep);
                        possibleProvider.remove(ep);
                        if(pkgFail!==null){
                            this.newFailReason(pkgFail,"Provider rejected because of uses directive ",ep);
                        }
                    }
                }
                if(provider===null){
                    provider=this.pickProvider(ip,possibleProvider,pkgFail);
                }
                if(provider!==null){
                    this.tempProvider.put(ip.pkg.pkg,provider);
                }
            }
            if(provider===null){
                if(ip.mustBeResolved()){
                    res=false;
                    if(failReason!==null){
                        failReason.append(this.vj$.FrameworkProperties.NL);
                        failReason.append(ip.pkgString());
                        failReason.append(" -- ");
                        failReason.append(pkgFail);
                    }
                }else {
                    if(this.framework.debug.resolver){
                        this.framework.debug.println("resolvePackages: Ok, no provider for optional "+ip.name);
                    }
                }
            }
            if(pkgFail!==null){
                pkgFail.setLength(0);
            }
        }
        return res;
    },
    
    tempProviderClone:function(){
        return this.tempProvider.clone(); 
    },
    
    pickProvider:function(ip,possibleProvider,failReason){
        if(this.framework.debug.resolver){
            this.framework.debug.println("pickProvider: for - "+ip);
        }
        var zombieExists=false; 
        for (var i=possibleProvider.iterator();i.hasNext();){
            var ep=i.next(); 
            this.tempBlackListChecks++;
            if(this.tempBlackList.contains(ep)){
                this.tempBlackListHits++;
                i.remove();
                if(failReason!==null){
                    this.newFailReason(failReason,"Collied with previous selection",ep);
                }
                continue;
            }
            if(ip.bpkgs===ep.bpkgs){
                if(this.framework.debug.resolver){
                    this.framework.debug.println("pickProvider: internal wire ok for - "+ep);
                }
                ip.internalOk=ep;
            }else if(!ep.checkPermission()){
                if(this.framework.debug.resolver){
                    this.framework.debug.println("pickProvider: no export permission for - "+ep);
                }
                i.remove();
                if(failReason!==null){
                    this.newFailReason(failReason,"No export permission for",ep);
                }
                continue;
            }
            if(ep.bpkgs.bg.bundle.state!==this.vj$.Bundle.INSTALLED){
                var oldTempProvider=this.tempProviderClone(); 
                if(this.checkUses(ep.uses,ep,ep.bpkgs)){
                    if(this.framework.debug.resolver){
                        this.framework.debug.println("pickProvider: "+ip+" - got resolved provider - "+ep);
                    }
                    return ep;
                }else {
                    this.tempProvider=oldTempProvider;
                    this.tempBlackList.add(ep);
                    i.remove();
                    if(failReason!==null){
                        this.newFailReason(failReason,"Uses directive block",ep);
                    }
                    continue;
                }
            }
            if(ep.zombie){
                zombieExists=true;
            }
        }
        if(zombieExists){
            for (var iep=possibleProvider.iterator();iep.hasNext();){
                var ep=iep.next(); 
                if(this.tempResolved.contains(ep.bpkgs.bg)){
                    if(this.framework.debug.resolver){
                        this.framework.debug.println("pickProvider: "+ip+" - got temp provider - "+ep);
                    }
                    return ep;
                }else if(ep.zombie){
                    var oldTempProvider=this.tempProviderClone(); 
                    if(this.checkUses(ep.uses,ep,ep.bpkgs)){
                        if(this.framework.debug.resolver){
                            this.framework.debug.println("pickProvider: "+ip+" - got zombie provider - "+ep);
                        }
                        return ep;
                    }
                    this.tempProvider=oldTempProvider;
                    this.tempBlackList.add(ep);
                    if(failReason!==null){
                        this.newFailReason(failReason,"Uses directive block",ep);
                    }
                    iep.remove();
                }
            }
        }
        var savedCollision=null; 
        for (var ep,_$itr=possibleProvider.iterator();_$itr.hasNext();){
            ep=_$itr.next();
            if(this.framework.debug.resolver){
                this.framework.debug.println("pickProvider: check possible provider - "+ep);
            }
            if(this.checkResolve(ep.bpkgs.bg,ep)){
                if(this.framework.debug.resolver){
                    this.framework.debug.println("pickProvider: "+ip+" - got provider - "+ep);
                }
                return ep;
            }
            if(this.tempCollision!==null&&savedCollision===null){
                savedCollision=this.tempCollision;
            }
            if(failReason!==null){
                this.newFailReason(failReason,"Could not resolve exporting bundle",ep);
            }
        }
        if(this.framework.debug.resolver){
            this.framework.debug.println("pickProvider: "+ip+" - found no provider");
        }
        if(savedCollision!==null){
            this.tempCollision=savedCollision;
        }
        return null;
    },
    
    newFailReason:function(failReason,string,ep){
        if(failReason.length()>0){
            failReason.append(" || ");
        }
        failReason.append(string);
        if(ep!==null){
            failReason.append(" - ");
            failReason.append(ep);
        }
        failReason.append(".");
    },
    
    checkResolve:function(bg,ep){
        if(this.tempResolved.contains(bg)){
            return true;
        }
    },
    
    checkUses:function(uses,bc,bpkgs){
    },
    
    checkBundleSingleton:function(bg){
    },
    
    checkBundleRequirements:function(bg){
    },
    
    checkRequireBundle:function(bg){
    },
    
    registerNewProviders:function(bundle){
    },
    
    registerNewWires:function(){
    },
    
    checkThread:function(){
    }
})
.endType();