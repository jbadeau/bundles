/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.itype('bundles.framework.BundleArchive') 

.props({
    AUTOSTART_SETTING_STOPPED:"stopped", 
    AUTOSTART_SETTING_EAGER:"eager", 
    AUTOSTART_SETTING_ACTIVATION_POLICY:"activation_policy" 
})
.protos({
    
    getAttribute:function(key){
    },
    
    getFileArchive:function(path){
    },
    
    getLocalizationEntries:function(localeFile){
    },
    
    getUnlocalizedAttributes:function(){
    },
    
    getBundleGeneration:function(){
    },
    
    setBundleGeneration:function(bg){
    },
    
    getBundleId:function(){
    },
    
    getBundleLocation:function(){
    },
    
    getBundleResourceStream:function(component,ix){
    },
    
    findResourcesPath:function(path){
    },
    
    getStartLevel:function(){
    },
    
    setStartLevel:function(level){
    },
    
    getLastModified:function(){
    },
    
    setLastModified:function(timemillisecs){
    },
    
    getAutostartSetting:function(){
    },
    
    setAutostartSetting:function(setting){
    },
    
    getJarLocation:function(){
    },
    
    getCertificateChains:function(onlyTrusted){
    },
    
    trustCertificateChain:function(trustedChain){
    },
    
    purge:function(){
    },
    
    close:function(){
    }
})
.endType();