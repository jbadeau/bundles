/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.PackageAdminImpl') 
.needs(['org.eclipse.vjet.vjo.java.util.Vector','org.eclipse.vjet.vjo.java.util.ArrayList',
    'org.eclipse.vjet.vjo.java.util.Iterator','org.eclipse.vjet.vjo.java.util.TreeSet',
    'org.eclipse.vjet.vjo.java.lang.Exception','org.eclipse.vjet.vjo.java.util.List',
    'bundles.framework.FrameworkContext','osgi.framework.Bundle',
    'osgi.service.packageadmin.ExportedPackage','bundles.framework.ExportedPackageImpl',
    'bundles.framework.ExportPkg','bundles.framework.BundleImpl',
    'bundles.framework.Pkg','osgi.framework.FrameworkListener',
    'osgi.framework.BundleException','osgi.framework.FrameworkEvent',
    'osgi.service.packageadmin.RequiredBundle','osgi.framework.VersionRange',
    'bundles.framework.BundleGeneration','bundles.framework.RequiredBundleImpl',
    'org.eclipse.vjet.vjo.java.lang.StringUtil','bundles.framework.BundleClassLoader'])
.needs('bundles.framework.Bundles','')
.satisfies('osgi.service.packageadmin.PackageAdmin')
.props({
    SPEC_VERSION:"1.2" 
})
.protos({
    fwCtx:null, 
    refreshSync:null, 
    
    constructs:function(fw){
        this.refreshSync=new this.vj$.Vector(1);
        this.fwCtx=fw;
    },
    
    
    getExportedPackages:function(bundle){
        if(arguments.length===1){
            if(osgi.framework.Bundle.clazz.isInstance(arguments[0])){
                return this.getExportedPackages_1_0_PackageAdminImpl_ovld(arguments[0]);
            }else if(arguments[0] instanceof String || typeof arguments[0]=="string"){
                return this.getExportedPackages_1_1_PackageAdminImpl_ovld(arguments[0]);
            }
        }
    },
    
    getExportedPackages_1_0_PackageAdminImpl_ovld:function(bundle){
        var pkgs=new this.vj$.ArrayList(); 
        if(bundle!==null){
            for (var i=bundle.getExports();i.hasNext();){
                var ep=i.next(); 
                if(ep.isExported()){
                    pkgs.add(new this.vj$.ExportedPackageImpl(ep));
                }
            }
        }else {
            for (var b,_$itr=this.fwCtx.bundles.getBundles().iterator();_$itr.hasNext();){
                b=_$itr.next();
                for (var i=b.getExports();i.hasNext();){
                    var ep=i.next(); 
                    if(ep.isExported()){
                        pkgs.add(new this.vj$.ExportedPackageImpl(ep));
                    }
                }
            }
        }
        var size=pkgs.size(); 
        if(size>0){
            return pkgs.toArray(vjo.createArray(null, size));
        }else {
            return null;
        }
    },
    
    getExportedPackages_1_1_PackageAdminImpl_ovld:function(name){
        var pkg=this.fwCtx.resolver.getPkg(name); 
        var res=null; 
        if(pkg!==null){
            {
                var size=pkg.exporters.size(); 
                if(size>0){
                    res=vjo.createArray(null, size);
                    var i=pkg.exporters.iterator(); 
                    for (var pos=0;pos<size;){
                        res[pos++]=new this.vj$.ExportedPackageImpl(i.next());
                    }
                }
            }
        }
        return res;
    },
    
    getExportedPackage:function(name){
        var p=this.fwCtx.resolver.getPkg(name); 
        if(p!==null){
            var ep=p.getBestProvider(); 
            if(ep!==null){
                return new this.vj$.ExportedPackageImpl(ep);
            }
        }
        return null;
    },
    
    
    
    refreshPackages:function(bundles){
        if(arguments.length===1){
            this.refreshPackages_1_0_PackageAdminImpl_ovld(arguments[0]);
        }else if(arguments.length===2){
            this.refreshPackages_2_0_PackageAdminImpl_ovld(arguments[0],arguments[1]);
        }
    },
    
    refreshPackages_1_0_PackageAdminImpl_ovld:function(bundles){
        this.refreshPackages(bundles,null);
    },
    
    refreshPackages_2_0_PackageAdminImpl_ovld:function(bundles,fl){
        this.fwCtx.perm.checkResolveAdminPerm();
        var restart=false; 
        if(bundles!==null){
            for (var i=0;i<bundles.length;i++){
                this.fwCtx.checkOurBundle(bundles[i]);
                if((bundles[i]).extensionNeedsRestart()){
                    restart=true;
                    break;
                }
            }
        }else {
            restart=this.fwCtx.bundles.checkExtensionBundleRestart();
        }
        if(restart){
            try {
                this.fwCtx.systemBundle.update();
            }
            catch(ignored){
            }
            return;
        }
        var thisClass=this; 
        {
            var t=
                vjo.make(this,Thread,this.fwCtx.threadGroup,"RefreshPackages")
                .protos({
                    run:function(){
                        this.vj$.parent.fwCtx.perm.callRefreshPackages0(thisClass,bundles.framework.Bundles,fl);
                    }
                })
                .endType(); 
            t.setDaemon(false);
            this.refreshSync.add(t);
            t.start();
            try {
                this.refreshSync.wait(500);
            }
            catch(ignore){
            }
        }
    },
    
    refreshPackages0:function(bundles){
        var fl;
        if (arguments.length == 2 && arguments[1]  instanceof Array){
            fl=arguments[1];
        }
        else {
            fl=[];
            for (var i=1; i<arguments.length; i++){
                fl[i-1]=arguments[i];
            }
        }
        if(this.fwCtx.debug.resolver){
            this.fwCtx.debug.println("PackageAdminImpl.refreshPackages() starting");
        }
        var startList=new this.vj$.ArrayList(); 
        var bi; 
        {
            var zombies=this.fwCtx.resolver.getZombieAffected(bundles); 
            bi=zombies.toArray(vjo.createArray(null, zombies.size()));
            {
                this.refreshSync.notifyAll();
            }
            for (var bx=bi.length;bx-->0;){
                if(bi[bx].state===this.vj$.Bundle.ACTIVE||bi[bx].state===this.vj$.Bundle.STARTING){
                    startList.add(0,bi[bx]);
                    try {
                        bi[bx].waitOnOperation(this.fwCtx.resolver,"PackageAdmin.refreshPackages",false);
                        var be=bi[bx].stop0(); 
                        if(be!==null){
                            this.fwCtx.frameworkError(bi[bx],be);
                        }
                    }
                    catch(ignore){
                    }
                }
            }
            var startPos=startList.size()-1; 
            var nextStart=startPos>=0?startList.get(startPos):null; 
            for (var bx=bi.length;bx-->0;){
                var be=null; 
                switch(bi[bx].state){
                    case this.vj$.Bundle.STARTING:
                    case this.vj$.Bundle.ACTIVE:
                    while(true){
                        try {
                            bi[bx].waitOnOperation(this.fwCtx.resolver,"PackageAdmin.refreshPackages",true);
                            break;
                        }
                        catch(we){
                            if(this.fwCtx.debug.resolver){
                                this.fwCtx.debug.println("PackageAdminImpl.refreshPackages() timeout on bundle stop, retry...");
                            }
                            this.fwCtx.frameworkWarning(bi[bx],we);
                        }
                    }
                        be=bi[bx].stop0();
                    if(be!==null){
                        this.fwCtx.frameworkError(bi[bx],be);
                    }
                    if(nextStart!==bi[bx]){
                        startList.add(startPos+1,bi[bx]);
                    }
                    case this.vj$.Bundle.STOPPING:
                    case this.vj$.Bundle.RESOLVED:
                        bi[bx].setStateInstalled(true);
                    if(bi[bx]===nextStart){
                        nextStart=--startPos>=0?startList.get(startPos):null;
                    }
                    case this.vj$.Bundle.INSTALLED:
                    case this.vj$.Bundle.UNINSTALLED:
                        break;
                }
                bi[bx].purge();
            }
        }
        if(this.fwCtx.debug.resolver){
            this.fwCtx.debug.println("PackageAdminImpl.refreshPackages() "+"all affected bundles now in state INSTALLED");
        }
        startBundles(startList);
        var fe=new this.vj$.FrameworkEvent(this.vj$.FrameworkEvent.PACKAGES_REFRESHED,this.fwCtx.systemBundle,null); 
        this.fwCtx.listeners.frameworkEvent(fe,fl);
        this.refreshSync.remove(this.vj$.Thread.currentThread());
        if(this.fwCtx.debug.resolver){
            this.fwCtx.debug.println("PackageAdminImpl.refreshPackages() done.");
        }
    },
    
    startBundles:function(slist){
        var triggers=slist.toArray(vjo.createArray(null, slist.size())); 
        for (var rb,_$itr=slist.iterator();_$itr.hasNext();){
            rb=_$itr.next();
            rb.getUpdatedState(triggers);
        }
        try {
            this.fwCtx.resolverHooks.endResolve(triggers);
        }
        catch(be){
            this.fwCtx.frameworkError(this.fwCtx.systemBundle,be);
        }
        for (var rb,_$itr=slist.iterator();_$itr.hasNext();){
            rb=_$itr.next();
            if(rb.getState()===this.vj$.Bundle.RESOLVED){
                try {
                    rb.start();
                }
                catch(be){
                    this.fwCtx.frameworkError(rb,be);
                }
            }
        }
    },
    
    resolveBundles:function(bundles){
        this.fwCtx.perm.checkResolveAdminPerm();
        {
            this.fwCtx.resolverHooks.checkResolveBlocked();
            var bl=new this.vj$.ArrayList(); 
            var res=true; 
            if(bundles!==null){
                for (var bundle,_$i0=0;_$i0<bundles.length;_$i0++){
                    bundle=bundles[_$i0];
                    if(bundle.getState()===this.vj$.Bundle.INSTALLED){
                        bl.add(/*>>*/bundle);
                    }else if(bundle.getState()===this.vj$.Bundle.UNINSTALLED){
                        res=false;
                    }
                }
            }else {
                for (var bundle,_$itr=this.fwCtx.bundles.getBundles().iterator();_$itr.hasNext();){
                    bundle=_$itr.next();
                    if(bundle.getState()===this.vj$.Bundle.INSTALLED){
                        bl.add(bundle);
                    }
                }
            }
            if(!bl.isEmpty()){
                var triggers=bl.toArray(vjo.createArray(null, bl.size())); 
                for (var bundle,_$itr=bl.iterator();_$itr.hasNext();){
                    bundle=_$itr.next();
                    var b=bundle; 
                    if(b.getUpdatedState(triggers)===this.vj$.Bundle.INSTALLED){
                        res=false;
                    }
                }
                try {
                    this.fwCtx.resolverHooks.endResolve(triggers);
                }
                catch(be){
                    this.fwCtx.frameworkError(this.fwCtx.systemBundle,be);
                }
            }
            return res;
        }
    },
    
    getRequiredBundles:function(symbolicName){
        var bgs=this.fwCtx.bundles.getBundleGenerations(symbolicName); 
        var res=new this.vj$.ArrayList(); 
        for (var bg,_$itr=bgs.iterator();_$itr.hasNext();){
            bg=_$itr.next();
            if((bg.bundle.isResolved()||bg.bundle.getRequiredBy().size()>0)&&!bg.isFragment()){
                res.add(new this.vj$.RequiredBundleImpl(bg.bpkgs));
            }
        }
        var s=res.size(); 
        if(s>0){
            return res.toArray(vjo.createArray(null, s));
        }else {
            return null;
        }
    },
    
    getBundles:function(symbolicName,versionRange){
        var vr=versionRange!==null?new this.vj$.VersionRange(org.eclipse.vjet.vjo.java.lang.StringUtil.trim(versionRange)):null; 
        var bgs=this.fwCtx.bundles.getBundles(symbolicName,vr); 
        var size=bgs.size(); 
        if(size>0){
            var res=vjo.createArray(null, size); 
            var i=bgs.iterator(); 
            for (var pos=0;pos<size;){
                res[pos++]=i.next().bundle;
            }
            return res;
        }else {
            return null;
        }
    },
    
    getFragments:function(bundle){
        if(bundle===null){
            return null;
        }
        var bg=bundle.current(); 
        if(bg.isFragment()){
            return null;
        }
        if(bg.isFragmentHost()){
            var fix=bg.fragments.clone(); 
            var r=vjo.createArray(null, fix.size()); 
            for (var i=fix.size()-1;i>=0;i--){
                r[i]=fix.get(i).bundle;
            }
            return r;
        }else {
            return null;
        }
    },
    
    getHosts:function(bundle){
        var b=bundle; 
        if(b!==null){
            var h=b.getHosts(false); 
            if(h!==null){
                var r=vjo.createArray(null, h.size()); 
                var pos=0; 
                for (var bg,_$itr=h.iterator();_$itr.hasNext();){
                    bg=_$itr.next();
                    r[pos++]=bg.bundle;
                }
                return r;
            }
        }
        return null;
    },
    
    getBundle:function(clazz){
        var cl=clazz.getClassLoader(); 
        if(bundles.framework.BundleClassLoader.isInstance(cl)){
            return cl.getBundle();
        }else {
            return null;
        }
    },
    
    getBundleType:function(bundle){
        var bg=bundle.current(); 
        return bg.isFragment() && !bg.isExtension()?this.vj$.PackageAdmin.BUNDLE_TYPE_FRAGMENT:0;
    }
})
.endType();