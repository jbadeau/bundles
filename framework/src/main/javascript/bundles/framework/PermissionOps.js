define([ "vjo" ], function(vjo) {

	return vjo.ctype("bundles.framework.PermissionOps")

	.protos({

		init : function() {
		},

		registerService : function() {
		},

		checkPermissions : function() {
			return false;
		},

		okClassAdminPerm : function(b) {
			return true;
		},

		checkExecuteAdminPerm : function(b) {
		},

		checkExtensionLifecycleAdminPerm : function(b) {
			if (arguments.length === 1) {
				this.checkExtensionLifecycleAdminPerm_1_0_PermissionOps_ovld(arguments[0]);
			}
			else if (arguments.length === 2) {
				this.checkExtensionLifecycleAdminPerm_2_0_PermissionOps_ovld(arguments[0], arguments[1]);
			}
		},

		checkExtensionLifecycleAdminPerm_1_0_PermissionOps_ovld : function(b) {
		},

		checkExtensionLifecycleAdminPerm_2_0_PermissionOps_ovld : function(b, checkContext) {
		},

		checkLifecycleAdminPerm : function(b) {
			if (arguments.length === 1) {
				this.checkLifecycleAdminPerm_1_0_PermissionOps_ovld(arguments[0]);
			}
			else if (arguments.length === 2) {
				this.checkLifecycleAdminPerm_2_0_PermissionOps_ovld(arguments[0], arguments[1]);
			}
		},

		checkLifecycleAdminPerm_1_0_PermissionOps_ovld : function(b) {
		},

		checkLifecycleAdminPerm_2_0_PermissionOps_ovld : function(b, checkContext) {
		},

		checkListenerAdminPerm : function(b) {
		},

		checkMetadataAdminPerm : function(b) {
		},

		checkResolveAdminPerm : function() {
		},

		checkResourceAdminPerm : function(b) {
		},

		okResourceAdminPerm : function(b) {
			return true;
		},

		checkContextAdminPerm : function(b) {
		},

		checkStartLevelAdminPerm : function() {
		},

		checkGetProtectionDomain : function() {
		},

		checkWeaveAdminPerm : function(b) {
		},

		okFragmentBundlePerm : function(b) {
			return true;
		},

		okHostBundlePerm : function(b) {
			return true;
		},

		okProvideBundlePerm : function(b) {
			return true;
		},

		okRequireBundlePerm : function(b) {
			return true;
		},

		okAllPerm : function(b) {
			return true;
		},

		hasExportPackagePermission : function(ep) {
			return true;
		},

		hasImportPackagePermission : function(b, ep) {
			return true;
		},

		checkRegisterServicePerm : function(clazz) {
		},

		checkGetServicePerms : function(sr) {
		},

		okGetServicePerms : function(sr) {
			return true;
		},

		filterGetServicePermission : function(srs) {
		},

		hasProvidePermission : function(bc) {
			return true;
		},

		hasRequirePermission : function(br) {
			if (arguments.length === 1) {
				return this.hasRequirePermission_1_0_PermissionOps_ovld(arguments[0]);
			}
			else if (arguments.length === 2) {
				return this.hasRequirePermission_2_0_PermissionOps_ovld(arguments[0], arguments[1]);
			}
		},

		hasRequirePermission_1_0_PermissionOps_ovld : function(br) {
			return true;
		},

		hasRequirePermission_2_0_PermissionOps_ovld : function(br, bc) {
			return true;
		},

		checkAdaptPerm : function(b, type) {
		},

		callGetBundleResourceStream : function(archive, name, ix) {
			return archive.getBundleResourceStream(name, ix);
		},

		callFindResourcesPath : function(archive, path) {
			return archive.findResourcesPath(path);
		},

		callSearchFor : function(cl, name, pkg, path, action, options, requestor, visited) {
			return cl.searchFor(name, pkg, path, action, options, requestor, visited);
		},

		callFindLibrary0 : function(cl, name) {
			return cl.findLibrary0(name);
		},

		callFinalizeActivation : function(b) {
			b.finalizeActivation();
		},

		createBundleThread : function(fc) {
			return new this.vj$.BundleThread(fc);
		},

		callUpdate0 : function(b, in_, wasActive) {
			b.update0(in_, wasActive, null);
		},

		callUninstall0 : function(b) {
			b.uninstall0();
		},

		callSetAutostartSetting : function(b, setting) {
			b.setAutostartSetting0(setting);
		},

		callGetHeaders0 : function(bg, locale) {
			return bg.getHeaders0(locale);
		},

		callFindEntries : function(bg, path, filePattern, recurse) {
			return bg.findEntries(path, filePattern, recurse);
		},

		newBundleClassLoader : function(bg) {
			return new this.vj$.BundleClassLoader(bg);
		},

		getBundleClassPathEntries : function(bg, name, onlyFirst) {
			return bg.getBundleClassPathEntries(name, onlyFirst);
		},

		getAccessControlContext : function(bundle) {
			return null;
		},

		callInstall0 : function(bs, location, in_, caller) {
			return bs.install0(location, in_, null, caller);
		},

		callBundleChanged : function(fwCtx, evt) {
			fwCtx.listeners.bundleChanged(evt);
		},

		callServiceChanged : function(fwCtx, receivers, evt, matchBefore) {
			fwCtx.listeners.serviceChanged(receivers, evt, matchBefore);
		},

		callRefreshPackages0 : function(pa, bundles, fl) {
			pa.refreshPackages0(bundles, fl);
		},

		callGetService : function(sr, b) {
			var srf = sr.service;
			return srf.getService(b, sr);
		},

		callUngetService : function(sr, b, instance) {
			var srf = sr.service;
			srf.ungetService(b, sr, instance);
		},

		callSetStartLevel : function(b, startlevel) {
			b.setStartLevel(startlevel);
		},

		callSetInitialBundleStartLevel0 : function(slc, startlevel) {
			slc.setInitialBundleStartLevel0(startlevel, true);
		},

		callShutdown : function(sb, restart) {
			sb.shutdown(restart);
		},

		getProtectionDomain : function(bg) {
			return null;
		},

		getBundleURL : function(fwCtx, s) {
			return new this.vj$.URL(null, s, fwCtx.urlStreamHandlerFactory.createURLStreamHandler(this.vj$.BundleURLStreamHandler.PROTOCOL));
		},

		getClassLoaderOf : function(c) {
			return c.getClassLoader();
		},

		purge : function(b, pc) {
		}

	})

	.endType();

});