/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.ExtensionContext') 
.needs(['org.eclipse.vjet.vjo.java.util.ArrayList','org.eclipse.vjet.vjo.java.lang.StringUtil'])

.protos({
    fwCtx:null, 
    ext:null, 
    activator:null, 
    bclls:null, 
    
    constructs:function(fwCtx,ext){
        this.bclls=new this.vj$.ArrayList();
        this.fwCtx=fwCtx;
        this.ext=ext;
        var extActivatorName=ext.archive.getAttribute("Extension-Activator"); 
        extActivatorName=null!==extActivatorName?org.eclipse.vjet.vjo.java.lang.StringUtil.trim(extActivatorName):null;
        try {
            var c=this.getClass().getClassLoader().loadClass(extActivatorName); 
            this.activator=c.newInstance();
            var activateMethod=c.getMethod("activate",[this.vj$.ExtensionContext.clazz]); 
            activateMethod.invoke(this.activator,[this]);
        }
        catch(e){
            this.activator=null;
            var msg="Failed to activate framework extension "+ext.symbolicName+":"+ext.version; 
            fwCtx.log(msg,e);
        }
    },
    
    registerService:function(clazzes,service,properties){
        return this.fwCtx.services.register(this.fwCtx.systemBundle,clazzes,service,properties);
    },
    
    getClassLoader:function(b){
        var bi=b; 
        return bi.getClassLoader();
    },
    
    getGeneration:function(bcl){
        return bcl.bpkgs.bg.generation;
    },
    
    addBundleClassLoaderListener:function(bcll){
        this.bclls.add(bcll);
    },
    
    bundleClassLoaderCreated:function(bcl){
        for (var bcll,_$itr=this.bclls.iterator();_$itr.hasNext();){
            bcll=_$itr.next();
            bcll.bundleClassLoaderCreated(bcl);
        }
    },
    
    bundleClassLoaderClosed:function(bcl){
        for (var bcll,_$itr=this.bclls.iterator();_$itr.hasNext();){
            bcll=_$itr.next();
            bcll.bundleClassLoaderClosed(bcl);
        }
    }
})
.endType();