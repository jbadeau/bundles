/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.IteratorIteratorSorted<A>') 
.needs('org.eclipse.vjet.vjo.java.util.NoSuchElementException','')

.satisfies('org.eclipse.vjet.vjo.java.util.Iterator<A>')
.protos({
    iter:null, 
    top:null, 
    comp:null, 
    size:0, 
    
    constructs:function(ilist,comp){
        this.comp=comp;
        this.size=ilist.size();
        var iters=vjo.createArray(null, this.size+1); 
        this.iter=iters;
        var topA=vjo.createArray(null, this.size+1); 
        this.top=topA;
        var pos=1; 
        for (var i=ilist.iterator();i.hasNext();){
            var si=i.next(); 
            if(si.hasNext()){
                this.top[pos]=si.next();
                this.iter[pos++]=si;
            }else {
                this.size--;
            }
        }
        for (pos=parseInt(this.size/2);pos>0;pos--){
            this.balance(pos);
        }
    },
    
    hasNext:function(){
        return this.size>0;
    },
    
    remove:function(){
        throw new UnsupportedOperationException();
    },
    
    next:function(){
        if(this.hasNext()){
            var res=this.top[1]; 
            if(this.iter[1].hasNext()){
                this.top[1]=this.iter[1].next();
            }else {
                this.top[1]=this.top[this.size];
                this.iter[1]=this.iter[this.size--];
            }
            this.balance(1);
            return res;
        }
        throw new org.eclipse.vjet.vjo.java.util.NoSuchElementException();
    },
    
    balance:function(current){
        var tmp=this.top[current]; 
        var itmp=this.iter[current]; 
        var child; 
        while(current*2<=this.size){
            child=current*2;
            if(child!==this.size&&this.comp.compare(this.top[child+1],this.top[child])>0){
                child++;
            }
            if(this.comp.compare(this.top[child],tmp)>0){
                this.top[current]=this.top[child];
                this.iter[current]=this.iter[child];
            }else {
                break;
            }
            current=child;
        }
        this.top[current]=tmp;
        this.iter[current]=itmp;
    }
})
.endType();