/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.URLStreamHandlerWrapper') 
.needs(['org.eclipse.vjet.vjo.java.util.ArrayList','org.eclipse.vjet.vjo.java.lang.IllegalArgumentException',
    'org.eclipse.vjet.vjo.java.lang.RuntimeException','org.eclipse.vjet.vjo.java.lang.IllegalStateException',
    'org.eclipse.vjet.vjo.java.lang.StringBuffer','bundles.framework.FrameworkContext',
    'osgi.framework.ServiceListener','osgi.framework.ServiceReference',
    'osgi.service.url.URLStreamHandlerService','osgi.framework.Constants',
    'osgi.service.url.URLConstants','osgi.framework.ServiceEvent',
    'bundles.framework.BundleImpl','osgi.framework.InvalidSyntaxException',
    'java.net.URL','java.net.InetAddress',
    'java.net.URLConnection','java.net.MalformedURLException'])
.inherits('java.net.URLStreamHandler')
.satisfies('osgi.service.url.URLStreamHandlerSetter')
.protos({
    framework:null, 
    protocol:null, 
    filter:null, 
    serviceListener:null, 
    best:null, 
    bestService:null, 
    currentFw:null, 
    
    constructs:function(fw,proto){
        this.framework=new this.vj$.ArrayList(2);
        this.base();
        this.protocol=proto;
        this.filter="(&("+this.vj$.Constants.OBJECTCLASS+"="+this.vj$.URLStreamHandlerService.clazz.getName()+")"+"("+this.vj$.URLConstants.URL_HANDLER_PROTOCOL+"="+this.protocol+"))";
        this.serviceListener=
            vjo.make(this,this.vj$.ServiceListener)
            .protos({
                serviceChanged:function(evt){
                    var ref=evt.getServiceReference(); 
                    var fw=(ref.getBundle()).fwCtx; 
                    if(fw===this.vj$.parent.currentFw){
                        switch(evt.getType()){
                            case this.vj$.ServiceEvent.MODIFIED:
                            case this.vj$.ServiceEvent.REGISTERED:
                            if(this.vj$.parent.best!==null&&this.vj$.parent.best.compareTo(ref)<0){
                                this.vj$.parent.best=ref;
                                this.vj$.parent.bestService=null;
                            }
                                break;
                            case this.vj$.ServiceEvent.MODIFIED_ENDMATCH:
                            case this.vj$.ServiceEvent.UNREGISTERING:
                            if(this.vj$.parent.best!==null&&this.vj$.parent.best.equals(ref)){
                                this.vj$.parent.best=null;
                                this.vj$.parent.bestService=null;
                            }
                        }
                    }
                }
            })
            .endType();
        this.framework.add(fw);
        try {
            fw.systemBundle.bundleContext.addServiceListener(this.serviceListener,this.filter);
        }
        catch(e){
            throw new this.vj$.IllegalArgumentException("Protocol name contains illegal characters: "+proto);
        }
        if(fw.debug.url){
            fw.debug.println("created wrapper for "+this.protocol+", filter="+this.filter+", "+this.toString());
        }
    },
    
    addFramework:function(fw){
        try {
            fw.systemBundle.bundleContext.addServiceListener(this.serviceListener,this.filter);
            this.framework.add(fw);
            if(fw.debug.url){
                fw.debug.println("created wrapper for "+this.protocol+", filter="+this.filter+", "+this.toString());
            }
        }
        catch(_no){
        }
    },
    
    removeFramework:function(fw){
        this.framework.remove(fw);
        return this.framework.isEmpty();
    },
    
    getService:function(){
        var fw; 
        if(this.framework.size()===1){
            fw=this.framework.get(0);
        }else {
            throw new this.vj$.RuntimeException("NYI - walk stack to get framework");
        }
        {
            if(this.best===null){
                try {
                    var refs=fw.systemBundle.bundleContext.getServiceReferences(this.vj$.URLStreamHandlerService.clazz.getName(),this.filter); 
                    if(refs!==null){
                        this.best=refs[0];
                    }
                }
                catch(_no){
                }
            }
            if(this.best===null){
                throw new this.vj$.IllegalStateException("null: Lost service for protocol="+this.protocol);
            }
            if(this.bestService===null){
                this.bestService=fw.systemBundle.bundleContext.getService(this.best);
            }
            if(this.bestService===null){
                throw new this.vj$.IllegalStateException("null: Lost service for protocol="+this.protocol);
            }
            this.currentFw=fw;
            return this.bestService;
        }
    },
    
    equals:function(u1,u2){
        return this.getService().equals(u1,u2);
    },
    
    getDefaultPort:function(){
        return this.getService().getDefaultPort();
    },
    
    getHostAddress:function(u){
        return this.getService().getHostAddress(u);
    },
    
    hashCode:function(u){
        return this.getService().hashCode(u);
    },
    
    hostsEqual:function(u1,u2){
        return this.getService().hostsEqual(u1,u2);
    },
    
    openConnection:function(u){
        try {
            return this.getService().openConnection(u);
        }
        catch(e){
            throw new this.vj$.MalformedURLException(e.getMessage());
        }
    },
    
    parseURL:function(u,spec,start,limit){
        this.getService().parseURL(this,u,spec,start,limit);
    },
    
    sameFile:function(u1,u2){
        return this.getService().sameFile(u1,u2);
    },
    
    
    setURL:function(u,protocol,host,port,file,ref){
        if(arguments.length===6){
            if(arguments[0] instanceof java.net.URL && (arguments[1] instanceof String || typeof arguments[1]=="string") && (arguments[2] instanceof String || typeof arguments[2]=="string") && typeof arguments[3]=="number" && (arguments[4] instanceof String || typeof arguments[4]=="string") && (arguments[5] instanceof String || typeof arguments[5]=="string")){
                this.setURL_6_0_URLStreamHandlerWrapper_ovld(arguments[0],arguments[1],arguments[2],arguments[3],arguments[4],arguments[5]);
            }else if(this.base && this.base.setURL){
                this.base.setURL.apply(this,arguments);
            }
        }else if(arguments.length===9){
            if(arguments[0] instanceof java.net.URL && (arguments[1] instanceof String || typeof arguments[1]=="string") && (arguments[2] instanceof String || typeof arguments[2]=="string") && typeof arguments[3]=="number" && (arguments[4] instanceof String || typeof arguments[4]=="string") && (arguments[5] instanceof String || typeof arguments[5]=="string") && (arguments[6] instanceof String || typeof arguments[6]=="string") && (arguments[7] instanceof String || typeof arguments[7]=="string") && (arguments[8] instanceof String || typeof arguments[8]=="string")){
                this.setURL_9_0_URLStreamHandlerWrapper_ovld(arguments[0],arguments[1],arguments[2],arguments[3],arguments[4],arguments[5],arguments[6],arguments[7],arguments[8]);
            }else if(this.base && this.base.setURL){
                this.base.setURL.apply(this,arguments);
            }
        }else if(this.base && this.base.setURL){
            this.base.setURL.apply(this,arguments);
        }
    },
    
    setURL_6_0_URLStreamHandlerWrapper_ovld:function(u,protocol,host,port,file,ref){
        var authority=null; 
        var userInfo=null; 
        if(host!==null&&host.length!==0){
            authority=(port===-1)?host:host+":"+port;
            var ix=host.lastIndexOf('@'); 
            if(ix!== -1){
                userInfo=host.substring(0,ix);
                host=host.substring(ix+1);
            }
        }
        var path=null; 
        var query=null; 
        if(file!==null){
            var ix=file.lastIndexOf('?'); 
            if(ix!== -1){
                query=file.substring(ix+1);
                path=file.substring(0,ix);
            }else {
                path=file;
            }
        }
        this.setURL(u,protocol,host,port,authority,userInfo,path,query,ref);
    },
    
    setURL_9_0_URLStreamHandlerWrapper_ovld:function(u,protocol,host,port,authority,userInfo,path,query,ref){
        setURL(u,protocol,host,port,authority,userInfo,path,query,ref);
    },
    
    toExternalForm:function(u){
        return this.getService().toExternalForm(u);
    },
    
    toString:function(){
        var sb=new this.vj$.StringBuffer(); 
        sb.append("URLStreamHandlerWrapper[");
        var ref=this.best; 
        sb.append("protocol="+this.protocol);
        if(ref!==null){
            sb.append(", id="+ref.getProperty(this.vj$.Constants.SERVICE_ID));
            sb.append(", rank="+ref.getProperty(this.vj$.Constants.SERVICE_RANKING));
        }else {
            sb.append(" no service tracked");
        }
        sb.append("]");
        return sb.toString();
    }
})
.endType();