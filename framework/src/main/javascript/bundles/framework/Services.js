/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.Services') 
.needs(['org.eclipse.vjet.vjo.java.util.HashMap','org.eclipse.vjet.vjo.java.util.List',
    'org.eclipse.vjet.vjo.java.lang.IllegalArgumentException','org.eclipse.vjet.vjo.java.util.ArrayList',
    'org.eclipse.vjet.vjo.java.lang.Math','org.eclipse.vjet.vjo.java.util.Iterator',
    'org.eclipse.vjet.vjo.java.util.Set','org.eclipse.vjet.vjo.java.util.Collection',
    'org.eclipse.vjet.vjo.java.util.HashSet','bundles.framework.ServiceRegistrationImpl',
    'bundles.framework.PermissionOps','bundles.framework.FrameworkContext',
    'osgi.framework.ServiceRegistration','bundles.framework.BundleImpl',
    'java.util.Dictionary','osgi.framework.ServiceFactory',
    'bundles.framework.PropertiesDictionary','bundles.framework.Util',
    'osgi.framework.ServiceReference','osgi.framework.ServiceEvent',
    'java.lang.ClassNotFoundException','osgi.framework.InvalidSyntaxException',
    'bundles.framework.LDAPExpr','osgi.framework.Constants',
    'osgi.framework.Bundle','vjo.java.lang.ObjectUtil',
    'org.eclipse.vjet.vjo.java.lang.ClassUtil'])
.needs('osgi.service.packageadmin.PackageAdmin','')
.props({
    sComp:null 
})
.protos({
    services:null, 
    classServices:null, 
    secure:null, 
    framework:null, 
    
    constructs:function(fwCtx,perm){
        this.services=new this.vj$.HashMap();
        this.classServices=new this.vj$.HashMap();
        this.framework=fwCtx;
        this.secure=perm;
    },
    
    clear:function(){
        this.services.clear();
        this.classServices.clear();
        this.secure=null;
        this.framework=null;
    },
    
    register:function(bundle,classes,service,properties){
        if(service===null){
            throw new this.vj$.IllegalArgumentException("Can't register null as a service");
        }
        for (var classe,_$i0=0;_$i0<classes.length;_$i0++){
            classe=classes[_$i0];
            var cls=classe; 
            if(cls===null){
                throw new this.vj$.IllegalArgumentException("Can't register as null class");
            }
            this.secure.checkRegisterServicePerm(cls);
            if(bundle.id!==0){
                if(vjo.java.lang.ObjectUtil.equals(cls,osgi.service.packageadmin.PackageAdmin.clazz.getName())){
                    throw new this.vj$.IllegalArgumentException("Registeration of a PackageAdmin service is not allowed");
                }
            }
            if(!(osgi.framework.ServiceFactory.isInstance(service))){
                if(!this.checkServiceClass(service,cls)){
                    throw new this.vj$.IllegalArgumentException("Service object is not an instance of "+cls);
                }
            }
        }
        var res=new this.vj$.ServiceRegistrationImpl(bundle,service,new this.vj$.PropertiesDictionary(properties,classes,null)); 
        {
            this.services.put(res,classes);
            for (var clazz,_$i1=0;_$i1<classes.length;_$i1++){
                clazz=classes[_$i1];
                var s=this.classServices.get(clazz); 
                if(s===null){
                    s=new this.vj$.ArrayList(1);
                    this.classServices.put(clazz,s);
                }
                var ip=this.vj$.Math.abs(this.vj$.Util.binarySearch(s,this.vj$.Services.sComp,res)+1); 
                s.add(ip,res);
            }
        }
        var r=res.getReference(); 
        bundle.fwCtx.perm.callServiceChanged(bundle.fwCtx,bundle.fwCtx.listeners.getMatchingServiceListeners(r),new this.vj$.ServiceEvent(this.vj$.ServiceEvent.REGISTERED,r),null);
        return res;
    },
    
    updateServiceRegistrationOrder:function(sr,classes){
        for (var clazz,_$i2=0;_$i2<classes.length;_$i2++){
            clazz=classes[_$i2];
            var s=this.classServices.get(clazz); 
            s.remove(sr);
            s.add(this.vj$.Math.abs(this.vj$.Util.binarySearch(s,this.vj$.Services.sComp,sr)+1),sr);
        }
    },
    
    checkServiceClass:function(service,cls){
        var sc=service.getClass(); 
        var scl=this.secure.getClassLoaderOf(sc); 
        var c=null; 
        var ok=false; 
        try {
            if(scl!==null){
                c=scl.loadClass(cls);
            }else {
                c=vjo.Class.forName(cls);
            }
            ok=c.isInstance(service);
        }
        catch(e){
            for (var csc=sc;csc!==null;csc=org.eclipse.vjet.vjo.java.lang.ClassUtil.getSuperclass(csc)){
                if(vjo.java.lang.ObjectUtil.equals(cls,csc.getName())){
                    ok=true;
                    break;
                }else {
                    var ic=csc.getInterfaces(); 
                    for (var iic=ic.length-1;iic>=0;iic--){
                        if(vjo.java.lang.ObjectUtil.equals(cls,ic[iic].getName())){
                            ok=true;
                            break;
                        }
                    }
                }
            }
        }
        return ok;
    },
    
    
    
    get:function(clazz){
        if(arguments.length===1){
            return this.get_1_0_Services_ovld(arguments[0]);
        }else if(arguments.length===2){
            return this.get_2_0_Services_ovld(arguments[0],arguments[1]);
        }else if(arguments.length===3){
            return this.get_3_0_Services_ovld(arguments[0],arguments[1],arguments[2]);
        }
    },
    
    get_1_0_Services_ovld:function(clazz){
        var v=this.classServices.get(clazz); 
        if(v!==null){
            var res=v.clone(); 
            return res;
        }
        return null;
    },
    
    get_2_0_Services_ovld:function(bundle,clazz){
        try {
            var srs=this.get(clazz,null,bundle); 
            if(this.framework.debug.service_reference){
                this.framework.debug.println("get service ref "+clazz+" for bundle "+bundle.location+" = "+(srs!==null?srs[0]:null));
            }
            if(srs!==null){
                return srs[0];
            }
        }
        catch(_never){
        }
        return null;
    },
    
    get_3_0_Services_ovld:function(clazz,filter,bundle){
        var s; 
        var ldap=null; 
        if(clazz===null){
            if(filter!==null){
                ldap=new this.vj$.LDAPExpr(filter);
                var matched=ldap.getMatchedObjectClasses(); 
                if(matched!==null){
                    var v=null; 
                    var vReadOnly=true; 
                    for (var match,_$itr=matched.iterator();_$itr.hasNext();){
                        match=_$itr.next();
                        var cl=this.classServices.get(match); 
                        if(cl!==null){
                            if(v===null){
                                v=cl;
                            }else {
                                if(vReadOnly){
                                    v=new this.vj$.ArrayList(v);
                                    vReadOnly=false;
                                }
                                v.addAll(cl);
                            }
                        }
                    }
                    if(v!==null){
                        s=v.iterator();
                    }else {
                        return null;
                    }
                }else {
                    s=this.services.keySet().iterator();
                }
            }else {
                s=this.services.keySet().iterator();
            }
        }else {
            var v=this.classServices.get(clazz); 
            if(v!==null){
                s=v.iterator();
            }else {
                return null;
            }
            if(filter!==null){
                ldap=new this.vj$.LDAPExpr(filter);
            }
        }
        var res=new this.vj$.ArrayList(); 
        while(s.hasNext()){
            var sr=s.next(); 
            var sri=sr.getReference(); 
            if(!this.secure.okGetServicePerms(sri)){
                continue;
            }
            if(filter===null||ldap.evaluate(sr.getProperties(),false)){
                if(bundle!==null){
                    var classes=this.services.get(sr); 
                    for (var i=0;i<classes.length;i++){
                        if(!sri.isAssignableTo(bundle,classes[i])){
                            sri=null;
                            break;
                        }
                    }
                }
                if(sri!==null){
                    res.add(sri);
                }
            }
        }
        if(res.isEmpty()){
            return null;
        }else {
            if(bundle!==null){
                this.framework.serviceHooks.filterServiceReferences(bundle.bundleContext,clazz,filter,false,res);
            }else {
                this.framework.serviceHooks.filterServiceReferences(null,clazz,filter,true,res);
            }
            if(res.isEmpty()){
                return null;
            }else {
                return res.toArray(vjo.createArray(null, res.size()));
            }
        }
    },
    
    removeServiceRegistration:function(sr){
        var classes=sr.getProperty(this.vj$.Constants.OBJECTCLASS); 
        this.services.remove(sr);
        for (var clazz,_$i3=0;_$i3<classes.length;_$i3++){
            clazz=classes[_$i3];
            var s=this.classServices.get(clazz); 
            if(s.size()>1){
                s.remove(sr);
            }else {
                this.classServices.remove(clazz);
            }
        }
    },
    
    getRegisteredByBundle:function(b){
        var res=new this.vj$.HashSet(); 
        for (var sr,_$itr=this.services.keySet().iterator();_$itr.hasNext();){
            sr=_$itr.next();
            if(sr.bundle===b){
                res.add(sr);
            }
        }
        return res;
    },
    
    getUsedByBundle:function(b){
        var res=new this.vj$.HashSet(); 
        for (var sr,_$itr=this.services.keySet().iterator();_$itr.hasNext();){
            sr=_$itr.next();
            if(sr.isUsedByBundle(b)){
                res.add(sr);
            }
        }
        return res;
    }
})
.inits(function(){
    this.vj$.Services.sComp=
        vjo.make(this,this.vj$.Util.Comparator)
        .protos({
            compare:function(a,b){
                return a.reference.compareTo(b.reference);
            }
        })
        .endType();
})
.endType();