define([ "vjo", "./Debug", "osgi/framework/Constants", "osgi/util/Environment", "osgi/util/StringUtil", "osgi/util/ObjectUtil", "osgi/util/StringBuilder", "./Alias", "osgi/framework/Version", "./Util" ], function(vjo, Debug, Constants, Environment, StringUtil, ObjectUtil, StringBuilder, Alias, Version, Util) {

	return vjo.ctype('bundles.framework.FrameworkProperties')

	.props({

		ALL_SIGNED_PROP : "org.bundles.framework.all_signed",

		AUTOMANIFEST_PROP : "org.bundles.framework.automanifest",

		AUTOMANIFEST_CONFIG_PROP : "org.bundles.framework.automanifest.config",

		BUNDLESTORAGE_PROP : "org.bundles.framework.bundlestorage",

		BUNDLESTORAGE_CHECKSIGNED_PROP : "org.bundles.framework.bundlestorage.checksigned",

		PATCH_PROP : "org.bundles.framework.patch",

		PATCH_CONFIGURL_PROP : "org.bundles.framework.patch.configurl",

		PATCH_DUMPCLASSES_PROP : "org.bundles.framework.patch.dumpclasses",

		PATCH_DUMPCLASSES_DIR_PROP : "org.bundles.framework.patch.dumpclasses.dir",

		SERVICE_CONDITIONALPERMISSIONADMIN_PROP : "org.bundles.framework.service.conditionalpermissionadmin",

		SERVICE_PERMISSIONADMIN_PROP : "org.bundles.framework.service.permissionadmin",

		BUNDLETHREAD_ABORT : "org.bundles.framework.bundlethread.abort",

		BUNDLETHREAD_TIMEOUT : "org.bundles.framework.bundlethread.timeout",

		SYSTEM_PACKAGES_BASE_PROP : "org.bundles.framework.system.packages.base",

		SYSTEM_PACKAGES_FILE_PROP : "org.bundles.framework.system.packages.file",

		SYSTEM_PACKAGES_VERSION_PROP : "org.bundles.framework.system.packages.version",

		IS_DOUBLECHECKED_LOCKING_SAFE_PROP : "org.bundles.framework.is_doublechecked_locking_safe",

		LDAP_NOCACHE_PROP : "org.bundles.framework.ldap.nocache",

		LISTENER_N_THREADS_PROP : "org.bundles.framework.listener.n_threads",

		MAIN_CLASS_ACTIVATION_PROP : "org.bundles.framework.main.class.activation",

		STRICTBOOTCLASSLOADING_PROP : "org.bundles.framework.strictbootclassloading",

		VALIDATOR_PROP : "org.bundles.framework.validator",

		SETCONTEXTCLASSLOADER_PROP : "org.bundles.osgi.setcontextclassloader",

		REGISTERSERVICEURLHANDLER_PROP : "org.bundles.osgi.registerserviceurlhandler",

		STARTLEVEL_USE_PROP : "org.bundles.startlevel.use",

		STARTLEVEL_COMPAT_PROP : "org.bundles.framework.startlevel.compat",

		KEY_KEYS : "org.bundles.framework.bundleprops.keys",

		TRUE : "true",

		FALSE : "false",

		NL : null,

		javaVersionMajor : -1,

		javaVersionMinor : -1,

		javaVersionMicro : -1,

		FRAMEWORK_EXECUTIONENVIRONMENT : null
	})

	.protos({

		STRICTBOOTCLASSLOADING : false,

		props : null,

		props_default : null,

		UNREGISTERSERVICE_VALID_DURING_UNREGISTERING : true,

		SETCONTEXTCLASSLOADER : false,

		REGISTERSERVICEURLHANDLER : true,

		isDoubleCheckedLockingSafe : false,

		constructs : function(initProps, fwCtx) {
			this.props = new Map();
			this.props_default = new Map();
			this.setAll(this.props, initProps);
			fwCtx.debug = new Debug(this);
			this.initProperties(fwCtx);
			this.initKFProperties();
			this.SETCONTEXTCLASSLOADER = this.getBooleanProperty(this.vj$.FrameworkProperties.SETCONTEXTCLASSLOADER_PROP);
			this.REGISTERSERVICEURLHANDLER = this.getBooleanProperty(this.vj$.FrameworkProperties.REGISTERSERVICEURLHANDLER_PROP);
			this.STRICTBOOTCLASSLOADING = this.getBooleanProperty(this.vj$.FrameworkProperties.STRICTBOOTCLASSLOADING_PROP);
			this.isDoubleCheckedLockingSafe = this.getBooleanProperty(this.vj$.FrameworkProperties.IS_DOUBLECHECKED_LOCKING_SAFE_PROP);
		},

		getBooleanProperty : function(key) {
			var v = this.getProperty(key);
			if (v && v !== null) {
				return StringUtil.equalsIgnoreCase(this.vj$.FrameworkProperties.TRUE, v);
			}
			return false;
		},

		getProperty : function(key) {
			if (ObjectUtil.equals(this.vj$.FrameworkProperties.KEY_KEYS, key)) {
				return this.makeKeys();
			}
			var v = this.props.get(key);
			if (typeof v === "undefined" || v === null) {
				v = Environment.getProperty(key);
				if (typeof v === "undefined" || v === null) {
					v = this.props_default.get(key);
					return typeof v !== "undefined" ? v : null;
				}
			}
			return StringUtil.trim(v);
		},

		setPropertyDefault : function(key, val) {
			if (!this.props.has(key)) {
				this.props_default.set(key, val);
			}
		},

		setPropertyIfNotSet : function(key, val) {
			if (!this.props.has(key)) {
				this.props.set(key, Environment.getProperty(key));
			}
		},

		getProperties : function() {
			var p = new Map();
			this.setAll(p, this.props_default);
			var sysProps = Environmnet.get();
			setAll(p, sysProps);
			setAll(p, this.props);
			p.set(this.vj$.FrameworkProperties.KEY_KEYS, this.makeKeys());
			return p;
		},

		makeKeys : function() {
			var sb = new StringBuilder();
			var it = this.props.keys();
			for ( var string in it) {
				if (sb.length() > 0) {
					sb.append(',');
				}
				sb.append(string);
			}
			it = this.props_default.keys();
			for ( var string in it) {
				string = _$itr.next();
				sb.append(',');
				sb.append(string);
			}
			return sb.toString();
		},

		initProperties : function(fwCtx) {
			this.setPropertyIfNotSet(Constants.FRAMEWORK_BOOTDELEGATION, "");
			this.setPropertyIfNotSet(Constants.FRAMEWORK_BSNVERSION, Constants.FRAMEWORK_BSNVERSION_MANAGED);
			this.setPropertyIfNotSet(Constants.FRAMEWORK_BUNDLE_PARENT, Constants.FRAMEWORK_BUNDLE_PARENT_BOOT);
			this.setPropertyIfNotSet(Constants.FRAMEWORK_EXECPERMISSION, "");
			/*
			 * if
			 * (!this.props.containsKey(this.vj$.FrameworkProperties.FRAMEWORK_EXECUTIONENVIRONMENT)) {
			 * var ee = new this.vj$.StringBuffer();
			 * ee.append("OSGi/Minimum-1.0"); ee.append(",OSGi/Minimum-1.1");
			 * ee.append(",OSGi/Minimum-1.2"); if (1 ===
			 * this.vj$.FrameworkProperties.javaVersionMajor) { for (var i =
			 * this.vj$.FrameworkProperties.javaVersionMinor; i > 1; i--) {
			 * ee.append((i > 5) ? ",JavaSE-1." : ",J2SE-1."); ee.append(i); } }
			 * this.props.put(this.vj$.FrameworkProperties.FRAMEWORK_EXECUTIONENVIRONMENT,
			 * ee.toString()); }
			 */
			this.setPropertyIfNotSet(Constants.FRAMEWORK_SYSTEMCAPABILITIES, "");
			this.setPropertyIfNotSet(Constants.FRAMEWORK_SYSTEMCAPABILITIES_EXTRA, "");
			this.setPropertyIfNotSet(Constants.FRAMEWORK_LANGUAGE, Environment.getProperty(Environment.LOCALE));
			this.setPropertyIfNotSet(Constants.FRAMEWORK_LIBRARY_EXTENSIONS, "");
			this.setPropertyIfNotSet(Constants.FRAMEWORK_OS_NAME, Alias.unifyOsName(Environment.getProperty(Environment.OS_NAME)));
			if (!this.props.has(Constants.FRAMEWORK_OS_VERSION)) {
				var ver = Environment.getProperty(Environment.OS_VERSION);
				var maj = 0;
				var min = 0;
				var mic = 0;
				var qual = null;
				if (ver !== null) {
					try {
						var st = ver.split(".");
						maj = parseInt(st[0]);
						if (st.length >= 2) {
							qual = st[1];
							min = parseInt(qual);
							qual = null;
							if (st.length >= 3) {
								qual = st[2];
								mic = parseInt(qual);
								qual = null;
								if (st.length >= 4) {
									qual = st[3];
								}
							}
						}
					}
					catch (ignore) {
					}
				}
				var osVersion;
				try {
					osVersion = new Version(maj, min, mic, qual);
				}
				catch (skip) {
					osVersion = new Version(maj, min, mic, null);
				}
				this.props.set(Constants.FRAMEWORK_OS_VERSION, osVersion.toString());
			}
			// setPropertyIfNotSet(Constants.FRAMEWORK_PROCESSOR,
			// this.vj$.Alias.unifyProcessor(this.vj$.System.getProperty("os.arch")));
			this.setPropertyIfNotSet(Constants.FRAMEWORK_SECURITY, "");
			this.setPropertyIfNotSet(Constants.FRAMEWORK_BEGINNING_STARTLEVEL, "1");
			this.setPropertyIfNotSet(Constants.FRAMEWORK_STORAGE, "fwdir");
			this.setPropertyIfNotSet(Constants.FRAMEWORK_STORAGE_CLEAN, "");
			this.setPropertyIfNotSet(Constants.FRAMEWORK_SYSTEMPACKAGES, "");
			this.setPropertyIfNotSet(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, "");
			this.setPropertyIfNotSet(Constants.FRAMEWORK_TRUST_REPOSITORIES, "");
			this.setPropertyIfNotSet(Constants.FRAMEWORK_WINDOWSYSTEM, "");
			this.props.set(Constants.FRAMEWORK_VERSION, "0.1.0");
			this.props.set(Constants.FRAMEWORK_VENDOR, "ESGroup AG");
			this.props.set(Constants.SUPPORTS_FRAMEWORK_REQUIREBUNDLE, this.vj$.FrameworkProperties.TRUE);
			this.props.set(Constants.SUPPORTS_FRAMEWORK_FRAGMENT, this.vj$.FrameworkProperties.TRUE);
			/*
			 * this.setPropertyIfNotSet(Constants.SUPPORTS_FRAMEWORK_EXTENSION,
			 * java.net.URLClassLoader.isInstance(this.getClass().getClassLoader()) &&
			 * fwCtx.id === 0 ? this.vj$.FrameworkProperties.TRUE :
			 * this.vj$.FrameworkProperties.FALSE);
			 * this.setPropertyIfNotSet(Constants.SUPPORTS_BOOTCLASSPATH_EXTENSION,
			 * this.vj$.FrameworkProperties.FALSE); if
			 * (this.getBooleanProperty(Constants.SUPPORTS_BOOTCLASSPATH_EXTENSION) &&
			 * !(java.net.URLClassLoader.isInstance(this.getClass().getClassLoader()) &&
			 * fwCtx.id === 1)) {
			 * this.props.put(Constants.SUPPORTS_BOOTCLASSPATH_EXTENSION,
			 * this.vj$.FrameworkProperties.FALSE); }
			 */
		},

		initKFProperties : function() {
			this.setPropertyDefault(this.vj$.FrameworkProperties.ALL_SIGNED_PROP, this.vj$.FrameworkProperties.FALSE);
			this.setPropertyDefault(this.vj$.FrameworkProperties.AUTOMANIFEST_PROP, this.vj$.FrameworkProperties.FALSE);
			this.setPropertyDefault(this.vj$.FrameworkProperties.AUTOMANIFEST_CONFIG_PROP, "!!/automanifest.props");
			this.setPropertyDefault(this.vj$.FrameworkProperties.BUNDLESTORAGE_PROP, "file");
			this.setPropertyDefault(this.vj$.FrameworkProperties.BUNDLESTORAGE_CHECKSIGNED_PROP, this.vj$.FrameworkProperties.TRUE);
			this.setPropertyDefault(this.vj$.FrameworkProperties.PATCH_PROP, this.vj$.FrameworkProperties.FALSE);
			this.setPropertyDefault(this.vj$.FrameworkProperties.PATCH_CONFIGURL_PROP, "!!/patches.props");
			this.setPropertyDefault(this.vj$.FrameworkProperties.PATCH_DUMPCLASSES_PROP, this.vj$.FrameworkProperties.FALSE);
			this.setPropertyDefault(this.vj$.FrameworkProperties.PATCH_DUMPCLASSES_DIR_PROP, "patchedclasses");
			this.setPropertyDefault(this.vj$.FrameworkProperties.SERVICE_CONDITIONALPERMISSIONADMIN_PROP, this.vj$.FrameworkProperties.TRUE);
			this.setPropertyDefault(this.vj$.FrameworkProperties.SERVICE_PERMISSIONADMIN_PROP, this.vj$.FrameworkProperties.TRUE);
			this.setPropertyDefault(this.vj$.FrameworkProperties.SYSTEM_PACKAGES_BASE_PROP, "");
			this.setPropertyDefault(this.vj$.FrameworkProperties.SYSTEM_PACKAGES_FILE_PROP, "");
			//this.setPropertyDefault(this.vj$.FrameworkProperties.SYSTEM_PACKAGES_VERSION_PROP, this.vj$.Integer.toString(this.vj$.FrameworkProperties.javaVersionMajor) + "." + this.vj$.FrameworkProperties.javaVersionMinor);
			this.setPropertyDefault(this.vj$.FrameworkProperties.IS_DOUBLECHECKED_LOCKING_SAFE_PROP, this.vj$.FrameworkProperties.javaVersionMajor >= 1 && this.vj$.FrameworkProperties.javaVersionMinor >= 5 ? this.vj$.FrameworkProperties.TRUE : this.vj$.FrameworkProperties.FALSE);
			this.setPropertyDefault(this.vj$.FrameworkProperties.LDAP_NOCACHE_PROP, this.vj$.FrameworkProperties.FALSE);
			this.setPropertyDefault(this.vj$.FrameworkProperties.MAIN_CLASS_ACTIVATION_PROP, "");
			this.setPropertyDefault(this.vj$.FrameworkProperties.STRICTBOOTCLASSLOADING_PROP, this.vj$.FrameworkProperties.FALSE);
			//this.setPropertyDefault(this.vj$.FrameworkProperties.VALIDATOR_PROP, this.getProperty(Constants.FRAMEWORK_TRUST_REPOSITORIES).length > 0 ? "JKSValidator" : "none");
			this.setPropertyDefault(this.vj$.FrameworkProperties.SETCONTEXTCLASSLOADER_PROP, this.vj$.FrameworkProperties.FALSE);
			this.setPropertyDefault(this.vj$.FrameworkProperties.REGISTERSERVICEURLHANDLER_PROP, this.vj$.FrameworkProperties.TRUE);
			this.setPropertyDefault(this.vj$.FrameworkProperties.STARTLEVEL_COMPAT_PROP, this.vj$.FrameworkProperties.FALSE);
			this.setPropertyDefault(this.vj$.FrameworkProperties.STARTLEVEL_USE_PROP, this.vj$.FrameworkProperties.TRUE);
		},

		setAll : function(target, source) {
			if (source && source !== null) {
				if (source.forEach) {
					source.forEach(function(value, key, map) {
						target.set(key, value);
					}.bind(this));
				}
				else {
					for ( var key in source) {
						target.set(key, source[key]);
					}
				}
			}
		}

	})

	.inits(function() {
	})

	.endType();

})