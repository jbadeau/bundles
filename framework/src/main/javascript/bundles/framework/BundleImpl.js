define([ "vjo", "osgi/framework/Bundle", "osgi/framework/Constants" ], function(vjo, Bundle, Constants) {

	return vjo.ctype("bundles.framework.BundleImpl")

	.satisfies(Bundle)

	.props({

		IDLE : 0,

		ACTIVATING : 1,

		DEACTIVATING : 2,

		RESOLVING : 3,

		UNINSTALLING : 4,

		UNRESOLVING : 5,

		UPDATING : 6

	})

	.protos({

		fwCtx : null,

		id : 0,

		location : null,

		secure : null,

		state : 0,

		generations : null,

		bundleDir : null,

		bundleContext : null,

		bactivator : null,

		cachedHeaders : null,

		operation : 0,

		resolveFailException : null,

		wasStarted : false,

		aborted : null,

		bundleThread : null,

		constructs : function() {
			if (arguments.length === 1) {
				this.constructs_1_0_BundleImpl_ovld(arguments[0]);
			}
			else if (arguments.length === 4) {
				this.constructs_4_0_BundleImpl_ovld(arguments[0], arguments[1], arguments[2], arguments[3]);
			}
		},

		constructs_1_0_BundleImpl_ovld : function(fw) {
			this.fwCtx = fw;
			this.secure = this.fwCtx.perm;
			this.id = 0;
			this.generations = [];
			this.location = Constants.SYSTEM_BUNDLE_LOCATION;
			this.state = Bundle.INSTALLED;
			this.bundleDir = this.fwCtx.getDataStorage(this.id);
		},

		constructs_4_0_BundleImpl_ovld : function(fw, ba, checkContext, caller) {
			this.fwCtx = fw;
			this.secure = this.fwCtx.perm;
			this.id = ba.getBundleId();
			this.location = ba.getBundleLocation();
			this.state = this.vj$.Bundle.INSTALLED;
			this.generations = new this.vj$.Vector(2);
			var gen = new this.vj$.BundleGeneration(this, ba, null, caller);
			this.generations.add(gen);
			gen.checkPermissions(checkContext);
			this.doExportImport();
			this.bundleDir = this.fwCtx.getDataStorage(this.id);
			if (gen.isExtension() && this.attachToFragmentHost(this.fwCtx.systemBundle.current())) {
				gen.setWired();
				this.state = this.vj$.Bundle.RESOLVED;
			}
		},

		getState : function() {
			return this.state;
		},

		start : function() {
			if (arguments.length === 0) {
				this.start_0_0_BundleImpl_ovld();
			}
			else if (arguments.length === 1) {
				this.start_1_0_BundleImpl_ovld(arguments[0]);
			}
		},

		start_0_0_BundleImpl_ovld : function() {
			this.start(0);
		},

		start_1_0_BundleImpl_ovld : function(options) {
			this.secure.checkExecuteAdminPerm(this);
			{
				var current = this.current();
				if (current.isFragment()) {
					throw new this.vj$.BundleException("Bundle#" + this.id + ", cannot start a fragment bundle", this.vj$.BundleException.INVALID_OPERATION);
				}
				if (this.state === this.vj$.Bundle.UNINSTALLED) {
					throw new this.vj$.IllegalStateException("Bundle is uninstalled");
				}
				this.fwCtx.resolverHooks.checkResolveBlocked();
				options &= 0xFF;
				if (this.fwCtx.startLevelController !== null) {
					if (this.getStartLevel() > this.fwCtx.startLevelController.getStartLevel()) {
						if ((options & this.vj$.Bundle.START_TRANSIENT) !== 0) {
							throw new this.vj$.BundleException("Bundle#" + this.id + ", can not transiently activate bundle with start level " + this.getStartLevel() + " when running on start level " + this.fwCtx.startLevelController.getStartLevel(), this.vj$.BundleException.START_TRANSIENT_ERROR);
						}
						else {
							this.setAutostartSetting(options);
							return;
						}
					}
				}
				this.waitOnOperation(this.fwCtx.resolver, "Bundle.start", false);
				if (this.state === this.vj$.Bundle.ACTIVE) {
					return;
				}
				if ((options & this.vj$.Bundle.START_TRANSIENT) === 0) {
					this.setAutostartSetting(options);
				}
				if ((options & this.vj$.Bundle.START_ACTIVATION_POLICY) !== 0 && current.lazyActivation) {
					if (this.vj$.Bundle.INSTALLED === this.getUpdatedState([ this ])) {
						throw this.resolveFailException;
					}
					if (this.vj$.Bundle.STARTING === this.state) {
						return;
					}
					this.state = this.vj$.Bundle.STARTING;
					this.bundleContext = new this.vj$.BundleContextImpl(this);
					this.operation = this.vj$.BundleImpl.ACTIVATING;
				}
				else {
					this.secure.callFinalizeActivation(this);
					return;
				}
			}
			this.secure.callBundleChanged(this.fwCtx, new this.vj$.BundleEvent(this.vj$.BundleEvent.LAZY_ACTIVATION, this));
			{
				this.operation = this.vj$.BundleImpl.IDLE;
				this.fwCtx.resolver.notifyAll();
			}
		},

		finalizeActivation : function() {
			{
				switch (this.getUpdatedState([ this ])) {
					case this.vj$.Bundle.INSTALLED:
						throw this.resolveFailException;
					case this.vj$.Bundle.STARTING:
						if (this.operation === this.vj$.BundleImpl.ACTIVATING) {
							return;
						}
					case this.vj$.Bundle.RESOLVED:
						this.state = this.vj$.Bundle.STARTING;
						this.operation = this.vj$.BundleImpl.ACTIVATING;
						if (this.fwCtx.debug.lazy_activation) {
							this.fwCtx.debug.println("activating #" + this.getBundleId());
						}
						if (null === this.bundleContext) {
							this.bundleContext = new this.vj$.BundleContextImpl(this);
						}
						e = this.bundleThread().callStart0(this);
						this.operation = this.vj$.BundleImpl.IDLE;
						this.fwCtx.resolver.notifyAll();
						if (e !== null) {
							throw e;
						}
						break;
					case this.vj$.Bundle.ACTIVE:
						break;
					case this.vj$.Bundle.STOPPING:
						throw new this.vj$.BundleException("Bundle#" + this.id + ", start called from BundleActivator.stop", this.vj$.BundleException.ACTIVATOR_ERROR);
					case this.vj$.Bundle.UNINSTALLED:
						throw new this.vj$.IllegalStateException("Bundle is in UNINSTALLED state");
				}
			}
		},

		start0 : function() {
			var archive = this.current().archive;
			var ba = archive.getAttribute(this.vj$.Constants.BUNDLE_ACTIVATOR);
			var bStarted = false;
			var res = null;
			this.fwCtx.listeners.bundleChanged(new this.vj$.BundleEvent(this.vj$.BundleEvent.STARTING, this));
			var oldLoader = null;
			if (this.fwCtx.props.SETCONTEXTCLASSLOADER) {
				oldLoader = this.vj$.Thread.currentThread().getContextClassLoader();
				this.vj$.Thread.currentThread().setContextClassLoader(this.getClassLoader());
			}
			var error_type = this.vj$.BundleException.MANIFEST_ERROR;
			try {
				if (ba !== null) {
					var c = this.getClassLoader().loadClass(org.eclipse.vjet.vjo.java.lang.StringUtil.trim(ba));
					error_type = this.vj$.BundleException.ACTIVATOR_ERROR;
					this.bactivator = c.newInstance();
					this.bactivator.start(this.bundleContext);
					bStarted = true;
				}
				else {
					var locations = this.fwCtx.props.getProperty(this.vj$.FrameworkProperties.MAIN_CLASS_ACTIVATION_PROP);
					if (locations.length > 0) {
						var mc = archive.getAttribute("Main-Class");
						if (mc !== null) {
							var locs = this.vj$.Util.splitwords(locations, ",");
							for (var loc, _$i0 = 0; _$i0 < locs.length; _$i0++) {
								loc = locs[_$i0];
								if (vjo.java.lang.ObjectUtil.equals(loc, this.location)) {
									if (this.fwCtx.debug.resolver) {
										this.fwCtx.debug.println("starting main class " + mc);
									}
									error_type = this.vj$.BundleException.ACTIVATOR_ERROR;
									var mainClass = this.getClassLoader().loadClass(org.eclipse.vjet.vjo.java.lang.StringUtil.trim(mc));
									this.bactivator = new this.vj$.MainClassBundleActivator(mainClass);
									this.bactivator.start(this.bundleContext);
									bStarted = true;
									break;
								}
							}
						}
					}
				}
				if (!bStarted) {
				}
			}
			catch (t) {
				res = new this.vj$.BundleException("Bundle#" + this.id + " start failed", error_type, t);
			}
			{
				if (!isBundleThread(this.vj$.Thread.currentThread())) {
					throw new this.vj$.RuntimeException("Aborted bundle thread ending execution");
				}
				var cause = null;
				if (this.aborted === org.eclipse.vjet.vjo.java.lang.BooleanUtil.TRUE) {
					if (this.vj$.Bundle.UNINSTALLED === this.state) {
						cause = new this.vj$.Exception("Bundle uninstalled during start()");
					}
					else {
						cause = new this.vj$.Exception("Bundle activator start() time-out");
					}
				}
				else {
					this.aborted = org.eclipse.vjet.vjo.java.lang.BooleanUtil.FALSE;
					if (this.vj$.Bundle.STARTING !== this.state) {
						cause = new this.vj$.Exception("Bundle changed state because of refresh during start()");
					}
				}
				if (cause !== null) {
					res = new this.vj$.BundleException("Bundle#" + this.id + " start failed", this.vj$.BundleException.STATECHANGE_ERROR, cause);
				}
			}
			if (this.fwCtx.debug.lazy_activation) {
				this.fwCtx.debug.println("activating #" + this.getBundleId() + " completed.");
			}
			if (this.fwCtx.props.SETCONTEXTCLASSLOADER) {
				this.vj$.Thread.currentThread().setContextClassLoader(oldLoader);
			}
			if (res === null) {
				this.state = this.vj$.Bundle.ACTIVE;
				this.fwCtx.listeners.bundleChanged(new this.vj$.BundleEvent(this.vj$.BundleEvent.STARTED, this));
			}
			else if (this.operation === this.vj$.BundleImpl.ACTIVATING) {
				this.startFailed();
			}
			return res;
		},

		startFailed : function() {
			this.state = this.vj$.Bundle.STOPPING;
			this.fwCtx.listeners.bundleChanged(new this.vj$.BundleEvent(this.vj$.BundleEvent.STOPPING, this));
			this.removeBundleResources();
			this.bundleContext.invalidate();
			this.bundleContext = null;
			this.state = this.vj$.Bundle.RESOLVED;
			this.fwCtx.listeners.bundleChanged(new this.vj$.BundleEvent(this.vj$.BundleEvent.STOPPED, this));
		},

		stop : function() {
			if (arguments.length === 0) {
				this.stop_0_0_BundleImpl_ovld();
			}
			else if (arguments.length === 1) {
				this.stop_1_0_BundleImpl_ovld(arguments[0]);
			}
		},

		stop_0_0_BundleImpl_ovld : function() {
			this.stop(0);
		},

		stop_1_0_BundleImpl_ovld : function(options) {
			var savedException = null;
			this.secure.checkExecuteAdminPerm(this);
			{
				if (this.current().isFragment()) {
					throw new this.vj$.BundleException("Bundle#" + this.id + ", can not stop a fragment", this.vj$.BundleException.INVALID_OPERATION);
				}
				if (this.state === this.vj$.Bundle.UNINSTALLED) {
					throw new this.vj$.IllegalStateException("Bundle is uninstalled");
				}
				this.waitOnOperation(this.fwCtx.resolver, "Bundle.stop", false);
				if ((options & this.vj$.Bundle.STOP_TRANSIENT) === 0) {
					this.setAutostartSetting(-1);
				}
				switch (this.state) {
					case this.vj$.Bundle.INSTALLED:
					case this.vj$.Bundle.RESOLVED:
					case this.vj$.Bundle.STOPPING:
					case this.vj$.Bundle.UNINSTALLED:
						return;
					case this.vj$.Bundle.ACTIVE:
					case this.vj$.Bundle.STARTING:
						savedException = this.stop0();
						break;
				}
			}
			if (savedException !== null) {
				if (osgi.framework.BundleException.isInstance(savedException)) {
					throw savedException;
				}
				else {
					throw savedException;
				}
			}
		},

		stop0 : function() {
			this.wasStarted = this.state === this.vj$.Bundle.ACTIVE;
			this.state = this.vj$.Bundle.STOPPING;
			this.operation = this.vj$.BundleImpl.DEACTIVATING;
			var savedException = this.bundleThread().callStop1(this);
			if (this.state !== this.vj$.Bundle.UNINSTALLED) {
				this.state = this.vj$.Bundle.RESOLVED;
				this.bundleThread().bundleChanged(new this.vj$.BundleEvent(this.vj$.BundleEvent.STOPPED, this));
				this.fwCtx.resolver.notifyAll();
				this.operation = this.vj$.BundleImpl.IDLE;
			}
			return savedException;
		},

		stop1 : function() {
			var res = null;
			this.fwCtx.listeners.bundleChanged(new this.vj$.BundleEvent(this.vj$.BundleEvent.STOPPING, this));
			if (this.wasStarted && this.bactivator !== null) {
				try {
					this.bactivator.stop(this.bundleContext);
				}
				catch (e) {
					res = new this.vj$.BundleException("Bundle#" + this.id + ", BundleActivator.stop() failed", this.vj$.BundleException.ACTIVATOR_ERROR, e);
				}
				{
					var cause = null;
					if (this.aborted === org.eclipse.vjet.vjo.java.lang.BooleanUtil.TRUE) {
						if (this.vj$.Bundle.UNINSTALLED === this.state) {
							cause = new this.vj$.Exception("Bundle uninstalled during stop()");
						}
						else {
							cause = new this.vj$.Exception("Bundle activator stop() time-out");
						}
					}
					else {
						this.aborted = org.eclipse.vjet.vjo.java.lang.BooleanUtil.FALSE;
						if (this.vj$.Bundle.STOPPING !== this.state) {
							cause = new this.vj$.Exception("Bundle changed state because of refresh during stop()");
						}
					}
					if (cause !== null) {
						res = new this.vj$.BundleException("Bundle stop failed", this.vj$.BundleException.STATECHANGE_ERROR, cause);
					}
				}
				this.bactivator = null;
			}
			if (this.operation === this.vj$.BundleImpl.DEACTIVATING) {
				this.stop2();
			}
			return res;
		},

		stop2 : function() {
			if (null !== this.bundleContext) {
				this.fwCtx.listeners.serviceListeners.hooksBundleStopped(this.bundleContext);
				this.removeBundleResources();
				this.bundleContext.invalidate();
				this.bundleContext = null;
			}
		},

		waitOnOperation : function(lock, src, longWait) {
			if (this.operation !== this.vj$.BundleImpl.IDLE) {
				var left = longWait ? 20000 : 500;
				var waitUntil = this.vj$.Util.timeMillis() + left;
				do {
					try {
						lock.wait(left);
						if (this.operation === this.vj$.BundleImpl.IDLE) {
							return;
						}
					}
					catch (_ie) {
					}
					left = waitUntil - this.vj$.Util.timeMillis();
				}
				while (left > 0);
				var op;
				switch (this.operation) {
					case this.vj$.BundleImpl.IDLE:
						return;
					case this.vj$.BundleImpl.ACTIVATING:
						op = "start";
						break;
					case this.vj$.BundleImpl.DEACTIVATING:
						op = "stop";
						break;
					case this.vj$.BundleImpl.RESOLVING:
						op = "resolve";
						break;
					case this.vj$.BundleImpl.UNINSTALLING:
						op = "uninstall";
						break;
					case this.vj$.BundleImpl.UNRESOLVING:
						op = "unresolve";
						break;
					case this.vj$.BundleImpl.UPDATING:
						op = "update";
						break;
					default:
						op = "unknown operation";
						break;
				}
				throw new this.vj$.BundleException(src + " called during " + op + " of Bundle#" + this.id, this.vj$.BundleException.STATECHANGE_ERROR);
			}
		},

		update : function() {
			if (arguments.length === 0) {
				this.update_0_0_BundleImpl_ovld();
			}
			else if (arguments.length === 1) {
				this.update_1_0_BundleImpl_ovld(arguments[0]);
			}
		},

		update_0_0_BundleImpl_ovld : function() {
			this.update(null);
		},

		update_1_0_BundleImpl_ovld : function(in_) {
			try {
				this.secure.checkLifecycleAdminPerm(this);
				if (this.current().isExtension()) {
					this.secure.checkExtensionLifecycleAdminPerm(this);
				}
				{
					var wasActive = this.state === this.vj$.Bundle.ACTIVE;
					switch (this.getState()) {
						case this.vj$.Bundle.ACTIVE:
							this.stop(this.vj$.Bundle.STOP_TRANSIENT);
						case this.vj$.Bundle.RESOLVED:
						case this.vj$.Bundle.INSTALLED:
							this.secure.callUpdate0(this, in_, wasActive);
							break;
						case this.vj$.Bundle.STARTING:
							throw new this.vj$.IllegalStateException("Bundle is in STARTING state");
						case this.vj$.Bundle.STOPPING:
							throw new this.vj$.IllegalStateException("Bundle is in STOPPING state");
						case this.vj$.Bundle.UNINSTALLED:
							throw new this.vj$.IllegalStateException("Bundle is in UNINSTALLED state");
					}
				}
			}
			finally {
				if (in_ !== null) {
					try {
						in_.close();
					}
					catch (ignore) {
					}
				}
			}
		},

		update0 : function(in_, wasActive, checkContext) {
			var wasResolved = this.state === this.vj$.Bundle.RESOLVED;
			var current = this.current();
			var oldFragment = current.fragment;
			var oldStartLevel = this.getStartLevel();
			var newArchive = null;
			var newGeneration = null;
			this.operation = this.vj$.BundleImpl.UPDATING;
			try {
				var bin;
				var archive = current.archive;
				if (in_ === null) {
					var update = archive !== null ? archive.getAttribute(this.vj$.Constants.BUNDLE_UPDATELOCATION) : null;
					if (update === null) {
						update = this.location;
					}
					var url = new this.vj$.URL(update);
					var fname = url.getFile();
					if (org.eclipse.vjet.vjo.java.lang.StringUtil.startsWith(fname, "file:")) {
						fname = fname.substring(5);
					}
					var file = new this.vj$.File(fname);
					if (file.isDirectory()) {
						bin = null;
					}
					else {
						bin = url.openStream();
					}
				}
				else {
					bin = in_;
				}
				newArchive = this.fwCtx.storage.updateBundleArchive(archive, bin);
				newGeneration = new this.vj$.BundleGeneration(this, newArchive, current, this);
				newGeneration.checkPermissions(checkContext);
				newArchive.setStartLevel(oldStartLevel);
				this.fwCtx.storage.replaceBundleArchive(archive, newGeneration.archive);
			}
			catch (e) {
				if (newArchive !== null) {
					newArchive.purge();
				}
				this.operation = this.vj$.BundleImpl.IDLE;
				if (wasActive) {
					try {
						this.start();
					}
					catch (be) {
						this.fwCtx.frameworkError(this, be);
					}
				}
				if (osgi.framework.BundleException.isInstance(e)) {
					throw e;
				}
				else {
					throw new this.vj$.BundleException("Failed to get update Bundle#" + this.id, this.vj$.BundleException.UNSPECIFIED, e);
				}
			}
			var saveZombie;
			if (oldFragment !== null) {
				saveZombie = oldFragment.hasHosts();
				if (saveZombie) {
					if (oldFragment.extension !== null) {
						if (vjo.java.lang.ObjectUtil.equals(oldFragment.extension, this.vj$.Constants.EXTENSION_BOOTCLASSPATH)) {
							this.fwCtx.systemBundle.bootClassPathHasChanged = true;
						}
					}
					else {
						for (var bundleGeneration, _$itr = oldFragment.getHosts().iterator(); _$itr.hasNext();) {
							bundleGeneration = _$itr.next();
							bundleGeneration.bpkgs.fragmentIsZombie(this);
						}
					}
				}
			}
			else {
				saveZombie = !current.unregisterPackages(false);
				if (!saveZombie) {
					current.closeClassLoader();
				}
			}
			var oldGen = current;
			this.state = this.vj$.Bundle.INSTALLED;
			if (saveZombie) {
				this.generations.add(0, newGeneration);
				this.fwCtx.bundles.addZombie(this);
			}
			else {
				this.generations.set(0, newGeneration);
			}
			this.doExportImport();
			if (!saveZombie) {
				oldGen.purge(false);
			}
			if (wasResolved) {
				this.bundleThread().bundleChanged(new this.vj$.BundleEvent(this.vj$.BundleEvent.UNRESOLVED, this));
			}
			this.bundleThread().bundleChanged(new this.vj$.BundleEvent(this.vj$.BundleEvent.UPDATED, this));
			this.operation = this.vj$.BundleImpl.IDLE;
			if (wasActive) {
				try {
					this.start();
				}
				catch (be) {
					this.fwCtx.frameworkError(this, be);
				}
			}
		},

		uninstall : function() {
			this.secure.checkLifecycleAdminPerm(this);
			if (this.current().isExtension()) {
				this.secure.checkExtensionLifecycleAdminPerm(this);
			}
			this.secure.callUninstall0(this);
		},

		uninstall0 : function() {
			{
				var current = this.current();
				if (!current.isUninstalled()) {
					try {
						current.archive.setStartLevel(-2);
					}
					catch (ignored) {
					}
				}
				var doPurge = false;
				switch (this.state) {
					case this.vj$.Bundle.UNINSTALLED:
						throw new this.vj$.IllegalStateException("Bundle is in UNINSTALLED state");
					case this.vj$.Bundle.STARTING:
					case this.vj$.Bundle.ACTIVE:
					case this.vj$.Bundle.STOPPING:
						exception;
						try {
							this.waitOnOperation(this.fwCtx.resolver, "Bundle.uninstall", true);
							exception = (this.state & (this.vj$.Bundle.ACTIVE | this.vj$.Bundle.STARTING)) !== 0 ? this.stop0() : null;
						}
						catch (se) {
							this.setStateInstalled(false);
							this.fwCtx.resolver.notifyAll();
							exception = se;
						}
						this.operation = this.vj$.BundleImpl.UNINSTALLING;
						if (exception !== null) {
							this.fwCtx.frameworkError(this, exception);
						}
					case this.vj$.Bundle.RESOLVED:
					case this.vj$.Bundle.INSTALLED:
						this.fwCtx.bundles.remove(this.location);
						if (this.operation !== this.vj$.BundleImpl.UNINSTALLING) {
							try {
								this.waitOnOperation(this.fwCtx.resolver, "Bundle.uninstall", true);
								this.operation = this.vj$.BundleImpl.UNINSTALLING;
							}
							catch (be) {
								if (this.bundleContext !== null) {
									this.bundleContext.invalidate();
									this.bundleContext = null;
								}
								this.operation = this.vj$.BundleImpl.UNINSTALLING;
								this.fwCtx.frameworkError(this, be);
							}
						}
						if (this.state === this.vj$.Bundle.UNINSTALLED) {
							this.operation = this.vj$.BundleImpl.IDLE;
							throw new this.vj$.IllegalStateException("Bundle is in UNINSTALLED state");
						}
						saveZombie = false;
						if (current.isFragment()) {
							if (this.isAttached()) {
								if (current.isExtension()) {
									if (current.isBootClassPathExtension()) {
										this.fwCtx.systemBundle.bootClassPathHasChanged = true;
									}
								}
								else {
									for (var hbg, _$itr = current.getHosts().iterator(); _$itr.hasNext();) {
										hbg = _$itr.next();
										if (hbg.bpkgs !== null) {
											hbg.bpkgs.fragmentIsZombie(this);
										}
									}
								}
								saveZombie = true;
							}
							else {
								doPurge = true;
							}
						}
						else {
							var pkgsUnregistered = current.unregisterPackages(false);
							if (pkgsUnregistered) {
								current.closeClassLoader();
								doPurge = true;
							}
							else {
								saveZombie = true;
							}
						}
						this.state = this.vj$.Bundle.INSTALLED;
						this.bundleThread().bundleChanged(new this.vj$.BundleEvent(this.vj$.BundleEvent.UNRESOLVED, this));
						this.cachedHeaders = current.getHeaders0(null);
						this.bactivator = null;
						this.state = this.vj$.Bundle.UNINSTALLED;
						oldGen = current;
						if (saveZombie) {
							this.generations.add(0, new this.vj$.BundleGeneration(oldGen));
							this.fwCtx.bundles.addZombie(this);
						}
						else {
							this.generations.set(0, new this.vj$.BundleGeneration(oldGen));
						}
						if (doPurge) {
							oldGen.purge(false);
						}
						this.operation = this.vj$.BundleImpl.IDLE;
						if (this.bundleDir !== null) {
							if (this.bundleDir.exists() && !this.bundleDir.delete_()) {
								this.fwCtx.frameworkError(this, new this.vj$.IOException("Failed to delete bundle data"));
							}
							this.bundleDir = null;
						}
						this.fwCtx.resolver.notifyAll();
						break;
				}
			}
			this.fwCtx.listeners.bundleChanged(new this.vj$.BundleEvent(this.vj$.BundleEvent.UNINSTALLED, this));
		},

		getBundleId : function() {
			return this.id;
		},

		getLocation : function() {
			this.secure.checkMetadataAdminPerm(this);
			return this.location;
		},

		getRegisteredServices : function() {
			this.checkUninstalled();
			var sr = this.fwCtx.services.getRegisteredByBundle(this);
			this.secure.filterGetServicePermission(sr);
			if (sr.size() > 0) {
				var res = vjo.createArray(null, sr.size());
				var pos = 0;
				for (var serviceRegistrationImpl, _$itr = sr.iterator(); _$itr.hasNext();) {
					serviceRegistrationImpl = _$itr.next();
					res[pos++] = serviceRegistrationImpl.getReference();
				}
				return res;
			}
			return null;
		},

		getServicesInUse : function() {
			this.checkUninstalled();
			var sr = this.fwCtx.services.getUsedByBundle(this);
			this.secure.filterGetServicePermission(sr);
			if (sr.size() > 0) {
				var res = vjo.createArray(null, sr.size());
				var pos = 0;
				for (var serviceRegistrationImpl, _$itr = sr.iterator(); _$itr.hasNext();) {
					serviceRegistrationImpl = _$itr.next();
					res[pos++] = serviceRegistrationImpl.getReference();
				}
				return res;
			}
			return null;
		},

		hasPermission : function(permission) {
			var fix = this.current();
			this.checkUninstalled();
			if (java.security.Permission.isInstance(permission)) {
				if (this.secure.checkPermissions()) {
					var pc = fix.getProtectionDomain().getPermissions();
					return pc !== null ? pc.implies(/* >> */permission) : false;
				}
				else {
					return true;
				}
			}
			else {
				return false;
			}
		},

		getBundleContext : function() {
			this.secure.checkContextAdminPerm(this);
			return this.bundleContext;
		},

		getResource : function(name) {
			this.checkUninstalled();
			if (this.secure.okResourceAdminPerm(this) && !this.current().isFragment()) {
				if (this.getUpdatedState([ this ]) !== this.vj$.Bundle.INSTALLED) {
					var cl0 = this.getClassLoader();
					if (cl0 !== null) {
						return cl0.getResource(name);
					}
				}
				else {
					var uv = this.secure.getBundleClassPathEntries(this.current(), name, true);
					if (uv !== null) {
						return uv.firstElement();
					}
				}
			}
			return null;
		},

		getSymbolicName : function() {
			return this.current().symbolicName;
		},

		getLastModified : function() {
			return this.current().timeStamp;
		},

		getSignerCertificates : function(signersType) {
			var onlyTrusted;
			if (signersType === this.vj$.Bundle.SIGNERS_ALL) {
				onlyTrusted = false;
			}
			else if (signersType === this.vj$.Bundle.SIGNERS_TRUSTED) {
				onlyTrusted = true;
			}
			else {
				throw new this.vj$.IllegalArgumentException("signersType not SIGNER_ALL or SIGNERS_TRUSTED");
			}
			var fix = this.current().archive;
			if (fix !== null) {
				var cs = fix.getCertificateChains(onlyTrusted);
				if (cs !== null) {
					var res = new this.vj$.HashMap();
					for (var chain, _$itr = cs.iterator(); _$itr.hasNext();) {
						chain = _$itr.next();
						var copy = new this.vj$.ArrayList(chain);
						res.put(chain.get(0), copy);
					}
					return res;
				}
			}
			return this.vj$.Collections.EMPTY_MAP;
		},

		getVersion : function() {
			return this.current().version;
		},

		adapt : function(type) {
			this.secure.checkAdaptPerm(this, type);
			return this.adaptSecure(type);
		},

		getDataFile : function(filename) {
			return this.bundleContext.getDataFile(filename);
		},

		compareTo : function(bundle) {
			return (new this.vj$.Long(this.getBundleId())).compareTo(new this.vj$.Long(bundle.getBundleId()));
		},

		getUpdatedState : function(triggers) {
			if (this.state === this.vj$.Bundle.INSTALLED) {
				try {
					{
						this.waitOnOperation(this.fwCtx.resolver, "Bundle.resolve", true);
						if (this.state === this.vj$.Bundle.INSTALLED) {
							var current = this.current();
							if (triggers !== null) {
								this.fwCtx.resolverHooks.beginResolve(triggers);
							}
							if (current.isFragment()) {
								var hosts = current.fragment.targets();
								if (!hosts.isEmpty()) {
									for (var host, _$itr = hosts.iterator(); _$itr.hasNext();) {
										host = _$itr.next();
										if (!host.bpkgs.isActive()) {
											host.bundle.getUpdatedState(null);
										}
										else {
											if (!current.fragment.isHost(host)) {
												this.attachToFragmentHost(host);
											}
										}
									}
									if (this.state === this.vj$.Bundle.INSTALLED && current.fragment.hasHosts()) {
										current.setWired();
										this.state = this.vj$.Bundle.RESOLVED;
										this.operation = this.vj$.BundleImpl.RESOLVING;
										this.bundleThread().bundleChanged(new this.vj$.BundleEvent(this.vj$.BundleEvent.RESOLVED, this));
										this.operation = this.vj$.BundleImpl.IDLE;
									}
								}
							}
							else {
								if (current.resolvePackages(triggers)) {
									current.setWired();
									this.state = this.vj$.Bundle.RESOLVED;
									this.operation = this.vj$.BundleImpl.RESOLVING;
									current.updateStateFragments();
									this.bundleThread().bundleChanged(new this.vj$.BundleEvent(this.vj$.BundleEvent.RESOLVED, this));
									this.operation = this.vj$.BundleImpl.IDLE;
								}
								else {
									var reason = current.bpkgs.getResolveFailReason();
									throw new this.vj$.BundleException("Bundle#" + this.id + ", unable to resolve: " + reason, reason === this.vj$.Resolver.RESOLVER_HOOK_VETO ? this.vj$.BundleException.REJECTED_BY_HOOK : this.vj$.BundleException.RESOLVE_ERROR);
								}
							}
							if (triggers !== null && triggers.length === 1) {
								var t = triggers;
								triggers = null;
								this.fwCtx.resolverHooks.endResolve(t);
							}
						}
					}
				}
				catch (be) {
					this.resolveFailException = be;
					this.fwCtx.frameworkError(this, be);
					if (triggers !== null && triggers.length === 1) {
						try {
							this.fwCtx.resolverHooks.endResolve(triggers);
						}
						catch (be2) {
							this.resolveFailException = be2;
							this.fwCtx.frameworkError(this, be2);
						}
					}
				}
			}
			return this.state;
		},

		attachToFragmentHost : function(host) {
			var fix = this.current();
			if (fix.isFragment() && this.secure.okFragmentBundlePerm(this)) {
				try {
					if (fix.isExtension()) {
						this.fwCtx.systemBundle.attachExtension(fix);
					}
					else {
						host.attachFragment(fix);
					}
					fix.fragment.addHost(host);
					return true;
				}
				catch (e) {
					if (e instanceof this.vj$.BundleException) {
						var be = e;
						throw be;
					}
					else if (e instanceof this.vj$.Exception) {
						this.fwCtx.frameworkWarning(this, e);
					}
				}
			}
			return false;
		},

		getDataRoot : function() {
			return this.bundleDir;
		},

		getClassLoader : function() {
			return this.current().getClassLoader();
		},

		setStateInstalled : function(sendEvent) {
			{
				if (this.bundleContext !== null) {
					this.bundleContext.invalidate();
					this.bundleContext = null;
				}
				var current = this.current();
				if (current.isFragment()) {
					current.fragment.removeHost(null);
				}
				else {
					current.closeClassLoader();
					current.unregisterPackages(true);
					current.bpkgs.registerPackages();
				}
				current.clearWiring();
				this.state = this.vj$.Bundle.INSTALLED;
				if (sendEvent) {
					this.operation = this.vj$.BundleImpl.UNRESOLVING;
					this.bundleThread().bundleChanged(new this.vj$.BundleEvent(this.vj$.BundleEvent.UNRESOLVED, this));
				}
				this.operation = this.vj$.BundleImpl.IDLE;
			}
		},

		purge : function() {
			this.fwCtx.bundles.removeZombie(this);
			var old;
			{
				var sub = this.generations.subList(1, this.generations.size());
				old = new this.vj$.Vector(sub);
				sub.clear();
			}
			for (var bg, _$itr = old.iterator(); _$itr.hasNext();) {
				bg = _$itr.next();
				bg.purge(true);
			}
		},

		getBundleArchive : function(generation) {
			{
				for (var bg, _$itr = this.generations.iterator(); _$itr.hasNext();) {
					bg = _$itr.next();
					if (bg.generation === generation) {
						return bg.archive;
					}
				}
			}
			return null;
		},

		getExports : function() {
			{
				var res;
				if (this.generations.size() > 1) {
					res = new this.vj$.HashSet();
					for (var bg, _$itr = this.generations.iterator(); _$itr.hasNext();) {
						bg = _$itr.next();
						var bp = bg.bpkgs;
						if (bp !== null) {
							for (var j = bp.getExports(); j.hasNext();) {
								res.add(j.next());
							}
						}
					}
					return res.iterator();
				}
				else {
					var bp = this.generations.get(0).bpkgs;
					if (bp !== null) {
						return bp.getExports();
					}
					else {
						return this.vj$.Collections.EMPTY_LIST.iterator();
					}
				}
			}
		},

		getHosts : function(zombieHosts) {
			var res = null;
			if (zombieHosts) {
				{
					for (var bg, _$itr = this.generations.iterator(); _$itr.hasNext();) {
						bg = _$itr.next();
						var h = bg.getHosts();
						if (h !== null) {
							if (res !== null) {
								res.addAll(h);
							}
							else {
								res = h;
							}
						}
					}
				}
			}
			else {
				res = this.current().getHosts();
			}
			return res;
		},

		getRequiredBy : function() {
			var res = null;
			{
				for (var bg, _$itr = this.generations.iterator(); _$itr.hasNext();) {
					bg = _$itr.next();
					var bp = bg.bpkgs;
					if (bp !== null) {
						var rb = bp.getRequiredBy();
						if (res !== null) {
							res.addAll(rb);
						}
						else {
							res = rb;
						}
					}
				}
			}
			return res !== null ? res : this.vj$.Collections.EMPTY_LIST;
		},

		setAutostartSetting : function(setting) {
			this.secure.callSetAutostartSetting(this, setting);
		},

		setAutostartSetting0 : function(setting) {
			try {
				var ba = this.current().archive;
				if (null !== ba) {
					ba.setAutostartSetting(setting);
				}
			}
			catch (e) {
				this.fwCtx.frameworkError(this, e);
			}
		},

		getAutostartSetting : function() {
			var ba = this.current().archive;
			return ba !== null ? ba.getAutostartSetting() : -1;
		},

		getStartLevel : function() {
			var ba = this.current().archive;
			if (ba !== null) {
				return ba.getStartLevel();
			}
			else {
				return 0;
			}
		},

		setStartLevel : function(n) {
			var ba = this.current().archive;
			if (ba !== null) {
				try {
					ba.setStartLevel(n);
				}
				catch (e) {
					this.fwCtx.frameworkError(this, new this.vj$.BundleException("Failed to set start level on #" + this.id, e));
				}
			}
		},

		toString : function() {
			if (arguments.length === 0) {
				return this.toString_0_0_BundleImpl_ovld();
			}
			else if (arguments.length === 1) {
				return this.toString_1_0_BundleImpl_ovld(arguments[0]);
			}
		},

		toString_0_0_BundleImpl_ovld : function() {
			return this.toString(0);
		},

		toString_1_0_BundleImpl_ovld : function(detail) {
			var sb = new this.vj$.StringBuffer();
			sb.append("BundleImpl[");
			sb.append("id=" + this.getBundleId());
			if (detail > 0) {
				sb.append(", state=" + this.getState());
			}
			if (detail > 1) {
				sb.append(", startlevel=" + this.getStartLevel());
			}
			if (detail > 3) {
				try {
					sb.append(", autostart setting=");
					sb.append(this.getAutostartSetting());
				}
				catch (e) {
					sb.append(e.toString());
				}
			}
			if (detail > 4) {
				sb.append(", loc=" + this.location);
			}
			if (detail > 4) {
				sb.append(", symName=" + this.getSymbolicName());
			}
			sb.append("]");
			return sb.toString();
		},

		findEntries : function(path, filePattern, recurse) {
			if (this.secure.okResourceAdminPerm(this)) {
				this.getUpdatedState([ this ]);
				var res = this.secure.callFindEntries(this.current(), path, filePattern, recurse);
				if (!res.isEmpty()) {
					return res.elements();
				}
			}
			return null;
		},

		getEntry : function(name) {
			if (this.secure.okResourceAdminPerm(this)) {
				this.checkUninstalled();
				try {
					var fix = this.current();
					if (vjo.java.lang.ObjectUtil.equals("/", name)) {
						return fix.getURL(0, "/");
					}
					var is = this.secure.callGetBundleResourceStream(fix.archive, name, 0);
					if (is !== null) {
						is.close();
						return fix.getURL(0, name);
					}
				}
				catch (_ignore) {
				}
			}
			return null;
		},

		getEntryPaths : function(path) {
			if (this.secure.okResourceAdminPerm(this)) {
				this.checkUninstalled();
				return this.secure.callFindResourcesPath(this.current().archive, path);
			}
			else {
				return null;
			}
		},

		getHeaders : function() {
			if (arguments.length === 0) {
				return this.getHeaders_0_0_BundleImpl_ovld();
			}
			else if (arguments.length === 1) {
				return this.getHeaders_1_0_BundleImpl_ovld(arguments[0]);
			}
		},

		getHeaders_0_0_BundleImpl_ovld : function() {
			return this.getHeaders(null);
		},

		getHeaders_1_0_BundleImpl_ovld : function(locale) {
			this.secure.checkMetadataAdminPerm(this);
			var res = this.secure.callGetHeaders0(this.current(), locale);
			if (res === null && this.cachedHeaders !== null) {
				res = this.cachedHeaders.cloneHD();
				if (this.cachedHeaders === null) {
					return this.getHeaders(locale);
				}
			}
			return res;
		},

		getResources : function(name) {
			this.checkUninstalled();
			var current = this.current();
			if (this.secure.okResourceAdminPerm(this) && !current.isFragment()) {
				var e = null;
				if (this.getUpdatedState([ this ]) !== this.vj$.Bundle.INSTALLED) {
					if (bundles.framework.SystemBundle.isInstance(this)) {
						e = this.getClassLoader().getResources(name);
					}
					else {
						var cl0 = current.getClassLoader();
						if (cl0 !== null) {
							e = cl0.getResourcesOSGi(name);
						}
					}
				}
				else {
					var uv = this.secure.getBundleClassPathEntries(current, name, false);
					if (uv !== null) {
						e = uv.elements();
					}
				}
				if (e !== null && e.hasMoreElements()) {
					return e;
				}
			}
			return null;
		},

		loadClass : function(name) {
			if (this.secure.okClassAdminPerm(this)) {
				this.checkUninstalled();
				var current = this.current();
				if (current.isFragment()) {
					throw new this.vj$.ClassNotFoundException("Can not load classes from fragment/extension bundles");
				}
				if (this.getUpdatedState([ this ]) === this.vj$.Bundle.INSTALLED) {
					throw new this.vj$.ClassNotFoundException(this.resolveFailException.getMessage());
				}
				var cl;
				if (bundles.framework.SystemBundle.isInstance(this)) {
					cl = this.getClassLoader();
				}
				else {
					cl = current.getClassLoader();
					if (cl === null) {
						throw new this.vj$.IllegalStateException("state is uninstalled?");
					}
				}
				return cl.loadClass(name);
			}
			else {
				throw new this.vj$.ClassNotFoundException("No AdminPermission to get class: " + name);
			}
		},

		current : function() {
			return this.generations.get(0);
		},

		extensionNeedsRestart : function() {
			return this.current().isExtension() && (this.state & (this.vj$.Bundle.INSTALLED | this.vj$.Bundle.UNINSTALLED)) !== 0;
		},

		isAttached : function() {
			var fix = this.current();
			return fix.fragment !== null && fix.fragment.hasHosts();
		},

		getFragmentHostName : function() {
			var fix = this.current();
			if (fix.isFragment()) {
				return fix.fragment.hostName;
			}
			else {
				return null;
			}
		},

		triggersActivationPkg : function(pkg) {
			return this.vj$.Bundle.STOPPING !== this.fwCtx.systemBundle.getState() && this.state === this.vj$.Bundle.STARTING && this.operation !== this.vj$.BundleImpl.ACTIVATING && this.current().isPkgActivationTrigger(pkg);
		},

		triggersActivationCls : function(name) {
			if (this.vj$.Bundle.STOPPING !== this.fwCtx.systemBundle.getState() && this.state === this.vj$.Bundle.STARTING && this.operation !== this.vj$.BundleImpl.ACTIVATING) {
				var pkg = "";
				var pos = name.lastIndexOf('.');
				if (pos !== -1) {
					pkg = name.substring(0, pos);
				}
				return this.current().isPkgActivationTrigger(pkg);
			}
			return false;
		},

		bundleThread : function() {
			{
				if (this.fwCtx.bundleThreads.isEmpty()) {
					this.bundleThread = this.secure.createBundleThread(this.fwCtx);
				}
				else {
					this.bundleThread = this.fwCtx.bundleThreads.removeFirst();
				}
				return this.bundleThread;
			}
		},

		adaptSecure : function(type) {
			var res = null;
			if (this.vj$.BundleRevision.clazz.equals(type)) {
				res = this.current().bundleRevision;
			}
			else if (this.vj$.BundleRevisions.clazz.equals(type)) {
				res = new this.vj$.BundleRevisionsImpl(this.generations);
			}
			else if (this.vj$.BundleWiring.clazz.equals(type)) {
				var bundleRevision = this.current().bundleRevision;
				if (bundleRevision !== null) {
					res = bundleRevision.getWiring();
				}
			}
			else if (this.fwCtx.startLevelController !== null && this.vj$.BundleStartLevel.clazz.equals(type)) {
				res = this.fwCtx.startLevelController.bundleStartLevel(this);
			}
			else if (this.vj$.BundleContext.clazz.equals(type)) {
				res = this.bundleContext;
			}
			else if (this.vj$.AccessControlContext.clazz.equals(type)) {
				res = this.secure.getAccessControlContext(this);
			}
			return res;
		},

		usesBundleGeneration : function(check) {
			return this.generations.contains(check);
		},

		hasZombies : function() {
			return this.generations.size() > 1;
		},

		isBundleThread : function(t) {
			return this.bundleThread === t;
		},

		resetBundleThread : function() {
			this.bundleThread = null;
		},

		doExportImport : function() {
			var current = this.current();
			if (!current.isFragment()) {
				current.bpkgs.registerPackages();
			}
		},

		removeBundleResources : function() {
			this.fwCtx.listeners.removeAllListeners(this.bundleContext);
			var srs = this.fwCtx.services.getRegisteredByBundle(this);
			for (var serviceRegistrationImpl, _$itr = srs.iterator(); _$itr.hasNext();) {
				serviceRegistrationImpl = _$itr.next();
				try {
					serviceRegistrationImpl.unregister();
				}
				catch (ignore) {
				}
			}
			var s = this.fwCtx.services.getUsedByBundle(this);
			for (var sri, _$itr = s.iterator(); _$itr.hasNext();) {
				sri = _$itr.next();
				sri.ungetService(this, false);
			}
		},

		checkUninstalled : function() {
			if (this.state === this.vj$.Bundle.UNINSTALLED) {
				throw new this.vj$.IllegalStateException("Bundle is in UNINSTALLED state");
			}
		},

		isResolved : function() {
			return (this.state & (this.vj$.Bundle.RESOLVED | this.vj$.Bundle.STARTING | this.vj$.Bundle.ACTIVE | this.vj$.Bundle.STOPPING)) !== 0;
		}

	})

	.endType();

});