/* @org.eclipse.vjet.dsf.resource.utils.CodeGen("VjoGenerator") */
vjo.ctype('bundles.framework.ResolverHooks') 
.needs(['org.eclipse.vjet.vjo.java.util.List','org.eclipse.vjet.vjo.java.util.Map',
    'org.eclipse.vjet.vjo.java.util.Collection','org.eclipse.vjet.vjo.java.util.ArrayList',
    'org.eclipse.vjet.vjo.java.lang.RuntimeException','org.eclipse.vjet.vjo.java.util.HashMap',
    'org.eclipse.vjet.vjo.java.lang.IllegalStateException','org.eclipse.vjet.vjo.java.util.AbstractCollection',
    'org.eclipse.vjet.vjo.java.util.Iterator','org.eclipse.vjet.vjo.java.util.ConcurrentModificationException',
    'org.eclipse.vjet.vjo.java.util.NoSuchElementException','bundles.framework.FrameworkContext',
    'osgi.util.tracker.ServiceTracker','osgi.framework.hooks.resolver.ResolverHookFactory',
    'bundles.framework.BundleImpl','bundles.framework.BundleGeneration',
    'osgi.framework.BundleContext','osgi.util.tracker.ServiceTrackerCustomizer',
    'osgi.framework.ServiceReference','osgi.framework.wiring.BundleRevision',
    'osgi.framework.BundleException','osgi.framework.hooks.resolver.ResolverHook',
    'osgi.framework.wiring.BundleRequirement','osgi.framework.wiring.BundleCapability',
    'bundles.framework.RemoveOnlyCollection','osgi.framework.Bundle',
    'org.eclipse.vjet.vjo.java.lang.BooleanUtil','org.eclipse.vjet.vjo.java.lang.ObjectUtil'])
.props({
    TrackedResolverHookFactory:vjo.ctype() 
    .protos({
        tracked:null, 
        bundle:null, 
        resolverHook:null, 
        
        constructs:function(tracked,bundle){
            this.tracked=tracked;
            this.bundle=bundle;
        },
        
        begin:function(triggers){
            this.resolverHook=this.tracked.begin(triggers);
            return this.resolverHook!==null;
        },
        
        getResolverHook:function(){
            return this.resolverHook;
        },
        
        resetResolverHook:function(){
            this.resolverHook=null;
        }
    })
    .endType(),
    ShrinkableSingletonCollection:vjo.ctype() 
    .inherits('org.eclipse.vjet.vjo.java.util.AbstractCollection<T>')
    .protos({
        singleton:null, 
        
        constructs:function(singleton){
            this.base();
            this.singleton=singleton;
        },
        
        addAll:function(c){
            throw new UnsupportedOperationException("Add not allowed");
        },
        
        isEmpty:function(){
            return this.singleton===null;
        },
        
        iterator:function(){
            return vjo.make(this,Iterator)
                .protos({
                    hasNext:false,
                    constructs:function(){
                        this.hasNext=!isEmpty();
                    },
                    hasNext:function(){
                        return this.hasNext;
                    },
                    next:function(){
                        if(this.hasNext){
                            this.hasNext=false;
                            if(isEmpty()){
                                throw new ConcurrentModificationException();
                            }
                            return this.vj$.parent.singleton;
                        }
                        throw new NoSuchElementException();
                    },
                    remove:function(){
                        if(this.hasNext||isEmpty()){
                            throw new IllegalStateException();
                        }
                        this.vj$.parent.singleton=null;
                    }
                })
                .endType();
        },
        
        size:function(){
            return this.singleton!==null?1:0;
        },
        
        add:function(br){
            if(arguments.length===1){
                if(arguments[0] instanceof Object){
                    return this.add_1_0_ShrinkableSingletonCollection_ovld(arguments[0]);
                }else if(this.base && this.base.add){
                    return this.base.add.apply(this,arguments);
                }
            }else if(this.base && this.base.add){
                return this.base.add.apply(this,arguments);
            }
        },
        
        add_1_0_ShrinkableSingletonCollection_ovld:function(br){
            throw new UnsupportedOperationException("Add not allowed");
        },
        
        remove:function(o){
            if(arguments.length===1){
                if(arguments[0] instanceof Object){
                    return this.remove_1_0_ShrinkableSingletonCollection_ovld(arguments[0]);
                }else if(this.base && this.base.remove){
                    return this.base.remove.apply(this,arguments);
                }
            }else if(this.base && this.base.remove){
                return this.base.remove.apply(this,arguments);
            }
        },
        
        remove_1_0_ShrinkableSingletonCollection_ovld:function(o){
            if(this.singleton!==null&&org.eclipse.vjet.vjo.java.lang.ObjectUtil.equals(this.singleton,o)){
                this.singleton=null;
                return true;
            }
            return false;
        }
    })
    .endType()
})
.protos({
    fwCtx:null, 
    resolverHookTracker:null, 
    active:null, 
    currentTriggers:null, 
    resolvableBundles:null, 
    blockThread:null, 
    
    constructs:function(frameworkContext){
        this.fwCtx=frameworkContext;
    },
    
    open:function(){
        if(this.fwCtx.debug.hooks){
            this.fwCtx.debug.println("Begin Tracking Resolver Hooks");
        }
        this.resolverHookTracker=new this.vj$.ServiceTracker(/*>>*/this.fwCtx.systemBundle.bundleContext,this.vj$.ResolverHookFactory.clazz,
            vjo.make(this,this.vj$.ServiceTrackerCustomizer)
            .protos({
                addingService:function(reference){
                    return new this.vj$.ResolverHooks.TrackedResolverHookFactory(/*>>*/this.vj$.parent.fwCtx.systemBundle.bundleContext.getService(reference),reference.getBundle());
                    return new this.vj$.ResolverHooks.TrackedResolverHookFactory(/*>>*/this.vj$.parent.fwCtx.systemBundle.bundleContext.getService(reference),reference.getBundle());
                },
                modifiedService:function(reference,service){
                },
                removedService:function(reference,service){
                    service.resetResolverHook();
                    service.resetResolverHook();
                }
            })
            .endType());
        this.resolverHookTracker.open();
    },
    
    close:function(){
        this.resolverHookTracker.close();
        this.resolverHookTracker=null;
    },
    
    isOpen:function(){
        return this.resolverHookTracker!==null;
    },
    
    beginResolve:function(triggers){
        if(!this.isOpen()){
            return;
        }
        if(this.currentTriggers===null){
            var triggerCollection=new this.vj$.ArrayList(); 
            for (var b,_$i0=0;_$i0<triggers.length;_$i0++){
                b=triggers[_$i0];
                triggerCollection.add(b.current().bundleRevision);
            }
            this.active=new this.vj$.ArrayList();
            for (var e,_$itr=this.resolverHookTracker.getTracked().entrySet().iterator();_$itr.hasNext();){
                e=_$itr.next();
                var rhf=this.resolverHookTracker.getService(e.getKey()); 
                if(null!==rhf){
                    this.blockResolveForHooks();
                    try {
                        if(rhf.begin(triggerCollection)){
                            this.active.add(rhf);
                            this.currentTriggers=triggers;
                        }
                    }
                    catch(re){
                        throw new this.vj$.BundleException("Resolver hook throw an exception, bid="+rhf.bundle.getBundleId(),this.vj$.BundleException.REJECTED_BY_HOOK,re);
                    }
                    finally {
                        this.unblockResolveForHooks();
                    }
                }
            }
            if(this.active.isEmpty()){
                this.active=null;
            }else {
                this.resolvableBundles=new this.vj$.HashMap();
            }
        }
    },
    
    endResolve:function(triggers){
        if(triggers===this.currentTriggers){
            var saved=null; 
            if(this.active!==null){
                this.blockResolveForHooks();
                for (var rhf,_$itr=this.active.iterator();_$itr.hasNext();){
                    rhf=_$itr.next();
                    var rh=rhf.getResolverHook(); 
                    try {
                        rh.end();
                    }
                    catch(re){
                        saved=new this.vj$.BundleException("Resolver end hook throw an exception, bid="+rhf.bundle.getBundleId(),this.vj$.BundleException.REJECTED_BY_HOOK,re);
                    }
                }
                this.unblockResolveForHooks();
                this.active=null;
                this.resolvableBundles=null;
            }
            this.currentTriggers=null;
            if(saved!==null){
                throw saved;
            }
        }
    },
    
    
    
    filterMatches:function(requirement,candidates){
        if(arguments.length===2){
            if(osgi.framework.wiring.BundleRequirement.clazz.isInstance(arguments[0]) && org.eclipse.vjet.vjo.java.util.Collection.clazz.isInstance(arguments[1])){
                this.filterMatches_2_0_ResolverHooks_ovld(arguments[0],arguments[1]);
            }else if(osgi.framework.wiring.BundleRequirement.clazz.isInstance(arguments[0]) && osgi.framework.wiring.BundleCapability.clazz.isInstance(arguments[1])){
                return this.filterMatches_2_1_ResolverHooks_ovld(arguments[0],arguments[1]);
            }
        }
    },
    
    filterMatches_2_0_ResolverHooks_ovld:function(requirement,candidates){
        if(this.hasHooks()){
            var c=new this.vj$.RemoveOnlyCollection(/*>>*/candidates); 
            this.blockResolveForHooks();
            try {
                for (var rhf,_$itr=this.active.iterator();_$itr.hasNext();){
                    rhf=_$itr.next();
                    var rh=this.checkActiveRemoved(rhf); 
                    try {
                        rh.filterMatches(requirement,c);
                    }
                    catch(re){
                        throw new this.vj$.BundleException("Resolver hook throw an exception, bid="+rhf.bundle.getBundleId(),this.vj$.BundleException.REJECTED_BY_HOOK,re);
                    }
                }
            }
            finally {
                this.unblockResolveForHooks();
            }
        }
    },
    
    filterMatches_2_1_ResolverHooks_ovld:function(requirement,candidate){
        if(this.hasHooks()){
            var c=new this.vj$.ResolverHooks.ShrinkableSingletonCollection(candidate); 
            this.filterMatches(requirement,c);
            return !c.isEmpty();
        }
        return true;
    },
    
    filterResolvable:function(bg){
        if(this.hasHooks()){
            var res=this.resolvableBundles.get(bg); 
            if(res===null){
                var c=new this.vj$.ResolverHooks.ShrinkableSingletonCollection(bg.bundleRevision); 
                this.blockResolveForHooks();
                try {
                    for (var rhf,_$itr=this.active.iterator();_$itr.hasNext();){
                        rhf=_$itr.next();
                        var rh=this.checkActiveRemoved(rhf); 
                        try {
                            rh.filterResolvable(c);
                        }
                        catch(re){
                            throw new this.vj$.BundleException("Resolver hook throw an exception, bid="+rhf.bundle.getBundleId(),this.vj$.BundleException.REJECTED_BY_HOOK,re);
                        }
                    }
                }
                finally {
                    this.unblockResolveForHooks();
                }
                res=org.eclipse.vjet.vjo.java.lang.BooleanUtil.valueOf_(!c.isEmpty());
                this.resolvableBundles.put(bg,res);
            }
            return org.eclipse.vjet.vjo.java.lang.BooleanUtil.booleanValue(res);
        }
        return true;
    },
    
    filterSingletonCollisions:function(singleton,candidates){
        if(this.hasHooks()){
            var c=new this.vj$.RemoveOnlyCollection(candidates); 
            this.blockResolveForHooks();
            try {
                for (var rhf,_$itr=this.active.iterator();_$itr.hasNext();){
                    rhf=_$itr.next();
                    var rh=this.checkActiveRemoved(rhf); 
                    try {
                        rh.filterSingletonCollisions(singleton,c);
                    }
                    catch(re){
                        throw new this.vj$.BundleException("Resolver hook throw an exception, bid="+rhf.bundle.getBundleId(),this.vj$.BundleException.REJECTED_BY_HOOK,re);
                    }
                }
            }
            finally {
                this.unblockResolveForHooks();
            }
        }
    },
    
    checkResolveBlocked:function(){
        if(this.blockThread!==null&&this.vj$.Thread.currentThread()===this.blockThread){
            throw new this.vj$.IllegalStateException("Resolve hooks aren't allowed to resolve bundle");
        }
    },
    
    hasHooks:function(){
        return this.active!==null;
    },
    
    checkActiveRemoved:function(rhf){
        var rh=rhf.getResolverHook(); 
        if(null===rh){
            throw new this.vj$.BundleException("Resolver hook service was unregistered",this.vj$.BundleException.REJECTED_BY_HOOK);
        }
        return rh;
    },
    
    blockResolveForHooks:function(){
        this.blockThread=this.vj$.Thread.currentThread();
    },
    
    unblockResolveForHooks:function(){
        this.blockThread=null;
    }
})
.endType();